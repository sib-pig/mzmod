package org.expasy.mzjava.project.qmpaper.fdr;

import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.AlignNdpSimFunc;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.expasy.mzjava.utils.function.Procedure;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Skips results that have the same OMS theoretical spectrum as the previous result.
 *
 * For example if the results have peptides:
 * <pre>
 * PEPSIDEK
 * PEPTIDEK
 * CERVILASR
 * CERVILASK
 * LLLSK
 * SSSEEEKK
 * </pre>
 *
 * This procedure only calculates scores for:
 *
 * <pre>
 * PEPSIDEK
 * CERVILASR
 * LLLSK
 * SSSEEEKK
 * </pre>
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class AdjustedFdrScoresProcedure implements Procedure<OmsQuery> {

    private final List<List<Double>> allScores;

    private final PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
    private final AlignNdpSimFunc<PepFragAnnotation, PepFragAnnotation> simFunc;

    public AdjustedFdrScoresProcedure(Tolerance fragmentTolerance, List<List<Double>> allScores) {

        simFunc = new AlignNdpSimFunc<>(fragmentTolerance);
        this.allScores = allScores;
    }

    @Override
    public void execute(OmsQuery query) {

        List<OmsResult> allResults = query.getResults();
        if (allResults.isEmpty())
            return;

        List<OmsResult> filteredResults = new ArrayList<>(query.getResultCount());
        filteredResults.add(allResults.get(0));

        PeptideSpectrum lastSpectrum = fragmenter.fragment(allResults.get(0).getPeptide(), 1);
        for (int i = 1; i < allResults.size(); i++) {

            OmsResult currResult = allResults.get(i);
            PeptideSpectrum currSpectrum = fragmenter.fragment(currResult.getPeptide(), 1);

            simFunc.calcSimilarity(lastSpectrum, currSpectrum);
            int matches = simFunc.getMatches();
            int peakCount = lastSpectrum.size() + currSpectrum.size();
            double difference = ((peakCount / 2.0) - matches) * 2;
            if (difference > 0) {

                lastSpectrum = fragmenter.fragment(currResult.getPeptide(), 1);
                filteredResults.add(currResult);
            }
        }

        allScores.add(calculateDeltaScores(filteredResults));
    }

    public List<Double> calculateDeltaScores(List<OmsResult> filteredResults) {

        List<Double> scores = new ArrayList<>();
        for (int i = 0; i < filteredResults.size() - 1; i++) {

            double currScore = filteredResults.get(i).getMetaScore();
            double nextScore = filteredResults.get(i + 1).getMetaScore();
            scores.add(currScore + (currScore - nextScore));
        }
        return scores;
    }
}
