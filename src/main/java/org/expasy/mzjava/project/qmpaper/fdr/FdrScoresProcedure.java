package org.expasy.mzjava.project.qmpaper.fdr;

import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.expasy.mzjava.utils.function.Procedure;

import java.util.ArrayList;
import java.util.List;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
public class FdrScoresProcedure implements Procedure<OmsQuery> {

    private final List<List<Double>> allScores;

    public FdrScoresProcedure(List<List<Double>> allScores) {

        this.allScores = allScores;
    }

    @Override
    public void execute(OmsQuery query) {

        List<OmsResult> filteredResults = query.getResults();

        List<Double> scores = new ArrayList<>();
        for (int i = 0; i < filteredResults.size() - 1; i++) {

            double currScore = filteredResults.get(i).getMetaScore();
            double nextScore = filteredResults.get(i + 1).getMetaScore();
            scores.add(currScore + (currScore - nextScore));
        }
        allScores.add(scores);
    }
}
