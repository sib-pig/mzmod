package org.expasy.mzjava.project.qmpaper.results;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.sparktool.mzmod.UnmatchedPeakCounter;

import java.io.*;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class HockeyStickToScatterTsv {

    public static void main(String[] args) throws Exception {

        final File root = new File("C:\\Users\\ohorlach\\Documents\\tmp\\quickmod-spark\\hockey-stick\\");
        final Set<String> spectra = HockeyStickToTsv.loadSpectra(new File(root, "moda"));

        File[] dirs = root.listFiles((FileFilter) DirectoryFileFilter.INSTANCE);
        if(dirs == null) dirs = new File[0];

        for (File dir : dirs) {

            process(dir, spectra);
        }
    }

    private static void process(File dir, Set<String> spectra) throws IOException {

        System.out.println("Processing " + dir.getName());                      //sout

        List<Record> records = HockeyStickToTsv.loadRecords(dir, spectra::contains);

        BufferedWriter writerCorrect = new BufferedWriter(new FileWriter(new File(dir, "shift_correct.tsv"), false));
        BufferedWriter writerDelta = new BufferedWriter(new FileWriter(new File(dir, "shift_delta-correct.tsv"), false));
        BufferedWriter writerWrong = new BufferedWriter(new FileWriter(new File(dir, "shift_wrong.tsv"), false));

        final PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
        final UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));

        for(Record record : records) {

            double score = record.getScore();
            double massShift = record.getMassShift();

            BufferedWriter writer;
            if(record.isExact()) {

                writer = writerCorrect;
            } else if(record.getOrCalculateUnmatchedPeaks(fragmenter, unmatchedPeakCounter) == 0) {

                writer = writerDelta;
            } else {

                writer = writerWrong;
            }
            writer.write(score + "\t" + massShift + "\n");
        }

        writerCorrect.close();
        writerDelta.close();
        writerWrong.close();
    }
}
