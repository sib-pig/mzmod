package org.expasy.mzjava.project.qmpaper.results;

import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.SharedPeakSimFunc;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptidePeakComparator {

    private final PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
    private final SharedPeakSimFunc<PepFragAnnotation, PepFragAnnotation> simFunc;
    private final Tolerance precursorTolerance;

    public PeptidePeakComparator(Tolerance precursorTolerance, Tolerance fragmentTolerance) {

        this.precursorTolerance = precursorTolerance;

        this.simFunc = new SharedPeakSimFunc<>(0, fragmentTolerance);
    }

    public boolean compare(Peptide peptide1, Peptide peptide2) {

        PeptideSpectrum spectrum1 = fragmenter.fragment(toBarePeptide(peptide1), 1);
        PeptideSpectrum spectrum2 = fragmenter.fragment(toBarePeptide(peptide2), 1);

        if(!precursorTolerance.withinTolerance(spectrum1.getPrecursor().getMz(), spectrum2.getPrecursor().getMz()))
            return false;

        double sharedPeakCount = simFunc.calcSimilarity(spectrum1, spectrum2);
        return 1 - sharedPeakCount < 0.000001;
    }

    private Peptide toBarePeptide(Peptide src) {

        final int size = src.size();
        List<AminoAcid> residues = new ArrayList<>(size);

        for(int i = 0; i < size; i++) {

            residues.add(src.getSymbol(i));
        }

        return new Peptide(residues);
    }
}