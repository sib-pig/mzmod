package org.expasy.mzjava.project.qmpaper.fdr.data;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.proteomics.mol.Peptide;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideKey {

    private final Peptide peptide;
    private final int hash;

    public PeptideKey(Peptide peptide) {

        Preconditions.checkNotNull(peptide);

        this.peptide = peptide;
        hash = peptide.toString().hashCode();
    }

    public Peptide getPeptide() {

        return peptide;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PeptideKey that = (PeptideKey) o;

        return peptide.equals(that.peptide);

    }

    @Override
    public int hashCode() {

        return hash;
    }

    @Override
    public String toString() {

        return peptide.toString();
    }
}
