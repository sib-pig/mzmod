package org.expasy.mzjava.project.qmpaper.results;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.sparktool.mzmod.UnmatchedPeakCounter;

import java.io.*;
import java.util.*;
import java.util.function.Predicate;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class HockeyStickToTsv {

    private static final double dbScoreCutoff = 0;

    public static void main(String[] args) throws Exception {

        final File root = new File("C:\\Users\\ohorlach\\Documents\\tmp\\quickmod-spark\\hockey-stick\\");

        final Set<String> spectra = loadSpectra(new File(root, "moda"));

        File[] dirs = root.listFiles((FileFilter) DirectoryFileFilter.INSTANCE);
        if(dirs == null) dirs = new File[0];

        for (File dir : dirs) {

            process(dir, spectra);
        }
    }

    private static void process(File dir, Set<String> spectra) throws IOException {

        System.out.println("Processing " + dir.getName());                      //sout

        List<Record> records = loadRecords(dir, spectra::contains);
//        List<Record> records = loadRecords(dir, spectrum -> true);

        writeExact(dir, records);

        for (int peakDiffCutoff = 0; peakDiffCutoff < 5; peakDiffCutoff++) {
            writeDeltaCorrect(dir, records, peakDiffCutoff);
        }

        writeNotExact(dir, records);
    }

    private static void writeExact(File dir, List<Record> records) throws IOException {

        int count = 0;
        int wrong = 0;
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(dir, "ranked.tsv"), false));
        for (Record record : records) {

            count += 1;
            if(!record.isExact()) wrong += 1;
            writer.write(count + "\t" + wrong + "\t" + record.getLine() + "\n");
        }
        writer.close();
    }

    private static void writeNotExact(File dir, List<Record> records) throws IOException {

        final PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
        final UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(dir, "wrong.tsv"), false));
        int count = 0;
        for (Record record : records) {

            count += 1;
            if(!record.isExact())
                writer.write(count + "\t" + record.getLine() + "\t" + record.getOrCalculateUnmatchedPeaks(fragmenter, unmatchedPeakCounter) + "\n");
        }
        writer.close();
    }

    private static void writeDeltaCorrect(File dir, List<Record> records, int peakDiffCutoff) throws IOException {

        final PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE);
        final UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));

        int count = 0;
        int wrong = 0;
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(dir, "ranked_" + peakDiffCutoff + ".tsv"), false));
        for (Record record : records) {

            count += 1;
            final double unmatchedPeaks = record.getOrCalculateUnmatchedPeaks(fragmenter, unmatchedPeakCounter);
            if(unmatchedPeaks > peakDiffCutoff) wrong += 1;

            writer.write(count + "\t" + wrong + "\t" + record.getLine() + "\t" + unmatchedPeaks + "\n");
        }
        writer.close();
    }

    static List<Record> loadRecords(File dir, Predicate<String> spectrumPredicate) throws IOException {

        File[] files = dir.listFiles((FilenameFilter) new PrefixFileFilter("part"));
        if(files == null) files = new File[0];

        List<Record> records = new ArrayList<>();
        for(File file : files) {

            BufferedReader reader = new BufferedReader(new FileReader(file));
            for(String line = reader.readLine(); line != null; line = reader.readLine()){


                final Record record = new Record(line);
                if (!record.getCorrectPeptide().startsWith("C(C2O)") && record.getDbScore() > dbScoreCutoff && spectrumPredicate.test(record.getSpectrum())) {

                    records.add(record);
                }
            }
        }

        Collections.sort(records);
        return records;
    }

    static Set<String> loadSpectra(File dir) throws IOException {

        File[] files = dir.listFiles((FilenameFilter) new PrefixFileFilter("part"));
        if(files == null) files = new File[0];

        Set<String> spectra = new HashSet<>();
        for(File file : files) {

            BufferedReader reader = new BufferedReader(new FileReader(file));
            for(String line = reader.readLine(); line != null; line = reader.readLine()){

                String[] split = line.split("\t");
                if(!spectra.add(split[11]))
                    throw new IllegalStateException("Have duplicate spectra");
            }
        }

        return spectra;
    }
}
