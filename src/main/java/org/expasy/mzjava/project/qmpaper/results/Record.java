package org.expasy.mzjava.project.qmpaper.results;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationResolver;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.sparktool.mzmod.UnmatchedPeakCounter;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
class Record implements Comparable<Record>{

    private final ModificationResolver modResolver = input -> Optional.of(new Modification("null", Mass.ZERO));

    private final double score;
    private final String line;
    private final String[] split;
    private final boolean exact;

    private int unmatchedPeaks = -1;

    public Record(String line) {

        this.line = line;

        String[] split = line.split("\t");
        if(split.length != 13)
            throw new IllegalArgumentException("Expected 11 columns but had " + split.length);

        this.split = split;
        this.score = Double.parseDouble(split[0]);
        this.exact = Boolean.parseBoolean(split[split.length - 1]);
    }

    @Override
    public int compareTo(Record o) {

        return Double.compare(o.score, score);
    }

    public String getLine() {

        return line;
    }

    public int getOrCalculateUnmatchedPeaks(PeptideFragmenter fragmenter, UnmatchedPeakCounter unmatchedPeakCounter) {

        if (exact)
            return  0;

        if(unmatchedPeaks == -1) {

            PeptideSpectrum spectrum1 = fragmenter.fragment(Peptide.parse(removeVariableMod(getLibPeptide())), 1);
            PeptideSpectrum spectrum2 = fragmenter.fragment(Peptide.parse(removeVariableMod(getCorrectPeptide())), 1);
            unmatchedPeaks  = unmatchedPeakCounter.asymmetricUnmatched(spectrum1, spectrum2);
        }

        return unmatchedPeaks;
    }

    private String removeVariableMod(String peptide) {

        return Peptide.parse(peptide, modResolver).toSymbolString().replace("C", "C(C2H3NO)");
    }

    public boolean isExact() {

        return exact;
    }

    public double getScore() {

        return score;
    }

    public double getMassShift() {

        return Double.parseDouble(split[3]);
    }

    public int getCharge() {

        return (int)Double.parseDouble(split[1]);
    }

    public String getSpectrum() {

        return split[11];
    }

    public String getLibPeptide() {

        return split[6];
    }

    public String getCorrectPeptide() {

        return split[8];
    }

    public double getDbScore() {

        return Double.parseDouble(split[7]);
    }
}
