package org.expasy.mzjava.project.qmpaper.results;

import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
class MetaScoreFMF extends AbstractScoreFMF {

    MetaScoreFMF(Cached<Tolerance> fragmentTolerance, boolean removeSingletons, boolean removeDeltaCorrect) {

        super(fragmentTolerance, removeSingletons, removeDeltaCorrect);
    }

    protected OmsResult findResult(OmsQuery query) {

        return query.getResult(0);
    }

    @Override
    protected OmsResult findSecondResult(OmsQuery query) {

        return query.getResult(1);
    }

    protected double calcScore(OmsQuery query, OmsResult result) {

        return result.getMetaScore();
    }

    @Override
    protected double calcSecondScore(OmsQuery query, OmsResult result) {

        return result.getMetaScore();
    }
}
