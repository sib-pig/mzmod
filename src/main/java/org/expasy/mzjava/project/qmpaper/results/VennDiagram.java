package org.expasy.mzjava.project.qmpaper.results;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class VennDiagram {

    public static void main(String[] args) throws Exception {

        final Set<String> spectra = HockeyStickToTsv.loadSpectra(new File("C:\\Users\\ohorlach\\Documents\\tmp\\quickmod-spark\\hockey-stick\\moda"));
        final List<Record> modStarRecords = HockeyStickToTsv.loadRecords(new File("C:\\Users\\ohorlach\\Documents\\tmp\\quickmod-spark\\hockey-stick\\metascore"), spectra::contains);
        final List<Record> modaRecords = HockeyStickToTsv.loadRecords(new File("C:\\Users\\ohorlach\\Documents\\tmp\\quickmod-spark\\hockey-stick\\moda"), spectra::contains);
        correctVenn(modStarRecords, modaRecords);
        errorVenn(modStarRecords, modaRecords);
    }

    private static void errorVenn(List<Record> modStarRecords, List<Record> modaRecords) throws IOException {

        Set<String> modStar = extractWrong(modStarRecords);
        Set<String> modA = extractWrong(modaRecords);

        Set<String> intersect = intersect(modStar, modA);
        modStar.removeAll(intersect);
        modA.removeAll(intersect);

        System.out.println("Wrong");                      //sout
        System.out.println("mod*\t" + modStar.size());                      //sout
        System.out.println("modA\t" + modA.size());                      //sout
        System.out.println("intersect\t" + intersect.size());                      //sout
    }

    private static void correctVenn(List<Record> modStarRecords, List<Record> modaRecords) throws IOException {

        Set<String> xTandem = new HashSet<>();
        Set<String> modStar = extractCorrect(xTandem, modStarRecords);
        Set<String> modA = extractCorrect(xTandem, modaRecords);

        xTandem.removeAll(modStar);
        xTandem.removeAll(modA);

        Set<String> intersect = intersect(modStar, modA);
        modStar.removeAll(intersect);
        modA.removeAll(intersect);

        System.out.println("Correct");                      //sout
        System.out.println("tandem only\t" + xTandem.size());                      //sout
        System.out.println("mod*\t" + modStar.size());                      //sout
        System.out.println("modA\t" + modA.size());                      //sout
        System.out.println("intersect\t" + intersect.size());                      //sout
    }

    private static Set<String> intersect(Set<String> setA, Set<String> setB) {

        Set<String> aIntersectB = new HashSet<>(setA);
        aIntersectB.retainAll(setB);

        return aIntersectB;
    }

    private static HashSet<String> extractCorrect(Set<String> allSpectra, List<Record> records) throws IOException {

        final HashSet<String> correctSpectra = new HashSet<>();
        for (Record record : records) {

            final String spectrum = record.getSpectrum();
            allSpectra.add(spectrum);
            if (record.isExact()) {

                correctSpectra.add(spectrum);
            }
        }

        return correctSpectra;
    }

    private static HashSet<String> extractWrong(List<Record> records) throws IOException {

        final HashSet<String> wrongSpectra = new HashSet<>();
        for (Record record : records) {

            if (!record.isExact()) {

                wrongSpectra.add(record.getSpectrum());
            }
        }

        return wrongSpectra;
    }
}
