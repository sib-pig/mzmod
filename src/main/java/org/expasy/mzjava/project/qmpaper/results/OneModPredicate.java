package org.expasy.mzjava.project.qmpaper.results;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.sparktool.data.PeptideMatch;

import java.util.function.Predicate;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
class OneModPredicate implements Predicate<PeptideMatch> {

    private final int minPeptideLength;
    private final int maxPeptideLength;
    private final String scoreName;
    private final double minPsmScore;

    private transient Modification carbamidomethyl;
    private transient Modification carbamidomethylMinusAmmonia;
    private transient Modification minusAmmonia;

    public OneModPredicate(int minPeptideLength, int maxPeptideLength, String scoreName, double minPsmScore) {

        this.minPeptideLength = minPeptideLength;
        this.maxPeptideLength = maxPeptideLength;
        this.scoreName = scoreName;
        this.minPsmScore = minPsmScore;
    }

    @Override
    public boolean test(PeptideMatch peptideMatch) {

        if(carbamidomethyl == null) {
            carbamidomethyl = Modification.parseModification("C2H3NO");
            minusAmmonia = Modification.parseModification("H-3N-1");
            carbamidomethylMinusAmmonia = Modification.parseModification("C2O");
        }

        if (peptideMatch.getScore(scoreName) <= minPsmScore)
            return false;

        Peptide peptide = peptideMatch.getPeptide();
        int peptideLength = peptide.size();
        if (peptideLength < minPeptideLength || peptideLength > maxPeptideLength)
            return false;

        int varModCount = 0;
        double massShift = 0;
        for (Modification mod : peptide.getModifications(ModAttachment.all)) {

            if (!mod.equals(carbamidomethyl)) {
                varModCount += 1;
                if (mod.equals(carbamidomethylMinusAmmonia)) {
                    massShift += minusAmmonia.getMolecularMass();
                } else {
                    massShift += mod.getMolecularMass();
                }
            }
        }
        return varModCount == 1 && massShift > 0;
    }
}
