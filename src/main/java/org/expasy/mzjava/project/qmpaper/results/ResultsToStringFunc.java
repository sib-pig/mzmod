package org.expasy.mzjava.project.qmpaper.results;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.proteomics.mol.CompositionCalc;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import scala.Tuple7;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
class ResultsToStringFunc implements Function<Tuple7<Double, Double, OmsResult, String, Boolean, PeptideMatch, Integer>, String> {

    private final String scoreName;

    private transient Modification carbamidomethyl;

    ResultsToStringFunc(String scoreName) {

        this.scoreName = scoreName;
    }

    @Override
    public String call(Tuple7<Double, Double, OmsResult, String, Boolean, PeptideMatch, Integer> input) throws Exception {

        if(carbamidomethyl == null)
            carbamidomethyl = Modification.parseModification("C2H3NO");

        final Double score = input._1();
        final Double secondScore = input._2();
        final OmsResult omsResult = input._3();
        final HitType hitType = omsResult.getHitType();
        final PeptideMatch peptideMatch = input._6();
        final Peptide correctPeptide = peptideMatch.getPeptide();
        final double dbSearchScore = peptideMatch.getScore(scoreName);

        final int position = omsResult.getPosition();
        final Peptide libPeptide = omsResult.getPeptide();

        final Composition compositionDifference = CompositionCalc.subtract(correctPeptide, libPeptide);
        final String spectrumName = input._4();
        final int charge = input._7();
        final boolean correct = input._5();

        return score + "\t" +
                charge + "\t" +
                hitType + "\t" +
                omsResult.getMassShift() + "\t" +
                (position + 1) + "\t" +
                omsResult.getPositionScore(position) + "\t" +
                libPeptide + "\t" +
                dbSearchScore + "\t" +
                correctPeptide + "\t" +
                (hitType == HitType.OPEN ? (getModIndex(correctPeptide) + 1) : "-1") + "\t" +
                compositionDifference + "\t" +
                spectrumName + "\t" +
                correct;
    }

    private int getModIndex(Peptide correctPeptide) {

        int[] modificationIndexes = correctPeptide.getModificationIndexes(ModAttachment.all);
        if (modificationIndexes.length == 1) {

            return (modificationIndexes[0]);
        } else if (modificationIndexes.length > 1) {

            for (int modificationIndex : modificationIndexes) {

                for (Modification mod : correctPeptide.getModificationsAt(modificationIndex, ModAttachment.all)) {

                    if (!mod.equals(carbamidomethyl))
                        return modificationIndex;
                }
            }

            throw new IllegalStateException("Peptide only has fixed modifications");
        } else {

            throw new IllegalStateException("Peptide is not modified");
        }
    }
}
