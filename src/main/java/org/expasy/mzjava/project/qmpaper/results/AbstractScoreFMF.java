package org.expasy.mzjava.project.qmpaper.results;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.mzmod.UnmatchedPeakCounter;
import scala.Tuple2;
import scala.Tuple7;

import java.util.*;
import java.util.stream.Collectors;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
abstract class AbstractScoreFMF implements FlatMapFunction<Tuple2<OmsQuery, PeptideMatch>, Tuple7<Double, Double, OmsResult, String, Boolean, PeptideMatch, Integer>> {

    private final boolean removeSingletons;
    private final boolean removeDeltaCorrect;
    private final Cached<Tolerance> fragmentTolerance;

    private transient PeptideFragmenter fragmenter;
    private transient UnmatchedPeakCounter unmatchedPeakCounter;

    protected AbstractScoreFMF(Cached<Tolerance> fragmentTolerance, boolean removeSingletons, boolean removeDeltaCorrect) {

        this.fragmentTolerance = fragmentTolerance;
        this.removeSingletons = removeSingletons;
        this.removeDeltaCorrect = removeDeltaCorrect;
    }

    @Override
    public Iterator<Tuple7<Double, Double, OmsResult, String, Boolean, PeptideMatch, Integer>> call(Tuple2<OmsQuery, PeptideMatch> input) throws Exception {

        OmsQuery query = removeSingletons ? removeSingletons(input._1()) : input._1();
        if (removeDeltaCorrect)
            query = removeDeltaCorrect(query);

        if(query.getResultCount() == 0)
            return Collections.emptyIterator();

        final PeptideMatch peptideMatch = input._2();
        final OmsResult result = findResult(query);
        final OmsResult secondResult = findSecondResult(query);

        final boolean correct = result.getPeptide().toSymbolString().equals(peptideMatch.toSymbolString());

        return Collections.singleton(
                new Tuple7<>(calcScore(query, result), calcSecondScore(query, secondResult), result, query.getSpectrum().getComment(), correct, peptideMatch, query.getPrecursor().getCharge())
        ).iterator();
    }

    private OmsQuery removeSingletons(OmsQuery omsQuery){

        List<OmsResult> filteredResults = omsQuery.getResults().stream().filter(result ->  result.getLibMemberCount() >1).collect(Collectors.toCollection(ArrayList::new));
        return new OmsQuery(omsQuery, filteredResults);
    }

    private OmsQuery removeDeltaCorrect(OmsQuery query){

        if (unmatchedPeakCounter == null) {

            unmatchedPeakCounter = new UnmatchedPeakCounter(fragmentTolerance.get());
            fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE_CONSTANT);
        }

        List<OmsResult> filteredResults = new ArrayList<>(query.getResultCount());
        filteredResults.add(query.getResult(0));

        PeptideSpectrum lastSpectrum = fragmenter.fragment(query.getResult(0).getPeptide(), 1);
        for (int i = 1; i < query.getResultCount(); i++) {

            OmsResult currResult = query.getResult(i);
            PeptideSpectrum currSpectrum = fragmenter.fragment(currResult.getPeptide(), 1);

            double difference = unmatchedPeakCounter.symmetricUnmatched(lastSpectrum, currSpectrum);
            if (difference > 0) {

                lastSpectrum = fragmenter.fragment(currResult.getPeptide(), 1);
                filteredResults.add(currResult);

                if (filteredResults.size() >= 3)
                    break;
            }
        }
        return new OmsQuery(query, filteredResults);
    }

    protected abstract OmsResult findResult(OmsQuery query);

    protected abstract OmsResult findSecondResult(OmsQuery query);

    protected abstract double calcScore(OmsQuery query, OmsResult result);

    protected abstract double calcSecondScore(OmsQuery query, OmsResult result);
}
