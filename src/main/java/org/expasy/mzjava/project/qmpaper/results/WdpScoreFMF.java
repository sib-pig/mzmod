package org.expasy.mzjava.project.qmpaper.results;

import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
class WdpScoreFMF extends AbstractScoreFMF {

    WdpScoreFMF(Cached<Tolerance> fragmentTolerance, boolean removeSingletons, boolean removeDeltaCorrect) {

        super(fragmentTolerance, removeSingletons, removeDeltaCorrect);
    }

    protected OmsResult findResult(OmsQuery query) {

        List<OmsResult> sortedResults = new ArrayList<>(query.getResults());
        Collections.sort(sortedResults, (r1, r2) -> Double.compare(r1.getMassShift(), r2.getMassShift()));

        return sortedResults.stream().max((r1, r2) -> Double.compare(r1.getWdp(), r2.getWdp())).get();
    }

    @Override
    protected OmsResult findSecondResult(OmsQuery query) {

        List<OmsResult> sortedResults = new ArrayList<>(query.getResults());
        Collections.sort(sortedResults, (r1, r2) -> Double.compare(r1.getMassShift(), r2.getMassShift()));

        Collections.sort(sortedResults, (r1, r2) -> Double.compare(r2.getWdp(), r1.getWdp()));

        return sortedResults.get(1);
    }

    protected double calcScore(OmsQuery query, OmsResult result) {

        return result.getWdp();
    }

    @Override
    protected double calcSecondScore(OmsQuery query, OmsResult result) {

        return result.getWdp();
    }
}
