package org.expasy.mzjava.project.qmpaper.results;

import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.broadcast.Broadcast;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.hadoop.OmsQueryValue;
import scala.Tuple2;

import java.util.*;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
class ResultSelectorFMF implements FlatMapFunction<Tuple2<Text, OmsQueryValue>, Tuple2<OmsQuery, PeptideMatch>> {

    private final Broadcast<Map<String, PeptideMatch>> psmMapBc;
    private final Broadcast<Set<String>> libPeptideSetBc;

    public ResultSelectorFMF(Broadcast<Map<String, PeptideMatch>> psmMapBc, Broadcast<Set<String>> libPeptideSetBc) {

        this.psmMapBc = psmMapBc;
        this.libPeptideSetBc = libPeptideSetBc;
    }

    @Override
    public Iterator<Tuple2<OmsQuery, PeptideMatch>> call(Tuple2<Text, OmsQueryValue> tuple) throws Exception {

        Map<String, PeptideMatch> psmMap = psmMapBc.value();
        Set<String> libPeptideSet = libPeptideSetBc.value();

        String key = tuple._1().toString();

        if (psmMap.containsKey(key)) {

            PeptideMatch psm = psmMap.get(key);
            OmsQuery query = tuple._2().get();
            int charge = query.getPrecursor().getCharge();
            String peptide = psm.toSymbolString();
            if (libPeptideSet.contains(charge + "_" + peptide)) {

                return Collections.singleton(new Tuple2<>(query, psm)).iterator();
            } else {

                return Collections.emptyIterator();
            }
        } else {

            return Collections.emptyIterator();
        }
    }
}
