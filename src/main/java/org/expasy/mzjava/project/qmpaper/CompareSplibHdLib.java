package org.expasy.mzjava.project.qmpaper;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.NdpSimFunc;
import org.expasy.mzjava.core.ms.spectrasim.SimFunc;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.hadoop.io.PeptideConsensusSpectrumValue;
import org.expasy.mzjava.proteomics.io.ms.spectrum.SptxtReader;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.sparktool.mstranslator.Lib2TSVParamsImpl;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import scala.Tuple2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class CompareSplibHdLib extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(CompareSplibHdLib.class.getName());

    private String libraryHdFile;
    private String spLibFileName;
    private String reportDir;

    public CompareSplibHdLib(String libraryHdFile, String reportDir, String spLibFileName, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,buffMemoryMB,runLocal);

        this.libraryHdFile = libraryHdFile;
        this.reportDir = reportDir;
        this.spLibFileName = spLibFileName;

        createOptions();
    }

    public CompareSplibHdLib() {

        createOptions();
    }

    public static void main(String[] args) {

        CompareSplibHdLib compareSplibHdLib =  new CompareSplibHdLib();
        try {
            compareSplibHdLib.parseOptions(args).run();
        } catch (MissingOptionException e) {
            compareSplibHdLib.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            compareSplibHdLib.printOptions(args, e.getMessage());
        } catch (IOException e) {
            compareSplibHdLib.printOptions(args, e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int run() throws IOException {
        long start = System.currentTimeMillis();

        final SparkConf conf = initSpark();
        final JavaSparkContext sc = new JavaSparkContext(conf);

        Map<String,PeptideConsensusSpectrum> splib = readSplibFile(new File(spLibFileName));

        final Lib2TSVParamsImpl parameters = new Lib2TSVParamsImpl();

        final Map<String, PeptideConsensusSpectrum> hdlib = sc.sequenceFile(libraryHdFile, SpectrumKey.class, PeptideConsensusSpectrumValue.class)
                .map(pair -> pair._2().get())
                .mapToPair(sp -> new Tuple2<>(sp.getPeptide().toString() + "-" + sp.getPrecursor().getCharge(), sp))
                .collectAsMap();

        Map<String,Tuple2<Integer,Double>> peptSimMap = compare(splib,hdlib);

        write2TSV(peptSimMap);

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("splib").withDescription("spectrum library splib file (required)").hasArg().withArgName("path").isRequired().create("spl"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdlib").withDescription("spectrum library sequence file (required)").hasArg().withArgName("path").isRequired().create("hl"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("reportDir").withDescription("directory for report files (required)").hasArg().withArgName("path").isRequired().create("or"));

    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.libraryHdFile = checkExistFileOption(line, "hl");
        this.spLibFileName = checkExistFileOption(line,"spl");
        this.reportDir = checkOutputDirOption(line,"or");
    }

    private void write2TSV(Map<String, Tuple2<Integer,Double>> peptSimMap) throws IOException{

        String tsvFileName = reportDir +File.separator+"SpactraSTMS2TransComp.tsv";

        BufferedWriter writer = new BufferedWriter(new FileWriter(tsvFileName));

        writer.write("peptide\tclusterSize\tsimlarity\n");
        for (String pept: peptSimMap.keySet())
            writer.write(pept+"\t"+peptSimMap.get(pept)._1()+"\t"+peptSimMap.get(pept)._2()+"\n");

        writer.close();

    }

    private Map<String,Tuple2<Integer,Double>> compare(Map<String,PeptideConsensusSpectrum> splib, Map<String,PeptideConsensusSpectrum> hdlib) throws IOException {

        SimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation> simFunc = new NdpSimFunc<>(10,new AbsoluteTolerance(0.1));

        Set<String> splibPepts = splib.keySet();

        Map<String,Tuple2<Integer,Double>> peptSimMap = new HashMap<>();
        for (String pept : splibPepts) {
            if (hdlib.containsKey(pept))
                peptSimMap.put(pept,new Tuple2<>(hdlib.get(pept).getMemberIds().size(),simFunc.calcSimilarity(splib.get(pept),hdlib.get(pept))));
        }

        LOGGER.info("SpLib: "+splib.size()+", HdLib: "+hdlib.size()+", common: "+peptSimMap.size());

        return peptSimMap;
    }

    private Map<String,PeptideConsensusSpectrum> readSplibFile(File spLibFile) throws IOException {

        SptxtReader reader = new SptxtReader(spLibFile, PeakList.Precision.DOUBLE);

        Map<String,PeptideConsensusSpectrum> map = new HashMap<>();

        while (reader.hasNext()) {
            PeptideConsensusSpectrum sp = reader.next();
            map.put(sp.getPeptide().toString()+"-"+sp.getPrecursor().getCharge(),sp);
        }

        return map;
    }

}
