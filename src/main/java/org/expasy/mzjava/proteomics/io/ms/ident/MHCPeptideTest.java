package org.expasy.mzjava.proteomics.io.ms.ident;


import org.expasy.mzjava.core.mol.MassCalculator;
import org.expasy.mzjava.proteomics.io.mol.FastaProteinReader;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Protein;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * @author Markus Muller
 * @version 0.0
 */
public class MHCPeptideTest {

    private final List<Protein> proteins;
    private final int minLength;
    private final int maxLength;
    private final double[] aaMasses;

    protected MHCPeptideTest() {
        proteins = new ArrayList<>();
        minLength = 7;
        maxLength = 9;
        aaMasses = new double[26];

        fillAAMasses();
    }

    private static final Logger LOGGER = Logger.getLogger(MHCPeptideTest.class.getName());

    public static void main(String[] args) {

        MHCPeptideTest mhcPeptideTest = new MHCPeptideTest();

        mhcPeptideTest.readProteins(args[0]);
        mhcPeptideTest.digestAll();
    }

    private void fillAAMasses() {
        aaMasses['A'-'A'] = AminoAcid.valueOf('A').getMassOfMonomer();
        aaMasses['C'-'A'] = AminoAcid.valueOf('C').getMassOfMonomer();
        aaMasses['D'-'A'] = AminoAcid.valueOf('D').getMassOfMonomer();
        aaMasses['E'-'A'] = AminoAcid.valueOf('E').getMassOfMonomer();
        aaMasses['F'-'A'] = AminoAcid.valueOf('F').getMassOfMonomer();
        aaMasses['G'-'A'] = AminoAcid.valueOf('G').getMassOfMonomer();
        aaMasses['H'-'A'] = AminoAcid.valueOf('H').getMassOfMonomer();
        aaMasses['I'-'A'] = AminoAcid.valueOf('I').getMassOfMonomer();
        aaMasses['K'-'A'] = AminoAcid.valueOf('K').getMassOfMonomer();
        aaMasses['L'-'A'] = AminoAcid.valueOf('L').getMassOfMonomer();
        aaMasses['M'-'A'] = AminoAcid.valueOf('M').getMassOfMonomer();
        aaMasses['N'-'A'] = AminoAcid.valueOf('N').getMassOfMonomer();
        aaMasses['P'-'A'] = AminoAcid.valueOf('P').getMassOfMonomer();
        aaMasses['Q'-'A'] = AminoAcid.valueOf('Q').getMassOfMonomer();
        aaMasses['R'-'A'] = AminoAcid.valueOf('R').getMassOfMonomer();
        aaMasses['S'-'A'] = AminoAcid.valueOf('S').getMassOfMonomer();
        aaMasses['T'-'A'] = AminoAcid.valueOf('T').getMassOfMonomer();
        aaMasses['V'-'A'] = AminoAcid.valueOf('V').getMassOfMonomer();
        aaMasses['W'-'A'] = AminoAcid.valueOf('W').getMassOfMonomer();
        aaMasses['Y'-'A'] = AminoAcid.valueOf('Y').getMassOfMonomer();
    }

    private void readProteins(String fastaFileName) {

        try {
            FastaProteinReader reader = new FastaProteinReader(new File(fastaFileName));

            while (reader.hasNext()) {
                proteins.add(reader.next());
            }
        } catch (FileNotFoundException e) {
            LOGGER.severe(e.getMessage());
        }  catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private void digest(Protein protein) {

        String seq = protein.toSymbolString();

        double[] masses = new double[seq.length()];
        for (int i = 0; i < seq.length(); i++) masses[i] = aaMasses[seq.charAt(i)-'A'];

        double[] bions = new double[maxLength];
        for (int i = 0; i < seq.length()-minLength; i++) {

            for (int j = 0; i+j <= seq.length()-1&& j < maxLength; j++) {
                if (j==0){
                    bions[0] = masses[i] + MassCalculator.PROTON_MASS;
                } else {
                    bions[j] = masses[i+j] + bions[j-1];
                }
            }

            // add to trellis
            // match here
        }
    }

    private void digestAll() {
        for (Protein protein : proteins) {
            digest(protein);
        }
    }
}
