package org.expasy.mzjava.proteomics.io.ms.ident;

import org.expasy.mzjava.proteomics.io.ms.ident.pepxml.v117.MsmsPipelineAnalysis;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatchResolver;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;

/**
 * This reader can be used if fixed modifications are not listedfor each search result. modifications are added in the
 * copyModInfo method
 *
 * @author Markus Muller
 * @version 0.0
 */
public class FixedModPepXmlReader extends PepXmlReader {
    public FixedModPepXmlReader(ModMassStorage modMassStorage, boolean discardAmbiguousSequences) {
        super(modMassStorage, discardAmbiguousSequences);
    }

    public FixedModPepXmlReader(ModMassStorage modMassStorage, boolean discardAmbiguousSequences, ModificationMatchResolver modMatchResolver) {
        super(modMassStorage, discardAmbiguousSequences, modMatchResolver);
    }

    @Override
    protected void copyModInfo(PeptideMatch peptideMatch, MsmsPipelineAnalysis.MsmsRunSummary.SpectrumQuery.SearchResult.SearchHit.ModificationInfo modInfo) {

        super.copyModInfo(peptideMatch,modInfo);

        peptideMatch.addModificationMatch(ModAttachment.N_TERM, Modification.parseModification("C3H4O"));
    }



}
