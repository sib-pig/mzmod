package org.expasy.mzjava.proteomics.io.ms.ident;

import org.expasy.mzjava.core.mol.MassCalculator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class MSMSTrellis {

    private final TrellisNode root;

    public MSMSTrellis() {
        root = new TrellisNode(MassCalculator.PROTON_MASS);
    }

    private class TrellisNode{

        private final double mz;
        private final List<TrellisNode> children;
        private final List<Integer> proteinIDs;

        public TrellisNode(double mz) {
            this.mz = mz;
            children = new ArrayList<>();
            proteinIDs = new ArrayList<>();
        }

    }

    public void add(double[] bions, int proteinID) {

    }

    public void minimize() {

    }

    public void match() {

    }
}
