package org.expasy.mzjava.proteomics.mol;

import gnu.trove.map.hash.TObjectIntHashMap;
import org.expasy.mzjava.core.mol.Atom;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class CompositionCalc {

    private CompositionCalc() {}

    public static Composition subtract(Peptide minuend, Peptide subtrahend) {

        return subtract(get(minuend), get(subtrahend));
    }

    public static Composition subtract(Composition minuend, Composition subtrahend){

        final TObjectIntHashMap<Atom> atomCounterMap = new TObjectIntHashMap<>();

        for(Atom atom : minuend.getAtoms()){

            atomCounterMap.put(atom, minuend.getCount(atom));
        }

        for(Atom atom : subtrahend.getAtoms()){

            final int count = -subtrahend.getCount(atom);
            final int difference = atomCounterMap.adjustOrPutValue(atom, count, count);
            if(difference == 0)
                atomCounterMap.remove(atom);
        }

        return new Composition(atomCounterMap, 0);
    }

    public static Composition get(Peptide peptide) {

        Composition.Builder builder = new Composition.Builder();

        for(int i = 0; i < peptide.size(); i++){

            builder.addAll(peptide.getSymbol(i).getCompositionOfMonomer());
        }

        for(Modification mod : peptide.getModifications(ModAttachment.all)){

            Mass mass = mod.getMass();

            if(mass instanceof Composition)
                builder.addAll((Composition)mass);
            else
                throw new IllegalStateException("Cannot calculate the composition of a peptide that has a modification that has a mass that is not a composition. The mass was " + mass);
        }

        builder.add(PeriodicTable.H, 2);
        builder.add(PeriodicTable.O, 1);

        return builder.build();
    }
}
