package org.expasy.mzjava.proteomics.mol.shuffling;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationList;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class PeptideShuffler {

    public enum ShufflingMethod {REVERSE,SHUFFLE}

    private final ShufflingMethod method;

    public PeptideShuffler(ShufflingMethod method) {

        checkNotNull(method);
        this.method = method;
    }

    public Peptide shuffle(Peptide peptide) {

        List<Integer> shuffledPos = getShuffledPos(peptide.size(),new HashSet<Integer>());

        return makeShuffledPeptide(shuffledPos,peptide);
    }

    public Peptide shuffle(Peptide peptide,Set<Integer> keepAA) {
        List<Integer> shuffledPos = getShuffledPos(peptide.size(),keepAA);

        return makeShuffledPeptide(shuffledPos,peptide);
    }

    public Peptide shuffleKeepNTerm(Peptide peptide) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        List<Integer> shuffledPos = getShuffledPos(peptide.size(),keepAA);

        return makeShuffledPeptide(shuffledPos,peptide);
    }

    public Peptide shuffleKeepCTerm(Peptide peptide) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(peptide.size()-1);
        List<Integer> shuffledPos = getShuffledPos(peptide.size(),keepAA);

        return makeShuffledPeptide(shuffledPos,peptide);
    }

    public Peptide shuffleKeepTermini(Peptide peptide) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        keepAA.add(peptide.size()-1);
        List<Integer> shuffledPos = getShuffledPos(peptide.size(),keepAA);

        return makeShuffledPeptide(shuffledPos,peptide);
    }


    private List<Integer> getShuffledPos(int size, Set<Integer> keepAA) {

        List<Integer> tmp = new ArrayList<Integer>();
        for (int i=0; i<size;i++) {
            if (!keepAA.contains(i)) {
                tmp.add(i);
            }
        }

        if (method== ShufflingMethod.SHUFFLE) {
            Collections.shuffle(tmp);
        }

        if (method== ShufflingMethod.REVERSE) {
            Collections.reverse(tmp);
        }

        List<Integer> shuffled = new ArrayList<Integer>();
        int j = 0;
        for (int i=0; i<size;i++) {
            if (keepAA.contains(i)) {
                shuffled.add(i);
            } else {
                shuffled.add(tmp.get(j));
                j++;
            }
        }
        return shuffled;
    }


    public Peptide makeShuffledPeptide(List<Integer> shuffledPos, Peptide peptide) {

        AminoAcid[] aas = new AminoAcid[peptide.size()];

        for (int i=0; i<peptide.size();i++) {
            aas[i] = peptide.getSymbol(shuffledPos.get(i));
        }

        PeptideBuilder shuffledPeptide = new PeptideBuilder(aas);

        ModificationList mods = peptide.getModifications(ModAttachment.nTermSet);
        for (Modification modif : mods) {
            shuffledPeptide.addModification(ModAttachment.N_TERM,modif);
        }

        mods = peptide.getModifications(ModAttachment.cTermSet);
        for (Modification modif : mods) {
            shuffledPeptide.addModification(ModAttachment.C_TERM,modif);
        }

        int[] modIndexes = peptide.getModificationIndexes(ModAttachment.sideChainSet);

        for (int modIndexe : modIndexes) {
            ModificationList modifs = peptide.getModificationsAt(modIndexe, ModAttachment.sideChainSet);
            for (Modification modif : modifs) {
                int j;
                for (j = 0; j < shuffledPos.size(); j++) {
                    if (shuffledPos.get(j) == modIndexe) break;
                }
                shuffledPeptide.addModification(j, modif);
            }
        }

        return shuffledPeptide.build();
    }
}
