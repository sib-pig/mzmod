package org.expasy.mzjava.proteomics.mol.shuffling;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.NdpSimFunc;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ControlledPeptideShuffler {
    protected double similarityThresh;
    protected int maxNrIter;
    protected int nrIter;
    protected Optional<Peptide> bestShuffledPeptide;
    protected PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.FLOAT_CONSTANT);

    public ControlledPeptideShuffler() {
        similarityThresh = 0.5;
        maxNrIter = 10;
        bestShuffledPeptide = Optional.absent();
    }

    public ControlledPeptideShuffler(double similarityThresh, int maxNrIter) {
        this.similarityThresh = similarityThresh;
        this.maxNrIter = maxNrIter;
        bestShuffledPeptide = Optional.absent();
    }

    public double getSimilarityThresh() {
        return similarityThresh;
    }

    public void setSimilarityThresh(double similarityThresh) {
        this.similarityThresh = similarityThresh;
    }

    public int getMaxNrIter() {
        return maxNrIter;
    }

    public void setMaxNrIter(int maxNrIter) {
        this.maxNrIter = maxNrIter;
    }

    public Optional<Peptide> shuffleKeepNTerm(Peptide peptide) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        return shuffle(peptide,keepAA);
    }

    public Optional<Peptide> shuffleKeepCTerm(Peptide peptide) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(peptide.size()-1);
        return shuffle(peptide,keepAA);
    }

    public Optional<Peptide> shuffleKeepTermini(Peptide peptide) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        keepAA.add(peptide.size()-1);
        return shuffle(peptide,keepAA);
    }

    public Optional<Peptide> shuffle(Peptide peptide) {
        return shuffle(peptide,new HashSet<Integer>());
    }


    public Optional<Peptide> shuffle(Peptide peptide,Set<Integer> keepAA) {

        if (peptide==null) {
            bestShuffledPeptide = Optional.absent();
            return Optional.absent();
        }

        if (peptide.size()-keepAA.size()<=1) {
            bestShuffledPeptide = Optional.of(peptide);
            return Optional.absent();
        }

        PeptideSpectrum originalSpec = getSpectrum(peptide);

        double maxSim = calcSimilarity(originalSpec,originalSpec);

        PeptideShuffler shuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        for (nrIter=0;nrIter<maxNrIter;nrIter++)  {
            Peptide shuffled = shuffler.shuffle(peptide,keepAA);
            PeptideSpectrum shuffledSpec = getSpectrum(shuffled);

            double sim = calcSimilarity(originalSpec,shuffledSpec);

            if (sim<=maxSim) {
                bestShuffledPeptide = Optional.of(shuffled);
            }

            if (sim<=similarityThresh) {
                return bestShuffledPeptide;
            }
        }

        return Optional.absent();
    }

    public Optional<Peptide> getBestShuffledPeptide() {
        return bestShuffledPeptide;
    }

    public int getNrIterations() {
        return nrIter;
    }

    protected double calcSimilarity(PeptideSpectrum original, PeptideSpectrum shuffled) {
        NdpSimFunc<PepFragAnnotation,PepFragAnnotation> simFunc = new NdpSimFunc<PepFragAnnotation,PepFragAnnotation>(0, new AbsoluteTolerance(0.05));

        return simFunc.calcSimilarity(original, shuffled);
    }

    protected PeptideSpectrum getSpectrum(Peptide peptide) {

        return peptideFragmenter.fragment(peptide, 1);
    }
}
