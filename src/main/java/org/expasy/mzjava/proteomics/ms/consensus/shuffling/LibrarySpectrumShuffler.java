package org.expasy.mzjava.proteomics.ms.consensus.shuffling;

import cern.jet.random.Uniform;
import cern.jet.random.engine.MersenneTwister;
import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;
import org.expasy.mzjava.proteomics.mol.shuffling.PeptideShuffler;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.utils.URIBuilder;

import java.util.*;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class LibrarySpectrumShuffler {

    public enum UnknownPeakShuffling {KEEP,MOVE,SAMPLE,SWAP}
    public enum PrecursorHandling {REPLACE,KEEP,KEEP_CHARGE_REDUCED}
    public enum PeptideTerminiHandling {KEEP_CTERM,KEEP_NTERM,KEEP_TERMINI,KEEP_NONE}

    protected UnknownPeakShuffling unknownPeakShuffling;
    protected double peakChangeProb;
    protected double mzStep;
    protected Optional<PeptideConsensusSpectrum> backGroundSpectrum;
    protected Optional<List<PeptideConsensusSpectrum>> backGroundSpectra;
    protected List<List<Double>> mzPools = new ArrayList<List<Double>>();
    protected float maxFragMz;
    protected int nrBins;
    protected PrecursorHandling keepPrecursor;

    protected Uniform probGenerator;

    public LibrarySpectrumShuffler() {
        unknownPeakShuffling = UnknownPeakShuffling.KEEP;
        peakChangeProb = 0.0;
        mzStep = 0.0;
        keepPrecursor = PrecursorHandling.REPLACE;
        backGroundSpectrum = Optional.absent();
        backGroundSpectra = Optional.absent();
    }

    public LibrarySpectrumShuffler(PrecursorHandling keepPrecursor, double peakChangeProb, double mzStep) {
        unknownPeakShuffling = UnknownPeakShuffling.MOVE;
        this.peakChangeProb = peakChangeProb;
        this.mzStep = mzStep;
        this.keepPrecursor = keepPrecursor;
        backGroundSpectrum = Optional.absent();
        backGroundSpectra = Optional.absent();
        probGenerator = new Uniform(0.0,1.0,new MersenneTwister());
    }


    public LibrarySpectrumShuffler(PeptideConsensusSpectrum backGroundSpectrum) {
        unknownPeakShuffling = UnknownPeakShuffling.SWAP;
        this.backGroundSpectrum = Optional.of(backGroundSpectrum);
        this.mzStep = 0.0;
        this.backGroundSpectra = Optional.absent();
        keepPrecursor = PrecursorHandling.REPLACE;

        probGenerator = new Uniform(0.0,1.0,new MersenneTwister());
    }

    public LibrarySpectrumShuffler(PrecursorHandling keepPrecursor,double peakChangeProb, List<PeptideConsensusSpectrum> backGroundSpectra) {
        unknownPeakShuffling = UnknownPeakShuffling.SAMPLE;
        this.peakChangeProb = peakChangeProb;
        this.backGroundSpectra = Optional.of(backGroundSpectra);
        this.mzStep = 0.0;
        this.backGroundSpectrum = Optional.absent();
        this.keepPrecursor = keepPrecursor;

        maxFragMz = 3000;
        nrBins = (int) Math.ceil(maxFragMz/300);

        for (int i=0; i<nrBins; i++) {
            mzPools.add(new ArrayList<Double>());
        }

        for (PeptideConsensusSpectrum libSp: backGroundSpectra) {
            for (int i=0;i<libSp.size();i++) {
                if (!libSp.hasAnnotationsAt(i)) {
                    int idx = Math.min((int) Math.round(libSp.getMz(i)/300.0),nrBins-1);
                    mzPools.get(idx).add(libSp.getMz(i));
                }
            }
        }

        probGenerator = new Uniform(0.0,1.0,new MersenneTwister());
    }

    public PeptideConsensusSpectrum shuffle(
            PeptideConsensusSpectrum spectrum,
            LibrarySpectrumShuffler.PeptideTerminiHandling peptideTerminiHandling)
    {

        PeptideConsensusSpectrum shuffled;
        if (peptideTerminiHandling==LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_NONE) {
            shuffled = shuffle(spectrum);
        } else if (peptideTerminiHandling==LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_CTERM) {
            shuffled = shuffleKeepCTerm(spectrum);
        } else if (peptideTerminiHandling==LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_NTERM) {
            shuffled = shuffleKeepNTerm(spectrum);
        } else if (peptideTerminiHandling==LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_TERMINI) {
            shuffled = shuffleKeepTermini(spectrum);
        } else {
            shuffled = shuffle(spectrum,new HashSet<Integer>());
        }

        return shuffled;
    }

    protected PeptideConsensusSpectrum shuffle(PeptideConsensusSpectrum spectrum) throws IllegalArgumentException{
        if (spectrum==null) throw new IllegalArgumentException("Library spectrum is null");

        Peptide peptide = spectrum.getPeptide();
        PeptideShuffler peptShuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffledPeptide = peptShuffler.shuffle(peptide);

        return shuffle(shuffledPeptide,spectrum);
    }

    protected PeptideConsensusSpectrum shuffleKeepNTerm(PeptideConsensusSpectrum spectrum) throws IllegalArgumentException{
        if (spectrum==null) throw new IllegalArgumentException("Library spectrum is null");

        Peptide peptide = spectrum.getPeptide();
        PeptideShuffler peptShuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffledPeptide = peptShuffler.shuffleKeepNTerm(peptide);

        return shuffle(shuffledPeptide,spectrum);
    }

    protected PeptideConsensusSpectrum shuffleKeepCTerm(PeptideConsensusSpectrum spectrum) throws IllegalArgumentException{
        if (spectrum==null) throw new IllegalArgumentException("Library spectrum is null");

        Peptide peptide = spectrum.getPeptide();
        PeptideShuffler peptShuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffledPeptide = peptShuffler.shuffleKeepCTerm(peptide);

        return shuffle(shuffledPeptide,spectrum);
    }

    protected PeptideConsensusSpectrum shuffleKeepTermini(PeptideConsensusSpectrum spectrum) throws IllegalArgumentException{
        if (spectrum==null) throw new IllegalArgumentException("Library spectrum is null");

        Peptide peptide = spectrum.getPeptide();
        PeptideShuffler peptShuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffledPeptide = peptShuffler.shuffleKeepTermini(peptide);

        return shuffle(shuffledPeptide,spectrum);
    }

    public PeptideConsensusSpectrum shuffle(PeptideConsensusSpectrum spectrum,Set<Integer> keepAA) throws IllegalArgumentException{
        if (spectrum==null) throw new IllegalArgumentException("Library spectrum is null");

        Peptide peptide = spectrum.getPeptide();
        PeptideShuffler peptShuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffledPeptide = peptShuffler.shuffle(peptide, keepAA);

        return shuffle(shuffledPeptide,spectrum);
    }

    protected PeptideConsensusSpectrum shuffle(Peptide shuffledPeptide, PeptideConsensusSpectrum spectrum) throws IllegalArgumentException{

        Set<UUID> memberIds = new HashSet<UUID>();
        memberIds.addAll(spectrum.getMemberIds());
        PeptideConsensusSpectrum shuffledSpec = new PeptideConsensusSpectrum(shuffledPeptide,spectrum.getPrecision(),memberIds);
        shuffledSpec.setStatus(PeptideConsensusSpectrum.Status.DECOY);
        shuffledSpec.setPrecursor(spectrum.getPrecursor());
        shuffledSpec.setComment(spectrum.getComment());
        shuffledSpec.setSpectrumSource(new URIBuilder("www.expasy.ch", "deliberator").build());

        addNotAnnotatedPeaks(spectrum, shuffledSpec);

        int[] annotIndexes = spectrum.getAnnotationIndexes();
        for (int i=0;i<annotIndexes.length;i++) {
            Optional<PepLibPeakAnnotation> annot = spectrum.getFirstAnnotation(i);
            if (annot.isPresent() && annot.get().getOptFragmentAnnotation().isPresent()){
                final PepLibPeakAnnotation srcAnnot = annot.get();
                PepFragAnnotation srcFragAnnot = srcAnnot.getOptFragmentAnnotation().get();
                PepFragAnnotation.Builder fragAnnotBuilder = new PepFragAnnotation.Builder(srcFragAnnot);
                double deltaMz = spectrum.getMz(annotIndexes[i])-srcFragAnnot.getTheoreticalMz();
                PeptideFragment fragment = srcFragAnnot.getFragment();
                PeptideFragment shuffledFrag = shuffledPeptide.createFragment(srcFragAnnot.getIonType(), fragment.size());
                fragAnnotBuilder.setFragment(shuffledFrag);
                PepFragAnnotation fragAnnot = fragAnnotBuilder.build();
                PepLibPeakAnnotation shuffledAnnot = new PepLibPeakAnnotation(srcAnnot.getMergedPeakCount(), srcAnnot.getMzStd(), srcAnnot.getIntensityStd(), Optional.of(fragAnnot));
                List<PepLibPeakAnnotation> annots = new ArrayList<PepLibPeakAnnotation>();
                annots.add(shuffledAnnot);
                shuffledSpec.add(fragAnnot.getTheoreticalMz()+deltaMz,spectrum.getIntensity(annotIndexes[i]),annots);
            }
        }

        shuffledSpec.clearProteinAccessionNumber();
        shuffledSpec.addProteinAccessionNumbers("decoy");
        return shuffledSpec;
    }

    protected void addNotAnnotatedPeaks(PeptideConsensusSpectrum originalSpec, Spectrum shuffledSpec) {
        if (unknownPeakShuffling== UnknownPeakShuffling.MOVE) {
            moveNotAnnotatedPeaks(originalSpec, shuffledSpec);
        } else if (unknownPeakShuffling== UnknownPeakShuffling.SWAP) {
            swapNotAnnotatedPeaks(originalSpec, shuffledSpec);
        }else if (unknownPeakShuffling== UnknownPeakShuffling.SAMPLE) {
            sampleNotAnnotatedPeaks(originalSpec, shuffledSpec);
        } else if (unknownPeakShuffling== UnknownPeakShuffling.KEEP) {
            copyNotAnnotatedPeaks(originalSpec, shuffledSpec);
        }
    }

    protected void moveNotAnnotatedPeaks(PeptideConsensusSpectrum originalSpec, Spectrum shuffledSpec) {
        for (int i=0; i<originalSpec.size();i++) {
            Optional<PepLibPeakAnnotation> annot = originalSpec.getFirstAnnotation(i);
            if (!annot.isPresent() || !annot.get().getOptFragmentAnnotation().isPresent()){
                double prob = probGenerator.nextDouble();
                double step = 0.0;
                if (prob<=peakChangeProb) step = mzStep;
                shuffledSpec.add(originalSpec.getMz(i)+step,originalSpec.getIntensity(i));
            }
        }
    }

    protected void swapNotAnnotatedPeaks(PeptideConsensusSpectrum originalSpec, Spectrum shuffledSpec) {

        double avgIntensityOrg = 0.0;
        double nrNonAnnotPeaks = 0;
        for (int i=0; i<originalSpec.size();i++) {
            Optional<PepLibPeakAnnotation> annot = originalSpec.getFirstAnnotation(i);
            if (!annot.isPresent() || !annot.get().getOptFragmentAnnotation().isPresent()){
                avgIntensityOrg += originalSpec.getIntensity(i);
                nrNonAnnotPeaks++;
            }
        }

        avgIntensityOrg /= nrNonAnnotPeaks;

        double avgIntensityBg = 0.0;
        nrNonAnnotPeaks = 0;
        for (int i=0; i<backGroundSpectrum.get().size();i++) {
            Optional<PepLibPeakAnnotation> annot = backGroundSpectrum.get().getFirstAnnotation(i);
            if (!annot.isPresent() || !annot.get().getOptFragmentAnnotation().isPresent()){
                avgIntensityBg +=backGroundSpectrum.get().getIntensity(i);
                nrNonAnnotPeaks++;
            }
        }
        avgIntensityBg /= nrNonAnnotPeaks;

        double fact = avgIntensityOrg/avgIntensityBg;

        for (int i=0; i<backGroundSpectrum.get().size();i++) {
            Optional<PepLibPeakAnnotation> annot = backGroundSpectrum.get().getFirstAnnotation(i);
            if (!annot.isPresent() || !annot.get().getOptFragmentAnnotation().isPresent()){
                shuffledSpec.add(backGroundSpectrum.get().getMz(i),backGroundSpectrum.get().getIntensity(i)*fact);
            }
        }
    }

    protected void sampleNotAnnotatedPeaks(PeptideConsensusSpectrum originalSpec, Spectrum shuffledSpec) {

        double precMz = originalSpec.getPrecursor().getMz();
        int precCharge = originalSpec.getPrecursor().getCharge();
        double precWindow = 20.0;

        Random randomGenerator = new Random();
        for (int i=0;i<originalSpec.size();i++) {
            Optional<PepLibPeakAnnotation> annot = originalSpec.getFirstAnnotation(i);
            if (!annot.isPresent() || !annot.get().getOptFragmentAnnotation().isPresent()){
                boolean done = false;
                if (keepPrecursor == PrecursorHandling.KEEP) {
                    // keep precursor
                    double dmz = Math.abs(originalSpec.getMz(i)-precMz);
                    if (dmz<precWindow/precCharge) {
                        shuffledSpec.add(originalSpec.getMz(i),originalSpec.getIntensity(i));
                        done = true;
                    }
                } else if (keepPrecursor == PrecursorHandling.KEEP_CHARGE_REDUCED) {
                    // for ETD data keep charge reduced precursors
                    double precMzDiffMin = precMz;
                    int precChargeMin = 1;

                    for (int c=1;c<=precCharge;c++) {
                        double dmz = Math.abs(originalSpec.getMz(i)-((precMz-1)*precCharge/c+1.0));
                        if (dmz<precMzDiffMin) {
                            precMzDiffMin = dmz;
                            precChargeMin = c;
                        }
                    }

                    if (precMzDiffMin<precWindow/precChargeMin) {
                        shuffledSpec.add(originalSpec.getMz(i),originalSpec.getIntensity(i));
                        done = true;
                    }
                }

                if (!done) {
                    // not near precursor
                    double prob = probGenerator.nextDouble();
                    if (prob<=peakChangeProb) {
                        int idx = Math.min((int) Math.round(originalSpec.getMz(i)/300.0),nrBins-1);
                        List<Double> mzPool = mzPools.get(idx);
                        if (mzPool.size()>1) {
                            shuffledSpec.add(mzPool.get(randomGenerator.nextInt(mzPool.size()-1)),originalSpec.getIntensity(i));
                        } else {
                            shuffledSpec.add(originalSpec.getMz(i)+7.0,originalSpec.getIntensity(i));
                        }
                    }  else {
                        shuffledSpec.add(originalSpec.getMz(i),originalSpec.getIntensity(i));
                    }
                }
            }
        }
    }

    protected void copyNotAnnotatedPeaks(Spectrum originalSpec, Spectrum shuffledSpec) {
        for (int i=0; i<originalSpec.size();i++) {
            if (!originalSpec.hasAnnotationsAt(i)) {
                shuffledSpec.add(originalSpec.getMz(i),originalSpec.getIntensity(i));
            }
        }
    }
}
