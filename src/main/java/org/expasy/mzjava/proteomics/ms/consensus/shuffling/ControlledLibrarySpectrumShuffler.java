package org.expasy.mzjava.proteomics.ms.consensus.shuffling;

import org.apache.spark.api.java.Optional;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrasim.NdpSimFunc;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ControlledLibrarySpectrumShuffler {
    protected double similarityThresh;
    protected int maxNrIter;
    protected int nrIter;
    protected double peakChangeProb;
    protected double mzStep;
    protected Optional<PeptideConsensusSpectrum> bestShuffledSpectrum;
    protected double lowestSimilarity;
    protected Optional<PeptideConsensusSpectrum> backGroundSpectrum;
    protected Optional<List<PeptideConsensusSpectrum>> backGroundSpectra;
    protected LibrarySpectrumShuffler.UnknownPeakShuffling unknownPeakShuffling;
    protected LibrarySpectrumShuffler.PrecursorHandling keepPrecursor;

    public ControlledLibrarySpectrumShuffler(double similarityThresh,int maxNrIter)
    {
        unknownPeakShuffling = LibrarySpectrumShuffler.UnknownPeakShuffling.MOVE;
        keepPrecursor = LibrarySpectrumShuffler.PrecursorHandling.REPLACE;
        this.similarityThresh = similarityThresh;
        this.maxNrIter = maxNrIter;
        peakChangeProb = 1.0;
        mzStep = 0.0;
        bestShuffledSpectrum = Optional.absent();
        this.backGroundSpectrum = Optional.absent();
        this.backGroundSpectra = Optional.absent();
    }

    public ControlledLibrarySpectrumShuffler(
            LibrarySpectrumShuffler.PrecursorHandling keepPrecursor,
            double similarityThresh,
            int maxNrIter,
            double peakChangeProb,
            double mzStep)
    {
        this.unknownPeakShuffling = LibrarySpectrumShuffler.UnknownPeakShuffling.MOVE;
        this.keepPrecursor = keepPrecursor;
        this.similarityThresh = similarityThresh;
        this.maxNrIter = maxNrIter;
        this.peakChangeProb = peakChangeProb;
        this.mzStep = mzStep;
        this.bestShuffledSpectrum = Optional.absent();
        this.backGroundSpectrum = Optional.absent();
        this.backGroundSpectra = Optional.absent();
    }

    public ControlledLibrarySpectrumShuffler(
            LibrarySpectrumShuffler.PrecursorHandling keepPrecursor,
            double similarityThresh,
            double peakChangeProb,
            int maxNrIter,
            PeptideConsensusSpectrum backGroundSpectrum)
    {
        this.unknownPeakShuffling =LibrarySpectrumShuffler.UnknownPeakShuffling.SWAP;
        this.keepPrecursor = keepPrecursor;
        this.similarityThresh = similarityThresh;
        this.maxNrIter = maxNrIter;
        this.peakChangeProb = peakChangeProb;
        this.mzStep = 0.0;
        this.bestShuffledSpectrum = Optional.absent();
        this.backGroundSpectrum = Optional.of(backGroundSpectrum);
        this.backGroundSpectra = Optional.absent();
    }

    public ControlledLibrarySpectrumShuffler(
            LibrarySpectrumShuffler.PrecursorHandling keepPrecursor,
            double similarityThresh,
            double peakChangeProb,
            int maxNrIter,
            List<PeptideConsensusSpectrum> backGroundSpectra)
    {
        unknownPeakShuffling =LibrarySpectrumShuffler.UnknownPeakShuffling.SAMPLE;
        this.keepPrecursor = keepPrecursor;
        this.similarityThresh = similarityThresh;
        this.maxNrIter = maxNrIter;
        this.peakChangeProb = peakChangeProb;
        this.mzStep = 0.0;
        this.bestShuffledSpectrum = Optional.absent();
        this.backGroundSpectrum = Optional.absent();
        this.backGroundSpectra = Optional.of(backGroundSpectra);
    }

    public double getSimilarityThresh() {
        return similarityThresh;
    }

    public void setSimilarityThresh(double similarityThresh) {
        this.similarityThresh = similarityThresh;
    }

    public int getMaxNrIter() {
        return maxNrIter;
    }

    public void setMaxNrIter(int maxNrIter) {
        this.maxNrIter = maxNrIter;
    }

    public Optional<PeptideConsensusSpectrum> shuffleKeepNTerm(PeptideConsensusSpectrum spectrum) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        return shuffle(spectrum,keepAA);
    }

    public Optional<PeptideConsensusSpectrum> shuffleKeepCTerm(PeptideConsensusSpectrum spectrum) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(spectrum.getPeptide().size()-1);
        return shuffle(spectrum,keepAA);
    }

    public Optional<PeptideConsensusSpectrum> shuffleKeepTermini(PeptideConsensusSpectrum spectrum) {
        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        keepAA.add(spectrum.getPeptide().size()-1);
        return shuffle(spectrum,keepAA);
    }

    public Optional<PeptideConsensusSpectrum> shuffle(PeptideConsensusSpectrum spectrum) {
        return shuffle(spectrum,new HashSet<Integer>());
    }

    public Optional<PeptideConsensusSpectrum> shuffle(
            PeptideConsensusSpectrum spectrum,
            LibrarySpectrumShuffler.PeptideTerminiHandling peptideTerminiHandling)
    {

        if (peptideTerminiHandling==LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_NONE) {
            return shuffle(spectrum);
        } else if (peptideTerminiHandling==LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_CTERM) {
            return shuffleKeepCTerm(spectrum);
        } else if (peptideTerminiHandling==LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_NTERM) {
            return shuffleKeepNTerm(spectrum);
        } else if (peptideTerminiHandling==LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_TERMINI) {
            return shuffleKeepTermini(spectrum);
        } else {
            return shuffle(spectrum);
        }
    }


    public Optional<PeptideConsensusSpectrum> shuffle(PeptideConsensusSpectrum spectrum,Set<Integer> keepAA) {

        if (spectrum==null) {
            bestShuffledSpectrum = Optional.absent();
            return Optional.absent();
        }

        if (spectrum.size()-keepAA.size()<=1) {
            bestShuffledSpectrum = (Optional<PeptideConsensusSpectrum>) Optional.of((PeptideConsensusSpectrum) spectrum.copy(new IdentityPeakProcessor<PepLibPeakAnnotation>()));
            return Optional.absent();
        }

        double maxSim = calcSimilarity(spectrum,spectrum);

        LibrarySpectrumShuffler shuffler;
        if (unknownPeakShuffling== LibrarySpectrumShuffler.UnknownPeakShuffling.MOVE) {
            shuffler = new LibrarySpectrumShuffler(keepPrecursor,peakChangeProb,mzStep);
        } else if (unknownPeakShuffling== LibrarySpectrumShuffler.UnknownPeakShuffling.SWAP) {
            shuffler = new LibrarySpectrumShuffler(backGroundSpectrum.get());
        } else if (unknownPeakShuffling== LibrarySpectrumShuffler.UnknownPeakShuffling.SAMPLE) {
            shuffler = new LibrarySpectrumShuffler(keepPrecursor,peakChangeProb,backGroundSpectra.get());
        } else {
            shuffler = new LibrarySpectrumShuffler(keepPrecursor,0.9,5.0);
        }

        for (nrIter=0;nrIter<maxNrIter;nrIter++)  {
            PeptideConsensusSpectrum shuffledSpec = shuffler.shuffle(spectrum, keepAA);

            double sim = calcSimilarity(spectrum,shuffledSpec);

            if (sim<=maxSim) {
                bestShuffledSpectrum = Optional.of(shuffledSpec);
                lowestSimilarity = sim;
            }

            if (sim<=similarityThresh) {
                return bestShuffledSpectrum;
            }
        }

        return Optional.absent();
    }

    public Optional<PeptideConsensusSpectrum> getBestShuffledPeptide() {
        return bestShuffledSpectrum;
    }

    public double getLowestSimilarity() {
        return lowestSimilarity;
    }

    public int getNrIterations() {
        return nrIter;
    }

    protected double calcSimilarity(PeptideConsensusSpectrum original, PeptideConsensusSpectrum shuffled) {
        NdpSimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation> simFunc = new NdpSimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation>(0, new AbsoluteTolerance(0.05));

        return simFunc.calcSimilarity(original, shuffled);
    }


}
