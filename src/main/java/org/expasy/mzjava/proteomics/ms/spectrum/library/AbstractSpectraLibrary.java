/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.spectrum.library;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Abstract implementation of a spectra library.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class AbstractSpectraLibrary<A extends PeakAnnotation, S extends PeakList<A>> implements SpectraLibrary<A, S> {

    protected enum Location {LIB_SMALLER, LIB_WITHIN, LIB_LARGER}

    private final IterativeReader<S> reader;
    private final LinkedList<S> readSpectra = new LinkedList<>();

    private S next;
    private Peak last;

    public AbstractSpectraLibrary(IterativeReader<S> reader) throws IOException {

        Preconditions.checkNotNull(reader);

        this.reader = reader;
        if (reader.hasNext()) next = reader.next();
        else next = null;
    }

    @Override
    public void close() throws IOException {

        reader.close();
    }

    @Override
    public List<S> getSpectra(Peak target, List<S> dest) throws IOException {

        if(last != null && compare(target, last) < 0)
            throw new IllegalArgumentException("The current target must be greater or equal to the last target. The last was " + last + ", current is " + target);
        last = target;

        if(dest == null) dest = new ArrayList<>();

        //Remove any spectra that are smaller than the window
        while (!readSpectra.isEmpty()) {

            Location location = relativeLocation(readSpectra.getFirst(), target);

            if (location == Location.LIB_SMALLER) {

                readSpectra.removeFirst();
            } else if(location == Location.LIB_WITHIN){

                break;
            } else {

                throw new IllegalStateException("Input is not sorted");
            }
        }

        if (next == null) {

            copySpectra(readSpectra, dest, target);
            return dest;
        }

        //Add next if it is within tolerance
        Location location = relativeLocation(next, target);
        if(location == Location.LIB_LARGER) {

            copySpectra(readSpectra, dest, target);
            return dest;
        } else if (location == Location.LIB_WITHIN) {

            readSpectra.add(next);
        }

        //Read spectra from reader until the reader is empty or the lib is larger
        while(next != null) {

            if (reader.hasNext()) {

                S lastSpectrum = next;
                next = reader.next();

                if(compare(next.getPrecursor(), lastSpectrum.getPrecursor()) < 0)
                    throw new IllegalStateException("The spectra in the reader are not sorted. The last spectra precursor = " + lastSpectrum.getPrecursor() + ", the current precursor is = " + next.getPrecursor());
            } else {
                next = null;
                break;
            }

            location = relativeLocation(next, target);
            if (location == Location.LIB_WITHIN) {

                readSpectra.add(next);
            } else if (location == Location.LIB_LARGER) {

                break;
            }
        }

        copySpectra(readSpectra, dest, target);
        return dest;
    }

    private int compare(Peak precursor1, Peak precursor2) {

        int cmp = Double.compare(precursor1.getCharge(), precursor2.getCharge());
        if (cmp != 0) {
            return cmp;
        }
        return Double.compare(precursor1.getMz(), precursor2.getMz());
    }

    protected void copySpectra(List<S> src, List<S> dest, Peak target) {

        dest.addAll(src);
    }

    protected abstract Location relativeLocation(S libSpectrum, Peak target);
}
