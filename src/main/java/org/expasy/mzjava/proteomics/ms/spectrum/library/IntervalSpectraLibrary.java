/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.proteomics.ms.spectrum.library;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IntervalSpectraLibrary<A extends PeakAnnotation, S extends PeakList<A>> extends AbstractSpectraLibrary<A, S> {

    private final double lowerBound, upperBound;

    public IntervalSpectraLibrary(IterativeReader<S> reader, double lowerBound, double upperBound) throws IOException {

        super(reader);

        Preconditions.checkArgument(lowerBound < upperBound);

        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    @Override
    protected Location relativeLocation(S libSpectrum, Peak target) {

        final Peak lib = libSpectrum.getPrecursor();

        final int targetCharge = target.getCharge();
        final int libCharge = lib.getCharge();
        if(libCharge > targetCharge) {

            return Location.LIB_LARGER;
        } else if(libCharge < targetCharge){

            return Location.LIB_SMALLER;
        } else {

            final double libMass = lib.getMass();
            final double targetMin = libMass + lowerBound;
            final double targetMax = libMass + upperBound;

            double targetMass = target.getMass();
            if(targetMass < targetMin)
                return Location.LIB_LARGER;
            else if(targetMass > targetMax)
                return Location.LIB_SMALLER;
            else
                return Location.LIB_WITHIN;
        }
    }
}
