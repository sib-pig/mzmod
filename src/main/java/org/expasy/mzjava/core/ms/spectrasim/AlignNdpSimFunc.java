/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

import java.util.BitSet;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AlignNdpSimFunc<X extends PeakAnnotation, Y extends PeakAnnotation> extends AbstractModAligner<X, Y> implements SimFunc<X, Y> {

    private final AlignmentPairList alignmentPairs = new AlignmentPairList();

    public AlignNdpSimFunc(Tolerance tolerance) {

        super(tolerance);

    }

    @Override
    public double calcSimilarity(PeakList<X> plX, PeakList<Y> plY) {

        reset();
        Peak yPrecursor = plY.getPrecursor();
        int precursorCharge = yPrecursor.getCharge();
        int[] peakCharges = new int[plX.size()];

        for (int i = 0; i < peakCharges.length; i++) {

            List<? extends X> annotations = plX.getAnnotations(i);
            if (annotations.isEmpty()) {

                peakCharges[i] = 0;
            } else {

                peakCharges[i] = annotations.get(0).getCharge();
            }
        }

        collectScorePairs(alignmentPairs, plX, plY, new double[]{yPrecursor.getMass() - plX.getPrecursor().getMass()}, peakCharges, precursorCharge - 1);   //todo is using precursor - 1 as the max charge a good idea?

        alignmentPairs.sort();
        double score = 0;
        BitSet libUsed = new BitSet(plX.size());
        BitSet queryUsed = new BitSet(plY.size());

        for (int i = 0; i < alignmentPairs.size(); i++) {

            score += alignmentPairs.getDpPart(i, libUsed, queryUsed, this);
        }

        return score / (plX.calcVectorLength() * plY.calcVectorLength());
    }

    @Override
    public int getTotalPeakCount() {

        return peakCount;
    }

    @Override
    public double getBestScore() {

        return 1;
    }

    @Override
    public double getWorstScore() {

        return 0;
    }
}
