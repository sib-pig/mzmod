/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import gnu.trove.map.hash.TIntObjectHashMap;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakListFactory;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Comparator;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public abstract class AbstractModAligner<X extends PeakAnnotation, Y extends PeakAnnotation> implements AlignmentSummary {

    protected final Tolerance tolerance;

    private int libPeaksShifted = 0;
    private int matches = 0;
    protected int peakCount;

    private final double[] targetMzs = new double[50];
    private int targetMzsSize = 0;

    private final SingleModMzGenerator singleModMzGenerator = new SingleModMzGenerator();
    private final MultyModMzGenerator multyModMzGenerator = new MultyModMzGenerator();

    private final AlignmentPairList alignmentPairs = new AlignmentPairList();

    public AbstractModAligner(Tolerance tolerance) {

        this.tolerance = tolerance;
    }

    public PeakList<X> makeAlignedPeakList(PeakList<X> plX, PeakList<Y> plY) {

        int precursorCharge = plY.getPrecursor().getCharge();
        int[] peakCharges = new int[plX.size()];

        for (int i = 0; i < peakCharges.length; i++) {

            List<X> annotations = plX.getAnnotations(i);
            if (annotations.isEmpty()) {

                peakCharges[i] = 0;
            } else {

                peakCharges[i] = annotations.get(0).getCharge();
            }
        }

        collectScorePairs(alignmentPairs, plX, plY, new double[]{plY.getPrecursor().getMass() - plX.getPrecursor().getMass()}, peakCharges, precursorCharge - 1);   //todo is using precursor - 1 as the max charge a good idea?

        alignmentPairs.sort();

        BitSet libUsed = new BitSet(plX.size());
        BitSet queryUsed = new BitSet(plY.size());

        int size = 0;
        for (int i = 0; i < alignmentPairs.size(); i++) {

            final int libIndex = alignmentPairs.getLibIndex(i);
            final int queryIndex = alignmentPairs.getQueryIndex(i);
            if (!libUsed.get(libIndex) && (queryIndex == -1 || !queryUsed.get(queryIndex))) {

                libUsed.set(libIndex);
                if(queryIndex != -1) queryUsed.set(queryIndex);
                size++;
            } else {

                alignmentPairs.setDuplicate(i, true);
            }
        }

        alignmentPairs.sort(new Comparator<AlignmentPairList.AlignmentPair>() {
            @Override
            public int compare(AlignmentPairList.AlignmentPair p1, AlignmentPairList.AlignmentPair p2) {

                return Double.compare(p1.getMz(), p2.getMz());
            }
        });

        double[] alignedMZs = new double[size];
        double[] alignedIntensities = new double[size];
        TIntObjectHashMap<List<X>> annotationMap = new TIntObjectHashMap<List<X>>();

        int n = 0;
        for (int i = 0; i < alignmentPairs.size(); i++) {

            if (!alignmentPairs.isDuplicate(i)) {

                alignedMZs[n] = alignmentPairs.getMz(i);
                alignedIntensities[n] = plX.getIntensity(alignmentPairs.getLibIndex(i));

                int libIndex = alignmentPairs.getLibIndex(i);
                List<X> annotations = plX.getAnnotations(libIndex);
                if (!annotations.isEmpty()) annotationMap.put(n, annotations);

                n++;
            }
        }

        PeakList<X> peakList = PeakListFactory.newPeakList(plX.getPrecision(), alignedMZs, alignedIntensities, alignedMZs.length);

        for (int index : annotationMap.keys()) {

            for (X /* A */ annotation : annotationMap.get(index)) {

                //noinspection unchecked
                peakList.addAnnotation(index, /*(A)*/(X) annotation.copy());
            }
        }

        return peakList;
    }

    protected void collectScorePairs(AlignmentPairList alignmentPairs, PeakList<? extends X> libPeakList, PeakList<? extends Y> queryPeakList, double[] deltaMasses, int[] peakCharges, int maxCharge) {

        alignmentPairs.clear();
        if(queryPeakList.size() == 0 || libPeakList.size() == 0) return;

        if(deltaMasses.length > 1) Arrays.sort(deltaMasses);
        ModMzGenerator modMzGenerator = getModMzGenerator(deltaMasses);
        double largestQueryMz = queryPeakList.getMz(queryPeakList.size() - 1);
        int toIndex = queryPeakList.size();

        for (int libIndex = 0; libIndex < libPeakList.size(); libIndex++) {

            double libMz = libPeakList.getMz(libIndex);
            double[] targetMzs = modMzGenerator.createTargetMzs(libMz, peakCharges[libIndex], maxCharge);
            int fromIndex = 0;
            boolean added = false;

            //For each m/z in the lib peak list collect all
            for (int i = 0; i < targetMzsSize; i++) {

                double targetMz = targetMzs[i];
                //If targetMz is larger than the largest m/z in the query list we are done
                if (tolerance.check(largestQueryMz, targetMz) == Tolerance.Location.LARGER) break;

                int queryIndex = queryPeakList.indexOf(fromIndex, toIndex, tolerance.getMin(targetMz));
                if (queryIndex < 0) queryIndex = -1 * (queryIndex + 1);
                fromIndex = queryIndex;

                if (queryIndex >= queryPeakList.size()) break;

                //Find all peaks that have a mz that is within tolerance of the targetMz
                double queryMz = queryPeakList.getMz(queryIndex);
                double max = tolerance.getMax(targetMz);

                boolean modified = modMzGenerator.isModified(i);
                while (queryMz <= max) {

                    alignmentPairs.add(libPeakList.getIntensity(libIndex), queryPeakList.getIntensity(queryIndex), libIndex, queryIndex, modified ? queryMz : targetMz, modified);
                    added = true;

                    queryIndex++;
                    queryMz = queryIndex < queryPeakList.size() ? queryPeakList.getMz(queryIndex) : Double.MAX_VALUE;
                }
            }

            if (!added)
                alignmentPairs.add(libPeakList.getIntensity(libIndex), 0, libIndex, -1, libPeakList.getMz(libIndex), false);
        }
    }

    private ModMzGenerator getModMzGenerator(double[] deltaMasses) {

        if(deltaMasses.length == 0) {

            singleModMzGenerator.deltaMass = 0;
            return singleModMzGenerator;
        } else if (deltaMasses.length == 1) {

            singleModMzGenerator.deltaMass = deltaMasses[0];
            return singleModMzGenerator;
        } else {

            multyModMzGenerator.deltaMasses = deltaMasses;
            return multyModMzGenerator;
        }
    }

    public int getLibPeaksShifted() {

        return libPeaksShifted;
    }

    public int getMatches() {

        return matches;
    }

    public void reset() {

        libPeaksShifted = 0;
        matches = 0;
        peakCount = 0;
    }

    public void incrementLibPeaksShifted() {

        libPeaksShifted++;
    }

    public void incrementMatch() {

        matches++;
    }

    public void incrementPeakCount(int count) {

        peakCount += count;
    }

    private static interface ModMzGenerator {

        double[] createTargetMzs(double libMz, int charge, int maxCharge);

        boolean isModified(int index);
    }

    private class SingleModMzGenerator implements ModMzGenerator {

        private double deltaMass;

        public final double[] createTargetMzs(double libMz, int charge, int maxCharge) {

            if (deltaMass == 0) {

                targetMzs[0] = libMz;
                targetMzsSize = 1;
                return targetMzs;
            }

            if (charge > 0) {

                if (deltaMass > 0) {

                    targetMzs[0] = libMz;
                    targetMzs[1] = libMz + (deltaMass / charge);
                    targetMzsSize = 2;
                } else if (deltaMass < 0) {

                    targetMzs[0] = libMz + (deltaMass / charge);
                    targetMzs[1] = libMz;
                    targetMzsSize = 2;
                }
            } else {

                if (deltaMass > 0) {

                    int n = 0;
                    targetMzs[n++] = libMz;
                    for (int z = maxCharge; z > 0; z--) {

                        targetMzs[n++] = libMz + (deltaMass / z);
                    }
                    targetMzsSize = n;
                } else if (deltaMass < 0) {

                    int n = 0;
                    for (int z = 1; z <= maxCharge; z++) {

                        targetMzs[n++] = libMz + (deltaMass / z);
                    }
                    targetMzs[n] = libMz;
                    targetMzsSize = n + 1;
                }
            }
            return targetMzs;
        }

        @Override
        public final boolean isModified(int index) {

            //If delta mass is positive the peak pair is modified if we are not processing the first target mz
            //if delta mass is negative the last target mz is the unmodified mz
            return deltaMass >= 0 ? index != 0 : index != targetMzsSize - 1;
        }
    }

    private class MultyModMzGenerator implements ModMzGenerator {

        private double[] deltaMasses;
        private int notModIndex = -1;

        public final double[] createTargetMzs(double libMz, int charge, int maxCharge) {

            if(deltaMasses.length != 2) throw new IllegalStateException("Can only deal with 2 mods");  //todo fix so that n mods can be dealt with

            int[] charges;
            if(charge == 0) {

                charges = new int[maxCharge];
                for (int i = 0; i < charges.length; i++) {

                    charges[i] = i + 1;
                }
            } else {

                charges = new int[]{charge};
            }
            targetMzsSize = 0;

            targetMzs[targetMzsSize++] = libMz;
            for(int z : charges) {

                targetMzs[targetMzsSize++] = libMz + (deltaMasses[0]/z);
                targetMzs[targetMzsSize++] = libMz + (deltaMasses[1]/z);
                targetMzs[targetMzsSize++] = libMz + ((deltaMasses[0] + deltaMasses[1])/z);
            }

            Arrays.sort(targetMzs, 0, targetMzsSize);

            notModIndex = -1;
            for (int i = 0; i < targetMzsSize; i++) {

                if(targetMzs[i] == libMz) notModIndex = i;
            }

            return targetMzs;
        }

        @Override
        public final boolean isModified(int index) {

            return index != notModIndex;
        }
    }
}
