/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.PeakListAligner;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.PeakPairSink;

import java.util.BitSet;
import java.util.Comparator;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class ModPeakListAligner<X extends PeakAnnotation, Y extends PeakAnnotation> extends AbstractModAligner<X, Y> implements PeakListAligner<X, Y> {

    private PeakPairSink<X, Y> sink;
    private final AlignmentPairList alignmentPairs = new AlignmentPairList();

    public ModPeakListAligner(Tolerance tolerance) {

        super(tolerance);
    }

    @Override
    public void align(PeakList<X> plX, PeakList<Y> plY) {

        Preconditions.checkNotNull(sink, "A sink needs to be set on this peak processor list");

        reset();
        int precursorCharge = plY.getPrecursor().getCharge();
        int[] peakCharges = new int[plX.size()];

        for (int i = 0; i < peakCharges.length; i++) {

            List<? extends X> annotations = plX.getAnnotations(i);
            if (annotations.isEmpty()) {

                peakCharges[i] = 0;
            } else {

                peakCharges[i] = annotations.get(0).getCharge();
            }
        }

        collectScorePairs(alignmentPairs, plX, plY, new double[]{plY.getPrecursor().getMass() - plX.getPrecursor().getMass()}, peakCharges, precursorCharge - 1);   //todo is using precursor - 1 as the max charge a good idea?

        alignmentPairs.sort();

        BitSet libUsed = new BitSet(plX.size());
        BitSet queryUsed = new BitSet(plY.size());

        for (int i = 0; i < alignmentPairs.size(); i++) {

            final int libIndex = alignmentPairs.getLibIndex(i);
            final int queryIndex = alignmentPairs.getQueryIndex(i);
            if (!libUsed.get(libIndex) && (queryIndex == -1 || !queryUsed.get(queryIndex))) {

                libUsed.set(libIndex);
                if (queryIndex > -1) queryUsed.set(queryIndex);
            } else {

                alignmentPairs.setDuplicate(i, true);
            }
        }

        for (int i = 0; i < plY.size(); i++) {

            if (!queryUsed.get(i)) alignmentPairs.add(0, plY.getIntensity(i), -1, i, plY.getMz(i), false);
        }

        alignmentPairs.sort(new Comparator<AlignmentPairList.AlignmentPair>() {
            @Override
            public int compare(AlignmentPairList.AlignmentPair p1, AlignmentPairList.AlignmentPair p2) {

                return Double.compare(p1.getMz(), p2.getMz());
            }
        });

        sink.begin(plX, plY);
        for (int i = 0; i < alignmentPairs.size(); i++) {

            if (!alignmentPairs.isDuplicate(i)) {

                final int libIndex = alignmentPairs.getLibIndex(i);
                final int queryIndex = alignmentPairs.getQueryIndex(i);

                final double xIntensity = libIndex > 0 ? plX.getIntensity(libIndex) : 0;
                final double yIntensity = queryIndex > 0 ? plY.getIntensity(queryIndex) : 0;

                sink.processPeakPair(alignmentPairs.getMz(i), xIntensity, yIntensity, plX.getAnnotations(libIndex), plY.getAnnotations(queryIndex));
            }
        }
        sink.end();
    }

    @Override
    public void setSink(PeakPairSink<X, Y> sink) {

        this.sink = sink;
    }
}
