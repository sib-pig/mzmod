/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Comparator;

/**
 * A list and pool of AlignmentPairs
 *
 * AlignmentPairs are constructed when the list is grown and reused after that.
 *
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AlignmentPairList {

    private AlignmentPair[] list;
    private int size = 0;

    private final int loadFactor = 100;

    public AlignmentPairList() {

        list = new AlignmentPair[0];
        grow(loadFactor);
    }

    private void grow(int increase) {

        AlignmentPair[] tmp = new AlignmentPair[list.length + increase];
        System.arraycopy(list, 0, tmp, 0, list.length);
        list = tmp;

        for(int i = size; i < list.length; i++) {

            list[i] = new AlignmentPair(-1, -1, -1, -1, -1, false);
        }
    }

    public void add(double libIntensity, double queryIntensity, int libIndex, int queryIndex, double mz, boolean modified){

        if(size >= list.length) grow(loadFactor);

        AlignmentPair alignmentPair = list[size];
        alignmentPair.reset(libIntensity, queryIntensity, libIndex, queryIndex, mz, modified);

        size++;
    }

    public void clear(){

        size = 0;
    }

    public void sort() {

        Arrays.sort(list, 0, size);
    }

    public void sort(Comparator<AlignmentPair> comparator) {

        Arrays.sort(list, 0, size, comparator);
    }

    public int size() {

        return size;
    }

    public double getDpPart(int index, BitSet libUsed, BitSet queryUsed, AlignmentSummary alignmentSummary) {

        return list[index].getDpPart(libUsed, queryUsed, alignmentSummary);
    }

    public int getLibIndex(int index) {

        return list[index].getLibIndex();
    }

    public int getQueryIndex(int index) {

        return list[index].getQueryIndex();
    }

    public void setDuplicate(int index, boolean duplicate) {

        list[index].setDuplicate(duplicate);
    }

    public boolean isDuplicate(int index) {

        return list[index].isDuplicate();
    }

    public double getMz(int index) {

        return list[index].getMz();
    }

    public boolean isModified(int index) {

        return list[index].isModified();
    }

    /**
     * Class to hold the information on which peak was aligned.
     */
    static class AlignmentPair implements Comparable<AlignmentPair> {

        private double dpPart, intensityDelta;
        private int libIndex, queryIndex;
        private double mz;

        private boolean modified;

        private boolean duplicate;

        private AlignmentPair(double libIntensity, double queryIntensity, int libIndex, int queryIndex, double mz, boolean modified) {

            reset(libIntensity, queryIntensity, libIndex, queryIndex, mz, modified);
        }

        private void reset(double libIntensity, double queryIntensity, int libIndex, int queryIndex, double mz, boolean modified) {

            this.libIndex = libIndex;
            this.queryIndex = queryIndex;
            this.mz = mz;
            this.modified = modified;

            dpPart = libIntensity * queryIntensity;
            intensityDelta = Math.abs(libIntensity - queryIntensity);

            duplicate = false;
        }

        @Override
        public int compareTo(AlignmentPair o) {

            return Double.compare(intensityDelta, o.intensityDelta);
        }

        @Override
        public boolean equals(Object o) {

            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AlignmentPair that = (AlignmentPair) o;

            return Double.compare(that.dpPart, dpPart) == 0 &&
                    duplicate == that.duplicate &&
                    Double.compare(that.intensityDelta, intensityDelta) == 0 &&
                    libIndex == that.libIndex && modified == that.modified &&
                    Double.compare(that.mz, mz) == 0 &&
                    queryIndex == that.queryIndex;
        }

        @Override
        public int hashCode() {

            int result;
            long temp;
            temp = Double.doubleToLongBits(dpPart);
            result = (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(intensityDelta);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            result = 31 * result + libIndex;
            result = 31 * result + queryIndex;
            temp = Double.doubleToLongBits(mz);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            result = 31 * result + (modified ? 1 : 0);
            result = 31 * result + (duplicate ? 1 : 0);
            return result;
        }

        double getDpPart(BitSet libUsed, BitSet queryUsed, AlignmentSummary alignmentSummary) {

            if (!libUsed.get(libIndex) && (queryIndex == -1 || !queryUsed.get(queryIndex))) {

                libUsed.set(libIndex);
                if (queryIndex != -1) queryUsed.set(queryIndex);

                if (modified) alignmentSummary.incrementLibPeaksShifted();
                if (queryIndex != -1) {

                    alignmentSummary.incrementMatch();
                    alignmentSummary.incrementPeakCount(2);
                } else {

                    alignmentSummary.incrementPeakCount(1);
                }
                return dpPart;
            }

            return 0;
        }

        @Override
        public String toString() {

            if (libIndex == -1 && queryIndex == -1) return "";

            return "dp part " + dpPart + " index [lib, query] [" + libIndex + ", " + queryIndex + "]" + " lib mz " + mz + " modified " + modified;
        }

        int getLibIndex() {

            return libIndex;
        }

        int getQueryIndex() {

            return queryIndex;
        }

        double getMz() {

            return mz;
        }

        void setDuplicate(boolean duplicate) {

            this.duplicate = duplicate;
        }

        boolean isDuplicate() {

            return duplicate;
        }

        boolean isModified() {

            return modified;
        }
    }
}
