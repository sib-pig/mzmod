package org.expasy.mzjava.core.io;

import java.io.IOException;
import java.util.function.Predicate;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PredicateIterativeReader<T> implements IterativeReader<T> {

    private final IterativeReader<T> reader;
    private final Predicate<T> predicate;

    private T next;

    public PredicateIterativeReader(IterativeReader<T> reader, Predicate<T> predicate) throws IOException {

        this.reader = reader;
        this.predicate = predicate;

        next = getNext();
    }

    private T getNext() throws IOException {

        if (!reader.hasNext())
            return null;

        while (reader.hasNext()) {

            T current = reader.next();
            if (predicate.test(current))
                return current;
        }

        return null;
    }

    @Override
    public boolean hasNext() {

        return next != null;
    }

    @Override
    public T next() throws IOException {

        T current = next;

        next = getNext();

        return current;
    }

    @Override
    public void close() throws IOException {

        reader.close();
    }
}
