package org.expasy.mzjava.sparktool.mzmod;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.fs.InvalidPathException;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.storage.StorageLevel;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.sparktool.data.*;
import org.expasy.mzjava.sparktool.hadoop.OmsQueryValue;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import org.expasy.mzjava.sparktool.util.SparkUtils;
import org.expasy.mzjava.stats.statscores.FitDecoyScoreStatistics;
import scala.Tuple2;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class OmsResultsExport extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(OmsResultsExport.class.getName());

    private String mzModSequenceFile;
    private String mzModTabFileDir;
    private int maxFdrSize;
    private float qValue;
    private float mzFragTolerance;
    private boolean forceDelete;

    protected OmsResultsExport(String mzModSequenceFile, String mzModTabFileDir,int maxFdrSize,float qValue,float mzFragTolerance,boolean forceDelete, int nrExec, int execMemoryMB, int bufferMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,bufferMemoryMB,runLocal);

        this.mzModSequenceFile = mzModSequenceFile;
        this.mzModTabFileDir = mzModTabFileDir;
        this.maxFdrSize = maxFdrSize;
        this.qValue = qValue;
        this.mzFragTolerance = mzFragTolerance;
        this.forceDelete = forceDelete;

        createOptions();
    }

    protected OmsResultsExport() {

        createOptions();
    }

    public static void main(String[] args) throws IOException {

        OmsResultsExport omsResultsExport =  new OmsResultsExport();
        try {
            omsResultsExport.parseOptions(args).run();
        } catch (MissingOptionException e) {
            omsResultsExport.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            omsResultsExport.printOptions(args, e.getMessage());
        } catch(NumberFormatException e) {
            omsResultsExport.printOptions(args, e.getMessage());
        } catch (InvalidPathException e) {
            omsResultsExport.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int run() throws IOException {

        long start = System.currentTimeMillis();

        final SparkConf conf = initSpark();
        final JavaSparkContext sc = new JavaSparkContext(conf);

        double defaultScoreDelta = 2;

        JavaRDD<OmsQuery> omsQueries = sc.sequenceFile(mzModSequenceFile, Text.class, OmsQueryValue.class)
                .mapToPair(input -> new Tuple2<>(input._1().toString(), input._2().get()))
                .values()
                .filter(query -> query.getResultCount() > 0 && query.getPrecursor().getCharge() > 0)
                .setName("OmsResults")
                .persist(StorageLevel.MEMORY_AND_DISK());

        final Cached<Tolerance> fragmentTolerance = new CachedAbsoluteTolerance(mzFragTolerance);

        final boolean filterResults = true;
        final MetaScoreExtractScoresPF extractScoresPF = new MetaScoreExtractScoresPF(defaultScoreDelta, fragmentTolerance, filterResults);
        process(maxFdrSize, sc, defaultScoreDelta, omsQueries, extractScoresPF, mzModTabFileDir + "/" + extractScoresPF.getName(), qValue);

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();
        return 0;
    }

    private void process(int maxFdrSize, JavaSparkContext sc, double defaultScoreDelta, JavaRDD<OmsQuery> omsQueries, AbstractExtractScoresPF extractScoresPF, String output, float qValue) throws IOException {

        final float omsCutOff = estimateFdrCutOff(sc, omsQueries, maxFdrSize, output, HitType.OPEN, extractScoresPF, qValue);
        final float exactCutOff = estimateFdrCutOff(sc, omsQueries, maxFdrSize, output, HitType.EXACT, extractScoresPF, qValue);

        int minCharge = omsQueries.min(new ChargeComparator()).getPrecursor().getCharge();
        int maxCharge = omsQueries.max(new ChargeComparator()).getPrecursor().getCharge();
        LOGGER.info("charges = " + minCharge + " - " + maxCharge);

        for (int i = minCharge; i <= maxCharge; i++) {

            final int currentCharge = i;
            LOGGER.info("Processing spectra with charge " + currentCharge);
            JavaRDD<OmsQuery> filteredQueries = omsQueries
                    .filter(query -> {

                                double cutOff = query.getResult(0).getHitType() == HitType.OPEN ? omsCutOff : exactCutOff;
                                return query.getPrecursor().getCharge() == currentCharge && query.getResult(0).getMetaScore() >= cutOff;
                            }
                    )
                    .setName("Filtered " + currentCharge + "+")
                    .persist(StorageLevel.MEMORY_AND_DISK());

            SparkUtils.saveAsTextFile(sc, filteredQueries, new ResultToStringFunction(defaultScoreDelta), output + "/results_" + currentCharge + "+");

            append(i + "+ OPEN\t" + filteredQueries.filter(query -> query.getResult(0).getHitType() == HitType.OPEN).count() + "\n", output);
            append(i + "+ EXACT\t" + filteredQueries.filter(query -> query.getResult(0).getHitType() == HitType.EXACT).count() + "\n", output);
            filteredQueries.unpersist();
        }
    }

    private float estimateFdrCutOff(JavaSparkContext sc, JavaRDD<OmsQuery> omsQueries, int maxFdrSize, String outputPath, HitType hitType, AbstractExtractScoresPF extractScoresPF, float qValue) throws IOException {

        JavaRDD<OmsQuery> filteredQueries = omsQueries
                .filter(query -> query.getResultCount() > 2 && query.getResult(0).getHitType() == hitType);

        long resultsCount = filteredQueries.count();
        double fdrSpectra = Math.min(resultsCount, maxFdrSize);
        double fraction = Math.min(1.0, fdrSpectra / resultsCount);

        JavaPairRDD<Double, Double> fdrData = filteredQueries
                .sample(false, fraction)
                .flatMapToPair(extractScoresPF);

        SparkUtils.saveAsTextFile(sc, fdrData, tuple -> tuple._1() + "\t" + tuple._2(), outputPath + "/" + hitType.toString().toLowerCase() + "_fdr_dist");

        float[] firstHit = toArray(fdrData.keys().collect());
        float[] secondHit = toArray(fdrData.values().collect());

        FitDecoyScoreStatistics scoreStatistics = new FitDecoyScoreStatistics(firstHit, secondHit);
        float cutOff = scoreStatistics.getScoreForQValue(qValue);

        File file = new File(outputPath + "/" + hitType.toString().toLowerCase() + "_fdr_parameters.txt");
        if (file.exists() && !file.delete()) throw new IllegalStateException("Could not delete " + file);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
        writer.write("score cut off\t\t" + cutOff + "\n");
        writer.write("target decoy ratio\t" + scoreStatistics.getTargetDecoyRatio() + "\n");
        writer.write("result count\t\t" + fdrSpectra);
        writer.close();

        return cutOff;
    }

    private void append(String text, String outputPath) throws IOException {

        File file = new File(outputPath + "/hit_counts.txt");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
        writer.write(text);
        writer.close();
    }

    private float[] toArray(List<Double> doubleList) {

        float[] array = new float[doubleList.size()];
        int i = 0;
        for (Double score : doubleList) {

            array[i++] = score.floatValue();
        }

        return array;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdMzMod").withDescription("MzMod results sequence file (required)").hasArg().withArgName("path").isRequired().create("hm"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("mzModReportDir").withDescription("output directory for MzMod result reports (required)").hasArg().withArgName("path").isRequired().create("rm"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("maxFDRSampleSize").withDescription("maximal number of PSMs for FDR calculation").hasArg().withArgName("integer").create("fs"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("qValue").withDescription("qValue for MzMod export (required)").isRequired().hasArg().withArgName("path").create("qv"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("forceDelete").withDescription("force deleting existing spark output directory").create("fd"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.forceDelete = false;
        if( line.hasOption( "fd" ) ) {
            this.forceDelete = true;
        }

        this.mzModSequenceFile = checkExistFileOption(line, "hm");
        this.mzModTabFileDir = checkSparkDirOption(line,"rm",forceDelete);
        this.maxFdrSize = checkIntOption(line, "fs", 0, Integer.MAX_VALUE, 500000);
        this.qValue = checkFloatOption(line, "qv",0,1,0.01f);
    }

    private static class ChargeComparator implements Comparator<OmsQuery>, Serializable {
        @Override
        public int compare(OmsQuery q1, OmsQuery q2) {

            return Integer.compare(q1.getPrecursor().getCharge(), q2.getPrecursor().getCharge());
        }
    }

    private static abstract class AbstractExtractScoresPF implements PairFlatMapFunction<OmsQuery, Double, Double> {

        private final double defaultScoreDelta;
        private final Cached<Tolerance> fragmentTolerance;
        private final boolean filterResults;
        private final String name;

        private transient PeptideFragmenter fragmenter;
        private transient UnmatchedPeakCounter unmatchedPeakCounter;

        public AbstractExtractScoresPF(double defaultScoreDelta, Cached<Tolerance> fragmentTolerance, boolean filterResults, String name) {

            this.defaultScoreDelta = defaultScoreDelta;
            this.fragmentTolerance = fragmentTolerance;
            this.filterResults = filterResults;
            this.name = name;
        }

        @Override
        public Iterator<Tuple2<Double, Double>> call(OmsQuery query) throws Exception {

            List<OmsResult> filteredResults;
            filteredResults = filterResults ? filterResults(query) : query.getResults();

            if (filteredResults.size() >= 3) {

                return Collections.singletonList(getScore(defaultScoreDelta, filteredResults)).iterator();
            } else {

                return Collections.emptyIterator();
            }
        }

        private List<OmsResult> filterResults(OmsQuery query) {

            if (unmatchedPeakCounter == null) {

                unmatchedPeakCounter = new UnmatchedPeakCounter(fragmentTolerance.get());
                fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE_CONSTANT);
            }

            List<OmsResult> filteredResults = new ArrayList<>(query.getResultCount());
            filteredResults.add(query.getResult(0));

            PeptideSpectrum lastSpectrum = fragmenter.fragment(query.getResult(0).getPeptide(), 1);
            for (int i = 1; i < query.getResultCount(); i++) {

                OmsResult currResult = query.getResult(i);
                PeptideSpectrum currSpectrum = fragmenter.fragment(currResult.getPeptide(), 1);

                double difference = unmatchedPeakCounter.symmetricUnmatched(lastSpectrum, currSpectrum);
                if (difference > 0) {

                    lastSpectrum = fragmenter.fragment(currResult.getPeptide(), 1);
                    filteredResults.add(currResult);

                    if (filteredResults.size() >= 3)
                        break;
                }
            }
            return filteredResults;
        }

        public String getName() {

            return name;
        }

        protected abstract Tuple2<Double, Double> getScore(double defaultScoreDelta, List<OmsResult> filteredResults);
    }

    protected static class MetaScoreExtractScoresPF extends AbstractExtractScoresPF {

        public MetaScoreExtractScoresPF(double defaultScoreDelta, Cached<Tolerance> fragmentTolerance, boolean filterResults) {

            super(defaultScoreDelta, fragmentTolerance, filterResults, "metascore");
        }

        @Override
        protected Tuple2<Double, Double> getScore(double defaultScoreDelta, List<OmsResult> filteredResults) {

            return new Tuple2<>(
                    filteredResults.get(0).getMetaScore(),
                    filteredResults.get(1).getMetaScore()
            );
        }
    }
}
