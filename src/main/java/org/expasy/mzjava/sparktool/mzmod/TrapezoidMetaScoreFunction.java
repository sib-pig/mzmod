package org.expasy.mzjava.sparktool.mzmod;

import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.NdpResult;
import org.expasy.mzjava.sparktool.position.data.PositionResult;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class TrapezoidMetaScoreFunction implements MetaScoreFunction {

    @Override
    public double calcMetaScore(NdpResult ndpResult, PositionResult posResult) {

        HitType hitType = ndpResult.getHitType();
        if (hitType == HitType.EXACT) {

            return ndpResult.getWndp() + 1 * posResult.getIonCurrentCoverage();
        } else {

            int r1 = Math.min(Math.max(0, posResult.getPosition() - 1), posResult.getResidueCount() - 3);
            int r2 = r1 + 2;
            double targetArea = 0;
            double w = 1.0 / posResult.getResidueCount();
            for (; r1 < r2; r1++) {

                targetArea += 0.5 * (posResult.getScore(r1) + posResult.getScore(r1 + 1)) * w;
            }

            double totalArea = 0;
            for (int i = 0; i < posResult.getResidueCount() - 1; i++) {

                totalArea += 0.5 * (posResult.getScore(i) + posResult.getScore(i + 1)) * w;
            }
            double modP = targetArea / totalArea;
            if(modP < 0 || modP > 1)
                throw new IllegalStateException("Mod p is not in the range 0-1, was " + modP);
            return ndpResult.getWndp() + modP * posResult.getIonCurrentCoverage();
        }
    }
}
