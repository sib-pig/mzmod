package org.expasy.mzjava.sparktool.mzmod;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.library.IntervalSpectraLibrary;
import org.expasy.mzjava.proteomics.ms.spectrum.library.SpectraLibrary;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;
import org.expasy.mzjava.sparktool.hadoop.IndexedPeptideSpectrumValue;
import org.expasy.mzjava.sparktool.hadoop.PeakAvroValueReader;


import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class CachedLibrary extends Cached<SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum>> {

    private final String libPath;
    private final double lowerBound;
    private final double upperBound;

    public CachedLibrary(String libPath, double lowerBound, double upperBound) {

        this.libPath = libPath;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    @Override
    protected SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum> build() {

        try {

            final PeakAvroValueReader<IndexedPeptideSpectrum> reader = new PeakAvroValueReader<>(
                    FileSystem.getLocal(new Configuration()), new Path(libPath), new IndexedPeptideSpectrumValue()
            );
            return new IntervalSpectraLibrary<>(reader, lowerBound, upperBound);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
