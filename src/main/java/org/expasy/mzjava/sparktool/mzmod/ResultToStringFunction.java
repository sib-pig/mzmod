package org.expasy.mzjava.sparktool.mzmod;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationList;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ResultToStringFunction implements Function<OmsQuery, String>{

    private final double defaultScoreDelta;
    private final int maxRank;

    public ResultToStringFunction(double defaultScoreDelta, int maxRank) {

        this.defaultScoreDelta = defaultScoreDelta;
        this.maxRank = maxRank;
    }

    public ResultToStringFunction(double defaultScoreDelta) {

        this.defaultScoreDelta = defaultScoreDelta;
        this.maxRank = 1;
    }

    public static String getHeaderLine() {
        String header =
                "QueryPeptide\t" +
                "LibPeptide\t" +
                "ProteinAC\t" +
                "Charge\t" +
                "PrecursorMz\t" +
                "SpectrumID\t" +
                "MassShift\t" +
                "Rank\t" +
                "WeightedDotProduct\t" +
                "MetaScore\t" +
                "DeltaMetaScore\t" +
                "IonCurrentCoverage\t" +
                "Position\t" +
                "BestPositionScore\t" +
                "PositionScores\t" +
                "Mode";

        return header;
    }

    @Override
    public String call(OmsQuery query) throws Exception {

        final NumberFormat numberInstance = NumberFormat.getNumberInstance();
        final StringBuilder buff = new StringBuilder();

        for (int r=0;r<maxRank;r++) {
            final OmsResult result = query.getResult(r);
            final Peptide peptide = result.getPeptide();
            final int position = result.getPosition();
            final MsnSpectrum querySpectrum = query.getSpectrum();

            if (buff.length()>0)
                buff.append("\n");

            //cluster peptide sequence
            if (result.getHitType() == HitType.OPEN) {
                createModifiedPeptide(buff, peptide, result.getMassShift(), position, numberInstance);
            } else {
                buff.append(result.getPeptide());
            }
            buff.append('\t');

            //lib peptide
            buff.append(peptide);
            buff.append('\t');

            //lib uuid
//        buff.append(result.getLibSpectrumId());
//        buff.append('\t');

            //protein acc
            String acs = result.getProteinAccessionNumbers();
            buff.append(acs.replaceAll(" ",","));
            buff.append('\t');

            //query charge
            buff.append(querySpectrum.getPrecursor().getCharge());
            buff.append('\t');

            //precursor m/z
            buff.append(querySpectrum.getPrecursor().getMz());
            buff.append('\t');

            //query spectrum name
            buff.append(querySpectrum.getComment());
            buff.append('\t');

            //query uuid
//        buff.append(querySpectrum.getId());
//        buff.append('\t');

            //mass shift
            buff.append(result.getMassShift());
            buff.append('\t');

            //rank
            buff.append(r+1);
            buff.append('\t');

            //ndp
            buff.append(result.getWdp());
            buff.append('\t');

            //meta score
            buff.append(result.getMetaScore());
            buff.append('\t');

            //delta score
            buff.append(query.getDeltaMetaScore(r, defaultScoreDelta));
            buff.append('\t');

            //ion current coverage
            buff.append(result.getIonCurrentCoverage());
            buff.append('\t');

            //positioning site
            buff.append(position);
            buff.append('\t');

            //positioning score
            buff.append(result.getPositionScore(position));
            buff.append('\t');

            //positioning (score/site)
            writePositionScores(buff, result);
            buff.append('\t');

            buff.append(result.getHitType());

        }

        return buff.toString();
    }

    private void createModifiedPeptide(StringBuilder sb, Peptide peptide, double massShift, int modifiedResidue, NumberFormat numberInstance) {

        ModificationList nTermMods = peptide.getModifications(EnumSet.of(ModAttachment.N_TERM));
        if (!nTermMods.isEmpty()) {

            appendMods(sb, nTermMods);
            sb.append('_');
        }
        for (int i = 0; i < peptide.size(); i++) {

            sb.append(peptide.getSymbol(i));

            List<Modification> sideChainMods = peptide.getModificationsAt(i, EnumSet.of(ModAttachment.SIDE_CHAIN));
            if (modifiedResidue == i) {

                sideChainMods = new ArrayList<>(sideChainMods);
                NumericMass mass = new NumericMass(massShift);
                sideChainMods.add(new Modification(numberInstance.format(massShift), mass));
            }
            if (!sideChainMods.isEmpty()) {

                appendMods(sb, sideChainMods);
            }
        }
        ModificationList cTermMods = peptide.getModifications(EnumSet.of(ModAttachment.C_TERM));
        if (!cTermMods.isEmpty()) {

            sb.append('_');
            appendMods(sb, nTermMods);
        }
    }

    private void appendMods(StringBuilder sb, List<Modification> modList) {

        if (modList.isEmpty()) return;

        sb.append('(');
        for (Modification mod : modList) {

            sb.append(mod.getLabel());
            sb.append(", ");
        }
        int length = sb.length();
        sb.delete(length - 2, length);
        sb.append(')');
    }

    private void writePositionScores(StringBuilder buff, OmsResult result) {

        for (int i = 0; i < result.getResidueCount(); i++) {

            if (i != 0)
                buff.append(',');

            buff.append(result.getPositionScore(i));
        }
    }
}
