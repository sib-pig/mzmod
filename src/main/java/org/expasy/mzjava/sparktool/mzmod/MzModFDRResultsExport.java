package org.expasy.mzjava.sparktool.mzmod;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.InvalidPathException;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.storage.StorageLevel;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.hadoop.OmsQueryValue;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import org.expasy.mzjava.sparktool.util.SparkUtils;
import org.expasy.mzjava.stats.statscores.FitDecoyScoreStatistics;
import scala.Tuple2;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Markus Muller
 * @version sqrt -1
 */
public class MzModFDRResultsExport extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(MzModFDRResultsExport.class.getName());

    private String mzModTargetResultsFile;
    private String mzModDecoyResultsFile;
    private String mzModTabFileDir;
    private MzModFDRResultsExportParams params;
    private float qValue;
    private double defaultScoreDelta = 2;
    private float defaultOMSScoreThreshold;
    private float defaultExactScoreThreshold;
    boolean forceDefaultThreshold;
    private boolean forceDelete;

    protected MzModFDRResultsExport(String mzModTargetResultsFile, String mzModDecoyResultsFile, String mzModTabFileDir,
                                    float qValue, boolean forceDefaultThreshold, float defaultOMSScoreThreshold,
                                    float defaultExactScoreThreshold, boolean forceDelete, int nrExec, int execMemoryMB,
                                    int bufferMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,bufferMemoryMB,runLocal);

        this.mzModTargetResultsFile = mzModTargetResultsFile;
        this.mzModDecoyResultsFile = mzModDecoyResultsFile;
        this.mzModTabFileDir = mzModTabFileDir;
        this.defaultScoreDelta = 2;
        this.qValue = qValue;
        this.forceDefaultThreshold = forceDefaultThreshold;
        this.defaultOMSScoreThreshold = defaultOMSScoreThreshold;
        this.defaultExactScoreThreshold = defaultExactScoreThreshold;
        this.forceDelete = forceDelete;

        createOptions();
    }

    protected MzModFDRResultsExport() {

        createOptions();
    }

    public static void main(String[] args) throws IOException {

        MzModFDRResultsExport omsResultsExport =  new MzModFDRResultsExport();
        try {
            omsResultsExport.parseOptions(args).run();
        } catch (MissingOptionException e) {
            omsResultsExport.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            omsResultsExport.printOptions(args, e.getMessage());
        } catch(NumberFormatException e) {
            omsResultsExport.printOptions(args, e.getMessage());
        } catch (InvalidPathException e) {
            omsResultsExport.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int run() throws IOException {

        long start = System.currentTimeMillis();

        final SparkConf conf = initSpark();
        final JavaSparkContext sc = new JavaSparkContext(conf);

        params = new MzModFDRResultsExportParamsImpl();

        JavaRDD<OmsQuery> targetOMSMatches = sc.sequenceFile(mzModTargetResultsFile, Text.class, OmsQueryValue.class)
                .mapToPair(input -> new Tuple2<>(input._1().toString(), input._2().get()))
                .values()
                .filter(query -> query.getResultCount() > 0 && query.getPrecursor().getCharge() > 0)
                .setName("OmsResults")
                .persist(StorageLevel.MEMORY_AND_DISK());

        JavaRDD<OmsQuery> decoyOMSMatches = sc.sequenceFile(mzModDecoyResultsFile, Text.class, OmsQueryValue.class)
                .mapToPair(input -> new Tuple2<>(input._1().toString(), input._2().get()))
                .values()
                .filter(query -> query.getResultCount() > 0 && query.getPrecursor().getCharge() > 0)
                .setName("OmsResults")
                .persist(StorageLevel.MEMORY_AND_DISK());

        process(sc, targetOMSMatches, decoyOMSMatches);

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();
        return 0;
    }

    private void process(JavaSparkContext sc, JavaRDD<OmsQuery> targetOMSMatches, JavaRDD<OmsQuery> decoyOMSMatches) throws IOException {

        int minCharge = targetOMSMatches.min(new ChargeComparator()).getPrecursor().getCharge();
        int maxCharge = targetOMSMatches.max(new ChargeComparator()).getPrecursor().getCharge();
        LOGGER.info("charges = " + minCharge + " - " + maxCharge);

        if (!forceDefaultThreshold) {
            defaultOMSScoreThreshold = estimateFdrCutOff(sc, targetOMSMatches, decoyOMSMatches, 0, HitType.OPEN, defaultOMSScoreThreshold);
            defaultExactScoreThreshold = estimateFdrCutOff(sc, targetOMSMatches, decoyOMSMatches, 0, HitType.EXACT, defaultExactScoreThreshold);
        }

        for (int i = minCharge; i <= maxCharge; i++) {

            final int currentCharge = i;

            float omsCutOff;
            float exactCutOff;
            if (!forceDefaultThreshold) {
                omsCutOff = estimateFdrCutOff(sc, targetOMSMatches, decoyOMSMatches, currentCharge, HitType.OPEN, defaultOMSScoreThreshold);
                exactCutOff = estimateFdrCutOff(sc, targetOMSMatches, decoyOMSMatches, currentCharge, HitType.EXACT, defaultExactScoreThreshold);
            } else {
                omsCutOff = defaultOMSScoreThreshold;
                exactCutOff = defaultExactScoreThreshold;
            }

            LOGGER.info("Processing spectra with charge " + currentCharge);
            JavaRDD<OmsQuery> filteredQueries = targetOMSMatches
                    .filter(query -> {

                                double cutOff = query.getResult(0).getHitType() == HitType.OPEN ? omsCutOff : exactCutOff;
                                return query.getPrecursor().getCharge() == currentCharge && query.getResult(0).getMetaScore() >= cutOff;
                            }
                    )
                    .setName("Filtered " + currentCharge + "+")
                    .persist(StorageLevel.MEMORY_AND_DISK());

            SparkUtils.saveAsTextFile(sc, filteredQueries, new ResultToStringFunction(defaultScoreDelta,params.maxRank()), mzModTabFileDir + "/results_" + currentCharge + "+");

            append(i + "+ OPEN\t" + filteredQueries.filter(query -> query.getResult(0).getHitType() == HitType.OPEN).count() + "\n", mzModTabFileDir);
            append(i + "+ EXACT\t" + filteredQueries.filter(query -> query.getResult(0).getHitType() == HitType.EXACT).count() + "\n", mzModTabFileDir);
            filteredQueries.unpersist();

            insertHeaderLine(mzModTabFileDir + "/results_" + currentCharge + "+/result.txt", ResultToStringFunction.getHeaderLine());

        }
    }

    private void insertHeaderLine(String fileName, String header) throws IOException {

        File outFile = new File(fileName+"___tmp");
        File inFile = new File(fileName);

        // input
        FileInputStream fis  = new FileInputStream(inFile);
        BufferedReader in = new BufferedReader
                (new InputStreamReader(fis));

        // output
        FileOutputStream fos = new FileOutputStream(outFile);
        PrintWriter out = new PrintWriter(fos);

        out.println(header);
        String thisLine = "";
        while ((thisLine = in.readLine()) != null) {
            out.println(thisLine);
        }
        out.flush();
        out.close();
        in.close();

        inFile.delete();
        outFile.renameTo(inFile);

    }

    private float estimateFdrCutOff(JavaSparkContext sc, JavaRDD<OmsQuery> targetOMSMatches, JavaRDD<OmsQuery> decoyOMSMatches, int charge, HitType hitType, float defaultScoreThreshold) throws IOException {

        final ScoreExtracter scoreExtracter = new ScoreExtracter(params.maxRank());

        JavaRDD<Double> targetScoresRDD;
        if (charge>0)
            targetScoresRDD = targetOMSMatches
                .filter(query -> query.getResultCount() > 0 && query.getResult(0).getHitType() == hitType && query.getPrecursor().getCharge() == charge)
                .flatMap(scoreExtracter);
        else
            targetScoresRDD = targetOMSMatches
                    .filter(query -> query.getResultCount() > 0 && query.getResult(0).getHitType() == hitType)
                    .flatMap(scoreExtracter);

        long resultsCount = targetScoresRDD.count();

        if (resultsCount<params.minFDRSampleSize())
            return defaultScoreThreshold;

        double nrMatches = Math.min(resultsCount, params.maxFdrSampleSize());
        double fraction = Math.min(1.0, nrMatches / resultsCount);

        targetScoresRDD = targetScoresRDD.sample(false, fraction);
        float[] targetScores = toArray(targetScoresRDD.collect());
        targetScoresRDD.unpersist();

        JavaRDD<Double> decoyScoresRDD;
        if (charge>0)
            decoyScoresRDD = decoyOMSMatches
                    .filter(query -> query.getResultCount() > 0 && query.getResult(0).getHitType() == hitType && query.getPrecursor().getCharge() == charge)
                    .flatMap(scoreExtracter);
        else
            decoyScoresRDD = decoyOMSMatches
                    .filter(query -> query.getResultCount() > 0 && query.getResult(0).getHitType() == hitType)
                    .flatMap(scoreExtracter);

        resultsCount = targetScoresRDD.count();
        nrMatches = Math.min(resultsCount, params.maxFdrSampleSize());
        fraction = Math.min(1.0, nrMatches / resultsCount);

        decoyScoresRDD = decoyScoresRDD.sample(false, fraction);
        float[] decoyScores = toArray(decoyScoresRDD.collect());
        decoyScoresRDD.unpersist();

        reportScores(mzModTabFileDir, "ScoreStats", hitType.toString().toLowerCase() + "_target_score_dist_Z" + charge + ".txt", targetScores);
        reportScores(mzModTabFileDir, "ScoreStats", hitType.toString().toLowerCase() + "_decoy_score_dist_Z" + charge + ".txt", decoyScores);

        FitDecoyScoreStatistics scoreStatistics = new FitDecoyScoreStatistics(toArray(targetScoresRDD.collect()), toArray(decoyScoresRDD.collect()));
        float cutOff = scoreStatistics.getScoreForQValue(qValue);

        File file = new File(mzModTabFileDir + "/ScoreStats/" + hitType.toString().toLowerCase() + "_fdr_parameters_Z"+charge+".txt");
        if (file.exists() && !file.delete()) throw new IllegalStateException("Could not delete " + file);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
        writer.write("score threshold\t\t" + cutOff + "\n");
        writer.write("target decoy ratio\t" + scoreStatistics.getQValue(cutOff) + "\n");
        writer.write("result count\t\t" + nrMatches);
        writer.close();

        return cutOff;
    }

    private void append(String text, String outputPath) throws IOException {

        File file = new File(outputPath + "/hit_counts.txt");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
        writer.write(text);
        writer.close();
    }

    private float[] toArray(List<Double> doubleList) {

        float[] array = new float[doubleList.size()];
        int i = 0;
        for (Double score : doubleList) {

            array[i++] = score.floatValue();
        }

        return array;
    }

    private void reportScores(String root,String subDir, String fileName, float[] scores) throws IOException,InvalidPathException{


        File dir = new File(root+"/"+subDir);
        try {
            if (!dir.exists())
                FileUtils.forceMkdir(dir);
        } catch (IOException e) {
            throw new InvalidPathException("Cannot delete directory "+dir.getAbsolutePath());
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(root+"/"+subDir+"/"+fileName));

        for (int i=0;i<scores.length;i++)
            writer.write(scores[i]+"\n");

        writer.flush();
        writer.close();
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdMzMod").withDescription("MzMod target results sequence file (required)").hasArg().withArgName("path").isRequired().create("hm"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdDecoyMzMod").withDescription("MzMod decoy results sequence file (required)").hasArg().withArgName("path").isRequired().create("hd"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("mzModReportDir").withDescription("output directory for MzMod result reports (required)").hasArg().withArgName("path").isRequired().create("rm"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("qValue").withDescription("qValue for MzMod export (required)").isRequired().hasArg().withArgName("path").create("qv"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("forceDelete").withDescription("force deleting existing spark output directory").create("fd"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("forceDefaultThreshold").withDescription("Default threshold in params file is applied. Overrides qValue.").create("ft"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.forceDelete = false;
        if( line.hasOption( "fd" ) ) {
            this.forceDelete = true;
        }

        this.forceDefaultThreshold = false;
        if( line.hasOption( "ft" ) ) {
            this.forceDefaultThreshold = true;
            this.defaultExactScoreThreshold = params.defaultExactScoreThreshold();
            this.defaultOMSScoreThreshold = params.defaultOMSScoreThreshold();
        }

        this.mzModTargetResultsFile = checkExistFileOption(line, "hm");
        this.mzModDecoyResultsFile = checkExistFileOption(line, "hd");
        this.mzModTabFileDir = checkSparkDirOption(line, "rm", forceDelete);
        this.qValue = checkFloatOption(line, "qv",0,1,0.01f);
    }

    private static class ChargeComparator implements Comparator<OmsQuery>, Serializable {
        @Override
        public int compare(OmsQuery q1, OmsQuery q2) {

            return Integer.compare(q1.getPrecursor().getCharge(), q2.getPrecursor().getCharge());
        }
    }

    protected static class ScoreExtracter implements FlatMapFunction<OmsQuery,Double> {

        private int maxRank;

        public  ScoreExtracter(int maxRank) {
            this.maxRank = maxRank;
        }

        @Override
        public Iterator<Double> call(OmsQuery omsQuery) throws Exception {

            int maxIdx = Math.min(omsQuery.getResultCount(),maxRank);

            if (maxIdx<=0)
                return Collections.emptyIterator();

            List<Double> scores = new ArrayList<>();
            for (int i=0;i<maxIdx;i++) {
                scores.add(omsQuery.getResult(i).getMetaScore());
            }
            return scores.iterator();
        }
    }

}
