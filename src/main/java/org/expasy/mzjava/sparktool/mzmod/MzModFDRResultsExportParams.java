package org.expasy.mzjava.sparktool.mzmod;

import java.io.Serializable;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface MzModFDRResultsExportParams extends Serializable {

    int maxFdrSampleSize();
    int maxRank();
    int minFDRSampleSize();
    float defaultOMSScoreThreshold();
    float defaultExactScoreThreshold();

}
