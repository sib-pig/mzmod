package org.expasy.mzjava.sparktool.mzmod;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.RankNormalizer;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.library.SpectraLibrary;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.CachedPpmTolerance;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;
import org.expasy.mzjava.sparktool.psmconverter.PSMConverterParams;
import org.expasy.mzjava.sparktool.psmconverter.PSMConverterParamsImpl;
import org.expasy.mzjava.sparktool.position.PositionFunction;
import org.expasy.mzjava.sparktool.position.annotation.DeterministicPosAnnotationFunction;
import org.expasy.mzjava.sparktool.position.position.BayesPosScoreFunction;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;

import java.util.Collections;
import java.util.EnumSet;
import java.util.function.Predicate;

/**
 * @author Markus Muller
 * @version sqrt -1
 */
public class MzModParametersImpl implements MzModParameters {

    // Precursor ion m/z tolerance in ppm
    private static final double precursorPpmTolerance = 20;

    // Mass shift bounds for OMS search. All mass shifts within these bounds are considered.
    private static final int lowerBound = -200;
    private static final int upperBound = 400;


    private static final int minCharge = 2;
    private static final int maxCharge = 4;
    /*
    Query spectrum predicate. Only spectra in the hadoop file passing this filter are considered for OMS . This filter is additional to the filter in Msn2HdmsnParametersImpl,
    which selects the spectra written to the hadoop file.
    */
    private static final Predicate<MsnSpectrum> msnSpectrumPredicate = spectrum -> spectrum.getPrecursor().getCharge() >= minCharge && spectrum.getPrecursor().getCharge() <= maxCharge;

    /*
    Library spectrum predicate. Only library spectra in the hadoop file passing this filter are considered for OMS. Any predicate can be defined here, just make sure that it is consistent
    with the spectrum predicate above.
    */
    private static final Predicate<IndexedPeptideSpectrum> libraryPredicate = entry -> entry.getPrecursor().getCharge() >= minCharge && entry.getPrecursor().getCharge() <= maxCharge;

    /*
    Similarity function to first evaluate the similarity between query and libray spectra (called wNDP_score in the MzMod paper Horlacher et al, JPR, 2016).
    This score is used in the first step and the list of maxOmsResults spectrum-spectrum matches with the highest spectrumSimFunc scores are retained.
     */
    public static final int maxOmsResults = 12;

    private static final double backboneWeight = 1.5;
    private static final double fragmentTolerance = 0.05;
    private static final IndexedSpectrumSimFunc spectrumSimFunc =
            new IndexedSpectrumSimFunc(new AbsoluteTolerance(fragmentTolerance), charge -> charge - 1, backboneWeight);

    /*
    Metascore function (called M_score in MzMod publication)
     */
    private static final MetaScoreFunction metaScoreFunction = new TrapezoidMetaScoreFunction();

    /*

     */
    private static final PositionFunction positionFunction =
            new PositionFunction(new DeterministicPosAnnotationFunction(EnumSet.of(IonType.b), EnumSet.of(IonType.y), new AbsoluteTolerance(fragmentTolerance), peak -> Collections.singleton(1), 0.01), new BayesPosScoreFunction());


    private static final PSMConverterParams PSM_CONVERTER_PARAMS = new PSMConverterParamsImpl();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Implementation of getters (no need to change this part)
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    /*
    Only query spectra for which Predicate is true are searched
     */
    public Cached<Predicate<MsnSpectrum>> spectrumPredicate() {

        return new Cached<Predicate<MsnSpectrum>>() {
            @Override
            protected Predicate<MsnSpectrum> build() {

                return msnSpectrumPredicate;
            }
        };
    }

    @Override
    /*
    Load spectrum library
     */
    public Cached<SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum>> library(String libPath) {

        return new CachedLibrary(libPath, lowerBound, upperBound);
    }

    @Override
    /*
    Only library spectra for which Predicate is true are searched
     */
    public Cached<Predicate<IndexedPeptideSpectrum>> libSpectrumPredicate() {

        return new Cached<Predicate<IndexedPeptideSpectrum>>() {
            @Override
            protected Predicate<IndexedPeptideSpectrum> build() {

                return libraryPredicate;
            }
        };
    }

    @Override
    /*
    Function to evaluate similarity between query and libary IndexedSpectrum objects
     */
    public Cached<IndexedSpectrumSimFunc> omsSimFunc() {

        return new Cached<IndexedSpectrumSimFunc>() {
            @Override
            protected IndexedSpectrumSimFunc build() {

                return spectrumSimFunc;
            }
        };
    }

    @Override
    /*
    Peak processors for query spectra
     */
    public Cached<PeakProcessorChain<PeakAnnotation>> processorChain() {

        return new Cached<PeakProcessorChain<PeakAnnotation>>() {
            @Override
            protected PeakProcessorChain<PeakAnnotation> build() {

                return new PeakProcessorChain<>()
//                        .add(new NPeaksPerSlidingWindowFilter<>(2, 10))
                        .add(new RankNormalizer<>());
            }
        };
    }

    @Override
    /*
    Maximal number of results stores per query spectrum
     */
    public int maxOmsResults() {

        return maxOmsResults;
    }

    @Override
    /*
    Precursor tolerance
     */
    public Cached<Tolerance> precursorTolerance() {

        return new CachedPpmTolerance(precursorPpmTolerance);
    }

    @Override
    /*
    Function to position modifications
     */
    public Cached<PositionFunction> positionFunction() {

        return new Cached<PositionFunction>() {
            @Override
            protected PositionFunction build() {

                return positionFunction;
            }
        };
    }

    @Override
    /*
    Function to score PSM after positioning
     */
    public Cached<MetaScoreFunction> metaScoreFunction() {

        return new Cached<MetaScoreFunction>() {
            @Override
            protected MetaScoreFunction build() {

                return metaScoreFunction;
            }
        };
    }

    /*
    SpectrumKeyFunction class is used to map the PSMs objects to MsnSpectrum objects.
    */
    public Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumPSMKeyFunction() {
        return PSM_CONVERTER_PARAMS.spectrumPSMKeyFunction();
    }


}
