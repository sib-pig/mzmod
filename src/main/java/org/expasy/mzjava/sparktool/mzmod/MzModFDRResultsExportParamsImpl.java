package org.expasy.mzjava.sparktool.mzmod;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class MzModFDRResultsExportParamsImpl implements MzModFDRResultsExportParams {

    /*
    Maximal number of scores to be considered for FDR calculation. Since FDR is calculated on the master, limiting this number avoids
    memory overflow.
     */
    private int maxFdrSampleSize = 10000;

    /*
    Maximal rank of a MzMod match per spectrum required to be considered for qValue calculation and for the final results list.
     */
    private int maxRank = 2;

    /*
    If the number of target scores is less then minFdrSampleSize, then the defaultScoreThreshold is applied.
     */
    private int minFdrSampleSize = 1000;

    /*
    Default modified match score threshold. Without the -ft option, the threshold is only applied for small sample sizes. With the -ft option, this threshold
     is applied irrespective of the sample size and actual qValue related to the threshold.
     */
    private float defaultOMSScoreThreshold = 1.0f;

    /*
    Default exact match score threshold. Without the -ft option, the threshold is only applied for small sample sizes. With the -ft option, this threshold
     is applied irrespective of the sample size and actual qValue related to the threshold.
     */
    private float defaultExactScoreThreshold = 1.0f;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Implementation of getters (no need to change this part)
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public int maxFdrSampleSize() {
        return maxFdrSampleSize;
    }

    @Override
    public int maxRank() {
        return maxRank;
    }

    @Override
    public int minFDRSampleSize() {
        return minFdrSampleSize;
    }

    @Override
    public float defaultOMSScoreThreshold() {
        return defaultOMSScoreThreshold;
    }

    @Override
    public float defaultExactScoreThreshold() {
        return defaultExactScoreThreshold;
    }

}
