package org.expasy.mzjava.sparktool.mzmod;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.library.SpectraLibrary;
import org.expasy.mzjava.sparktool.data.*;
import org.expasy.mzjava.sparktool.position.PositionFunction;
import org.expasy.mzjava.sparktool.position.data.PositionResult;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class OmsFunc implements Function<OmsQuery, OmsQuery> {

    private final Cached<SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum>> spectrumLibrary;
    private final Cached<Predicate<IndexedPeptideSpectrum>> libSpectrumPredicate;
    private final Cached<IndexedSpectrumSimFunc> omsSimFunc;
    private final Cached<PeakProcessorChain<PeakAnnotation>> processorChain;
    private final Cached<PositionFunction> positionFunction;
    private final Cached<Tolerance> precursorTolerance;
    private final Cached<MetaScoreFunction> metaScoreFunction;
    private final int maxOmsResults;

    public OmsFunc(MzModParameters parameters, String libPath) {

        this.spectrumLibrary = parameters.library(libPath);
        this.libSpectrumPredicate = parameters.libSpectrumPredicate();
        this.omsSimFunc = parameters.omsSimFunc();
        this.processorChain = parameters.processorChain();
        this.positionFunction = parameters.positionFunction();
        this.maxOmsResults = parameters.maxOmsResults();
        this.metaScoreFunction = parameters.metaScoreFunction();
        this.precursorTolerance = parameters.precursorTolerance();
    }

    @Override
    public OmsQuery call(OmsQuery query) throws Exception {

        SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum> library = spectrumLibrary.get();

        final Peak precursor = query.getPrecursor();
        List<IndexedPeptideSpectrum> libSpectra = new ArrayList<>();
        library.getSpectra(precursor, libSpectra);

        final MsnSpectrum querySpectrum = query.getSpectrum();
        final Peak queryPrecursor = querySpectrum.getPrecursor();
        final IndexedSpectrum queryPl = query.getOrBuildIndexedSpectrum(processorChain.get());

        List<NdpResult> ndpResults = libSpectra.stream()
                .filter(libSpectrumPredicate.get())
                .map(libPl -> {

                    final HitType hitType;
                    if (precursorTolerance.get().withinTolerance(libPl.calcTheoreticalMz(), queryPrecursor.getMz()))
                        hitType = HitType.EXACT;
                    else
                        hitType = HitType.OPEN;

                    final double wndp = omsSimFunc.get().calcSimilarity(libPl, queryPl);
                    final double massShift = queryPrecursor.getMass() - libPl.getPrecursor().getMass();
                    return new NdpResult(libPl.getPeptide(), libPl.getId(), toAccString(libPl.getProteinAccessionNumbers()), libPl.getMemberCount(), wndp, massShift, hitType);
                })
                .filter(ndpResult -> ndpResult.getWndp() > 0)
                .collect(Collectors.toCollection(() -> new ArrayList<>(libSpectra.size())));

        return new OmsQuery(query, metaScore(querySpectrum, ndpResults));
    }

    private String toAccString(Set<String> proteinAccessionNumbers) {

        Iterator<String> it = proteinAccessionNumbers.iterator();
        if (! it.hasNext())
            return "";

        StringBuilder sb = new StringBuilder();
        for (;;) {
            String e = it.next();
            sb.append(e);
            if (! it.hasNext())
                return sb.toString();
            sb.append(' ');
        }
    }

    private List<OmsResult> metaScore(MsnSpectrum querySpectrum, List<NdpResult> ndpResults) {

        if (ndpResults.isEmpty())
            return Collections.emptyList();

        Collections.sort(ndpResults, (r1, r2) -> Double.compare(r2.getWndp(), r1.getWndp()));

        int ssmSize = ndpResults.size();
        List<OmsResult> results = new ArrayList<>(ssmSize < maxOmsResults ? ssmSize : maxOmsResults * 2);
        double maxMetaScore = 0;
        int count = 0;
        for (NdpResult ndpResult : ndpResults) {

            PositionResult posResult = positionFunction.get().position(querySpectrum, ndpResult);
            final double metaScore = metaScoreFunction.get().calcMetaScore(ndpResult, posResult);
            if (metaScore > maxMetaScore) {

                count = 0;
                maxMetaScore = metaScore;
            }

            results.add(new OmsResult(metaScore, ndpResult, posResult));
            count += 1;

            if (count >= maxOmsResults)
                break;
        }

        Collections.sort(results, (r1, r2) -> Double.compare(r2.getMetaScore(), r1.getMetaScore()));
        if (results.size() <= maxOmsResults) {

            return results;
        } else {

            return results.subList(0, maxOmsResults);
        }
    }
}
