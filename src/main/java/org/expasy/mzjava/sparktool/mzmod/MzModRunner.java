package org.expasy.mzjava.sparktool.mzmod;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.fs.InvalidPathException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.hadoop.OmsQueryValue;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import org.expasy.mzjava.sparktool.util.SparkUtils;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import scala.Tuple2;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MzModRunner extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(MzModRunner.class.getName());

    private String spectraHdFile;
    private String libraryHdFile;
    private float querySpectraSampelFraction;
    private String sparkDir;
    private boolean forceDelete;

    public MzModRunner(String spectraHdFile, String libraryHdFile, String sparkDir, float sampleFraction, boolean forceDelete, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,buffMemoryMB,runLocal);

        this.spectraHdFile = spectraHdFile;
        this.libraryHdFile = libraryHdFile;
        this.querySpectraSampelFraction = sampleFraction;
        this.sparkDir = sparkDir;
        this.forceDelete = forceDelete;

        createOptions();
    }

    public MzModRunner() {

        createOptions();
    };

    public String getSpectraHdFile() {
        return spectraHdFile;
    }

    public String getLibraryHdFile() {
        return libraryHdFile;
    }

    public float getQuerySpectraSampelFraction() {
        return querySpectraSampelFraction;
    }

    public String getSparkDir() {
        return sparkDir;
    }

    public static void main(String[] args) throws IOException {

        MzModRunner mzModRunner =  new MzModRunner();
        try {
            mzModRunner.parseOptions(args).run();
        } catch (MissingOptionException e) {
            mzModRunner.printOptions(args,e.getMessage());
        } catch (ParseException e) {
            mzModRunner.printOptions(args, e.getMessage());
        } catch(NumberFormatException e) {
            mzModRunner.printOptions(args, e.getMessage());
        } catch (InvalidPathException e) {
            mzModRunner.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int run() throws IOException {

        long start = System.currentTimeMillis();

        final SparkConf conf = initSpark();
        final JavaSparkContext sc = new JavaSparkContext(conf);

        final MzModParameters parameters = new MzModParametersImpl();

        JavaRDD<OmsQuery> omsQueries = SparkUtils.msnSpectra(sc, spectraHdFile, parameters.spectrumPredicate())
                .map(OmsQuery::new);

        if(querySpectraSampelFraction < 1)
            omsQueries = omsQueries.sample(false, querySpectraSampelFraction);

        LOGGER.info("Queries " + omsQueries.count());

        JavaRDD<OmsQuery> omsResults = omsQueries
                .map(new OmsFunc(parameters, libraryHdFile))
                .setName("OmsResults")
                .persist(StorageLevel.MEMORY_AND_DISK());

        SpectrumKeyFunction<MsnSpectrum> keyFunction = parameters.spectrumPSMKeyFunction().get();
        omsResults.mapToPair(query -> new Tuple2<>(new Text(keyFunction.apply(query.getSpectrum())), new OmsQueryValue(query)))
                .saveAsNewAPIHadoopFile(sparkDir, Text.class, OmsQueryValue.class, SequenceFileOutputFormat.class);

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdSpec").withDescription("query spectrum sequence file (required)").hasArg().withArgName("path").isRequired().create("hs"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdLib").withDescription("spectrum library sequence file (required)").hasArg().withArgName("path").isRequired().create("hl"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("sampleFact").withDescription("fraction of query spectra").hasArg().withArgName("float").create("sf"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdMzModDir").withDescription("MzMod result directory (required)").isRequired().hasArg().withArgName("path").create("hm"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("forceDelete").withDescription("force deleting existing spark output directory").create("fd"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.spectraHdFile = checkExistFileOption(line, "hs");
        this.libraryHdFile = checkExistFileOption(line, "hl");
        this.querySpectraSampelFraction = checkFloatOption(line, "sf", 0, 1, 1);

        this.forceDelete = false;
        if( line.hasOption( "fd" ) ) {
            this.forceDelete = true;
        }
        this.sparkDir = checkSparkDirOption(line, "hm",forceDelete);
    }

}
