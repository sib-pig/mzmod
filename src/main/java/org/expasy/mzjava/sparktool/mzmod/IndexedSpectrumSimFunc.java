package org.expasy.mzjava.sparktool.mzmod;

import gnu.trove.function.TIntFunction;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;

import java.util.BitSet;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedSpectrumSimFunc {

    private final TIntFunction maxFragChargeFunction;
    private final Tolerance tolerance;
    private final double backboneWeight;

    private final double[] targetMzs = new double[50];
    private int targetMzsSize = 0;
    private final SingleModMzGenerator modMzGenerator = new SingleModMzGenerator();

    private final BitSet queryIntensityUsed = new BitSet(1000);

    public IndexedSpectrumSimFunc(Tolerance tolerance, TIntFunction maxFragChargeFunction, double backboneWeight) {

        this.tolerance = tolerance;
        this.maxFragChargeFunction = maxFragChargeFunction;
        this.backboneWeight = backboneWeight;
    }

    public double calcSimilarity(IndexedSpectrum libPL, IndexedSpectrum queryPL) {

        final double deltaMass = queryPL.getPrecursor().getMass() - libPL.getPrecursor().getMass();

        if (queryPL.isEmpty() || libPL.isEmpty()) return 0.0;

        queryIntensityUsed.clear();

        final int libSize = libPL.size();
        final int maxCharge = maxFragChargeFunction.execute(queryPL.getPrecursor().getCharge());

        final BitSet libWeightedPeaks = new BitSet(libSize);
        final BitSet queryWeightedPeaks = new BitSet(libSize);
        double dpSum = collectScorePairs(0, libSize, libPL, queryPL, deltaMass, maxCharge, queryIntensityUsed, libWeightedPeaks, queryWeightedPeaks);

        if (backboneWeight == 1) {
            return dpSum / (libPL.getVectorLength() * queryPL.getVectorLength());
        } else
            return dpSum / (dist(libPL, libWeightedPeaks) * dist(queryPL, queryWeightedPeaks));
    }

    protected double dist(IndexedSpectrum peakList, BitSet weightedPeaks) {

        double dist = 0;

        for (int i = 0; i < peakList.size(); i++) {

            double x = peakList.getNthIntensity(i) * (weightedPeaks.get(i) ? backboneWeight : 1);
            dist += x * x;
        }

        return Math.sqrt(dist);
    }

    private double collectScorePairs(int startLibIndex, int endLibIndex, IndexedSpectrum libPeakList, IndexedSpectrum queryPeakList, double deltaMass, int maxCharge, BitSet queryIntensityUsed, BitSet weightedLibPeaks, BitSet weightedQueryPeaks) {

        modMzGenerator.deltaMass = deltaMass;
        double largestQueryMz = queryPeakList.getMaxMz();
        final int querySize = queryPeakList.size();

        double wdpPartSum = 0;

        for (int libIndex = startLibIndex; libIndex < endLibIndex; libIndex++) {

            final double libMz = libPeakList.getNthMz(libIndex);
            final double[] targetMzs1 = modMzGenerator.createTargetMzs(libMz, libPeakList.getCharge(libIndex), maxCharge);

            int selectedQueryMzIndex = -1;
            double maxQueryIntensity = 0;
            //For each m/z in the lib peak list find the peak that has the highest intensity
            for (int i = 0; i < targetMzsSize; i++) {

                final double targetMz = targetMzs1[i];
                //If targetMz is larger than the largest m/z in the query list we are done
                if (tolerance.check(largestQueryMz, targetMz) == Tolerance.Location.LARGER) break;

                final double queryMinMz = tolerance.getMin(targetMz);
                int queryMzIndex = queryPeakList.fastIndexOf(queryMinMz);
                if (queryMzIndex >= querySize) break;

                //Find all peaks that have a mz that is within tolerance of the targetMz
                double queryMz = queryPeakList.getMz(queryMzIndex);
                while (queryMz <= tolerance.getMax(targetMz) && queryMzIndex < querySize) {

                    if (queryMz >= queryMinMz) {

                        double currIntensity = queryPeakList.getIntensity(queryMzIndex);
                        if (currIntensity > maxQueryIntensity && !queryIntensityUsed.get(queryPeakList.toIntensityIndex(queryMzIndex))) {

                            maxQueryIntensity = currIntensity;
                            selectedQueryMzIndex = queryMzIndex;
                        }
                    }
                    queryMzIndex++;
                    queryMz = queryMzIndex < querySize ? queryPeakList.getMz(queryMzIndex) : Double.MAX_VALUE;
                }
            }

            if (selectedQueryMzIndex != -1) {

                queryIntensityUsed.set(queryPeakList.toIntensityIndex(selectedQueryMzIndex));
                double weight;
                if (libPeakList.getCharge(libIndex) == 0) {
                    weight = backboneWeight;
                    weightedLibPeaks.set(libIndex);
                    weightedQueryPeaks.set(queryPeakList.toIntensityIndex(selectedQueryMzIndex));
                } else {
                    weight = 1;
                }
                final double libIntensity = libPeakList.getNthIntensity(libIndex);
                final double queryIntensity = queryPeakList.getIntensity(selectedQueryMzIndex);
                wdpPartSum += libIntensity * queryIntensity * weight;
            } else if (libPeakList.getCharge(libIndex) == 0) {

                weightedLibPeaks.set(libIndex);
            }
        }

        return wdpPartSum;
    }

    private class SingleModMzGenerator {

        private double deltaMass;

        public final double[] createTargetMzs(double libMz, int charge, int maxCharge) {

            if (deltaMass == 0) {

                targetMzs[0] = libMz;
                targetMzsSize = 1;
                return targetMzs;
            }

            if (charge > 0) {

                if (deltaMass > 0) {

                    targetMzs[0] = libMz;
                    targetMzs[1] = libMz + (deltaMass / charge);
                    targetMzsSize = 2;
                } else if (deltaMass < 0) {

                    targetMzs[0] = libMz + (deltaMass / charge);
                    targetMzs[1] = libMz;
                    targetMzsSize = 2;
                }
            } else {

                if (deltaMass > 0) {

                    int n = 0;
                    targetMzs[n++] = libMz;
                    for (int z = maxCharge; z > 0; z--) {

                        targetMzs[n++] = libMz + (deltaMass / z);
                    }
                    targetMzsSize = n;
                } else if (deltaMass < 0) {

                    int n = 0;
                    for (int z = 1; z <= maxCharge; z++) {

                        targetMzs[n++] = libMz + (deltaMass / z);
                    }
                    targetMzs[n] = libMz;
                    targetMzsSize = n + 1;
                }
            }
            return targetMzs;
        }
    }
}
