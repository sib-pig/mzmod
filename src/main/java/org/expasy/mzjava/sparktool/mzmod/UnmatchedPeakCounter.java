package org.expasy.mzjava.sparktool.mzmod;

import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.AbstractModAligner;
import org.expasy.mzjava.core.ms.spectrasim.AlignmentPairList;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;

import java.util.Arrays;
import java.util.BitSet;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class UnmatchedPeakCounter extends AbstractModAligner<PepFragAnnotation, PepFragAnnotation> {

    public UnmatchedPeakCounter(Tolerance tolerance) {

        super(tolerance);
    }

    /**
     * Count the number of peaks in plX that do not have a match in plY.
     *
     * @param plX peak list X
     * @param plY peak list Y
     * @return the number of peaks in plX that do not have a match in plY
     */
    public int asymmetricUnmatched(PeakList<PepFragAnnotation> plX, PeakList<PepFragAnnotation> plY) {

        final AlignmentPairList alignmentPairs = calcAlignmentPairList(plX, plY);

        int missing = 0;
        for (int i = 0; i < alignmentPairs.size(); i++) {

            int indexX = alignmentPairs.getLibIndex(i);
            int indexY = alignmentPairs.getQueryIndex(i);

            if (indexX != -1 && indexY == -1)
                missing += 1;
            else if (indexX == -1 && indexY != -1)
                missing += 1;
        }

        return missing;
    }

    /**
     * Counts the number of peaks in plX and plY that don't have a match.
     *
     * @param plX peak list X
     * @param plY peak list Y
     * @return the number of peaks in plX and plY that don't have a match.
     */
    public int symmetricUnmatched(PeakList<PepFragAnnotation> plX, PeakList<PepFragAnnotation> plY) {

        final PeakList<PepFragAnnotation> plSmall;
        final PeakList<PepFragAnnotation> plLarge;
        if (plX.size() <= plY.size()) {
            plSmall = plX;
            plLarge = plY;
        } else {
            plSmall = plY;
            plLarge = plX;
        }

        final AlignmentPairList alignmentPairs = calcAlignmentPairList(plLarge, plSmall);

        int missing = 0;
        final BitSet peaksSet = new BitSet(plSmall.size());
        for (int i = 0; i < alignmentPairs.size(); i++) {

            int indexX = alignmentPairs.getLibIndex(i);
            int indexY = alignmentPairs.getQueryIndex(i);

            if (indexX != -1 && indexY == -1)
                missing += 1;
            else if (indexX != -1) {
                peaksSet.set(indexY);
            }
        }

        missing += plSmall.size() - peaksSet.cardinality();

        return missing;
    }

    private AlignmentPairList calcAlignmentPairList(PeakList<PepFragAnnotation> plX, PeakList<PepFragAnnotation> plY) {

        final double massDiff = plY.getPrecursor().getMass() - plX.getPrecursor().getMass();
        final AlignmentPairList alignmentPairs = new AlignmentPairList();
        final int[] peakCharges = new int[plX.size()];
        Arrays.fill(peakCharges, 1);
        collectScorePairs(alignmentPairs, plX, plY, new double[]{massDiff}, peakCharges, 1);
        return alignmentPairs;
    }
}
