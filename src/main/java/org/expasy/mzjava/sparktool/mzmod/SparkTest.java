package org.expasy.mzjava.sparktool.mzmod;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.hadoop.fs.InvalidPathException;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.sparktool.util.SparkRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SparkTest extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(SparkTest.class.getName());

    private int arraySize;

    public SparkTest(int arraySize, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,buffMemoryMB,runLocal);

        this.arraySize = arraySize;

        createOptions();
    }

    public SparkTest() {

        createOptions();
    };

    public static void main(String[] args) throws IOException {

        SparkTest sparkTest =  new SparkTest();
        try {
            sparkTest.parseOptions(args).run();
        } catch (MissingOptionException e) {
            sparkTest.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            sparkTest.printOptions(args, e.getMessage());
        } catch(NumberFormatException e) {
            sparkTest.printOptions(args, e.getMessage());
        } catch (InvalidPathException e) {
            sparkTest.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int run() throws IOException {

        long start = System.currentTimeMillis();

        final SparkConf conf = initSpark();
        final JavaSparkContext sc = new JavaSparkContext(conf);

        MersenneTwister rg = new MersenneTwister();

        ArrayList<Double> values = new ArrayList<>();
        for (int i=0;i<this.arraySize;i++) {
            values.add(rg.nextGaussian());
        }


        double sum = sc.parallelize(values).reduce((accum, v) -> (accum + v));
        System.out.println("Mean value :"+sum/arraySize);

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("size").withDescription("array size").hasArg().withArgName("integer").create("s"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.arraySize = checkIntOption(line, "s", 0, Integer.MAX_VALUE, 1000000);
    }

}
