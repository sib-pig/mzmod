package org.expasy.mzjava.sparktool.deliberator;

import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;

import java.util.Collections;
import java.util.Iterator;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class DeLiberatorFunction implements FlatMapFunction<PeptideConsensusSpectrum, PeptideConsensusSpectrum> {

    private final LibrarySpectrumShufflerFactory spectrumShufflerFactory;

    public DeLiberatorFunction(LibrarySpectrumShufflerFactory spectrumShufflerFactory) {

        this.spectrumShufflerFactory = spectrumShufflerFactory;
    }

    @Override
    public Iterator<PeptideConsensusSpectrum> call(PeptideConsensusSpectrum input) throws Exception {

        Optional<PeptideConsensusSpectrum> shuffled = spectrumShufflerFactory.newSpectrumShuffler()
                .shuffle(input, spectrumShufflerFactory.getPeptideTerminiHandling());

        return shuffled.isPresent() ? Collections.singleton(shuffled.get()).iterator() : Collections.emptyIterator();
    }
}
