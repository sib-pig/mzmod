package org.expasy.mzjava.sparktool.hadoop;

import org.expasy.mzjava.hadoop.io.AbstractAvroWritable;
import org.expasy.mzjava.sparktool.avro.IndexedSpectrumReader;
import org.expasy.mzjava.sparktool.avro.IndexedSpectrumWriter;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedSpectrumValue extends AbstractAvroWritable<IndexedSpectrum> {

    public IndexedSpectrumValue() {

        super(new IndexedSpectrumWriter(), new IndexedSpectrumReader());
    }

    public IndexedSpectrumValue(IndexedSpectrum value) {

        super(value, new IndexedSpectrumWriter(), new IndexedSpectrumReader());
    }
}
