package org.expasy.mzjava.sparktool.hadoop;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.hadoop.io.AbstractAvroWritable;
import org.expasy.mzjava.hadoop.io.SpectrumKey;

import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakAvroValueReader<T> implements IterativeReader<T> {

    private T current;
    private final SequenceFile.Reader reader;

    private final SpectrumKey key;
    private final AbstractAvroWritable<T> value;

    public PeakAvroValueReader(FileSystem fs, Path path, AbstractAvroWritable<T> value) throws IOException {

        this.key = new SpectrumKey();
        this.value = value;

        reader = new SequenceFile.Reader(fs.getConf(), SequenceFile.Reader.file(path));
    }

    @Override
    public void close() throws IOException {

        reader.close();
    }

    public boolean hasNext() {

        if (current != null) return true;

        boolean next;
        try {
            next = reader.next(key, value);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        if (!next) return false;

        current = value.get();

        return true;
    }

    public T next() throws IOException {

        if (current == null) {

            boolean hasNext = hasNext();
            if (!hasNext) throw new NoSuchElementException();
        }

        T next = current;

        current = null;

        return next;
    }
}
