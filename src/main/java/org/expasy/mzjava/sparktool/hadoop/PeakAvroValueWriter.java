package org.expasy.mzjava.sparktool.hadoop;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.SequenceFile;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.hadoop.io.AbstractAvroWritable;
import org.expasy.mzjava.hadoop.io.DefaultSpectrumKeyRawComparator;
import org.expasy.mzjava.hadoop.io.SpectrumKey;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakAvroValueWriter<T> {

    private final SequenceFile.CompressionType compressionType;
    private final FileSystem fs;
    private final Path path;
    private final Path tmpPath;

    private SequenceFile.Writer writer;

    private final SpectrumKey key;
    private final AbstractAvroWritable<T> value;
    private final RawComparator<SpectrumKey> rawKeyComparator = new DefaultSpectrumKeyRawComparator();

    public PeakAvroValueWriter(FileSystem fs, Path path, AbstractAvroWritable<T> value, SequenceFile.CompressionType compressionType) {

        this.fs = fs;
        key = new SpectrumKey();
        this.value = value;

        this.path = path;
        this.tmpPath = new Path(path.getParent(), path.getName() + ".tmp");
        this.compressionType = compressionType;
    }

    public void write(Peak precursor, T spectrum) throws IOException {

        if (writer == null) createWriter();

        key.set(precursor);
        value.set(spectrum);

        writer.append(key, value);
    }

    public void close() throws IOException {

        if (writer != null) {

            writer.close();
            SequenceFile.Sorter sorter = new SequenceFile.Sorter(fs, rawKeyComparator, key.getClass(), value.getClass(), fs.getConf());
            sorter.sort(new Path[]{tmpPath}, path, true);
        }
    }

    private void createWriter() throws IOException {

        writer = SequenceFile.createWriter(fs.getConf(),
                SequenceFile.Writer.file(tmpPath),
                SequenceFile.Writer.keyClass(key.getClass()),
                SequenceFile.Writer.valueClass(value.getClass()),
                SequenceFile.Writer.compression(compressionType));
    }
}
