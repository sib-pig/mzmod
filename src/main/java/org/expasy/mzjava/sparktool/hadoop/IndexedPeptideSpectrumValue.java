package org.expasy.mzjava.sparktool.hadoop;

import org.expasy.mzjava.hadoop.io.AbstractAvroWritable;
import org.expasy.mzjava.sparktool.avro.IndexedPeptideSpectrumReader;
import org.expasy.mzjava.sparktool.avro.IndexedPeptideSpectrumWriter;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedPeptideSpectrumValue extends AbstractAvroWritable<IndexedPeptideSpectrum> {

    public IndexedPeptideSpectrumValue() {

        super(new IndexedPeptideSpectrumWriter(), new IndexedPeptideSpectrumReader());
    }

    public IndexedPeptideSpectrumValue(IndexedPeptideSpectrum value) {

        super(value, new IndexedPeptideSpectrumWriter(), new IndexedPeptideSpectrumReader());
    }
}
