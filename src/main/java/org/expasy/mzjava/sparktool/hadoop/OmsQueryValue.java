package org.expasy.mzjava.sparktool.hadoop;

import org.expasy.mzjava.hadoop.io.AbstractAvroWritable;
import org.expasy.mzjava.sparktool.avro.OmsQueryReader;
import org.expasy.mzjava.sparktool.avro.OmsQueryWriter;
import org.expasy.mzjava.sparktool.data.OmsQuery;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class OmsQueryValue extends AbstractAvroWritable<OmsQuery> {

    public OmsQueryValue() {

        super(new OmsQueryWriter(), new OmsQueryReader());
    }

    public OmsQueryValue(OmsQuery value) {

        super(value, new OmsQueryWriter(), new OmsQueryReader());
    }
}
