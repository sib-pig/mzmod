package org.expasy.mzjava.sparktool.hadoop;

import org.expasy.mzjava.hadoop.io.AbstractAvroWritable;
import org.expasy.mzjava.sparktool.avro.PeptideMatchReader;
import org.expasy.mzjava.sparktool.avro.PeptideMatchWriter;
import org.expasy.mzjava.sparktool.data.PeptideMatch;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideMatchValue extends AbstractAvroWritable<PeptideMatch> {

    public PeptideMatchValue() {

        super(new PeptideMatchWriter(), new PeptideMatchReader());
    }

    public PeptideMatchValue(PeptideMatch peptideMatch) {

        super(peptideMatch, new PeptideMatchWriter(), new PeptideMatchReader());
    }
}
