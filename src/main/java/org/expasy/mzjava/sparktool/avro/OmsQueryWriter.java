package org.expasy.mzjava.sparktool.avro;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.avro.io.MsnSpectrumWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class OmsQueryWriter extends AbstractAvroWriter<OmsQuery> {

    private final OmsQuerySchemaBuilder schemaBuilder = new OmsQuerySchemaBuilder();

    private MsnSpectrumWriter spectrumWriter = new MsnSpectrumWriter(Optional.<PeakList.Precision>absent());
    private OmsResultsWriter omsResultWriter = new OmsResultsWriter();

    @Override
    public Class getObjectClass() {

        return OmsQuery.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }

    @Override
    public void write(OmsQuery value, Encoder out) throws IOException {

        spectrumWriter.write(value.getSpectrum(), out);
        writeArray(omsResultWriter, value.getResults(), out);
    }
}
