package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.sparktool.data.ConsensusKey;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class ConsensusKeyWriter extends AbstractAvroWriter<ConsensusKey> {

    private final ConsensusKeySchemaBuilder schemaBuilder = new ConsensusKeySchemaBuilder();
    private final PeptideWriter peptideWriter = new PeptideWriter();

    @Override
    public Class getObjectClass() {

        return schemaBuilder.getObjectClass();
    }

    @Override
    public void write(ConsensusKey consensusKey, Encoder encoder) throws IOException {

        encoder.writeInt(consensusKey.getCharge());
        peptideWriter.write(consensusKey.getPeptide(), encoder);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
