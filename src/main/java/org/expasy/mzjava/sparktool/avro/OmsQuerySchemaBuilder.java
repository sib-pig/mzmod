package org.expasy.mzjava.sparktool.avro;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.expasy.mzjava.avro.io.MsnSpectrumWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.sparktool.data.OmsQuery;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class OmsQuerySchemaBuilder extends ObjectSchemaBuilder {

    private final MsnSpectrumWriter spectrumWriter = new MsnSpectrumWriter(Optional.<PeakList.Precision>absent());
    private final OmsResultSchemaBuilder peptideSsmResultSchemaBuilder = new OmsResultSchemaBuilder();

    @Override
    public Class getObjectClass() {

        return OmsQuery.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("spectrum", spectrumWriter.createSchema()));
        fields.add(createSchemaField("omsResults", Schema.createArray(peptideSsmResultSchemaBuilder.createSchema())));
    }
}

