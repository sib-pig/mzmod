package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.io.Encoder;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedPeakListWriter {

    private final Map<PeakList.Precision, IndexedPeaksWriter> peakListWriterMap = new HashMap<>(5);

    public IndexedPeakListWriter() {

        peakListWriterMap.put(PeakList.Precision.DOUBLE, new DoublePeakListWriter());
        peakListWriterMap.put(PeakList.Precision.DOUBLE_CONSTANT, new DoublePeakListWriter());
        peakListWriterMap.put(PeakList.Precision.DOUBLE_FLOAT, new DoubleFloatPeakListWriter());
        peakListWriterMap.put(PeakList.Precision.FLOAT, new FloatPeakListWriter());
        peakListWriterMap.put(PeakList.Precision.FLOAT_CONSTANT, new FloatPeakListWriter());
    }

    public void write(IndexedSpectrum peakList, Encoder out) throws IOException {

        IndexedPeaksWriter peakListWriter = peakListWriterMap.get(peakList.getPrecision());
        out.writeIndex(peakListWriter.getUnionIndex());
        peakListWriter.writePeaks(peakList, out);
    }

    private abstract static  class IndexedPeaksWriter {

        private final int unionIndex;

        protected IndexedPeaksWriter(int unionIndex) {

            this.unionIndex = unionIndex;
        }

        private void writePeaks(IndexedSpectrum peakList, Encoder out) throws IOException {

            out.writeArrayStart();
            out.setItemCount(peakList.size());

            for(int i = 0; i < peakList.size(); i++){

                out.startItem();
                writeMz(peakList.getMz(i), out);
                writeIntensity(peakList.getIntensity(i), out);
                out.writeInt(peakList.getCharge(i));
            }
            out.writeArrayEnd();
        }

        protected abstract void writeMz(double mz, Encoder out) throws IOException;

        protected abstract void writeIntensity(double intensity, Encoder out) throws IOException;

        public int getUnionIndex() {

            return unionIndex;
        }
    }

    private static class DoublePeakListWriter extends IndexedPeaksWriter {

        private DoublePeakListWriter() {

            super(0);
        }

        @Override
        protected void writeMz(double mz, Encoder out) throws IOException {

            out.writeDouble(mz);
        }

        @Override
        protected void writeIntensity(double intensity, Encoder out) throws IOException {

            out.writeDouble(intensity);
        }
    }

    private static class DoubleFloatPeakListWriter extends IndexedPeaksWriter {

        private DoubleFloatPeakListWriter() {

            super(1);
        }

        @Override
        protected void writeMz(double mz, Encoder out) throws IOException {
            out.writeDouble(mz);
        }

        @Override
        protected void writeIntensity(double intensity, Encoder out) throws IOException {
            out.writeFloat((float)intensity);
        }
    }

    private static class FloatPeakListWriter extends IndexedPeaksWriter {

        private FloatPeakListWriter() {

            super(2);
        }

        @Override
        protected void writeMz(double mz, Encoder out) throws IOException {
            out.writeFloat((float) mz);
        }

        @Override
        protected void writeIntensity(double intensity, Encoder out) throws IOException {
            out.writeFloat((float)intensity);
        }
    }
}
