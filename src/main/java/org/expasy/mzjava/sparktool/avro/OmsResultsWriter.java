package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.avro.io.UUIDWriter;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class OmsResultsWriter extends AbstractAvroWriter<OmsResult> {

    private final OmsResultSchemaBuilder schemaBuilder = new OmsResultSchemaBuilder();

    private final PeptideWriter peptideWriter = new PeptideWriter();
    private final UUIDWriter uuidWriter = new UUIDWriter();

    @Override
    public Class getObjectClass() {

        return OmsResult.class;
    }

    @Override
    public void write(OmsResult value, Encoder out) throws IOException {

        out.writeEnum(value.getHitType().ordinal());
        peptideWriter.write(value.getPeptide(), out);
        uuidWriter.write(value.getLibSpectrumId(), out);
        out.writeString(value.getProteinAccessionNumbers());
        out.writeInt(value.getLibMemberCount());

        out.writeDouble(value.getMassShift());
        out.writeDouble(value.getMetaScore());
        out.writeDouble(value.getWdp());
        out.writeDouble(value.getIonCurrentCoverage());
        out.writeInt(value.getPosition());
        out.writeArrayStart();
        final int scoreCount = value.getResidueCount();
        out.setItemCount(scoreCount);

        for(int i = 0; i < scoreCount; i++) {

            out.startItem();
            out.writeDouble(value.getPositionScore(i));
        }
        out.writeArrayEnd();
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
