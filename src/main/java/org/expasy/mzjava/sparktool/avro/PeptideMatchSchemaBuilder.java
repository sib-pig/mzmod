package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.sparktool.data.PeptideMatch;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideMatchSchemaBuilder extends ObjectSchemaBuilder {

    private final PeptideWriter peptideWriter = new PeptideWriter();

    @Override
    public Class getObjectClass() {

        return PeptideMatch.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("peptide", peptideWriter.createSchema()));
        fields.add(createSchemaField("proteinAcc", Schema.createArray(Schema.create(Schema.Type.STRING))));
        fields.add(createSchemaField("scores", Schema.createMap(Schema.create(Schema.Type.DOUBLE))));
        fields.add(createSchemaField("charge", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("isDecoy", Schema.create(Schema.Type.BOOLEAN)));
    }
}
