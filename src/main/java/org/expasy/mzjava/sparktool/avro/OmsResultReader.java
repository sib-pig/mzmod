package org.expasy.mzjava.sparktool.avro;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.array.TDoubleArrayList;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.UUIDReader;
import org.expasy.mzjava.proteomics.avro.io.PeptideReader;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class OmsResultReader extends AbstractAvroReader<OmsResult> {

    private final OmsResultSchemaBuilder schemaBuilder = new OmsResultSchemaBuilder();

    private final PeptideReader peptideReader = new PeptideReader();
    private final UUIDReader uuidReader = new UUIDReader();
    private final HitType[] hitTypes = HitType.values();

    @Override
    public Class getObjectClass() {

        return OmsResult.class;
    }

    @Override
    public OmsResult read(Decoder in) throws IOException {

        final HitType hitType = hitTypes[in.readEnum()];
        final Peptide peptide = peptideReader.read(in);
        final UUID libSpectrumId = uuidReader.read(in);
        final String proteinAccessionNumbers = in.readString();
        final int libMemberCount = in.readInt();
        final double massShift = in.readDouble();
        final double metaScore = in.readDouble();

        final double wndp = in.readDouble();
        final double ionCurrentCoverage = in.readDouble();
        final int position = in.readInt();

        TDoubleList posScores = new TDoubleArrayList();
        for(long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
            for (long j = 0; j < i; j++) {

                posScores.add(in.readDouble());
            }
        }

        return new OmsResult(hitType, peptide, libSpectrumId, proteinAccessionNumbers, libMemberCount, massShift, metaScore, wndp, ionCurrentCoverage, position, posScores.toArray());
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
