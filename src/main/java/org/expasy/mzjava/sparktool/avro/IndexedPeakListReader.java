package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.io.Decoder;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;

import java.io.IOException;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedPeakListReader {

    private final IndexedPeaksReader[] peakListReaders = new IndexedPeaksReader[]{
            new DoublePeaksReader(),
            new DoubleFloatPeaksReader(),
            new FloatPeaksReader()
    };

    public void read(IndexedSpectrum.Builder builder, Decoder in) throws IOException {

        peakListReaders[in.readIndex()].readPeakList(in, builder);
    }

    private abstract static class IndexedPeaksReader {

        public void readPeakList(Decoder in, IndexedSpectrum.Builder builder) throws IOException {

            for (long i = in.readArrayStart(); i != 0; i = in.arrayNext()) {
                for (long j = 0; j < i; j++) {

                    builder.addPeak(readMz(in), readIntensity(in), in.readInt());
                }
            }
        }

        protected abstract double readMz(Decoder in) throws IOException;

        protected abstract double readIntensity(Decoder in) throws IOException;
    }

    private static class DoublePeaksReader extends IndexedPeaksReader {

        @Override
        protected double readMz(Decoder in) throws IOException {

            return in.readDouble();
        }

        @Override
        protected double readIntensity(Decoder in) throws IOException {

            return in.readDouble();
        }
    }

    private static class DoubleFloatPeaksReader extends IndexedPeaksReader {

        @Override
        protected double readMz(Decoder in) throws IOException {

            return in.readDouble();
        }

        @Override
        protected double readIntensity(Decoder in) throws IOException {

            return in.readFloat();
        }
    }

    private static class FloatPeaksReader extends IndexedPeaksReader {

        @Override
        protected double readMz(Decoder in) throws IOException {

            return in.readFloat();
        }

        @Override
        protected double readIntensity(Decoder in) throws IOException {

            return in.readFloat();
        }
    }
}
