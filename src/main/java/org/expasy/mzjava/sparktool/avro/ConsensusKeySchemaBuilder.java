package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.sparktool.data.ConsensusKey;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ConsensusKeySchemaBuilder extends ObjectSchemaBuilder {

    private final PeptideWriter peptideWriter = new PeptideWriter();

    @Override
    public Class getObjectClass() {

        return ConsensusKey.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("charge", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("spectrum", peptideWriter.createSchema()));
    }
}
