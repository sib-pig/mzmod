package org.expasy.mzjava.sparktool.avro;

import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroIO;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class ObjectSchemaBuilder extends AvroIO<AbstractAvroReader> {

    @Override
    protected String getRecordName() {

        return getObjectClass().getSimpleName();
    }

    @Override
    protected String getRecordNameSpace() {

        return getObjectClass().getPackage().getName().replace(".mzjava.", ".mzjava_avro.");
    }

    public abstract Class getObjectClass();
}
