package org.expasy.mzjava.sparktool.avro;

import com.google.common.collect.Lists;
import org.apache.avro.Schema;
import org.expasy.mzjava.avro.io.PeakWriter;
import org.expasy.mzjava.avro.io.UUIDWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedSpectrumSchemaBuilder extends ObjectSchemaBuilder {

    private final PeakWriter peakWriter = new PeakWriter();
    private final UUIDWriter uuidWriter = new UUIDWriter();

    @Override
    public Class getObjectClass() {

        return IndexedSpectrum.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("precursor", peakWriter.createSchema()));
        fields.add(createSchemaField("id", uuidWriter.createSchema()));
        fields.add(createSchemaField("precision", createEnumSchema(PeakList.Precision.class, PeakList.Precision.values())));

        fields.add(createSchemaField("peakList", Schema.createUnion(Lists.newArrayList(
                createPeakListSchema("Double", Schema.Type.DOUBLE, Schema.Type.DOUBLE),
                createPeakListSchema("DoubleFloat", Schema.Type.DOUBLE, Schema.Type.FLOAT),
                createPeakListSchema("Float", Schema.Type.FLOAT, Schema.Type.FLOAT)
        ))));
    }

    private Schema createPeakListSchema(String name, Schema.Type mzPrecision, Schema.Type intensityPrecision) {

        Schema peakListSchema = Schema.createRecord(name + "IndexedPeakList", "", getRecordNameSpace() + ".peaklist", false);
        peakListSchema.setFields(Lists.newArrayList(createSchemaField("peakList", Schema.createArray(createPeakSchema(name, mzPrecision, intensityPrecision)))));

        return peakListSchema;
    }

    private Schema createPeakSchema(String name, Schema.Type mzPrecision, Schema.Type intensityPrecision) {

        Schema recordDDPeak = Schema.createRecord(name + "Peak", "", getRecordNameSpace() + ".peaklist", false);
        recordDDPeak.setFields(Lists.newArrayList(
                createSchemaField("mz", Schema.create(mzPrecision)),
                createSchemaField("intensity", Schema.create(intensityPrecision)),
                createSchemaField("charge", Schema.create(Schema.Type.INT))
        ));
        return recordDDPeak;
    }
}
