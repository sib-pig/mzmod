package org.expasy.mzjava.sparktool.avro;

import gnu.trove.map.TObjectDoubleMap;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.proteomics.avro.io.PeptideReader;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class PeptideMatchReader extends AbstractAvroReader<PeptideMatch> {

    private final PeptideMatchSchemaBuilder schemaBuilder = new PeptideMatchSchemaBuilder();
    private final PeptideReader peptideReader = new PeptideReader();

    @Override
    public Class<PeptideMatch> getObjectClass() {

        return PeptideMatch.class;
    }

    @Override
    public PeptideMatch read(Decoder decoder) throws IOException {

        Peptide peptide = peptideReader.read(decoder);
        Set<String> proteinAcc = readStringArray(new HashSet<>(), decoder);
        TObjectDoubleMap<String> scoreMap = readMap(input -> input, decoder);
        return new PeptideMatch(peptide, proteinAcc.stream(), scoreMap, decoder.readInt(),decoder.readBoolean());
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
