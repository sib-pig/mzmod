package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.avro.io.PeakWriter;
import org.expasy.mzjava.avro.io.UUIDWriter;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class IndexedPeptideSpectrumWriter extends AbstractAvroWriter<IndexedPeptideSpectrum> {

    private final IndexedPeptideSpectrumSchemaBuilder schemaBuilder = new IndexedPeptideSpectrumSchemaBuilder();
    private final PeptideWriter peptideWriter = new PeptideWriter();
    private final PeakWriter peakWriter = new PeakWriter();
    private final UUIDWriter uuidWriter = new UUIDWriter();
    private final IndexedPeakListWriter peakListWriter = new IndexedPeakListWriter();

    @Override
    public Class getObjectClass() {

        return schemaBuilder.getObjectClass();
    }

    @Override
    public void write(IndexedPeptideSpectrum value, Encoder out) throws IOException {

        peptideWriter.write(value.getPeptide(), out);
        out.writeInt(value.getMemberCount());
        writeStringArray(value.getProteinAccessionNumbers(), out);
        peakWriter.write(value.getPrecursor(), out);
        uuidWriter.write(value.getId(), out);
        out.writeEnum(value.getPrecision().ordinal());
        peakListWriter.write(value, out);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
