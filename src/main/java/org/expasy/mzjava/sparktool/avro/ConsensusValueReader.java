package org.expasy.mzjava.sparktool.avro;

import gnu.trove.map.TObjectDoubleMap;
import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.MsnSpectrumReader;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class ConsensusValueReader extends AbstractAvroReader<ConsensusValue> {

    private final ConsensusValueSchemaBuilder schemaBuilder = new ConsensusValueSchemaBuilder();
    private final MsnSpectrumReader spectrumReader = new MsnSpectrumReader();

    @Override
    public Class<ConsensusValue> getObjectClass() {

        return ConsensusValue.class;
    }

    @Override
    public ConsensusValue read(Decoder decoder) throws IOException {

        Set<String> proteinAcc = readStringArray(new HashSet<>(), decoder);
        TObjectDoubleMap<String> scoreMap = readMap(input -> input, decoder);
        MsnSpectrum spectrum = spectrumReader.read(decoder);
        return new ConsensusValue(proteinAcc, scoreMap, spectrum);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
