package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class StringKeyReader extends AbstractAvroReader<String> {

    @Override
    public Class<String> getObjectClass() {

        return String.class;
    }

    @Override
    public String read(Decoder decoder) throws IOException {

        return decoder.readString();
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("key", Schema.create(Schema.Type.STRING)));
    }
}
