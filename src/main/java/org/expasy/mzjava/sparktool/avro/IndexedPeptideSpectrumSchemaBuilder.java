package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedPeptideSpectrumSchemaBuilder extends IndexedSpectrumSchemaBuilder {

    private final PeptideWriter peptideWriter = new PeptideWriter();

    @Override
    public Class getObjectClass() {

        return IndexedPeptideSpectrum.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("peptide", peptideWriter.createSchema()));
        fields.add(createSchemaField("memberCount", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("proteinAccessionNumbers", Schema.createArray(Schema.create(Schema.Type.STRING))));
        super.createRecordFields(fields);
    }
}
