package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.MsnSpectrumReader;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class OmsQueryReader extends AbstractAvroReader<OmsQuery> {

    private final OmsQuerySchemaBuilder schemaBuilder = new OmsQuerySchemaBuilder();

    private final MsnSpectrumReader spectrumReader = new MsnSpectrumReader();
    private final OmsResultReader omsResultReader = new OmsResultReader();

    @Override
    public Class getObjectClass() {

        return OmsQuery.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }

    @Override
    public OmsQuery read(Decoder in) throws IOException {

        MsnSpectrum spectrum = spectrumReader.read(in);
        List<OmsResult> omsResults = readArray(omsResultReader, new ArrayList<>(), in);

        return new OmsQuery(spectrum, omsResults);
    }
}
