package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class StringKeyWriter extends AbstractAvroWriter<String> {

    @Override
    public Class<String> getObjectClass() {

        return String.class;
    }

    @Override
    public void write(String string, Encoder encoder) throws IOException {

        encoder.writeString(string);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("key", Schema.create(Schema.Type.STRING)));
    }
}
