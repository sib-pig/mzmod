package org.expasy.mzjava.sparktool.avro;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.avro.io.MsnSpectrumWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class ConsensusValueWriter extends AbstractAvroWriter<ConsensusValue> {

    private final ConsensusValueSchemaBuilder schemaBuilder = new ConsensusValueSchemaBuilder();
    private final MsnSpectrumWriter spectrumWriter = new MsnSpectrumWriter(Optional.<PeakList.Precision>absent());

    @Override
    public Class getObjectClass() {

        return schemaBuilder.getObjectClass();
    }

    @Override
    public void write(ConsensusValue consensusValue, Encoder encoder) throws IOException {

        writeStringArray(consensusValue.getProteinAccSet(), encoder);
        writeMap(consensusValue.getScoreMap(), score -> score, encoder);
        spectrumWriter.write(consensusValue.getSpectrum(), encoder);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
