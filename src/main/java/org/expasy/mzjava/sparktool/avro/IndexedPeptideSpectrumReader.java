package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.PeakReader;
import org.expasy.mzjava.avro.io.UUIDReader;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.avro.io.PeptideReader;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class IndexedPeptideSpectrumReader extends AbstractAvroReader<IndexedPeptideSpectrum> {

    private final IndexedPeptideSpectrumSchemaBuilder schemaBuilder = new IndexedPeptideSpectrumSchemaBuilder();

    private final PeptideReader peptideReader = new PeptideReader();
    private final PeakReader peakReader = new PeakReader();
    private final UUIDReader uuidReader = new UUIDReader();
    private final IndexedPeakListReader peakListReader = new IndexedPeakListReader();
    private final PeakList.Precision[] precisionValues = PeakList.Precision.values();

    private final IndexedSpectrum.Builder builder = new IndexedSpectrum.Builder();

    @Override
    public Class getObjectClass() {

        return IndexedPeptideSpectrum.class;
    }

    @Override
    public IndexedPeptideSpectrum read(Decoder in) throws IOException {

        Peptide peptide = peptideReader.read(in);
        int memberCount = in.readInt();
        Set<String> accessionNumbers = readStringArray(new HashSet<>(), in);
        Peak precursor = peakReader.read(in);
        UUID id = uuidReader.read(in);
        PeakList.Precision precision = precisionValues[in.readEnum()];

        peakListReader.read(builder, in);

        return new IndexedPeptideSpectrum(peptide, accessionNumbers, memberCount, builder, precursor, id, precision);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
