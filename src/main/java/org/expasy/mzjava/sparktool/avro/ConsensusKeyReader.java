package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.proteomics.avro.io.PeptideReader;
import org.expasy.mzjava.sparktool.data.ConsensusKey;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class ConsensusKeyReader extends AbstractAvroReader<ConsensusKey> {

    private final ConsensusKeySchemaBuilder schemaBuilder = new ConsensusKeySchemaBuilder();
    private final PeptideReader peptideReader = new PeptideReader();

    @Override
    public Class<ConsensusKey> getObjectClass() {

        return ConsensusKey.class;
    }

    @Override
    public ConsensusKey read(Decoder decoder) throws IOException {

        return new ConsensusKey(decoder.readInt(), peptideReader.read(decoder));
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
