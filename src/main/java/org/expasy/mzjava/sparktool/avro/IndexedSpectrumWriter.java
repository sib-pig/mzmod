package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.avro.io.PeakWriter;
import org.expasy.mzjava.avro.io.UUIDWriter;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class IndexedSpectrumWriter extends AbstractAvroWriter<IndexedSpectrum> {

    private final IndexedSpectrumSchemaBuilder schemaBuilder = new IndexedSpectrumSchemaBuilder();

    private final PeakWriter peakWriter = new PeakWriter();
    private final UUIDWriter uuidWriter = new UUIDWriter();
    private final IndexedPeakListWriter peakListWriter = new IndexedPeakListWriter();

    @Override
    public Class getObjectClass() {

        return schemaBuilder.getObjectClass();
    }

    @Override
    public void write(IndexedSpectrum value, Encoder out) throws IOException {

        peakWriter.write(value.getPrecursor(), out);
        uuidWriter.write(value.getId(), out);
        out.writeEnum(value.getPrecision().ordinal());
        peakListWriter.write(value, out);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
