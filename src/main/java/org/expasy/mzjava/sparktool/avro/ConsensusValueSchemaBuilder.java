package org.expasy.mzjava.sparktool.avro;

import com.google.common.base.Optional;
import org.apache.avro.Schema;
import org.expasy.mzjava.avro.io.MsnSpectrumWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.sparktool.data.ConsensusValue;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ConsensusValueSchemaBuilder extends ObjectSchemaBuilder {

    private final MsnSpectrumWriter spectrumWriter = new MsnSpectrumWriter(Optional.<PeakList.Precision>absent());

    @Override
    public Class getObjectClass() {

        return ConsensusValue.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("proteinAcc", Schema.createArray(Schema.create(Schema.Type.STRING))));
        fields.add(createSchemaField("scores", Schema.createMap(Schema.create(Schema.Type.DOUBLE))));
        fields.add(createSchemaField("spectrum", spectrumWriter.createSchema()));
    }
}
