package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.expasy.mzjava.avro.io.UUIDWriter;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.OmsResult;

import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class OmsResultSchemaBuilder  extends ObjectSchemaBuilder {

    private final PeptideWriter peptideWriter = new PeptideWriter();
    private final UUIDWriter uuidWriter = new UUIDWriter();

    @Override
    public Class getObjectClass() {

        return OmsResult.class;
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        fields.add(createSchemaField("hitType", createEnumSchema(HitType.class, HitType.values())));
        fields.add(createSchemaField("peptide", peptideWriter.createSchema()));
        fields.add(createSchemaField("libSpectrumId", uuidWriter.createSchema()));
        fields.add(createSchemaField("proteinAccessionNumbers", Schema.create(Schema.Type.STRING)));
        fields.add(createSchemaField("libMemberCount", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("massShift", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("metaScore", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("wndp", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("ionCurrentCoverage", Schema.create(Schema.Type.DOUBLE)));
        fields.add(createSchemaField("position", Schema.create(Schema.Type.INT)));
        fields.add(createSchemaField("residuePosScores", Schema.createArray(Schema.create(Schema.Type.DOUBLE))));
    }
}
