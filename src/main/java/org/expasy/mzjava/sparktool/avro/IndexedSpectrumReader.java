package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Decoder;
import org.expasy.mzjava.avro.io.AbstractAvroReader;
import org.expasy.mzjava.avro.io.AvroReader;
import org.expasy.mzjava.avro.io.PeakReader;
import org.expasy.mzjava.avro.io.UUIDReader;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroReader.class)
public class IndexedSpectrumReader extends AbstractAvroReader<IndexedSpectrum> {

    private final IndexedSpectrumSchemaBuilder schemaBuilder = new IndexedSpectrumSchemaBuilder();

    private final PeakReader peakReader = new PeakReader();
    private final UUIDReader uuidReader = new UUIDReader();
    private final IndexedPeakListReader peakListReader = new IndexedPeakListReader();
    private final PeakList.Precision[] precisionValues = PeakList.Precision.values();

    private final IndexedSpectrum.Builder builder = new IndexedSpectrum.Builder();


    @Override
    public Class getObjectClass() {

        return schemaBuilder.getObjectClass();
    }

    @Override
    public IndexedSpectrum read(Decoder in) throws IOException {

        Peak precursor = peakReader.read(in);
        UUID id = uuidReader.read(in);
        PeakList.Precision precision = precisionValues[in.readEnum()];

        peakListReader.read(builder, in);

        return builder.build(precursor, id, precision);
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
