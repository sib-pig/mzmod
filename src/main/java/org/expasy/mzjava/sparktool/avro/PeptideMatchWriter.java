package org.expasy.mzjava.sparktool.avro;

import org.apache.avro.Schema;
import org.apache.avro.io.Encoder;
import org.expasy.mzjava.avro.io.AbstractAvroWriter;
import org.expasy.mzjava.avro.io.AvroWriter;
import org.expasy.mzjava.proteomics.avro.io.PeptideWriter;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.openide.util.lookup.ServiceProvider;

import java.io.IOException;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
@ServiceProvider(service=AvroWriter.class)
public class PeptideMatchWriter extends AbstractAvroWriter<PeptideMatch> {

    private final PeptideMatchSchemaBuilder schemaBuilder = new PeptideMatchSchemaBuilder();
    private final PeptideWriter peptideWriter = new PeptideWriter();

    @Override
    public Class<PeptideMatch> getObjectClass() {

        return PeptideMatch.class;
    }

    @Override
    public void write(PeptideMatch peptideMatch, Encoder encoder) throws IOException {

        peptideWriter.write(peptideMatch.getPeptide(), encoder);
        writeStringArray(peptideMatch.getProteinAcc(), encoder);
        writeMap(peptideMatch.getScoreMap(), score -> score, encoder);
        encoder.writeInt(peptideMatch.getCharge());
        encoder.writeBoolean(peptideMatch.isDecoy());
    }

    @Override
    protected void createRecordFields(List<Schema.Field> fields) {

        schemaBuilder.createRecordFields(fields);
    }
}
