package org.expasy.mzjava.sparktool.spectraconverter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.util.RegExpFileFilter;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import org.expasy.mzjava.sparktool.util.SparkUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkState;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ConvertMsn2HdMsn extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(ConvertMsn2HdMsn.class.getName());

    protected String sparkDir;
    private String spectraRootDirName;
    private boolean doSort;
    private Pattern regex;
    private boolean forceDelete;

    public ConvertMsn2HdMsn(String spectraRootDirName, Pattern regex, String sparkDir, boolean doSort, boolean forceDelete, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec, execMemoryMB, buffMemoryMB, runLocal);

        this.spectraRootDirName = spectraRootDirName;
        this.doSort = doSort;
        this.regex = regex;
        this.sparkDir = sparkDir;
        this.forceDelete = forceDelete;

        createOptions();
    }

    public ConvertMsn2HdMsn() {

        createOptions();
    }


    public static void main(String[] args) throws URISyntaxException, IOException {

        ConvertMsn2HdMsn convertMsn2HdMsn =  new ConvertMsn2HdMsn();
        try {
            convertMsn2HdMsn.parseOptions(args).run();
        } catch (MissingOptionException e) {
            convertMsn2HdMsn.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            convertMsn2HdMsn.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int run() throws IOException {

        SparkConf conf = initSpark();

        JavaSparkContext sc = new JavaSparkContext(conf);

        String hdFileName = sparkDir +File.separator+"spectra";

        Accumulator<Integer> spectrumCounter = sc.accumulator(0);
        final Msn2HdmsnParametersImpl parameters = new Msn2HdmsnParametersImpl();

        List<File> msPathList = Arrays.asList(new File(spectraRootDirName).listFiles(new RegExpFileFilter(regex)));
        checkState(!msPathList.isEmpty());

        JavaRDD<MsnSpectrum> spectra = sc.parallelize(msPathList, msPathList.size())
                .flatMap(new MsnConverterFunction(spectrumCounter, parameters));

        if (doSort)
            SparkUtils.saveSortMsnSpectra(sc, hdFileName, spectra);
        else
            SparkUtils.saveMsnSpectra(sc, hdFileName,spectra);

        LOGGER.info("Converted " + spectrumCounter.value() + " spectra");

        sc.stop();
        return 0;
    }

    public String getSpectraRootDirName() {
        return spectraRootDirName;
    }

    public boolean isDoSort() {
        return doSort;
    }

    public Pattern getRegex() {
        return regex;
    }

    public String getSparkDir() {
        return sparkDir;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("specDir").withDescription("spectrum root dierctory (required)").hasArg().withArgName("path").isRequired().create("sd"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("specSort").withDescription("sort spectra in hadoop file").create("ss"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("specRegexp").withDescription("regular expression of spectrum files (e.g. \\.mgf$)").hasArg().withArgName("string").create("rs"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdSpecDir").withDescription("spark output directory (required)").isRequired().hasArg().withArgName("path").create("hs"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("forceDelete").withDescription("force deleting existing spark output directory").create("fd"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.spectraRootDirName = checkExistDirOption(line, "sd");
        this.regex = checkRegExOption(line, "rs");
        this.doSort = line.hasOption("ss")?true:false;

        this.forceDelete = false;
        if( line.hasOption( "fd" ) ) {
            this.forceDelete = true;
        }
        this.sparkDir = checkSparkDirOption(line, "hs",forceDelete);
    }


    private static class MsnConverterFunction implements FlatMapFunction<File,MsnSpectrum> {

        private final Accumulator<Integer> spectrumCounter;
        private final Msn2HdmsnParametersImpl parameters;

        public MsnConverterFunction(Accumulator<Integer> spectrumCounter, Msn2HdmsnParametersImpl parameters) {

            this.spectrumCounter = spectrumCounter;
            this.parameters = parameters;
        }

        @Override
        public Iterator<MsnSpectrum> call(File msFile) throws Exception {

            int spectrumCount = 0;

            final Predicate<MsnSpectrum> spectrumPredicate = parameters.spectrumPredicate().get();
            final PeakProcessorChain<PeakAnnotation> peakProcessorChain = parameters.processorChain().get();

            LOGGER.info("Converting " + msFile.getAbsolutePath() + " file ");
            final IterativeReader<MsnSpectrum> msReader = parameters.msnReaderFactory().get().newMsnReader(msFile, PeakList.Precision.FLOAT);


            List<MsnSpectrum> spectra = new ArrayList<>();
            int cnt = 0;
            while (msReader.hasNext()) {

                try {
                    MsnSpectrum spectrum = msReader.next();
                    cnt++;
                    if (!spectrumPredicate.test(spectrum)) continue;
                    if (spectrum.getComment().isEmpty()) {
                        int scan = cnt;
                        if (spectrum.getScanNumbers().size()>0)
                            scan = spectrum.getScanNumbers().getFirst().getValue();
                        String name = msFile.getName();
                        int idx = name.indexOf(".");
                        if (idx>=0)
                            name = name.substring(0,idx);
                        spectrum.setComment(name+"."+scan+"."+scan+"."+spectrum.getPrecursor().getCharge());
                    }

                    spectrumCount += 1;
                    spectra.add(spectrum.copy(peakProcessorChain));
                } catch (Exception e) {
                    LOGGER.warning(e.getMessage());
                }
            }

            msReader.close();

            spectrumCounter.add(spectrumCount);

            return spectra.iterator();
        }
    }

}
