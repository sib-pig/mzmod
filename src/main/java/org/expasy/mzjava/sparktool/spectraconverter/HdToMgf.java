package org.expasy.mzjava.sparktool.spectraconverter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.core.io.ms.spectrum.MgfWriter;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.sparktool.mstranslator.NormalizeRT;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import java.io.File;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class HdToMgf extends SparkRunner {
    private static final Logger LOGGER = Logger.getLogger(NormalizeRT.class.getName());

    private String hdSpectraPath;
    private String reportDir;

    public HdToMgf(String hdSpectraPath, String reportDir, boolean doSort, String sparkDir, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec, execMemoryMB, buffMemoryMB, runLocal);

        this.hdSpectraPath = hdSpectraPath;
        this.reportDir = reportDir;

        createOptions();
    }

    public HdToMgf() {

        createOptions();
    }

    public static void main(String[] args) {

        HdToMgf converter = new HdToMgf();
        try {
            converter.parseOptions(args).run();
        } catch (MissingOptionException e) {
            converter.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            converter.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int run() throws IOException {
        long start = System.currentTimeMillis();

        SparkConf conf = initSpark();

        final JavaSparkContext sc = new JavaSparkContext(conf);

        final List<MsnSpectrum> spectra = sc.sequenceFile(hdSpectraPath, SpectrumKey.class, MsnSpectrumValue.class)
                .map(pair -> pair._2().get())
                .collect();

        String mgfFile = reportDir + File.separator + "spectra.mgf";

        MgfWriter writer = new MgfWriter(new File(mgfFile), PeakList.Precision.DOUBLE);

        spectra.forEach(sp -> {
            try {
                writer.write(sp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        writer.close();

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts == null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdmsn").withDescription("spectrum sequence file (required)").hasArg().withArgName("path").isRequired().create("hs"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("reportDir").withDescription("directory for report files (required)").hasArg().withArgName("path").isRequired().create("or"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);


        this.hdSpectraPath = checkExistFileOption(line, "hs");
        this.reportDir = checkOutputDirOption(line,"or");

    }

}
