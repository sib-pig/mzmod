package org.expasy.mzjava.sparktool.spectraconverter;

import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.util.MsnReaderFactory;

import java.io.Serializable;
import java.util.function.Predicate;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface Msn2HdmsnParameters extends Serializable {

    Cached<Predicate<MsnSpectrum>> spectrumPredicate();

    Cached<PeakProcessorChain<PeakAnnotation>> processorChain();

    Cached<MsnReaderFactory> msnReaderFactory();
}
