package org.expasy.mzjava.sparktool.spectraconverter;

import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.NPeaksPerSlidingWindowFilter;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.util.MsnReaderFactory;
import org.expasy.mzjava.sparktool.util.MsnReaderFactoryImpl;

import java.util.function.Predicate;


/**
 * @author Markus Muller
 * @version 0.0
 */
public class Msn2HdmsnParametersImpl implements Msn2HdmsnParameters {

    // MsnSpectrum predicate. A implementation of Predicate<MsnSpectrum> has to be provided, to select the spectra that are written to the
    // hadoop file.
    private static final int minPeakCount = 20;
    private static final Predicate<MsnSpectrum> msnSpectrumPredicate = spectrum -> spectrum.size() >= minPeakCount;

    // MsnReaderFactory determines, which reader should be used for a spectrum file with a given extension. The default MzJava readers are
    // selected by MsnReaderFactoryImpl. In case these readers do not suffice, the user needs to implement his/hers own reader and configure
    // the MsnReaderFactory correspondingly.
    private static final MsnReaderFactory msnReaderFactory = new MsnReaderFactoryImpl();

    // Here successive processing steps of the spectra prior to conversion to a hadoop file can be defined. No processing is done if no
    // PeakProcessors are added.
    private static final PeakProcessorChain<PeakAnnotation> msnProcessorChain = new PeakProcessorChain<>()
            .add(new NPeaksPerSlidingWindowFilter<>(2, 10));



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Implementation of getters (no need to change this part)
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public Cached<Predicate<MsnSpectrum>> spectrumPredicate() {

        return new Cached<Predicate<MsnSpectrum>>() {
            @Override
            protected Predicate<MsnSpectrum> build() {

                return msnSpectrumPredicate;
            }
        };
    }

    public Cached<PeakProcessorChain<PeakAnnotation>> processorChain() {

        return new Cached<PeakProcessorChain<PeakAnnotation>>() {
            @Override
            protected PeakProcessorChain<PeakAnnotation> build() {

                return msnProcessorChain;
            }
        };
    }

    public Cached<MsnReaderFactory> msnReaderFactory() {

        return new Cached<MsnReaderFactory>() {
            @Override
            protected MsnReaderFactory build() {

                return msnReaderFactory;
            }
        };
    }

}
