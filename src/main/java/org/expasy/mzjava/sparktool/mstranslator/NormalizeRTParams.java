package org.expasy.mzjava.sparktool.mstranslator;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import scala.Tuple2;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface NormalizeRTParams  extends Serializable {

    boolean normalizeRT();

    Map<String,Double> standardPeptidesRTMap();

    Cached<SpectrumKeyFunction<MsnSpectrum>> keyFunction();

    Cached<Function<Tuple2<String,PeptideMatch>,Boolean>> peptideMatchPredicate();

    Cached<? extends RTTransform> rtTransformer();

}
