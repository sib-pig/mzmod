package org.expasy.mzjava.sparktool.mstranslator;

import org.expasy.mzjava.core.ms.peaklist.AbstractPeakProcessor;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;

import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class TransitionSelector extends AbstractPeakProcessor<PepLibPeakAnnotation, PepLibPeakAnnotation> {

    protected final Comparator<PepLibPeak> comparator;
    protected final SortedSet<PepLibPeak> selectedTrans;
    protected final int nrTransitions;

    public TransitionSelector(Comparator<PepLibPeak> comparator, int nrTransitions) {
        this.comparator = comparator;
        this.selectedTrans = new TreeSet<PepLibPeak>(comparator);
        this.nrTransitions = nrTransitions;
    }

    @Override
    public void start(int size) {

        selectedTrans.clear();

        sink.start(nrTransitions);
    }

    @Override
    public void processPeak(double mz, double intensity, List<PepLibPeakAnnotation> annotations) {

        PepLibPeak peak = new PepLibPeak(mz,intensity,annotations);
        if (selectedTrans.size()<nrTransitions)
            selectedTrans.add(peak);
        else {

            if (comparator.compare(selectedTrans.first(),peak)<0) {
                selectedTrans.remove(selectedTrans.first());
                selectedTrans.add(peak);
            }

        }

    }

    @Override
    public void end() {

        selectedTrans.forEach(peak -> sink.processPeak(peak.getMz(), peak.getIntens(), peak.getPeakAnnotations()));

        sink.end();
    }
}
