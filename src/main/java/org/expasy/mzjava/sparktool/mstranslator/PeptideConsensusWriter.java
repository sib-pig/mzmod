package org.expasy.mzjava.sparktool.mstranslator;

import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * @author Markus Muller
 * @version 0.0
 */
public abstract class PeptideConsensusWriter {

    protected Map<SpectrumKey,Iterable<PeptideConsensusSpectrum>> transitions;
    protected BufferedWriter writer;

    public void write(Map<SpectrumKey, Iterable<PeptideConsensusSpectrum>> transitions,String outputFile) throws IOException{

        this.transitions = transitions;
        this.writer = new BufferedWriter(new FileWriter(outputFile));

        writeHeader();
        writeTransitions();
        writeTail();

        writer.close();
    }

    protected void writeTransitions()  throws IOException {

        for (SpectrumKey key : transitions.keySet()) {
            transitions.get(key).forEach(t -> writeTransition(key,t));
        }
    }


    protected abstract void writeTransition(SpectrumKey spectrumKey, PeptideConsensusSpectrum consensus);
    protected abstract void writeHeader()  throws IOException;
    protected abstract void writeTail()  throws IOException;
}
