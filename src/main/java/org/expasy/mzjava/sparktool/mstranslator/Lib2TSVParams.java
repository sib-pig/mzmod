package org.expasy.mzjava.sparktool.mstranslator;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import scala.Tuple2;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface Lib2TSVParams extends Serializable {
    Cached<? extends Comparator<PepLibPeak>> peakComparator();

    int nrTransition();

    Cached<? extends PeptideConsensusWriter> consensusWriter();

    Cached<? extends Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean>> librarySpectrumFilter();

    Cached<? extends RTTransform> rtTransformer();

    Cached<SpectrumKeyFunction<MsnSpectrum>> keyFunction();

}
