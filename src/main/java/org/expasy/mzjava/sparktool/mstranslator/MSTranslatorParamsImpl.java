package org.expasy.mzjava.sparktool.mstranslator;

import com.google.common.collect.Lists;
import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.NPeaksPerSlidingWindowFilter;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.NHighestPeaksNormalizer;
import org.expasy.mzjava.core.ms.spectrasim.NdpSimFunc;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.DefaultPeakListAligner;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer.PeakPairIntensitySqrtTransformer;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.CachedPpmTolerance;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.liberator.ConsensusMemberSelector;
import org.expasy.mzjava.sparktool.liberator.MSTConsensusMemberSelector;
import org.expasy.mzjava.sparktool.liberator.MaxSimScoreClusterSelector;
import org.expasy.mzjava.sparktool.util.*;
import org.expasy.mzjava.utils.URIBuilder;
import scala.Tuple2;

import java.util.*;
import java.util.function.Predicate;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class MSTranslatorParamsImpl implements MSTranslatorParams {

    // Msn spectrum processing
    private static final int minPeakCount = 20;
    private static final Predicate<MsnSpectrum> msnSpectrumPredicate = spectrum -> spectrum.size() >= minPeakCount;
    private static final MsnReaderFactory msnReaderFactory = new MsnReaderFactoryImpl();
    private static final PeakProcessorChain<PeakAnnotation> msnProcessorChain = new PeakProcessorChain<>()
            .add(new NPeaksPerSlidingWindowFilter<>(2, 10));

    // PSM processing
    private static final List<String> modifications =
            Lists.newArrayList("Oxidation:O", "Carbamidomethyl:C2H3NO", "Phospho:HO3P", "Deamidated:H-1N-1O", "Ammonia-loss:H-3N-1", "Acetyl:C2H2O", "Methyl:CH2", "Dimethyl:C2H4", "Trimethyl:C3H6", "Amidated:H-1", "Glu->pyro-Glu:H-2O-1", "Propionyl:C3H4O", "Pyro-carbamidomethyl:C2O");
    private static final double modifMatchMassTol =  0.015;

    // This object links the msn spectrum to the psm
    private static final SpectrumKeyFunction<MsnSpectrum> spectrumKeyFunction = new XTandemMgfKeyFunction();

    private static final PsmReaderFactory psmReaderFactory = new PsmReaderFactoryImpl();


    // Liberator parameters

    private static final double minClusterNdpScore = 0.6;
    // Mass tolerance to match fragment ions
    private static final double fragmentToleranceAbsValue = 0.05;
    private static final AbsoluteTolerance fragmentToleranceAbs = new AbsoluteTolerance(fragmentToleranceAbsValue);


    // Maximum number of spectra to calculate consensus. The algorithm takes the
    // PSMs with maxConsensusMembers highest scores.
    private static final int maxConsensusMembers = 300;

    // minimal and maximal charge states included
    private static final int minCharge = 2;
    private static final int maxCharge = 4;

    // name of the score and score threshold
    private static final String scoreName = "hyperscore";
    private static final double minPsmScore = 30;

    // minumum and maximum peptide length
    private static final int minPeptideLength = 7;
    private static final int maxPeptideLength = 40;

    // precursor mass tolerance in ppm
    private static final double precursorTolerance = 10;

    private static final String decoyProtPattern = "DECOY_";
    private static final int maxRank = 1;

    // Predicate to filter PeptideMatch objects used for library creation.
    private static final Predicate<PeptideMatch> psmPredicate = new PeptideMatchPredicate(minCharge, maxCharge, minPeptideLength, maxPeptideLength,
            scoreName, minPsmScore, PeptideMatchPredicate.ScoreOrder.LARGER, maxRank);

    //Comparator to sort PSMs for consensus building
    private static final Comparator<ConsensusValue> sortPSMComparator = (v1, v2) -> Double.compare(v2.getScore(scoreName), v1.getScore(scoreName));

    // Precision for PeakList masses, float will do in most cases.
    private static final PeakList.Precision peakListPrecision =  PeakList.Precision.FLOAT;

    // Define the PeptideFragmenter, i.e. ion types, neutral losses, ect to annotate library peaks.
    private static final  PeptideFragmenter peptideFragmenter = new PeptideFragmenter(EnumSet.of(IonType.a, IonType.b, IonType.y), PeakList.Precision.FLOAT_CONSTANT);


    // Consensus spectrum builder class

    // only peak which appear in at least minNumberSpectraWithPeak spectra matching the peptide are included in consensus
    private static final int minNumberSpectraWithPeak = 2;
    // only peak which appear in at least minSpectraWithPeakPercentage % of the spectra matching the peptide  are included in consensus
    // the maximum of {minNumberSpectraWithPeak, minSpectraWithPeakPercentage*#spectra} is taken as a threshold
    private static final double minSpectraWithPeakPercentage = 0.2;

    private static final PeptideConsensusSpectrum.Builder consensusBuilder =
            PeptideConsensusSpectrum.builder(PeakList.Precision.FLOAT, new URIBuilder("org.expasy", "liberator").build())
                        .setConsensusParameters(fragmentToleranceAbsValue, fragmentToleranceAbsValue, AbstractMergePeakFilter.IntensityMode.SUM_INTENSITY)
                        .setAnnotationParameters(fragmentToleranceAbs, peptideFragmenter)
                        .setFilterParameters(minSpectraWithPeakPercentage, minNumberSpectraWithPeak);


    // object that defines how consensus members are selected
    private static final ConsensusMemberSelector consensusMemberSelector =
            new MSTConsensusMemberSelector(
                        new MaxSimScoreClusterSelector(),
                        new NdpSimFunc<>(0, new DefaultPeakListAligner<>(fragmentToleranceAbs), new PeakPairIntensitySqrtTransformer<>()),minClusterNdpScore);


    private static final PeakProcessorChain consensusProcessorChain = new PeakProcessorChain<>()
            .add(new NHighestPeaksNormalizer(1,10000.0));


    // Normalize retention time

    private static final boolean normalizeRT = true;

    private static final Map<String, Double> peptideRTMap = new HashMap<>();
    private static void setPeptideRTMap() {
        peptideRTMap.put("TPVSDRVTK",938.48982);
        peptideRTMap.put("TC(Carbamidomethyl)VADESAENC(Carbamidomethyl)DK",1013.03076);
        peptideRTMap.put("C(Carbamidomethyl)C(Carbamidomethyl)AAADPHEC(Carbamidomethyl)YAK",1070.5818);
        peptideRTMap.put("ADDKETC(Carbamidomethyl)FAEEGK",1203.3516);
        peptideRTMap.put("NEC(Carbamidomethyl)FLQHKDDNPNLPR",1853.21064);
        peptideRTMap.put("KVPQVSTPTLVEVSR",2438.22684);
        peptideRTMap.put("RHPDYSVVLLLR",2964.16494);
        peptideRTMap.put("SLHTLFGDKLC(Carbamidomethyl)TVATLR",3204.4758);
        peptideRTMap.put("VFDEFKPLVEEPQNLIK",3703.80456);
        peptideRTMap.put("HPYFYAPELLFFAK",4355.6646);
        peptideRTMap.put("YKAAFTEC(Carbamidomethyl)C(Carbamidomethyl)QAADK",1456.40922);
        peptideRTMap.put("KQTALVELVK",2020.24776);
        peptideRTMap.put("QNC(Carbamidomethyl)ELFEQLGEYK",3234.34098);
        peptideRTMap.put("EFNAETFTFHADIC(Carbamidomethyl)TLSEK",3574.79538);
        peptideRTMap.put("EFNAETFTFHADIC(Carbamidomethyl)TLSEKER",3338.0247);
        peptideRTMap.put("NYAEAKDVFLGMFLYEYAR",4665.4998);
        peptideRTMap.put("RMPC(Carbamidomethyl)AEDYLSVVLNQLC(Carbamidomethyl)VLHEK",4755.06582);
        peptideRTMap.put("ALVLIAFAQYLQQC(Carbamidomethyl)PFEDHVK",4948.64478);
    }

    private static final RTTransform rtTransformer = new RTAffineTransform();

    // transition selection and export

    // class to compare PepLibPeak objects in order to sort them according to their importance for
    // transition selection
    private static final Comparator<PepLibPeak> peakComparator = new PepLibPeakComparator();

    // number of exported transitions
    private static final int nrTransition = 6;

    private static final PeptideConsensusWriter peptideConsensusWriter = new OpenSwathPeptideConsensusWriter();

    private static final Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean> libSpectrumFilter = new LibSpectrumFilter();

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    // Msn spectrum processing
    @Override
    public Cached<Predicate<MsnSpectrum>> msnSpectrumPredicate() {

        return new Cached<Predicate<MsnSpectrum>>() {
            @Override
            protected Predicate<MsnSpectrum> build() {
                return msnSpectrumPredicate;
            }
        };
    }

    @Override
    public Cached<PeakProcessorChain<PeakAnnotation>> msnProcessorChain() {

        return new Cached<PeakProcessorChain<PeakAnnotation>>() {
            @Override
            protected PeakProcessorChain<PeakAnnotation> build() {
                return msnProcessorChain;
            }
        };
    }

    @Override
    public Cached<MsnReaderFactory> msnReaderFactory(){

        return new Cached<MsnReaderFactory>() {
            @Override
            protected MsnReaderFactory build() {
                return msnReaderFactory;
            }
        };
    }


    @Override
    public Cached<List<String>> modifications() {
        return new Cached<List<String>>() {
            @Override
            protected List<String> build() {
                return modifications;
            }
        };
    }

    @Override
    public double modifMatchMassTol() {
        return modifMatchMassTol;
    }

    @Override
    public Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumKeyFunction() {
        return new Cached<SpectrumKeyFunction<MsnSpectrum>>() {
            @Override
            protected SpectrumKeyFunction<MsnSpectrum> build() {

                return spectrumKeyFunction;
            }
        };
    }

    public Cached<PsmReaderFactory> psmReaderFactory() {

        return new Cached<PsmReaderFactory>() {
            @Override
            protected PsmReaderFactory build() {
                return psmReaderFactory;
            }
        };
    }


    // Liberator parameters

    @Override
    public Cached<Predicate<PeptideMatch>> peptideMatchPredicate() {

        return new Cached<Predicate<PeptideMatch>>() {
            @Override
            protected Predicate<PeptideMatch> build() {

                return psmPredicate;
            }
        };
    }

    @Override
    /*
    Comparator to sort PSMs for consensus building
     */
    public Cached<Comparator<ConsensusValue>> consensusValueComparator() {

        return new Cached<Comparator<ConsensusValue>>() {
            @Override
            protected Comparator<ConsensusValue> build() {

                return sortPSMComparator;
            }
        };
    }

    @Override
    public PeakList.Precision precision() {

        return peakListPrecision;
    }


    @Override
    public int maxConsensusMembers() {

        return maxConsensusMembers;
    }

    @Override
    public Cached<Tolerance> fragmentTolerance() {

        return new Cached<Tolerance>() {
            @Override
            protected Tolerance build() {
                return fragmentToleranceAbs;
            }
        };

    }

    @Override
    public Cached<PeptideFragmenter> peptideFragmenter() {

        return new Cached<PeptideFragmenter>() {
            @Override
            protected PeptideFragmenter build() {
                return peptideFragmenter;
            }

        };
    }

    @Override
    /*
    Consensus spectrum builder class
     */
    public Cached<PeptideConsensusSpectrum.Builder> consensusBuilder() {

        return new Cached<PeptideConsensusSpectrum.Builder>() {
            @Override
            protected PeptideConsensusSpectrum.Builder build() {

                return consensusBuilder;
            }
        };
    }

    @Override
    public Cached<ConsensusMemberSelector> consensusMemberSelector() {

        return new Cached<ConsensusMemberSelector>() {
            @Override
            protected ConsensusMemberSelector build() {

                return consensusMemberSelector;
            }
        };
    }

    @Override
    public Cached<PeakProcessorChain> consensusProcessorChain() {
        return null;
    }

    @Override
    public Cached<Tolerance> precursorTolerance() {

        return new CachedPpmTolerance(precursorTolerance);
    }

    // normalize retention time

    @Override
    public boolean normalizeRT() {
        return normalizeRT;
    }

    @Override
    public Cached<Map<String, Double>> standardPeptidesRTMap() {

        setPeptideRTMap();
        return new Cached<Map<String, Double>>() {
            @Override
            protected Map<String, Double> build() {
                return peptideRTMap;
            }
        };

    }

    @Override
    public Cached<RTTransform> rtTransformer(){

        return new Cached<RTTransform>() {
            @Override
            protected RTTransform build() {

                return rtTransformer;
            }
        };

    }


    @Override
    public Cached<Comparator<PepLibPeak>> peakComparator() {
        return new Cached<Comparator<PepLibPeak>>() {
            @Override
            protected Comparator<PepLibPeak> build() {

                return peakComparator;
            }
        };

    }

    @Override
    public int nrTransition() {
        return nrTransition;
    }

    @Override
    public Cached<PeptideConsensusWriter> consensusWriter() {
        return new Cached<PeptideConsensusWriter>() {
            @Override
            protected PeptideConsensusWriter build() {

                return peptideConsensusWriter;
            }
        };

    }

    @Override
    public Cached<Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean>> librarySpectrumFilter() {
        return new Cached<Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean>>() {
            @Override
            protected Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean> build() {

                return libSpectrumFilter;
            }
        };
    }

}
