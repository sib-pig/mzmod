package org.expasy.mzjava.sparktool.mstranslator;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import org.expasy.mzjava.sparktool.util.XTandemMgfKeyFunction;
import scala.Tuple2;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class NormalizeRTParamsImpl implements NormalizeRTParams {


    @Override
    public boolean normalizeRT() {
        return false;
    }

    @Override
    public Map<String, Double> standardPeptidesRTMap() {

        Map<String, Double> peptideRTMap = new HashMap<>();
        peptideRTMap.put("TPVSDRVTK",938.48982);
        peptideRTMap.put("TC(Carbamidomethyl)VADESAENC(Carbamidomethyl)DK",1013.03076);
        peptideRTMap.put("C(Carbamidomethyl)C(Carbamidomethyl)AAADPHEC(Carbamidomethyl)YAK",1070.5818);
        peptideRTMap.put("ADDKETC(Carbamidomethyl)FAEEGK",1203.3516);
        peptideRTMap.put("NEC(Carbamidomethyl)FLQHKDDNPNLPR",1853.21064);
        peptideRTMap.put("KVPQVSTPTLVEVSR",2438.22684);
        peptideRTMap.put("RHPDYSVVLLLR",2964.16494);
        peptideRTMap.put("SLHTLFGDKLC(Carbamidomethyl)TVATLR",3204.4758);
        peptideRTMap.put("VFDEFKPLVEEPQNLIK",3703.80456);
        peptideRTMap.put("HPYFYAPELLFFAK",4355.6646);
        peptideRTMap.put("YKAAFTEC(Carbamidomethyl)C(Carbamidomethyl)QAADK",1456.40922);
        peptideRTMap.put("KQTALVELVK",2020.24776);
        peptideRTMap.put("QNC(Carbamidomethyl)ELFEQLGEYK",3234.34098);
        peptideRTMap.put("EFNAETFTFHADIC(Carbamidomethyl)TLSEK",3574.79538);
        peptideRTMap.put("EFNAETFTFHADIC(Carbamidomethyl)TLSEKER",3338.0247);
        peptideRTMap.put("NYAEAKDVFLGMFLYEYAR",4665.4998);
        peptideRTMap.put("RMPC(Carbamidomethyl)AEDYLSVVLNQLC(Carbamidomethyl)VLHEK",4755.06582);
        peptideRTMap.put("ALVLIAFAQYLQQC(Carbamidomethyl)PFEDHVK",4948.64478);

        return peptideRTMap;
    }

    @Override
    /*
    SpectrumKeyFunction class is used to map the PSMs objects to MsnSpectrum objects.
     */
    public Cached<SpectrumKeyFunction<MsnSpectrum>> keyFunction() {
        return new Cached<SpectrumKeyFunction<MsnSpectrum>>() {
            @Override
            protected SpectrumKeyFunction<MsnSpectrum> build() {

//                return new MgfTitleKeyFunction();
                return new XTandemMgfKeyFunction();
            }
        };
    }

    @Override
    /*
    This function returns a predicate to filter PeptideMatch objects (see implementation of Predicate interface
    for details.
     */
    public  Cached<Function<Tuple2<String,PeptideMatch>,Boolean>> peptideMatchPredicate() {

        return new Cached<Function<Tuple2<String,PeptideMatch>,Boolean>>() {
            @Override
            protected Function<Tuple2<String,PeptideMatch>,Boolean> build() {

                return psm -> standardPeptidesRTMap().containsKey(psm._2().getPeptide().toString());
            }
        };
    }

    public Cached<? extends RTTransform> rtTransformer(){

        return new Cached<RTAffineTransform>() {
            @Override
            protected RTAffineTransform build() {

                return new RTAffineTransform();
            }
        };

    }


}
