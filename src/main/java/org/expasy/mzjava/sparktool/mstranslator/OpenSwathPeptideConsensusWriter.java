package org.expasy.mzjava.sparktool.mstranslator;

import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;

import java.io.IOException;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class OpenSwathPeptideConsensusWriter extends PeptideConsensusWriter {

    public OpenSwathPeptideConsensusWriter() {
    }

    protected void writeTransition(SpectrumKey spectrumKey, PeptideConsensusSpectrum consensus) {

        Peptide peptide = consensus.getPeptide();
        String peptideSeq = peptide.toSymbolString();
        String peptideMod = peptide.toString();
        peptideMod = peptideMod.replaceAll("_","");

        String protein = "";
        for (String ac : consensus.getProteinAccessionNumbers()) {
            if (protein.isEmpty()) protein = ac;
            else protein += ";"+ac;
        }

        double rt = consensus.getRetentionTime().isPresent()?consensus.getRetentionTime().get().getTime():0.0;

        for (int i=0;i<consensus.size();i++) {
            String annotSptxt = "";
            String annot = "";
            for (PepLibPeakAnnotation a : consensus.getAnnotations(i))  {
                if (a.getOptFragmentAnnotation().isPresent()) {

                    if (!annotSptxt.isEmpty()) annotSptxt += ";";
                    annotSptxt += a.getOptFragmentAnnotation().get().toSptxtString();

                    if (!annot.isEmpty()) annot += ";";
                    annot += a.getOptFragmentAnnotation().get().toSptxtString();
                }
            }

            String mzPrecStr = String.format("%.5f",spectrumKey.getMz());
            String mzFragStr = String.format("%.5f",consensus.getMz(i));
            String rtStr = String.format("%.2f",rt);
            String intensStr = String.format("%.3f",consensus.getIntensity(i));

            String line = mzPrecStr+"\t"+
                    mzFragStr+"\t"+
                    rtStr+"\t"+   // norm rt
                    peptide+"\\"+spectrumKey.getCharge()+"_"+annot+"\t"+  //57863_DIVEAVIPR/2_y6
                    "0.0\t"+ // CE
                    intensStr+"\t"+
                    peptide+"\\"+spectrumKey.getCharge()+"\t"+
                    "0\t"+ //is decoy
                    peptideSeq+"\t"+
                    protein+"\t"+
                    annotSptxt+"\t"+ // y6/0.01
                    peptideMod+"\t"+
                    "0\t"+ // nr mc
                    "0\t"+ // nr replicates?
                    peptide.getModificationCount()+"\t"+
                    spectrumKey.getCharge()+"\t"+
                    "light\n";
            try {
                writer.write(line);
                writer.flush();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        }

    }

    protected void writeHeader() throws IOException {
        writer.write("PrecursorMz\tProductMz\tTr_recalibrated\ttransition_name\tCE\tLibraryIntensity\ttransition_group_id\tdecoy\tPeptideSequence\tProteinName\tAnnotation\tFullPeptideName\tMissedCleavages\tReplicates\tNrModifications\tCharge\tGroupLabel\n");
        writer.flush();
    }

    protected void writeTail()  throws IOException {
    }

}
