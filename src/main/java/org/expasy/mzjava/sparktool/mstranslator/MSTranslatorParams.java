package org.expasy.mzjava.sparktool.mstranslator;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.liberator.ConsensusMemberSelector;
import org.expasy.mzjava.sparktool.util.MsnReaderFactory;
import org.expasy.mzjava.sparktool.util.PsmReaderFactory;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import scala.Tuple2;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface MSTranslatorParams extends Serializable {

    Cached<Predicate<MsnSpectrum>> msnSpectrumPredicate();

    Cached<PeakProcessorChain<PeakAnnotation>> msnProcessorChain();

    Cached<MsnReaderFactory> msnReaderFactory();

    Cached<List<String>> modifications();

    double modifMatchMassTol() ;

    Cached<PsmReaderFactory> psmReaderFactory();

    Cached<Predicate<PeptideMatch>> peptideMatchPredicate();

    Cached<Comparator<ConsensusValue>> consensusValueComparator();

    PeakList.Precision precision();

    Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumKeyFunction();

    Cached<PeakProcessorChain> consensusProcessorChain();

    Cached<PeptideConsensusSpectrum.Builder> consensusBuilder();

    int maxConsensusMembers();

    Cached<ConsensusMemberSelector> consensusMemberSelector();

    Cached<Tolerance> precursorTolerance();

    Cached<Tolerance>  fragmentTolerance();

    Cached<PeptideFragmenter> peptideFragmenter();

    boolean normalizeRT();

    Cached<Map<String,Double>> standardPeptidesRTMap();

    Cached<RTTransform> rtTransformer();

    Cached<Comparator<PepLibPeak>> peakComparator();

    int nrTransition();

    Cached<PeptideConsensusWriter> consensusWriter();

    Cached<Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean>> librarySpectrumFilter();


}
