package org.expasy.mzjava.sparktool.mstranslator;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.hadoop.io.PeptideConsensusSpectrumValue;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ExportLib2TansTSV extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(ExportLib2TansTSV.class.getName());

    private String libraryHdFile;
    private String reportDir;

    public ExportLib2TansTSV(String libraryHdFile, String reportDir, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,buffMemoryMB,runLocal);

        this.libraryHdFile = libraryHdFile;
        this.reportDir = reportDir;

        createOptions();
    }

    public ExportLib2TansTSV() {

        createOptions();
    }

    public static void main(String[] args) {

        ExportLib2TansTSV exportLib2TansTSV =  new ExportLib2TansTSV();
        try {
            exportLib2TansTSV.parseOptions(args).run();
        } catch (MissingOptionException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        } catch (IOException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int run() throws IOException {
        long start = System.currentTimeMillis();

        final SparkConf conf = initSpark();
        final JavaSparkContext sc = new JavaSparkContext(conf);

        final Lib2TSVParamsImpl parameters = new Lib2TSVParamsImpl();

        final Map<SpectrumKey, Iterable<PeptideConsensusSpectrum>> transitions = sc.sequenceFile(libraryHdFile, SpectrumKey.class, PeptideConsensusSpectrumValue.class)
                .mapToPair(pair -> new Tuple2<>(new SpectrumKey(pair._1().getMz(),pair._1().getCharge()), pair._2().get()))
                .filter(parameters.librarySpectrumFilter().get())
                .mapValues(spec -> spec.copy(new TransitionSelector(parameters.peakComparator().get(), parameters.nrTransition())))
                .groupByKey().collectAsMap();

        write2TSV(transitions,parameters.consensusWriter().get());

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdlib").withDescription("spectrum library sequence file (required)").hasArg().withArgName("path").isRequired().create("hl"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("reportDir").withDescription("directory for report files (required)").hasArg().withArgName("path").isRequired().create("or"));

    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.libraryHdFile = checkExistFileOption(line, "hl");
        this.reportDir = checkOutputDirOption(line,"or");
    }

    private void write2TSV(Map<SpectrumKey, Iterable<PeptideConsensusSpectrum>> transitions, PeptideConsensusWriter writer) throws IOException{

        String outFileName = new File(libraryHdFile).getName().replace(".hdlib", ".csv");

        String tsvFileName = reportDir +File.separator+outFileName;

        writer.write(transitions, tsvFileName);

    }

}
