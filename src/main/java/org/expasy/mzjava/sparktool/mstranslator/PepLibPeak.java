package org.expasy.mzjava.sparktool.mstranslator;

import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class PepLibPeak {

    private final double mz;
    private final double intens;
    private final List<PepLibPeakAnnotation> peakAnnotations;
    private final int peakCount;
    private final Set<IonType> ionTypes;
    private final Set<Integer> charges;
    private final double peakIntensStd;
    private final int fragSize;

    public PepLibPeak(double mz, double intens, List<PepLibPeakAnnotation> peakAnnotations) {
        this.mz = mz;
        this.intens = intens;
        this.peakAnnotations = peakAnnotations;
        this.ionTypes = new HashSet<>();
        this.charges = new HashSet<>();
        this.peakCount = peakAnnotations.isEmpty()?0:peakAnnotations.get(0).getMergedPeakCount();
        this.peakIntensStd = peakAnnotations.isEmpty()?0.0:peakAnnotations.get(0).getIntensityStd();
        this.fragSize = !peakAnnotations.isEmpty()&&peakAnnotations.get(0).getOptFragmentAnnotation().isPresent()?
                peakAnnotations.get(0).getOptFragmentAnnotation().get().getFragment().size():0;

        for (PepLibPeakAnnotation annot: peakAnnotations) {
            if (annot.getOptFragmentAnnotation().isPresent()) {

                this.ionTypes.add(annot.getOptFragmentAnnotation().get().getIonType());
                this.charges.add(annot.getOptFragmentAnnotation().get().getCharge());
            }

        }
    }

    public double getMz() {
        return mz;
    }

    public double getIntens() {
        return intens;
    }

    public List<PepLibPeakAnnotation> getPeakAnnotations() {
        return peakAnnotations;
    }

    public Set<IonType> getIonTypes() {
        return ionTypes;
    }

    public Set<Integer> getCharges(){
        return charges;
    }

    public int getPeakCount() {
        return peakCount;
    }

    public double getPeakIntensStd() {
        return peakIntensStd;
    }

    public int getFragSize() {
        return fragSize;
    }
}
