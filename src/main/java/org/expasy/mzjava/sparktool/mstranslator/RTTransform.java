package org.expasy.mzjava.sparktool.mstranslator;

import scala.Tuple3;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Markus Muller
 * @version 0.0
 */
public abstract class RTTransform implements Serializable {

    protected Map<String, List<Tuple3<String,Double, Double>>> rtData;


    public void setData(Map<String, Iterable<Tuple3<String,Double, Double>>> rtData) {
        this.rtData = new HashMap<>();
        for (String runID:rtData.keySet()) {
            this.rtData.put(runID,new ArrayList<>());
            rtData.get(runID).forEach(t -> this.rtData.get(runID).add(t));
        }

        calcRTCorrParams();
    }

    public abstract void read(String paramsFileName) throws IOException;
    public abstract void write(String paramsFileName) throws IOException;
    public abstract com.google.common.base.Function<Double,Double> getRTFunction(String runID);
    protected abstract void calcRTCorrParams();
}
