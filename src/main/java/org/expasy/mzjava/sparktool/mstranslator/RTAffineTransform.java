package org.expasy.mzjava.sparktool.mstranslator;


import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.spark.api.java.function.Function;
import scala.Tuple3;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class RTAffineTransform extends RTTransform {

    private Map<String,Function<Double,Double>> rtCorrectionFcts;

    public RTAffineTransform() {
    }

    @Override
    public com.google.common.base.Function<Double, Double> getRTFunction(String runID) {
        return  new com.google.common.base.Function<Double,Double>() {
            @Override
            public Double apply(Double rt) {
                try {
                    return rtCorrectionFcts.get(runID).call(rt);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return rt;
            }
        };
    }

    @Override
    public void read(String rtDataFileName) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(rtDataFileName));

        reader.readLine();

        rtData = new HashMap<>();

        String  line = null;
        while ((line = reader.readLine()) != null) {
            String[] fields = line.split("\\t");

            rtData.put(fields[0],new ArrayList<>());
            rtData.get(fields[0]).add(new Tuple3<>(fields[1],Double.parseDouble(fields[2]),Double.parseDouble(fields[3])));
        }

        calcRTCorrParams();
    }

    @Override
    public void write(String rtDataFileName) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(rtDataFileName));

        writer.write("RunID\tPeptide\tRT_nominal\tRT_measured\n");
        for (String runID : rtData.keySet()) {
            Iterable<Tuple3<String,Double, Double>> data = rtData.get(runID);
            data.forEach(d -> {
                try {
                    writer.write(runID + "\t" + d._1() + "\t" + d._2() + "\t" + d._3() + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        writer.close();
    }

    protected void calcRTCorrParams() {

        rtCorrectionFcts = new HashMap<>();
        for (String runId:rtData.keySet()){
            Iterable<Tuple3<String,Double,Double>> rts = rtData.get(runId);
            SimpleRegression regression = new SimpleRegression();
            rts.forEach(pair -> regression.addData(pair._2(), pair._3()));
            rtCorrectionFcts.put(runId, x -> regression.getSlope()*x + regression.getIntercept());
        }
    }

}
