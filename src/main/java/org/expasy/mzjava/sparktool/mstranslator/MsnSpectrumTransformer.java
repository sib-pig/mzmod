package org.expasy.mzjava.sparktool.mstranslator;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class MsnSpectrumTransformer implements Function<MsnSpectrum,MsnSpectrum> {

    private final RTTransform rtTransform;
    private final SpectrumKeyFunction spectrumKeyFunction;

    public MsnSpectrumTransformer(RTTransform rtTransform, SpectrumKeyFunction spectrumKeyFunction) {
        this.rtTransform = rtTransform;
        this.spectrumKeyFunction = spectrumKeyFunction;
    }

    @Override
    public MsnSpectrum call(MsnSpectrum msnSpectrum) throws Exception {
        String runID = spectrumKeyFunction.getRunID(msnSpectrum);

        return msnSpectrum.copy(rtTransform.getRTFunction(runID));
    }
}
