package org.expasy.mzjava.sparktool.mstranslator;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import org.expasy.mzjava.sparktool.util.SparkUtils;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import scala.Tuple2;
import scala.Tuple3;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author Oliver Horlacher
 * @author Markus Mueller
 * @version sqrt -1
 */
public class NormalizeRT extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(NormalizeRT.class.getName());

    private String hdSpectraPath;
    private String hdPsmPath;
    private boolean doSort;
    private String sparkDir;
    private String reportDir;
    private boolean forceDelete;

    public NormalizeRT(String hdSpectraPath, String hdPsmPath, String reportDir, String sparkDir, boolean doSort, boolean forceDelete, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec, execMemoryMB, buffMemoryMB, runLocal);

        this.hdSpectraPath = hdSpectraPath;
        this.hdPsmPath = hdPsmPath;
        this.doSort = doSort;
        this.reportDir = reportDir;
        this.sparkDir = sparkDir;
        this.forceDelete = forceDelete;

        createOptions();
    }

    public NormalizeRT() {

        createOptions();
    }

    public static void main(String[] args) {

        NormalizeRT normalizeRT = new NormalizeRT();
        try {
            normalizeRT.parseOptions(args).run();
        } catch (MissingOptionException e) {
            normalizeRT.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            normalizeRT.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String getHdSpectraPath() {
        return hdSpectraPath;
    }

    public String getHdPsmPath() {
        return hdPsmPath;
    }

    public int run() throws IOException {

        long start = System.currentTimeMillis();

        SparkConf conf = initSpark();

        final JavaSparkContext sc = new JavaSparkContext(conf);

        NormalizeRTParamsImpl parameters = new NormalizeRTParamsImpl();

        SpectrumKeyFunction<MsnSpectrum> keyFunction = parameters.keyFunction().get();
        final JavaPairRDD<String, MsnSpectrum> spectrumRdd = sc.sequenceFile(hdSpectraPath, SpectrumKey.class, MsnSpectrumValue.class)
                .mapToPair(sp -> new Tuple2<>(keyFunction.apply(sp._2().get()), sp._2().get()));

        //Read PSM's and Spectra
        Map<String, Iterable<Tuple3<String, Double, Double>>> rtData =
                sc.sequenceFile(hdPsmPath, Text.class, PeptideMatchValue.class)
                        .mapToPair(psm -> new Tuple2<>(psm._1().toString(), psm._2().get()))
                        .filter(psm -> parameters.standardPeptidesRTMap().containsKey(psm._2().getPeptide().toString()))
                        .leftOuterJoin(spectrumRdd)
                        .filter(pair -> pair._2()._2().isPresent() && !pair._2()._2().get().getRetentionTimes().isEmpty())
                        .mapToPair(pair ->
                                new Tuple2<>(keyFunction.getRunID(pair._2()._2().get()),
                                        new Tuple3<>(pair._2()._1().getPeptide().toString(), parameters.standardPeptidesRTMap().get(pair._2()._1().getPeptide().toString()),
                                                pair._2()._2().get().getRetentionTimes().getFirst().getTime())))
                        .groupByKey()
                        .collectAsMap();

        RTTransform rtTransform = parameters.rtTransformer().get();
        rtTransform.setData(rtData);

        report(rtTransform);
        transformSpectra(spectrumRdd,rtTransform,sc,parameters);

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts == null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdmsn").withDescription("spectrum sequence file (required)").hasArg().withArgName("path").isRequired().create("hs"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdpsm").withDescription("psm sequence file (required)").hasArg().withArgName("path").isRequired().create("hp"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("spsort").withDescription("sort spectra in hadoop file").create("s"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("reportDir").withDescription("directory for report files (required)").hasArg().withArgName("path").isRequired().create("or"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("sparkDir").withDescription("spark output directory (required)").isRequired().hasArg().withArgName("path").create("os"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("forceDelete").withDescription("force deleting existing spark output directory").create("fd"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);


        this.hdSpectraPath = checkExistFileOption(line, "hs");
        this.hdPsmPath = checkExistFileOption(line, "hp");
        this.doSort = line.hasOption("s") ? true : false;
        this.reportDir = checkOutputDirOption(line,"or");

        this.forceDelete = false;
        if( line.hasOption( "fd" ) ) {
            this.forceDelete = true;
        }
        this.sparkDir = checkSparkDirOption(line, "ho",forceDelete);

    }

    private void printRTData(JavaSparkContext sc, NormalizeRTParams parameters, JavaPairRDD<String, MsnSpectrum> spectrumRdd) {

        SpectrumKeyFunction<MsnSpectrum> keyFunction = parameters.keyFunction().get();

        Map<String, Iterable<Tuple2<String, Double>>> peptideRTs =
                sc.sequenceFile(hdPsmPath, Text.class, PeptideMatchValue.class)
                        .mapToPair(psm -> new Tuple2<>(psm._1().toString(), psm._2().get()))
                        .filter(psm -> parameters.standardPeptidesRTMap().containsKey(psm._2().getPeptide().toString()))
                        .leftOuterJoin(spectrumRdd)
                        .filter(pair -> pair._2()._2().isPresent() && !pair._2()._2().get().getRetentionTimes().isEmpty())
                        .mapToPair(pair ->
                                new Tuple2<>(keyFunction.getRunID(pair._2()._2().get()),
                                        new Tuple2<>(pair._2()._1().getPeptide().toString(),
                                                pair._2()._2().get().getRetentionTimes().getFirst().getTime())) )
                        .groupByKey()
                        .collectAsMap();

        for (String runId : peptideRTs.keySet()) {
            Multimap<String, Double> peptideRTMap = ArrayListMultimap.create();

            peptideRTs.get(runId).forEach(pair -> peptideRTMap.put(pair._1(), pair._2()));

            for (String peptide : peptideRTMap.keySet()) {
                Collection<Double> rts = peptideRTMap.get(peptide);
                double[] rtArray = new double[rts.size()];
                Iterator<Double> it = rts.iterator();
                for (int i = 0; i < rts.size(); i++) rtArray[i] = it.next();
                double medianRT = StatUtils.percentile(rtArray, 0.5);

                System.out.println(runId + ": " + peptide + " - " + medianRT);
            }
        }

    }

    private void report(RTTransform rtTransform) throws IOException {

        String rtCorrParamsFileName = reportDir + File.separator + "RTCorrData.txt";

        rtTransform.write(rtCorrParamsFileName);
    }

    private void transformSpectra(JavaPairRDD<String, MsnSpectrum> spectrumRdd, RTTransform rtTransform, JavaSparkContext sc, NormalizeRTParams parameters) throws IOException {

        final JavaRDD<MsnSpectrum> spectrumRddTransformed =
                spectrumRdd.mapValues(new MsnSpectrumTransformer(rtTransform, parameters.keyFunction().get()))
                        .values();

        String hdTransSpectraFileName = sparkDir + File.separator + "spectra_rt";

        File hdFile = new File(hdTransSpectraFileName);
        if (hdFile.exists()) FileUtils.deleteQuietly(hdFile);

        if (doSort)
            SparkUtils.saveSortMsnSpectra(sc, hdTransSpectraFileName, spectrumRddTransformed);
        else
            SparkUtils.saveMsnSpectra(sc, hdTransSpectraFileName, spectrumRddTransformed);

    }
}