package org.expasy.mzjava.sparktool.mstranslator;

import org.expasy.mzjava.core.ms.spectrum.IonType;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.Set;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class PepLibPeakComparator implements Comparator<PepLibPeak> {

    private final Set<IonType>  ionTypes;

    public PepLibPeakComparator() {
        this.ionTypes = EnumSet.of(IonType.b,IonType.y);
    }

    @Override
    public int compare(PepLibPeak pepLibPeak1, PepLibPeak pepLibPeak2) {

        int result = compareIonTypes(pepLibPeak1,pepLibPeak2);
        if (result != 0) return result;

        result = comparePosition(pepLibPeak1,pepLibPeak2);
        if (result != 0) return result;

        result = Integer.compare(pepLibPeak1.getPeakCount(),pepLibPeak2.getPeakCount());
        if (result != 0) return result;

        if (pepLibPeak1.getPeakCount()<=3 || pepLibPeak2.getPeakCount()<=3)
            result = Double.compare(pepLibPeak1.getIntens(),pepLibPeak2.getIntens());
        else {
            double relIntens1 = pepLibPeak1.getIntens()/pepLibPeak1.getPeakIntensStd();
            double relIntens2 = pepLibPeak2.getIntens()/pepLibPeak2.getPeakIntensStd();
            result = Double.compare(relIntens1, relIntens2);
        }

        return result;
    }


    private int compareIonTypes(PepLibPeak pepLibPeak1, PepLibPeak pepLibPeak2) {

        boolean b1 = false;
        boolean b2 = false;
        for (IonType it : ionTypes) {
            if (!b1) b1 = pepLibPeak1.getIonTypes().contains(it);
            if (!b2) b2 = pepLibPeak2.getIonTypes().contains(it);
            if (b1&&b2) break;
        }

        if (b1&&b2 || !b1&&!b2) return 0;
        if (!b1&&b2) return -1;
        else return 1;
    }

    private int comparePosition(PepLibPeak pepLibPeak1, PepLibPeak pepLibPeak2) {
        boolean b1 = pepLibPeak1.getFragSize()!=1;
        boolean b2 = pepLibPeak2.getFragSize()!=1;

        if (b1&&b2 || !b1&&!b2) return 0;
        if (!b1&&b2) return -1;
        else return 1;
    }
}
