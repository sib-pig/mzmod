package org.expasy.mzjava.sparktool.mstranslator;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.util.LibSpectrumFilter;
import org.expasy.mzjava.sparktool.util.MgfTitleKeyFunction;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import scala.Tuple2;

import java.util.Comparator;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class Lib2TSVParamsImpl implements Lib2TSVParams {

    @Override
    public Cached<? extends Comparator<PepLibPeak>> peakComparator() {
        return new Cached<PepLibPeakComparator>() {
            @Override
            protected PepLibPeakComparator build() {

                return new PepLibPeakComparator();
            }
        };

    }

    @Override
    public int nrTransition() {
        return 6;
    }

    @Override
    public Cached<? extends PeptideConsensusWriter> consensusWriter() {
        return new Cached<OpenSwathPeptideConsensusWriter>() {
            @Override
            protected OpenSwathPeptideConsensusWriter build() {

                return new OpenSwathPeptideConsensusWriter();
            }
        };

    }

    @Override
    public Cached<? extends Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean>> librarySpectrumFilter() {
        return new Cached<LibSpectrumFilter>() {
            @Override
            protected LibSpectrumFilter build() {

                return new LibSpectrumFilter(false);
            }
        };

    }

    public Cached<? extends RTTransform> rtTransformer(){

        return new Cached<RTAffineTransform>() {
            @Override
            protected RTAffineTransform build() {

                return new RTAffineTransform();
            }
        };

    }

    public Cached<SpectrumKeyFunction<MsnSpectrum>> keyFunction() {
        return new Cached<SpectrumKeyFunction<MsnSpectrum>>() {
            @Override
            protected SpectrumKeyFunction<MsnSpectrum> build() {

                return new MgfTitleKeyFunction();
//                return XTandemMgfKeyFunction();
            }
        };
    }

}
