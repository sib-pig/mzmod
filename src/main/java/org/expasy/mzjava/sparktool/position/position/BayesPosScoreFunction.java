package org.expasy.mzjava.sparktool.position.position;

import org.expasy.mzjava.sparktool.position.data.PositionIntermediate;
import org.expasy.mzjava.sparktool.position.data.PositionResult;

import java.util.Arrays;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class BayesPosScoreFunction extends PosScoreFunction {

    private final PositionResult.Builder builder = new PositionResult.Builder();

    @Override
    public PositionResult apply(PositionIntermediate intermediate) {

        final int residueCount = intermediate.getResidueCount();
        double[] pDataGivenSite = new double[residueCount];
        double modSitesSum = 0;

        for (int i = 0; i < residueCount; i++) {

            final double likelihood = product(i, intermediate);
            pDataGivenSite[i] = likelihood;
            modSitesSum += likelihood;
        }

        double[] pDataGivenNotSite = new double[residueCount];
        double notModSiteSum = 0;
        for (int i = 0; i < pDataGivenNotSite.length; i++) {

            double sum = 0;
            for (int j = 0; j < pDataGivenSite.length; j++) {

                if (j != i) sum += pDataGivenSite[j];
            }

            pDataGivenNotSite[i] = sum;
            notModSiteSum += sum;
        }

        toProbability(pDataGivenSite, modSitesSum);
        toProbability(pDataGivenNotSite, notModSiteSum);

        double[] pSite = new double[residueCount];
        Arrays.fill(pSite, 1.0 / residueCount);

        int position = 0;
        double maxP = 0;
        builder.reset(residueCount);
        for (int i = 0; i < residueCount; i++) {

            double pDGivenMS = pDataGivenSite[i];
            double pDGivenNS = pDataGivenNotSite[i];

            final double pS = pSite[i];
            final double pSGivenD = (pDGivenMS * pS) / ((pDGivenMS * pS) + (pDGivenNS * (1 - pS)));
            builder.setScore(i, pSGivenD);
            if (pSGivenD > maxP) {

                position = i;
                maxP = pSGivenD;
            }
        }

        return builder.build(position, intermediate.getIonCurrentCoverage());
    }

    private void toProbability(double[] modSites, double modSitesSum) {

        for (int i = 0; i < modSites.length; i++) {

            modSites[i] = modSites[i] / modSitesSum;
        }
    }
}
