package org.expasy.mzjava.sparktool.position.annotation;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.sparktool.position.data.PeakSet;
import org.expasy.mzjava.sparktool.position.data.PositionCandidate;
import org.expasy.mzjava.sparktool.position.data.PositionIntermediate;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class DeterministicPosAnnotationFunction implements PosAnnotationFunction {

    private final Set<IonType> forwardIonTypes;
    private final Set<IonType> reverseIonTypes;
    private final Set<IonType> allIonTypes;
    private final Tolerance fragmentTolerance;
    private final Function<Peak, Set<Integer>> chargeFunction;
    private final double modProbOffset;
    private final PositionIntermediate.Builder builder;

    public DeterministicPosAnnotationFunction(Set<IonType> forwardIonTypes, Set<IonType> reverseIonTypes, Tolerance fragmentTolerance,
                                              Function<Peak, Set<Integer>> chargeFunction, double modProbOffset) {

        checkIonTypes(FragmentType.FORWARD, forwardIonTypes);
        checkIonTypes(FragmentType.REVERSE, reverseIonTypes);

        this.forwardIonTypes = forwardIonTypes;
        this.reverseIonTypes = reverseIonTypes;
        this.fragmentTolerance = fragmentTolerance;
        this.chargeFunction = chargeFunction;
        this.modProbOffset = modProbOffset;

        this.allIonTypes = EnumSet.copyOf(forwardIonTypes);
        allIonTypes.addAll(reverseIonTypes);
        this.builder = new PositionIntermediate.Builder();
    }

    private void checkIonTypes(FragmentType expectedFragType, Set<IonType> ionTypes) {

        for (IonType ionType : ionTypes)
            Preconditions.checkArgument(expectedFragType.equals(ionType.getFragmentType()));
    }

    @Override
    public PositionIntermediate apply(PositionCandidate candidate) {

        Set<Integer> charges = chargeFunction.apply(candidate.getPrecursor());

        List<PeakSet> forwardPeakSets = Lists.newArrayList(
                new PeakSet(charges, forwardIonTypes, Mass.ZERO),
                new PeakSet(charges, forwardIonTypes, candidate.getMassShift())
        );
        List<PeakSet> reversePeakSets = Lists.newArrayList(
                new PeakSet(charges, reverseIonTypes, Mass.ZERO),
                new PeakSet(charges, reverseIonTypes, candidate.getMassShift())
        );

        final TIntSet peaks = new TIntHashSet(candidate.getPeakCount());
        final int residueCount = candidate.getResidueCount();
        builder.reset(residueCount);
        //Treat first residue specially because the reverse series mus be modified
        builder.setModProbability(FragmentType.FORWARD, 0,
                calcModP(candidate.extractIntensity(peaks, forwardPeakSets, 0, fragmentTolerance))
        );
        builder.setModProbability(FragmentType.REVERSE, 0, 1 - modProbOffset);
        for (int i = 1; i < residueCount - 1; i++) {

            builder.setModProbability(FragmentType.FORWARD, i,
                    calcModP(candidate.extractIntensity(peaks, forwardPeakSets, i, fragmentTolerance))
            );
            builder.setModProbability(FragmentType.REVERSE, i,
                    calcModP(candidate.extractIntensity(peaks, reversePeakSets, i, fragmentTolerance))
            );
        }
        //Treat last residue specially because the forward series must be modified
        int lastResidue = residueCount - 1;
        builder.setModProbability(FragmentType.FORWARD, lastResidue, 1 - modProbOffset);
        builder.setModProbability(FragmentType.REVERSE, lastResidue,
                calcModP(candidate.extractIntensity(peaks, reversePeakSets, lastResidue, fragmentTolerance))
        );

        double ionCurrentCoverage = candidate.getIonCurrent(peaks) / candidate.getTotalIonCurrent();
        if(ionCurrentCoverage < 0 || ionCurrentCoverage > 1)
            throw new IllegalStateException("Ion current coverage should be in the range 0 <= x <= 1 but was " + ionCurrentCoverage);
        return builder.build(candidate.getMassShift(), ionCurrentCoverage);
    }

    public double getIonCurrentCoverage(PositionCandidate candidate, Set<Mass> massShifts) {

        Set<Integer> charges = chargeFunction.apply(candidate.getPrecursor());

        final TIntSet peaks = new TIntHashSet(candidate.getPeakCount());
        for (int charge : charges) {
            candidate.addPeaksMatching(peaks, allIonTypes, massShifts, charge, fragmentTolerance);
        }

        double ionCurrentCoverage = candidate.getIonCurrent(peaks) / candidate.getTotalIonCurrent();
        if(ionCurrentCoverage < 0 || ionCurrentCoverage > 1)
            throw new IllegalStateException("Ion current coverage should be in the range 0 <= x <= 1 but was " + ionCurrentCoverage + " mass shifts " + massShifts);
        return ionCurrentCoverage;
    }

    private double calcModP(double[] intensitySums) {

        double sum = 0;
        for (double intensity : intensitySums) {
            sum += intensity;
        }

        if (sum == 0.0)
            return 0.5;

        double min = modProbOffset;
        double max = 1 - min;

        double p = intensitySums[1] / sum;
        if (p < min) p = min;
        if (p > max) p = max;

        return p;
    }
}
