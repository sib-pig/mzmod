package org.expasy.mzjava.sparktool.position;

import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.NdpResult;
import org.expasy.mzjava.sparktool.position.annotation.PosAnnotationFunction;
import org.expasy.mzjava.sparktool.position.data.PositionCandidate;
import org.expasy.mzjava.sparktool.position.data.PositionIntermediate;
import org.expasy.mzjava.sparktool.position.data.PositionResult;
import org.expasy.mzjava.sparktool.position.position.PosScoreFunction;

import java.util.Collections;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PositionFunction {

    private final PosAnnotationFunction modAnnotatorFunction;
    private final PosScoreFunction positionFunction;

    private final PositionResult.Builder builder = new PositionResult.Builder();

    public PositionFunction(PosAnnotationFunction annotatorFunction, PosScoreFunction scoreFunction) {

        this.modAnnotatorFunction = annotatorFunction;
        this.positionFunction = scoreFunction;
    }

    public PositionResult position(MsnSpectrum querySpectrum, NdpResult ndpResult) {

        final PositionCandidate posCandidate = new PositionCandidate(new NumericMass(ndpResult.getMassShift()), ndpResult.getLibPeptide(), querySpectrum);
        if (ndpResult.getHitType() == HitType.OPEN) {

            final PositionIntermediate intermediate = modAnnotatorFunction.apply(posCandidate);
            return positionFunction.apply(intermediate);
        } else {

            int residueCount = ndpResult.getLibPeptide().size();
            builder.reset(residueCount);
            for (int i = 0; i < residueCount; i++) {

                builder.setScore(i, 1);
            }

            return builder.build(0, modAnnotatorFunction.getIonCurrentCoverage(posCandidate, Collections.singleton(Mass.ZERO)));
        }
    }
}
