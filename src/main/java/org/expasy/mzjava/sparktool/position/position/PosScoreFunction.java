/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * https://sourceforge.net/projects/javaprotlib/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.sparktool.position.position;

import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.sparktool.position.data.PositionIntermediate;
import org.expasy.mzjava.sparktool.position.data.PositionResult;

import java.util.function.Function;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public abstract class PosScoreFunction implements Function<PositionIntermediate, PositionResult> {

    protected double product(int modifiedResidue, PositionIntermediate intermediate) {

        final int residueCount = intermediate.getResidueCount();
        double sumOfLog = 0;
        for(int i = 0; i < residueCount; i++){

            sumOfLog += Math.log(intermediate.getProbability(FragmentType.FORWARD, i, modifiedResidue));
            sumOfLog += Math.log(intermediate.getProbability(FragmentType.REVERSE, i, modifiedResidue));
        }

        return Math.exp(sumOfLog);
    }
}
