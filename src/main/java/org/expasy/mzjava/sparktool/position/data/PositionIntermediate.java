package org.expasy.mzjava.sparktool.position.data;

import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PositionIntermediate {

    private static final String msgUnknownFragType = "Cannot get mod probability for fragment type ";

    private final double[] pModForward, pModReverse;
    private final Mass massShift;
    private final double ionCurrentCoverage;

    private PositionIntermediate(double[] pModForward, double[] pModReverse, Mass massShift, double ionCurrentCoverage) {

        this.pModForward = pModForward;
        this.pModReverse = pModReverse;
        this.massShift = massShift;
        this.ionCurrentCoverage = ionCurrentCoverage;
    }

    public double getIonCurrentCoverage() {

        return ionCurrentCoverage;
    }

    public double getModProbability(FragmentType fragmentType, int residue) {

        switch (fragmentType) {

            case FORWARD:
                return pModForward[residue];
            case REVERSE:
                return pModReverse[residue];
            case UNKNOWN:
            case INTERNAL:
            case MONOMER:
            case INTACT:
            default:
                throw new IllegalStateException(msgUnknownFragType + fragmentType);
        }
    }

    public double getProbability(FragmentType fragmentType, int residue, int modifiedResidue) {

        if (fragmentType == FragmentType.FORWARD) {

            return residue >= modifiedResidue ? pModForward[residue] : 1 - pModForward[residue];
        } else if (fragmentType == FragmentType.REVERSE) {

            return residue <= modifiedResidue ? pModReverse[residue] : 1 - pModReverse[residue];
        } else {

            throw new IllegalStateException(msgUnknownFragType + fragmentType);
        }
    }

    public int getResidueCount() {

        return pModForward.length;
    }

    public Mass getMassShift() {

        return massShift;
    }

    public static class Builder {

        private double[] pModForward, pModReverse;

        public Builder reset(int residues) {

            pModForward = new double[residues];
            pModReverse = new double[residues];

            return this;
        }

        public Builder setModProbability(FragmentType fragmentType, int residue, double pMod) {

            switch (fragmentType) {

                case FORWARD:
                    pModForward[residue] = pMod;
                    break;
                case REVERSE:
                    pModReverse[residue] = pMod;
                    break;
                case UNKNOWN:
                case INTERNAL:
                case MONOMER:
                case INTACT:
                default:
                    throw new IllegalStateException(msgUnknownFragType + fragmentType);
            }

            return this;
        }

        public PositionIntermediate build(Mass massShift, double ionCurrentCoverage) {

            checkNotNull(massShift);

            double[] tmpForwardMod = pModForward;
            double[] tmpReverseMod = pModReverse;

            pModForward = null;
            pModReverse = null;

            return new PositionIntermediate(tmpForwardMod, tmpReverseMod, massShift, ionCurrentCoverage);
        }
    }
}