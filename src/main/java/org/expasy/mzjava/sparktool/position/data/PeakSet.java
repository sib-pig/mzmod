package org.expasy.mzjava.sparktool.position.data;

import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.ms.spectrum.IonType;

import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeakSet {

    private final Set<Integer> charges;
    private final Set<IonType> ionTypes;
    private final Mass massShift;

    public PeakSet(Set<Integer> charges, Set<IonType> ionTypes, Mass massShift) {

        checkNotNull(charges);
        checkArgument(!charges.isEmpty());
        checkNotNull(ionTypes);
        checkArgument(!ionTypes.isEmpty());
        checkNotNull(massShift);

        this.charges = charges;
        this.ionTypes = ionTypes;
        this.massShift = massShift;
    }

    public Set<Integer> getCharges() {

        return charges;
    }

    public Set<IonType> getIonTypes() {

        return ionTypes;
    }

    public Mass getMassShift() {

        return massShift;
    }
}
