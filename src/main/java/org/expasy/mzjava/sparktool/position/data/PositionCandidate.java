package org.expasy.mzjava.sparktool.position.data;

import gnu.trove.set.TIntSet;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.AAMassCalculator;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PositionCandidate {

    private final int residueCount;
    private final PeakList<PeakAnnotation> peakList;
    private final Mass massShift;

    private final List<PeptideFragment> forwardFragments;
    private final List<PeptideFragment> reverseFragments;

    public PositionCandidate(Mass massShift, Peptide unmodifiedPeptide, PeakList<PeakAnnotation> peakList) {

        this.massShift = massShift;

        checkNotNull(massShift);
        checkNotNull(unmodifiedPeptide);
        checkNotNull(peakList);

        this.residueCount = unmodifiedPeptide.size();
        this.peakList = peakList;

        int peptideSize = unmodifiedPeptide.size();
        forwardFragments = new ArrayList<>(peptideSize);
        reverseFragments = new ArrayList<>(peptideSize);

        for (int i = 1; i <= peptideSize; i++) {

            forwardFragments.add(unmodifiedPeptide.createFragment(0, i, FragmentType.FORWARD));
        }

        for (int i = 0; i < peptideSize; i++) {

            reverseFragments.add(unmodifiedPeptide.createFragment(i, peptideSize, FragmentType.REVERSE));
        }
    }

    public double getIntensity(TIntSet peaks, int residue, IonType ionType, Mass massShift, int charge, Tolerance tolerance) {

        if (residue > forwardFragments.size())
            return 0.0;

        final int index = getPeakIndex(residue, ionType, massShift, charge, tolerance);
        if (index == -1) {

            return 0;
        } else {

            peaks.add(index);
            return peakList.getIntensity(index);
        }
    }

    public int getPeakIndex(int residue, IonType ionType, Mass massShift, int charge, Tolerance tolerance){

        final double mass;
        if (ionType.getFragmentType() == FragmentType.FORWARD) {

            mass = forwardFragments.get(residue).calculateMass(ionType);
        } else if (ionType.getFragmentType() == FragmentType.REVERSE) {

            mass = reverseFragments.get(residue).calculateMass(ionType);
        } else {

            throw new IllegalStateException("Cannot get intensity for " + ionType + " ions");
        }
        final double mz = AAMassCalculator.getInstance().calculateMz(mass + massShift.getMolecularMass(), charge, ionType);
        final int index = peakList.getClosestIndex(mz);
        return tolerance.withinTolerance(mz, peakList.getMz(index)) ? index : -1;
    }

    public double[] extractIntensity(TIntSet peaks, List<PeakSet> peakSets, int residue, Tolerance tolerance) {

        double[] intensities = new double[peakSets.size()];
        for (int i = 0; i < peakSets.size(); i++) {

            PeakSet peakSet = peakSets.get(i);
            Set<IonType> ionTypes = peakSet.getIonTypes();
            Mass peakMassShift = peakSet.getMassShift();

            double sum = 0;
            for (int charge : peakSet.getCharges()) {

                for (IonType ionType : ionTypes) {

                    sum += getIntensity(peaks, residue, ionType, peakMassShift, charge, tolerance);
                }
            }

            intensities[i] = sum;
        }

        return intensities;
    }

    public int getResidueCount() {

        return residueCount;
    }

    public void addPeaksMatching(TIntSet peaks, Set<IonType> ionTypes, Set<Mass> massShifts, int charge, Tolerance tolerance) {

        for (int r = 0; r < residueCount; r++) {

            for (IonType ionType : ionTypes) {

                for (Mass shift : massShifts) {

                    int index = getPeakIndex(r, ionType, shift, charge, tolerance);
                    if(index != -1)
                        peaks.add(index);
                }
            }
        }
    }

    public double getTotalIonCurrent() {

        return peakList.getTotalIonCurrent();
    }

    public double getIonCurrent(TIntSet peaks) {

        final double[] ionCurrent = {0};

        peaks.forEach(index -> {

            ionCurrent[0] += peakList.getIntensity(index);
            return true;
        });

        return ionCurrent[0];
    }

    public Mass getMassShift() {

        return massShift;
    }

    public Peak getPrecursor() {

        return peakList.getPrecursor();
    }

    public int getPeakCount() {

        return peakList.size();
    }
}
