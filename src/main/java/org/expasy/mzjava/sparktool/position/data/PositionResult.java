package org.expasy.mzjava.sparktool.position.data;

import com.google.common.base.Preconditions;

import java.util.Arrays;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PositionResult {

    private final int position;
    private final double[] scores;
    private final double ionCurrentCoverage;

    private PositionResult(int position, double[] scores, double ionCurrentCoverage) {

        this.position = position;
        this.scores = scores;
        this.ionCurrentCoverage = ionCurrentCoverage;
    }

    public double getIonCurrentCoverage() {

        return ionCurrentCoverage;
    }

    public int getPosition() {

        return position;
    }

    public double getScore(int residue) {

        return scores[residue];
    }

    public int getResidueCount() {

        return scores.length;
    }

    @Override
    public String toString() {

        return "PositionResult{" +
                "position=" + position +
                ", positionScore=" + scores[position] +
                ", scores=" + Arrays.toString(scores) +
                '}';
    }

    public static class Builder {

        private double[] scores;

        public Builder reset(int residueCount){

            scores = new double[residueCount];
            return this;
        }

        public Builder setScore(int residue, double score){

            scores[residue] = score;
            return this;
        }

        public PositionResult build(int position, double ionCurrentCoverage){

            Preconditions.checkNotNull(scores);

            double[] tmpScores = scores;
            scores = null;
            return new PositionResult(position, tmpScores, ionCurrentCoverage);
        }
    }
}
