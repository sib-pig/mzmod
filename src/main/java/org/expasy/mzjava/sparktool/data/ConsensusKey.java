package org.expasy.mzjava.sparktool.data;

import org.expasy.mzjava.proteomics.mol.Peptide;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The hash code is calculated from the peptide's string representation because of an idiosyncrasy
 * of enums and spark.
 * <p>
 * In Java an enum's hash code is determined by it's position in memory. Spark uses the hash code when performing
 * group by key to determine which keys to compare. If objects that are equal hav different hash codes on different
 * VM's then duplicate ConsensusKey's are produced.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ConsensusKey {

    private final int charge;
    private final Peptide peptide;
    private final int hashCode;

    public ConsensusKey(int charge, Peptide peptide) {

        checkNotNull(peptide);

        this.charge = charge;
        this.peptide = peptide;

        //Using string due to issue with enums and spark
        int result = charge;
        result = 31 * result + peptide.toString().hashCode();
        hashCode = result;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof ConsensusKey)) return false;

        ConsensusKey that = (ConsensusKey) o;

        return charge == that.charge && peptide.equals(that.peptide);
    }

    @Override
    public int hashCode() {

        return hashCode;
    }

    public int getCharge() {

        return charge;
    }

    public Peptide getPeptide() {

        return peptide;
    }
}
