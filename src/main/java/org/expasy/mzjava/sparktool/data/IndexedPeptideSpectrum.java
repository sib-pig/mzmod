package org.expasy.mzjava.sparktool.data;

import com.google.common.base.Objects;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.proteomics.mol.Peptide;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedPeptideSpectrum extends IndexedSpectrum {

    private final Peptide peptide;
    private final int memberCount;
    private final Set<String> proteinAccessionNumbers;

    public IndexedPeptideSpectrum(Peptide peptide, Set<String> proteinAccessionNumbers, int memberCount, PeakList src, PeakProcessorChain processorChain) {

        super(src, processorChain);
        this.peptide = peptide;
        this.memberCount = memberCount;
        this.proteinAccessionNumbers = new HashSet<>(proteinAccessionNumbers);
    }

    public IndexedPeptideSpectrum(Peptide peptide, Set<String> proteinAccessionNumbers, int memberCount, Builder builder, Peak precursor, UUID id, Precision precision) {

        super(builder, precursor, id, precision);
        this.peptide = peptide;
        this.memberCount = memberCount;
        this.proteinAccessionNumbers = Collections.unmodifiableSet(new HashSet<>(proteinAccessionNumbers));
    }

    public Peptide getPeptide() {

        return peptide;
    }

    public int getMemberCount() {

        return memberCount;
    }

    public Set<String> getProteinAccessionNumbers() {

        return proteinAccessionNumbers;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final IndexedPeptideSpectrum other = (IndexedPeptideSpectrum) obj;
        return Objects.equal(this.getId(), other.getId()) && Objects.equal(this.peptide, other.peptide);
    }

    @Override
    public int hashCode() {

        return getId().hashCode();
    }

    public double calcTheoreticalMz() {

        return peptide.calculateMz(getPrecursor().getCharge());
    }
}
