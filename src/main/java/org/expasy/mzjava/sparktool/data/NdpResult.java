package org.expasy.mzjava.sparktool.data;

import org.expasy.mzjava.proteomics.mol.Peptide;

import java.util.Objects;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class NdpResult {

    private final Peptide libPeptide;
    private final UUID libSpectrumId;
    private final String proteinAccessionNumbers;
    private final int libMemberCount;
    private final double wndp;
    private final double massShift;
    private final HitType hitType;

    public NdpResult(Peptide libPeptide, UUID libSpectrumId, String proteinAccessionNumbers, int libMemberCount, double wndp, double massShift, HitType hitType) {

        this.libPeptide = libPeptide;
        this.libSpectrumId = libSpectrumId;
        this.proteinAccessionNumbers = proteinAccessionNumbers;
        this.libMemberCount = libMemberCount;
        this.wndp = wndp;
        this.massShift = massShift;
        this.hitType = hitType;
    }

    public double getWndp() {

        return wndp;
    }

    public int getLibMemberCount() {

        return libMemberCount;
    }

    public double getMassShift() {

        return massShift;
    }

    public Peptide getLibPeptide() {

        return libPeptide;
    }

    public HitType getHitType() {

        return hitType;
    }

    public UUID getLibSpectrumId() {

        return libSpectrumId;
    }

    public String getProteinAccessionNumbers() {

        return proteinAccessionNumbers;
    }

    @Override
    public int hashCode() {

        //Using string of peptide and hit type to avoid issues with spark and hashing enums
        return Objects.hash(libPeptide.toString(), libSpectrumId, wndp, massShift, hitType.toString());
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final NdpResult other = (NdpResult) obj;
        return Objects.equals(this.libPeptide, other.libPeptide) &&
                Objects.equals(this.libSpectrumId, other.libSpectrumId) &&
                Objects.equals(this.wndp, other.wndp) &&
                Objects.equals(this.massShift, other.massShift) &&
                Objects.equals(this.hitType, other.hitType);
    }

    @Override
    public String toString() {

        return "NdpResult{" +
                "libPeptide=" + libPeptide +
                ", wndp=" + wndp +
                ", massShift=" + massShift +
                ", hitType=" + hitType +
                '}';
    }
}
