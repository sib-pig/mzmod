package org.expasy.mzjava.sparktool.data;

import gnu.trove.impl.unmodifiable.TUnmodifiableObjectDoubleMap;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideMatch {

    private final Peptide peptide;
    private final Set<String> proteinAcc;
    private final TObjectDoubleMap<String> scoreMap;
    private final int charge;
    private final boolean isDecoy;

    public PeptideMatch(Peptide peptide, Stream<String> proteinAccStream, TObjectDoubleMap<String> scoreMap, int charge) {

        this.peptide = peptide;
        proteinAcc = Collections.unmodifiableSet(proteinAccStream.collect(Collectors.toSet()));
        this.scoreMap = new TUnmodifiableObjectDoubleMap<>(new TObjectDoubleHashMap<>(scoreMap));
        this.charge = charge;
        this.isDecoy = false;
    }

    public PeptideMatch(Peptide peptide, Stream<String> proteinAccStream,
                        TObjectDoubleMap<String> scoreMap, int charge, boolean isDecoy) {

        this.peptide = peptide;
        proteinAcc = Collections.unmodifiableSet(proteinAccStream.collect(Collectors.toSet()));
        this.scoreMap = new TUnmodifiableObjectDoubleMap<>(new TObjectDoubleHashMap<>(scoreMap));
        this.charge = charge;
        this.isDecoy = isDecoy;
    }

    public Peptide getPeptide() {

        return peptide;
    }

    public Set<String> getProteinAcc() {

        return proteinAcc;
    }

    public TObjectDoubleMap<String> getScoreMap() {

        return scoreMap;
    }

    public double getScore(String score) {

        return scoreMap.get(score);
    }

    public int getCharge() {

        return charge;
    }

    public String toSymbolString() {

        return peptide.replace(EnumSet.of(AminoAcid.I, AminoAcid.L), AminoAcid.J).toSymbolString();
    }

    public boolean isDecoy() {

        return isDecoy;
    }
}
