package org.expasy.mzjava.sparktool.data;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;

import java.util.*;

/**
 * An immutable peak list that indexes the peaks to allow fast access to the peaks
 * sorted by intensity.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedSpectrum extends Spectrum<PeakAnnotation> {

    private static final String mutationExceptionMessage = "IndexedSpectrum needs to be immutable therefore all mutator methods throw UnsupportedOperationException";

    private final short[] intensityToMzMap;
    private final short[] mzToIntensityMap;
    private final double vectorLength;
    private final short[] lookup;
    //Peak charges ordered by intensity
    private final byte[] peakCharges;
    private final double minMz, maxMz;
    private int maxIndex;

    public IndexedSpectrum(PeakList src) {

        this(src, new PeakProcessorChain<>());
    }

    public IndexedSpectrum(PeakList src, PeakProcessorChain processorChain) {

        this(toChargedPeakList(src, processorChain), src.getPrecursor(), src.getId(), src.getPrecision());
    }

    protected IndexedSpectrum(IndexedSpectrum.Builder builder, Peak precursor, UUID id, Precision precision) {

        this(builder.peaks, precursor, id, precision);
        builder.reset();
    }

    private IndexedSpectrum(List<ChargedPeak> peaks, Peak precursor, UUID id, Precision precision) {

        super(peaks.size(), precision);

        Preconditions.checkArgument(!peaks.isEmpty(), "Indexed spectra cannot be empty");

        super.setPrecursor(precursor);
        super.setId(id);

        final int size = peaks.size();

        if(peaks.size() > Short.MAX_VALUE)
            throw new IllegalStateException("Source peak list is to large");

        maxIndex = size - 1;
        lookup = new short[(int) peaks.get(maxIndex).mz + 2];
        Arrays.fill(lookup, (short) -1);

        peakCharges = new byte[size];

        final List<PeakAnnotation> annotations = Collections.emptyList();
        for (ChargedPeak peak : peaks) {

            final double mz = peak.mz;
            final double intensity = peak.intensity;

            super.add(mz, intensity, annotations);

            final int lookupIndex = (int) mz;
            int currIndex = lookup[lookupIndex];
            if (currIndex == -1)
                lookup[lookupIndex] = peak.mzIndex;
        }

        int lastIndex = 0;
        for (int i = 0; i < lookup.length; i++) {

            int currentIndex = lookup[i];
            if (currentIndex == -1)
                lookup[i] = (short)lastIndex;
            else
                lastIndex = currentIndex;
        }

        Collections.sort(peaks, (p1, p2) -> Double.compare(p2.intensity, p1.intensity));
        intensityToMzMap = new short[size];
        mzToIntensityMap = new short[size];
        for (int i = 0; i < peaks.size(); i++) {

            final ChargedPeak peak = peaks.get(i);
            final short mzIndex = peak.mzIndex;
            intensityToMzMap[i] = mzIndex;
            mzToIntensityMap[mzIndex] = (short)i;

            peakCharges[i] = peak.charge;
        }

        vectorLength = calcVectorLength();
        minMz = getMz(0);
        maxMz = getMz(maxIndex);
    }

    private static List<ChargedPeak> toChargedPeakList(PeakList src, PeakProcessorChain processorChain) {

        List<ChargedPeak> peaks = new ArrayList<>(src.size());

        //noinspection unchecked
        processorChain.process(src, new PeakSink<PeakAnnotation>() {
            private int index = 0;

            @Override
            public void start(int size) {
                //Empty because the size has already been set
            }

            @Override
            public void processPeak(double mz, double intensity, List<PeakAnnotation> annotations) {

                final int charge = annotations.isEmpty() ? 0 : annotations.get(0).getCharge();
                peaks.add(new ChargedPeak(mz, intensity, charge, index++));
            }

            @Override
            public void end() {
                //Empty because the size has already been set
            }
        });

        return peaks;
    }

    public final int fastIndexOf(double mzKey) {

        if(mzKey <= minMz)
            return 0;
        if(mzKey >= maxMz)
            return maxIndex;
        return lookup[(int) mzKey];
    }

    public final double getNthMz(int intensityIndex) {

        return getMz(intensityToMzMap[intensityIndex]);
    }

    public final double getNthIntensity(int intensityIndex) {

        return getIntensity(intensityToMzMap[intensityIndex]);
    }

    public final int toIntensityIndex(int mzIndex) {

        return mzToIntensityMap[mzIndex];
    }

    public final double getVectorLength() {

        return vectorLength;
    }

    public final double getMaxMz() {

        return maxMz;
    }

    @Override
    public PeakList<PeakAnnotation> copy(PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor) {

        throw new UnsupportedOperationException();
    }

    @Override
    public PeakList<PeakAnnotation> copy(PeakProcessorChain<PeakAnnotation> peakProcessorChain) {

        throw new UnsupportedOperationException();
    }

    public final int getCharge(int intensityIndex) {

        return peakCharges[intensityIndex];
    }

    @Override
    public void setId(UUID id) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void setIntensityAt(double intensity, int index) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void setPrecursor(Peak precursor) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void setMsLevel(int msLevel) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void addPeaks(PeakList<PeakAnnotation> peakList) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public <T extends PeakAnnotation> void addPeaksNoAnnotations(PeakList<T> peakList) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public <T extends PeakAnnotation> void addPeaks(PeakList<T> peakList, Function<List<T>, List<PeakAnnotation>> annotationConverter) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void addSorted(double[] mzs, double[] intensities) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void addSorted(double[] mzs, double[] intensities, int length) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public int add(double mz, double intensity) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public int add(double mz, double intensity, PeakAnnotation annotation) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public int add(double mz, double intensity, Collection<? extends PeakAnnotation> annotations) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void addAnnotation(int index, PeakAnnotation annotation) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void addAnnotations(int index, Collection<PeakAnnotation> annotations) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void ensureCapacity(int minCapacity) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public boolean removeAnnotation(PeakAnnotation annotation, int index) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void clearAnnotationsAt(int index) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void clearAnnotations() {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void sortAnnotations(Comparator<PeakAnnotation> comparator) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void apply(PeakProcessor<PeakAnnotation, PeakAnnotation> peakProcessor) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    @Override
    public void apply(PeakProcessorChain<PeakAnnotation> peakProcessorChain) {

        throw new UnsupportedOperationException(mutationExceptionMessage);
    }

    private static class ChargedPeak {

        private final double mz;
        private final double intensity;
        private final byte charge;
        private final short mzIndex;

        public ChargedPeak(double mz, double intensity, int charge, int mzIndex) {

            this.mz = mz;
            this.intensity = intensity;
            this.charge = (byte)charge;
            this.mzIndex = (short)mzIndex;
        }
    }

    public static class Builder {

        private final List<ChargedPeak> peaks = new ArrayList<>();
        private double lastMz = Integer.MIN_VALUE;

        public Builder addPeak(double mz, double intensity, int charge){

            Preconditions.checkArgument(mz > lastMz);

            final int mzIndex = peaks.size();
            peaks.add(new ChargedPeak(mz, intensity, charge, mzIndex));

            lastMz = mz;

            return this;
        }

        public IndexedSpectrum build(Peak precursor, UUID id, Precision precision){

            IndexedSpectrum spectrum = new IndexedSpectrum(peaks, precursor, id, precision);

            reset();

            return spectrum;
        }

        private void reset() {

            peaks.clear();
            lastMz = Integer.MIN_VALUE;
        }
    }
}
