package org.expasy.mzjava.sparktool.data;

import org.expasy.mzjava.core.ms.PpmTolerance;
import org.expasy.mzjava.core.ms.Tolerance;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class CachedPpmTolerance extends Cached<Tolerance> {

    private final double ppmTolerance;

    public CachedPpmTolerance(double ppmTolerance) {

        this.ppmTolerance = ppmTolerance;
    }

    @Override
    protected Tolerance build() {

        return new PpmTolerance(ppmTolerance);
    }
}
