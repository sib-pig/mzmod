package org.expasy.mzjava.sparktool.data;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class OmsQuery {

    private final MsnSpectrum spectrum;
    private final List<OmsResult> results;

    private transient IndexedSpectrum indexedSpectrum;

    public OmsQuery(MsnSpectrum spectrum) {

        checkNotNull(spectrum);

        this.spectrum = spectrum;
        this.results = Collections.emptyList();
    }

    public OmsQuery(OmsQuery omsQuery, List<OmsResult> results) {

        checkNotNull(omsQuery);
        checkNotNull(results);

        this.spectrum = omsQuery.spectrum;
        this.results = results.isEmpty() ? Collections.emptyList() : new ArrayList<>(results);
    }

    public OmsQuery(MsnSpectrum spectrum, List<OmsResult> results) {

        checkNotNull(spectrum);
        checkNotNull(results);

        this.spectrum = spectrum;
        this.results = results;
    }

    public MsnSpectrum getSpectrum() {

        return spectrum;
    }

    public List<OmsResult> getResults() {

        return Collections.unmodifiableList(results);
    }

    public Peak getPrecursor(){

        return spectrum.getPrecursor();
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OmsQuery omsQuery = (OmsQuery) o;

        return spectrum.getId().equals(omsQuery.spectrum.getId());
    }

    @Override
    public int hashCode() {

        return spectrum.getId().hashCode();
    }

    @Override
    public String toString() {

        return "OmsQuery{" +
                "best_peptide = " + (results.isEmpty() ? "-" : results.get(0).getPeptide().toString()) +
                ", best_mass_shift = " + (results.isEmpty() ? "-" : NumberFormat.getInstance().format(results.get(0).getMassShift())) +
                '}';
    }

    public OmsResult getResult(int index) {

        return results.get(index);
    }

    public Optional<OmsResult> getResult(Peptide peptide){

        for (OmsResult omsResult : results) {

            if(omsResult.getPeptide().equals(peptide))
                return Optional.of(omsResult);
        }

        return Optional.empty();
    }

    public int getResultCount() {

        return results.size();
    }

    public IndexedSpectrum getOrBuildIndexedSpectrum(PeakProcessorChain<PeakAnnotation> processorChain) {

        if(indexedSpectrum == null)
            indexedSpectrum = new IndexedSpectrum(spectrum, processorChain);

        return indexedSpectrum;
    }

    public double getDeltaMetaScore(int index, double defaultDelta) {

        final double delta;
        if (index + 1 < results.size()) {
            delta = getResult(index).getMetaScore() - getResult(index + 1).getMetaScore();
        } else {
            delta = defaultDelta;
        }

        return delta;
    }

    public double getMetaPlusDeltaScore(int index, double defaultDelta) {

        final double metaScore1 = getResult(index).getMetaScore();
        final double delta;
        if (index + 1 < results.size()) {
            delta = metaScore1 - getResult(index + 1).getMetaScore();
        } else {
            delta = defaultDelta;
        }

        return metaScore1 + delta;
    }

    public double getMetaPlusDeltaScore(int index, int targetIndex, double defaultDelta) {

        Preconditions.checkArgument(targetIndex > index);

        final double metaScore1 = getResult(index).getMetaScore();
        final double delta;
        if (targetIndex < results.size()) {
            delta = metaScore1 - getResult(targetIndex).getMetaScore();
        } else {
            delta = defaultDelta;
        }

        return metaScore1 + delta;
    }
}
