package org.expasy.mzjava.sparktool.data;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.position.data.PositionResult;

import java.text.NumberFormat;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class OmsResult {

    private final HitType hitType;
    private final Peptide peptide;
    private final UUID libSpectrumId;
    private final String proteinAccessionNumbers;

    private final int libMemberCount;
    private final double massShift;

    private final double metaScore;
    private final double wdp;

    private final double ionCurrentCoverage;
    private final int position;
    private final double[] residuePosScores;

    public OmsResult(HitType hitType, Peptide peptide, UUID libSpectrumId, String proteinAccessionNumbers, int libMemberCount, double massShift, double metaScore, double wdp, double ionCurrentCoverage, int position, double[] residuePosScores) {

        this.hitType = hitType;
        this.peptide = peptide;
        this.libSpectrumId = libSpectrumId;
        this.proteinAccessionNumbers = proteinAccessionNumbers;
        this.libMemberCount = libMemberCount;
        this.massShift = massShift;
        this.metaScore = metaScore;
        this.wdp = wdp;
        this.ionCurrentCoverage = ionCurrentCoverage;
        this.position = position;
        this.residuePosScores = residuePosScores;
    }

    public OmsResult(double metaScore, NdpResult ndpResult, PositionResult posResult) {

        this.hitType = ndpResult.getHitType();
        this.peptide = ndpResult.getLibPeptide();
        this.libSpectrumId = ndpResult.getLibSpectrumId();
        this.proteinAccessionNumbers = ndpResult.getProteinAccessionNumbers();
        libMemberCount = ndpResult.getLibMemberCount();
        this.massShift = ndpResult.getMassShift();
        this.metaScore = metaScore;
        this.wdp = ndpResult.getWndp();
        this.ionCurrentCoverage = posResult.getIonCurrentCoverage();
        this.position = posResult.getPosition();
        final int residueCount = posResult.getResidueCount();
        this.residuePosScores = new double[residueCount];
        for (int i = 0; i < residueCount; i++) {

            residuePosScores[i] = posResult.getScore(i);
        }
    }

    public OmsResult(float metaScore, OmsResult src) {

        this.hitType = src.getHitType();
        this.peptide = src.getPeptide();
        this.libSpectrumId = src.getLibSpectrumId();
        this.proteinAccessionNumbers = src.getProteinAccessionNumbers();
        this.libMemberCount = src.getLibMemberCount();
        this.massShift = src.getMassShift();
        this.metaScore = metaScore;
        this.wdp = src.getWdp();
        this.ionCurrentCoverage = src.getIonCurrentCoverage();
        this.position = src.getPosition();
        final int residueCount = src.getResidueCount();
        this.residuePosScores = new double[residueCount];
        for (int i = 0; i < residueCount; i++) {

            residuePosScores[i] = src.getPositionScore(i);
        }
    }

    public int getLibMemberCount() {

        return libMemberCount;
    }

    public double getMetaScore() {

        return metaScore;
    }

    public HitType getHitType() {

        return hitType;
    }

    public Peptide getPeptide() {

        return peptide;
    }

    public double getMassShift() {

        return massShift;
    }

    public double getWdp() {

        return wdp;
    }

    public double getIonCurrentCoverage() {

        return ionCurrentCoverage;
    }

    public UUID getLibSpectrumId() {

        return libSpectrumId;
    }

    public int getPosition() {

        return position;
    }

    public int getResidueCount() {

        return residuePosScores.length;
    }

    public double getPositionScore(int residue) {

        return residuePosScores[residue];
    }

    public String getProteinAccessionNumbers() {

        return proteinAccessionNumbers;
    }

    @Override
    public String toString() {

        return "OmsResult{" +
                "hitType=" + hitType +
                ", metaScore=" + metaScore +
                ", peptide=" + peptide +
                ", massShift=" + NumberFormat.getNumberInstance().format(massShift) +
                '}';
    }
}
