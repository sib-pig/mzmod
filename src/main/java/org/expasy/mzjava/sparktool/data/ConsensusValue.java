package org.expasy.mzjava.sparktool.data;

import gnu.trove.TCollections;
import gnu.trove.map.TObjectDoubleMap;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;

import java.util.Collections;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ConsensusValue {

    private final TObjectDoubleMap<String> scoreMap;
    private final MsnSpectrum spectrum;
    private final Set<String> proteinAccSet;

    public ConsensusValue(Set<String> proteinAccSet, TObjectDoubleMap<String> scoreMap, MsnSpectrum spectrum) {

        this.scoreMap = TCollections.unmodifiableMap(scoreMap);
        this.spectrum = spectrum;
        this.proteinAccSet = Collections.unmodifiableSet(proteinAccSet);
    }

    public double getScore(String scoreName){

        return scoreMap.get(scoreName);
    }

    public TObjectDoubleMap<String> getScoreMap() {

        return scoreMap;
    }

    public MsnSpectrum getSpectrum() {

        return spectrum;
    }

    public Set<String> getProteinAccSet() {

        return proteinAccSet;
    }
}
