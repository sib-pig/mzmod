package org.expasy.mzjava.sparktool.data;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
public enum HitType {

    OPEN, EXACT
}
