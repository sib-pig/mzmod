package org.expasy.mzjava.sparktool.data;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class CachedAbsoluteTolerance extends Cached<Tolerance> {

    private final double error;

    public CachedAbsoluteTolerance(double error) {

        this.error = error;
    }

    @Override
    protected Tolerance build() {

        return new AbsoluteTolerance(error);
    }
}
