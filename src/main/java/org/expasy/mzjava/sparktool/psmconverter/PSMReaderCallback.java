package org.expasy.mzjava.sparktool.psmconverter;

import org.apache.spark.Accumulator;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.ident.PeptideProteinMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.expasy.mzjava.proteomics.ms.ident.UnresolvableModificationMatchException;
import org.expasy.mzjava.sparktool.data.PeptideMatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PSMReaderCallback implements org.expasy.mzjava.proteomics.io.ms.ident.PSMReaderCallback {

    private static final Logger LOGGER = Logger.getLogger(PSMReaderCallback.class.getName());

    private final Accumulator<Integer> skippedPsmCounter;

    private final Map<String, List<PeptideMatch>> psmMap;
    private final PSMConverterParams params;
    private final Pattern decoyPattern;

    public PSMReaderCallback(PSMConverterParams params, Map<String, List<PeptideMatch>> psmMap, Accumulator<Integer> skippedPsmCounter) {

        this.skippedPsmCounter = skippedPsmCounter;
        this.psmMap = psmMap;
        this.params = params;
        this.decoyPattern = Pattern.compile(params.getDecoyProtPattern());
    }

    @Override
    public void resultRead(SpectrumIdentifier identifier, org.expasy.mzjava.proteomics.ms.ident.PeptideMatch searchResult) {

        try {
            Peptide peptide = searchResult.toPeptide();

            String key = params.spectrumPSMKeyFunction().get().apply(identifier, peptide);

            if (!psmMap.containsKey(key)) {
                psmMap.put(key,new ArrayList<>());
            }

            if (searchResult.getRank().isPresent()) {
                searchResult.getScoreMap().put("rank", searchResult.getRank().get());
            }

            boolean decoyFlag = false;
            if (params.flagDecoyProteins()) {
                if (searchResult.containsOnlyProteinMatch(decoyPattern)) decoyFlag = true;
            }

            if (searchResult.getProteinMatches().get(0).getHitType()== PeptideProteinMatch.HitType.DECOY) System.out.println(searchResult.getProteinMatches().get(0).getAccession()+": "+decoyFlag);
            System.out.println("==============================>"+searchResult.getProteinMatches().get(0).getAccession()+": "+decoyFlag);

            PeptideMatch psm = new PeptideMatch(peptide,
                    searchResult.getProteinMatches().stream().map(PeptideProteinMatch::getAccession),
                    searchResult.getScoreMap(),identifier.getAssumedCharge().get(),decoyFlag);


            psmMap.get(key).add(psm);

        } catch (UnresolvableModificationMatchException e) {

            LOGGER.log(Level.INFO, "Could not resolve modification for " + e.getMatch().getMassShift(), e);
            skippedPsmCounter.add(1);
        }
    }
}
