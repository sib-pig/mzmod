package org.expasy.mzjava.sparktool.psmconverter;

import org.apache.spark.Accumulator;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.ident.ModListModMatchResolver;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import scala.Tuple2;

import java.io.File;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PSMPairFMF implements PairFlatMapFunction<File, String, PeptideMatch> {

    private static final Logger LOGGER = Logger.getLogger(PSMPairFMF.class.getName());

    protected final Accumulator<Integer> skippedPsmCounter;
    protected final Accumulator<Integer> psmCounter;
    protected final PSMConverterParams params;
    protected Pattern decoyProtPattern;

    public PSMPairFMF(PSMConverterParams params, Accumulator<Integer> psmCounter, Accumulator<Integer> skippedPsmCounter) {

        this.psmCounter = psmCounter;
        this.skippedPsmCounter = skippedPsmCounter;
        this.params = params;

        this.decoyProtPattern = Pattern.compile(params.getDecoyProtPattern());

    }

    @Override
    public Iterator<Tuple2<String, PeptideMatch>> call(File file) throws Exception {

        LOGGER.info("Reading " + file);

        ModListModMatchResolver modMatchResolver = new ModListModMatchResolver(new AbsoluteTolerance(params.modMassTol()),
                params.modifications().get().stream().map(Modification::parseModification).collect(Collectors.toSet()));

        final Map<String, List<PeptideMatch>> psmMap = new HashMap<>();

        PSMReaderCallback callback = new PSMReaderCallback(params, psmMap, skippedPsmCounter);

        params.psmReaderFactory().get().newPsmReader(file,modMatchResolver,params.getDecoyProtPattern()).parse(file, callback);

        List<Tuple2<String, PeptideMatch>> tuples = extractMatches(psmMap);

        psmCounter.add(tuples.size());

        return tuples.iterator();
    }

    protected List<Tuple2<String, PeptideMatch>> extractMatches(Map<String, List<PeptideMatch>> psmMap) {

        List<Tuple2<String, PeptideMatch>> results = new ArrayList<>();

        for (String spectrumID : psmMap.keySet()) {

            List<PeptideMatch> psms = psmMap.get(spectrumID);

            for (PeptideMatch psm : psms) {

                if (!params.psmPredicate().get().test(psm)) continue;

                Set<String> trueACs =  removeDecoyProt(psm);
                if (!trueACs.isEmpty()) {
                    // removes decoys from ac list
                    results.add(new Tuple2<>(spectrumID,new PeptideMatch(psm.getPeptide(),trueACs.stream(),psm.getScoreMap(),psm.getCharge())));
                } else {
                    // in case only decoys
                    results.add(new Tuple2<>(spectrumID,psm));
                }

            }
        }

        return results;
    }

    private Set<String> removeDecoyProt(PeptideMatch psm) {

        Set<String> acs = psm.getProteinAcc();
        Set<String> trueACs = new HashSet<>(acs);

        Matcher matcher;
        for (String ac : acs) {
            matcher = decoyProtPattern.matcher(ac);
            if (matcher.find()) trueACs.remove(ac);
        }

        return trueACs;
    }

}
