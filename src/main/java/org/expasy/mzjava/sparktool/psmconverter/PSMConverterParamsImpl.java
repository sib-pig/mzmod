package org.expasy.mzjava.sparktool.psmconverter;

import com.google.common.collect.Lists;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.util.*;

import java.util.List;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class PSMConverterParamsImpl implements PSMConverterParams {

    // When reading PSM results files the modifications are mapped to the modifications defined here. Modifications are mapped by means of there mass only
    // (this will change in future versions, where the mapping will be done by mass and amino acid)
    private static final List<String> modifications =
            Lists.newArrayList("Oxidation:O", "Carbamidomethyl:C2H3NO", "Phospho:HO3P", "Deamidated:H-1N-1O", "Ammonia-loss:H-3N-1", "Acetyl:C2H2O", "Methyl:CH2", "Dimethyl:C2H4", "Trimethyl:C3H6", "Amidated:H-1", "Glu->pyro-Glu:H-2O-1", "Propionyl:C3H4O", "Pyro-carbamidomethyl:C2O");

    // Mass tolerance for the modification mapping. This value is independent of instrument precision, but refers to the precision of the modification mass values
    // indicated in the PSM result files.
    private static final double modifMatchMassTol =  0.015;

    // Remove decoy proteins from list of psm proteins for every PSM. remove PSM if there are only decoy proteins.
    private static final boolean flagDecoyProteins = true;
    private static final String decoyProtPattern = "decoy_";


    // Predicate to filter PeptideMatch objects before writing them to hadoop file
    private static final double minPsmScore = 0.0;
    private static final int minCharge = 2;
    private static final int maxCharge = 4;
    private static final int minPeptideLength = 7;
    private static final int maxPeptideLength = 40;
    private static final String scoreName = "hyperscore";
    private static final PeptideMatchPredicate.ScoreOrder order = PeptideMatchPredicate.ScoreOrder.LARGER;
    private static final int maxRank = 1;

    private static final PeptideMatchPredicate psmPredicate =
            new PeptideMatchPredicate(minCharge, maxCharge, minPeptideLength, maxPeptideLength, scoreName, minPsmScore, order, maxRank);

    // This object links the msn spectrum to the psm. This object maps the MsnSpectrum and corresponding PSM to the same key. This is of crucial importance
    // when creating consensus spectra of for spectrum visualization. E. g. new MgfTitleKeyFunction() or new XTandemMgfKeyFunction()
    private static final SpectrumKeyFunction<MsnSpectrum> spectrumPSMKeyFunction = new XTandemMgfKeyFunction();

    // MsnReaderFactory determines, which reader should be used for a psm file with a given extension. The default MzJava readers are
    // selected by PsmReaderFactoryImpl. In case these readers do not suffice, the user needs to implement his/hers own reader and configure
    // the PsmReaderFactory correspondingly.
    private static final PsmReaderFactory psmReaderFactory = new PsmReaderFactoryImpl();


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Implementation of getters (no need to change this part)
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public Cached<List<String>> modifications() {
        return new Cached<List<String>>() {
            @Override
            protected List<String> build() {
                return modifications;

            }
        };
    }

    @Override
    public double modMassTol() {
        return modifMatchMassTol;
    }

    @Override
    public Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumPSMKeyFunction() {
        return new Cached<SpectrumKeyFunction<MsnSpectrum>>() {
            @Override
            protected SpectrumKeyFunction<MsnSpectrum> build() {

                return spectrumPSMKeyFunction;
            }
        };
    }

    public Cached<PsmReaderFactory> psmReaderFactory() {

        return new Cached<PsmReaderFactory>() {
            @Override
            protected PsmReaderFactory build() {

                return psmReaderFactory;
            }
        };
    }

    public Cached<PeptideMatchPredicate> psmPredicate() {

        return new Cached<PeptideMatchPredicate>() {
            @Override
            protected PeptideMatchPredicate build() {

                return psmPredicate;
            }
        };
    }

    public String getDecoyProtPattern() {return decoyProtPattern;}
    public boolean flagDecoyProteins() {return flagDecoyProteins;}
}
