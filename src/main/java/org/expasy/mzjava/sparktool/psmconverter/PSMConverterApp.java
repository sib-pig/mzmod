package org.expasy.mzjava.sparktool.psmconverter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;
import org.expasy.mzjava.sparktool.util.RegExpFileFilter;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import org.expasy.mzjava.sparktool.util.SparkUtils;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkState;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PSMConverterApp extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(PSMConverterApp.class.getName());

    private String psmRootDirName;
    private boolean doSort;
    private Pattern regex;
    private String sparkDir;
    private boolean forceDelete;

    protected PSMConverterApp(String psmRootDirName, Pattern regex, String sparkDir, boolean doSort, boolean removeDecoyProteins, boolean forceDelete, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec, execMemoryMB, buffMemoryMB, runLocal);

        this.psmRootDirName = psmRootDirName;
        this.doSort = doSort;
        this.regex = regex;
        this.sparkDir = sparkDir;
        this.forceDelete = forceDelete;

        createOptions();
    }

    public PSMConverterApp() {

        createOptions();
    }

    public static void main(String[] args) throws IOException {


        PSMConverterApp PSMConverterApp =  new PSMConverterApp();
        try {
            PSMConverterApp.parseOptions(args).run();
        } catch (MissingOptionException e) {
            PSMConverterApp.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            PSMConverterApp.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public int run() throws IOException{

        long start = System.currentTimeMillis();

        LOGGER.info("Psm input path " + psmRootDirName);
        List<File> psmFileList = Arrays.asList(new File(psmRootDirName).listFiles(new RegExpFileFilter(regex)));
        checkState(!psmFileList.isEmpty());

        SparkConf conf = initSpark();

        JavaSparkContext sc = new JavaSparkContext(conf);

        PSMConverterParamsImpl params = new PSMConverterParamsImpl();

        Accumulator<Integer> skippedPsmCounter = sc.accumulator(0);
        Accumulator<Integer> psmCounter = sc.accumulator(0);

        JavaPairRDD<String, PeptideMatch> peptideMatches = sc.parallelize(psmFileList, psmFileList.size())
                .flatMapToPair(
                        new PSMPairFMF(params, psmCounter, skippedPsmCounter)
                )
                .setName("Peptide Matches")
                .persist(StorageLevel.MEMORY_AND_DISK());

        JavaPairRDD<Text, PeptideMatchValue> hadoopPsms = peptideMatches
                .sortByKey()
                .mapToPair(input -> new Tuple2<>(new Text(input._1()), new PeptideMatchValue(input._2())));

        String tmpDir = sparkDir +File.separator+"psm_part.hdpsm";
        String hdFile = sparkDir +File.separator+"psm.hdpsm";

        hadoopPsms.saveAsNewAPIHadoopFile(tmpDir, Text.class, PeptideMatchValue.class, SequenceFileOutputFormat.class);

        if (doSort)
            SparkUtils.mergeSortSequenceFiles(tmpDir, hdFile, Text.class, PeptideMatchValue.class, sc.hadoopConfiguration());
        else
            SparkUtils.mergeSequenceFiles(tmpDir,hdFile,Text.class,PeptideMatchValue.class,sc.hadoopConfiguration());

        LOGGER.info(psmCounter.value() + " PSM's, skipped " + skippedPsmCounter.value() + " PSM's due to unknown modifications");
        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    public String getPsmRootDirName() {
        return psmRootDirName;
    }

    public boolean isDoSort() {
        return doSort;
    }

    public Pattern getRegex() {
        return regex;
    }

    public String getSparkDir() {
        return sparkDir;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("psmDir").withDescription("spectrum root dierctory (required)").hasArg().withArgName("path").isRequired().create("pd"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("psmSort").withDescription("sort spectra in hadoop file").create("sp"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("psmRegex").withDescription("regular expression of psm files (e.g. \\.xml$)").hasArg().withArgName("string").create("rp"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdPsmDir").withDescription("spark output directory (required)").isRequired().hasArg().withArgName("path").create("hp"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("forceDelete").withDescription("force deleting existing spark output directory").create("fd"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.psmRootDirName = checkExistDirOption(line, "pd");
        this.regex = checkRegExOption(line, "rp");
        this.doSort = line.hasOption("sp")?true:false;

        this.forceDelete = false;
        if( line.hasOption( "fd" ) ) {
            this.forceDelete = true;
        }
        this.sparkDir = checkSparkDirOption(line, "hp",forceDelete);

    }

}
