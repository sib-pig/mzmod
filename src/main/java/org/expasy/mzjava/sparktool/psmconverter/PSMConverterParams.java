package org.expasy.mzjava.sparktool.psmconverter;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.util.PeptideMatchPredicate;
import org.expasy.mzjava.sparktool.util.PsmReaderFactory;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;

import java.io.Serializable;
import java.util.List;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface PSMConverterParams extends Serializable {

    Cached<List<String>> modifications();

    double modMassTol() ;

    Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumPSMKeyFunction();

    Cached<PsmReaderFactory> psmReaderFactory();

    Cached<PeptideMatchPredicate> psmPredicate();

    String getDecoyProtPattern();

    boolean flagDecoyProteins();
}
