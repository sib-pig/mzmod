package org.expasy.mzjava.sparktool.liberator;

import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;
import org.expasy.mzjava.sparktool.util.PeptideMatchPredicate;
import scala.Tuple2;

import java.util.Collections;
import java.util.Iterator;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
public class PeptideMatchFilter implements PairFlatMapFunction<Tuple2<Text, PeptideMatchValue>, String, PeptideMatch> {

    private final Cached<PeptideMatchPredicate> peptideMatchPredicate;

    public PeptideMatchFilter(Cached<PeptideMatchPredicate> peptideMatchPredicate) {

        this.peptideMatchPredicate = peptideMatchPredicate;
    }

    @Override
    public Iterator<Tuple2<String, PeptideMatch>> call(Tuple2<Text, PeptideMatchValue> input) throws Exception {

        PeptideMatch peptideMatch = input._2().get();
        if (peptideMatchPredicate.get().test(peptideMatch)) {

            return Collections.singleton(new Tuple2<>(input._1().toString(), peptideMatch)).iterator();
        } else {

            return Collections.emptyIterator();
        }
    }
}
