package org.expasy.mzjava.sparktool.liberator;

import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.Optional;
import org.apache.spark.storage.StorageLevel;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.ConsensusKey;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.deliberator.DeLiberatorFunction;
import org.expasy.mzjava.sparktool.deliberator.LibrarySpectrumShufflerFactory;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;
import org.expasy.mzjava.sparktool.util.PeptideMatchPredicate;
import org.expasy.mzjava.stats.statscores.FitDecoyScoreStatistics;
import scala.Tuple2;

import java.util.Collections;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class LiberatorApp {

//    private static final Logger LOGGER = Logger.getLogger(LiberatorApp.class.getName());

    private final LiberatorParameters parameters;

    public LiberatorApp(LiberatorParameters parameters) {

        this.parameters = parameters;
    }

    public JavaPairRDD<Optional<PeptideMatch>, MsnSpectrum> executePsmSpectraJoin(JavaPairRDD<Text, PeptideMatchValue> psmRdd, JavaPairRDD<SpectrumKey, MsnSpectrumValue> spectrumRdd) {

        //Read PSM's
        JavaPairRDD<String, PeptideMatch> peptideMatches = psmRdd
                .flatMapToPair(new PeptideMatchFilter(parameters.peptideMatchPredicate()))
                .setName("peptideMatches");

        if (parameters.filterFDR()) {

            String scoreName = parameters.peptideMatchPredicate().get().getScoreName();
            float threshold = calcFDRThreshold(peptideMatches, scoreName, parameters.qValue(), parameters.peptideMatchPredicate().get().getMinPsmScore());

            System.out.println("FDR " + parameters.qValue() + " threshold: " + threshold);

            PeptideMatchPredicate.ScoreOrder scoreOrder = parameters.peptideMatchPredicate().get().getOrder();

            if (scoreOrder== PeptideMatchPredicate.ScoreOrder.LARGER)
                peptideMatches = peptideMatches.filter(psm -> psm._2().getScore(scoreName) >= threshold);
            else
                peptideMatches = peptideMatches.filter(psm -> psm._2().getScore(scoreName) <= threshold);
        }

        //Read hdmsn spectra
        JavaPairRDD<String, MsnSpectrum> spectra = spectrumRdd.flatMapToPair(new SpectrumKeyFMF(parameters.spectrumPSMKeyFunction(), parameters.inputSpectrumPredicate()));

        //Join PSM's and spectra
        return peptideMatches.rightOuterJoin(spectra).mapToPair(Tuple2::_2)
                .setName("psmSpectraJoin")
                .persist(StorageLevel.MEMORY_AND_DISK());
    }

    public JavaRDD<PeptideConsensusSpectrum> createLibrary(JavaPairRDD<Optional<PeptideMatch>, MsnSpectrum> psmSpectraJoin) {

        return psmSpectraJoin
                .flatMapToPair(input -> {

                    Optional<PeptideMatch> optPsm = input._1();
                    if (optPsm.isPresent()) {

                        PeptideMatch psm = optPsm.get();
                        MsnSpectrum spectrum = input._2();

                        return Collections.singleton(new Tuple2<>(
//                                new ConsensusKey(spectrum.getPrecursor().getCharge(), psm.getPeptide().replace(EnumSet.of(AminoAcid.I, AminoAcid.L), AminoAcid.J)),
                                new ConsensusKey(spectrum.getPrecursor().getCharge(), psm.getPeptide()),
                                new ConsensusValue(psm.getProteinAcc(), psm.getScoreMap(), spectrum))).iterator();
                    } else {
                        return Collections.emptyIterator();
                    }
                })
                .groupByKey()
                .map(new ConsensusFunction(parameters))
                .setName("Consensus Spectra")
                .persist(StorageLevel.MEMORY_AND_DISK());
    }

    public JavaRDD<PeptideConsensusSpectrum> createDecoy(JavaRDD<PeptideConsensusSpectrum> consensusSpectra, LibrarySpectrumShufflerFactory spectrumShufflerFactory) {

        return consensusSpectra.flatMap(new DeLiberatorFunction(spectrumShufflerFactory))
                .persist(StorageLevel.MEMORY_AND_DISK())
                .setName("Decoy Spectra");
    }

    public JavaRDD<MsnSpectrum> getUnidentifiedSpectra(JavaPairRDD<Optional<PeptideMatch>, MsnSpectrum> psmSpectrumJoin) {

        return psmSpectrumJoin
                .flatMap(tuple -> {

                    Optional<PeptideMatch> optPepMatch = tuple._1();
                    return !optPepMatch.isPresent() ? Collections.singleton(tuple._2()).iterator() : Collections.emptyIterator();
                })
                .persist(StorageLevel.MEMORY_AND_DISK())
                .setName("Unidentified spectra");
    }

    public float calcFDRThreshold(JavaPairRDD<String, PeptideMatch> psmRdd, String scoreName, float qValue, double oldThreshold) {
        List<Double> targetScoreL = psmRdd
                .filter(psm -> !psm._2().isDecoy())
                .map(psm -> psm._2().getScore(scoreName)).collect();
        List<Double>  decoyScoreL = psmRdd
                .filter(psm -> psm._2().isDecoy())
                .map(psm -> psm._2().getScore(scoreName)).collect();

        if (targetScoreL.isEmpty() || decoyScoreL.isEmpty() )
            return (float) oldThreshold;

        double[] targetScores = new double[targetScoreL.size()];
        for (int i=0;i<targetScoreL.size();i++) targetScores[i] = targetScoreL.get(i);
        double[] decoyScores = new double[decoyScoreL.size()];
        for (int i=0;i<decoyScoreL.size();i++) decoyScores[i] = decoyScoreL.get(i);

        FitDecoyScoreStatistics fdrCalculator = new FitDecoyScoreStatistics(targetScores,decoyScores);

        return fdrCalculator.getScoreForQValue(qValue);
    }

}
