package org.expasy.mzjava.sparktool.liberator;

import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.expasy.mzjava.sparktool.deliberator.LibrarySpectrumShufflerFactory;
import org.expasy.mzjava.sparktool.util.PeptideMatchPredicate;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;

import java.io.Serializable;
import java.util.Comparator;
import java.util.function.Predicate;

/**
 * @author Markus Muller
 * @version sqrt -1
 */
public interface LiberatorParameters extends Serializable {

    Cached<PeptideMatchPredicate> peptideMatchPredicate();

    Cached<Comparator<ConsensusValue>> consensusValueComparator();

    PeakList.Precision precision();

    Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumPSMKeyFunction();

    Cached<PeakProcessorChain> processorChain();

    Cached<PeptideConsensusSpectrum.Builder> consensusBuilder();

    int maxConsensusMembers();

    Cached<Predicate<MsnSpectrum>> inputSpectrumPredicate();

    Cached<ConsensusMemberSelector> consensusMemberSelector();

    Cached<LibrarySpectrumShufflerFactory> spectrumShufflerFactory();

    Cached<Tolerance> precursorTolerance();

    Cached<Tolerance> fragmentTolerance();

    boolean filterFDR();

    float qValue();
}
