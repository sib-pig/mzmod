package org.expasy.mzjava.sparktool.liberator;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;
import org.expasy.mzjava.sparktool.util.SparkRunner;
import org.expasy.mzjava.sparktool.util.SparkUtils;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author Oliver Horlacher
 * @author Markus Mueller
 * @version sqrt -1
 */
public class LiberatorRunner extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(LiberatorRunner.class.getName());

    private String hdSpectraPath;
    private String hdPsmPath;
    private boolean indexConsensus;
    private String sparkDir;
    private boolean forceDelete;

    public LiberatorRunner(String hdSpectraPath, String hdPsmPath, String sparkDir, boolean indexConsensus, boolean forceDelete, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,buffMemoryMB,runLocal);

        this.hdSpectraPath = hdSpectraPath;
        this.hdPsmPath = hdPsmPath;
        this.indexConsensus = indexConsensus;
        this.sparkDir = sparkDir;
        this.forceDelete = forceDelete;

        createOptions();

    }

    public LiberatorRunner() {

        createOptions();
    }

    public static void main(String[] args) {

        LiberatorRunner liberatorRunner =  new LiberatorRunner();
        try {
            liberatorRunner.parseOptions(args).run();
        } catch (MissingOptionException e) {
            liberatorRunner.printOptions(args,e.getMessage());
        } catch (ParseException e) {
            liberatorRunner.printOptions(args, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String getHdSpectraPath() {
        return hdSpectraPath;
    }

    public String getHdPsmPath() {
        return hdPsmPath;
    }

    public String getSparkDir() {
        return sparkDir;
    }

    public boolean isIndexConsensus() {
        return indexConsensus;
    }

    public int run() throws  IOException{

        long start = System.currentTimeMillis();

        SparkConf conf = initSpark();

        final JavaSparkContext sc = new JavaSparkContext(conf);

        LiberatorParameters parameters = new LiberatorParametersImpl();

        LiberatorApp app = new LiberatorApp(parameters);

        //Read PSM's and Spectra
        final JavaPairRDD<Text, PeptideMatchValue> psmRdd = sc.sequenceFile(hdPsmPath, Text.class, PeptideMatchValue.class);
        final JavaPairRDD<SpectrumKey, MsnSpectrumValue> spectrumRdd = sc.sequenceFile(hdSpectraPath + "/*", SpectrumKey.class, MsnSpectrumValue.class);
        JavaPairRDD<Optional<PeptideMatch>, MsnSpectrum> psmSpectraJoin = app.executePsmSpectraJoin(psmRdd, spectrumRdd);

        psmSpectraJoin.mapToPair(new ToleranceCheckFunction(parameters.precursorTolerance()));

        // Create library
        JavaRDD<PeptideConsensusSpectrum> consensusSpectra = app.createLibrary(psmSpectraJoin);
        final long consensusCount = consensusSpectra.count();
        if (indexConsensus) {
            SparkUtils.saveIndexedLibrary(sc, sparkDir + "/indexed_library", consensusSpectra, parameters.processorChain());
        } else {
            SparkUtils.saveLibrary(sc, sparkDir + "/raw_library", consensusSpectra);
        }

        //Create Decoy
        JavaRDD<PeptideConsensusSpectrum> decoySpectra = app.createDecoy(consensusSpectra, parameters.spectrumShufflerFactory().get());
        final long decoyCount = decoySpectra.count();
        if (indexConsensus) {
            SparkUtils.saveIndexedLibrary(sc, sparkDir + "/indexed_decoy", decoySpectra, parameters.processorChain());
        } else {
            SparkUtils.saveLibrary(sc, sparkDir + "/raw_decoy", decoySpectra);
        }

        //Find unidentified spectra
        JavaRDD<MsnSpectrum> unidentifiedSpectra = app.getUnidentifiedSpectra(psmSpectraJoin);
        final long unidentifiedCount = unidentifiedSpectra.count();
        SparkUtils.saveSortMsnSpectra(sc, sparkDir + "/unidentified_spectra", unidentifiedSpectra);

        LOGGER.info("Created " + consensusCount + " consensus");
        LOGGER.info("Created " + decoyCount + " decoys");
        LOGGER.info("Spectra unidentified " + unidentifiedCount);
        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdMsn").withDescription("spectrum sequence file (required)").hasArg().withArgName("path").isRequired().create("hs"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdPsm").withDescription("psm sequence file (required)").hasArg().withArgName("path").isRequired().create("hp"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("indexCons").withDescription("index peaks in consensus spectra").create("ic"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdLibDir").withDescription("output directory (required)").isRequired().hasArg().withArgName("path").create("hl"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("forceDelete").withDescription("force deleting existing spark output directory").create("fd"));
    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.hdSpectraPath = checkExistFileOption(line,"hs");
        this.hdPsmPath = checkExistFileOption(line,"hp");

        this.forceDelete = false;
        if( line.hasOption( "fd" ) ) {
            this.forceDelete = true;
        }
        this.sparkDir = checkSparkDirOption(line, "hl",forceDelete);

        this.indexConsensus = false;
        if( line.hasOption( "ic" ) ) {
            this.indexConsensus = true;
        }
    }

}
