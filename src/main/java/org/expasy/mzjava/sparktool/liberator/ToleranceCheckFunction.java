package org.expasy.mzjava.sparktool.liberator;

import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.PairFunction;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import scala.Tuple2;

/**
 * Function that checks that the theoretical and actual m/z are within tolerance.
 * <p>
 * This can be used to remove PSM's that are assigned to the second isotope
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
class ToleranceCheckFunction implements PairFunction<Tuple2<Optional<PeptideMatch>, MsnSpectrum>, Optional<PeptideMatch>, MsnSpectrum> {

    private final Cached<Tolerance> precursorTolerance;

    public ToleranceCheckFunction(Cached<Tolerance> precursorTolerance) {

        this.precursorTolerance = precursorTolerance;
    }

    @Override
    public Tuple2<Optional<PeptideMatch>, MsnSpectrum> call(Tuple2<Optional<PeptideMatch>, MsnSpectrum> tuple) throws Exception {

        if (tuple._1().isPresent()) {

            final Peptide peptide = tuple._1().get().getPeptide();
            final Peak precursor = tuple._2().getPrecursor();
            if (precursorTolerance.get().withinTolerance(peptide.calculateMz(precursor.getCharge()), precursor.getMz()))
                return tuple;
            else
                return new Tuple2<>(Optional.<PeptideMatch>absent(), tuple._2());
        } else {

            return tuple;
        }
    }
}
