package org.expasy.mzjava.sparktool.liberator;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.NPeaksPerSlidingWindowFilter;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.RankNormalizer;
import org.expasy.mzjava.core.ms.spectrasim.NdpSimFunc;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.DefaultPeakListAligner;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.transformer.PeakPairIntensitySqrtTransformer;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.consensus.shuffling.ControlledLibrarySpectrumShuffler;
import org.expasy.mzjava.proteomics.ms.consensus.shuffling.LibrarySpectrumShuffler;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.CachedPpmTolerance;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.expasy.mzjava.sparktool.deliberator.LibrarySpectrumShufflerFactory;
import org.expasy.mzjava.sparktool.psmconverter.PSMConverterParams;
import org.expasy.mzjava.sparktool.psmconverter.PSMConverterParamsImpl;
import org.expasy.mzjava.sparktool.util.PeptideMatchPredicate;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import org.expasy.mzjava.utils.URIBuilder;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.function.Predicate;

/**
 * @author Markus Muller
 * @version sqrt -1
 */
public class LiberatorParametersImpl implements LiberatorParameters {

    /*
    Score threshold used to fragment minimal spanning tree (see MzMod publication for details: Horlacher et al, JPR, 2016)
     */
    private static final double minClusterNdpScore = 0.6;
    /*
    Maximum number of spectra to calculate consensus. The algorithm selects the PSMs with maxConsensusMembers highest scores
    for consensus spectrum calculation
     */
    private static final int maxConsensusMembers = 300;
    /*
    Peaks in consensus spectra are filtered according to their reproducibility. For more than nrSpectra spectra included for consensus creation (nrSpectra>1),
    a peak has to be present in at least minPeakCountPerConsensus and minPeakProbPerConsensus*nrSpectra MS/MS spectra.
     */
    private static final int minPeakCountPerConsensus = 2;
    private static final float minPeakProbPerConsensus = 0.2f;

    // MS/MS fragment ion mz tolerance for peak annotation
    private static final double absFragmentTolerance = 0.05;

    // PeptideFragmenter used for peak annotation. This object defines ion types included for annotations. It is also possible to include
    // neutral losses (see documentation in MzJava)
    private static final PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.a, IonType.b, IonType.y), PeakList.Precision.DOUBLE_CONSTANT);

    /*
    Consensus spectrum builder object. See PeptideConsensusSpectrum class in MzJava for more details.
     */
    private static final PeptideConsensusSpectrum.Builder consensusBuilder = PeptideConsensusSpectrum.builder(PeakList.Precision.FLOAT, new URIBuilder("org.expasy", "liberator").build())
            .setConsensusParameters(absFragmentTolerance, absFragmentTolerance, AbstractMergePeakFilter.IntensityMode.SUM_INTENSITY)
            .setAnnotationParameters(new AbsoluteTolerance(absFragmentTolerance), fragmenter)
            .setFilterParameters(minPeakProbPerConsensus, minPeakCountPerConsensus);

    /*
    MSTConsensusMemberSelector implements the algorithm to select the spectra included in the consensus spectra calculation
    (see MzMod publication for details: Horlacher et al, JPR, 2016). Any implementation of ConsensusMemberSelector class could
    be assigned to consensusMemberSelector
     */
    private static final ConsensusMemberSelector consensusMemberSelector = new MSTConsensusMemberSelector(
            new MaxSimScoreClusterSelector(),
            new NdpSimFunc<>(0, new DefaultPeakListAligner<>(new AbsoluteTolerance(absFragmentTolerance)), new PeakPairIntensitySqrtTransformer<>()),
            minClusterNdpScore);

    /*
    PSM predicate. Only PSMs passing this filter are considered for consensus creation and inserted into the library.
     */
    private static final double minPsmScore = 0;
    private static final int minCharge = 2;
    private static final int maxCharge = 4;
    private static final int minPeptideLength = 7;
    private static final int maxPeptideLength = 40;
    private static final String scoreName = "hyperscore";
    private static final PeptideMatchPredicate.ScoreOrder order = PeptideMatchPredicate.ScoreOrder.LARGER;
    private static final double precursorTolerance = 10;
    private static final int maxRank = 1;
    private static final boolean filterFDR = true;
    private static final float qValue = 0.01f;

    private static final PeptideMatchPredicate psmPredicate =
            new PeptideMatchPredicate(minCharge, maxCharge, minPeptideLength, maxPeptideLength, scoreName, minPsmScore, order, maxRank);

    /*
    Spectrum predicate. Only spectra in the hadoop file passing this filter are considered for consensus creation. This filter is additional to the filter in Msn2HdmsnParametersImpl,
    which selects the spectra written to the hadoop file.
     */
    private static final Predicate<MsnSpectrum> msnSpectrumPredicate = spectrum -> spectrum.getPrecursor().getCharge() > 0;

    /*
    Decoy library spectra creation parameters. The method for decoy library creation is described in Ahrne, Ohta et al. Proteomics, 2011 (doi: 10.1002/pmic.201000665).
    In brief, a decoy spectrum is created by shuffling each library spectrum.
     */

    // What do do with precursor peak : REPLACE,KEEP,KEEP_CHARGE_REDUCED
    private static final LibrarySpectrumShuffler.PrecursorHandling precHandling = LibrarySpectrumShuffler.PrecursorHandling.KEEP;
    // Decoy spectra cannot be too similar to original spectrum
    private static final double similarityThresh = 0.9;
    // Number of trials to find decoy spectrum not too similar to original spectrum
    private static final int maxNrIter = 10;
    // Peak with no annotation are changed with this probability
    private static final double noAnnatPeakChangeProb = 0.2;
    // Peaks with no annotation which are changed are shifted by this mass shift
    private static final double mzShift = 7.0;
    private static final ControlledLibrarySpectrumShuffler consensusShuffler =
            new ControlledLibrarySpectrumShuffler(precHandling, similarityThresh, maxNrIter, noAnnatPeakChangeProb, mzShift);

    // While shuffling the peptide sequence what to do with the termini: KEEP_CTERM,KEEP_NTERM,KEEP_TERMINI,KEEP_NONE
    private static final LibrarySpectrumShuffler.PeptideTerminiHandling peptideTerminiHandling = LibrarySpectrumShuffler.PeptideTerminiHandling.KEEP_CTERM;


    // Here successive processing steps of the spectra prior to conversion to a hadoop file can be defined. This step is only applied for
    // indexed library spectra used for MzMod OMS searches, and not for normal spectrum library creation!! This spectrum processing can
    // have a strong influence on the MzMod results, which alo depends on the scoring function used.
    // No processing is done if no PeakProcessors are added.
    private static final PeakProcessorChain<PeakAnnotation> specProcessorChain = new PeakProcessorChain<>()
            .add(new NPeaksPerSlidingWindowFilter<>(2, 10))
            .add(new RankNormalizer<>());


    // Needed for SpectrumKeyFunction<MsnSpectrum>, which has to be the same in psm file conversion
    private static final PSMConverterParams PSM_CONVERTER_PARAMS = new PSMConverterParamsImpl();



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Implementation of getters (no need to change this part)
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    /*
    This function returns a predicate to filter PeptideMatch objects (see implementation of Predicate interface
    for details.
     */
    public Cached<PeptideMatchPredicate> peptideMatchPredicate() {

        return new Cached<PeptideMatchPredicate>() {
            @Override
            protected PeptideMatchPredicate build() {

                return psmPredicate;
            }
        };
    }

    @Override
    /*
    Comparator to sort PSMs for consensus building
     */
    public Cached<Comparator<ConsensusValue>> consensusValueComparator() {

        return new Cached<Comparator<ConsensusValue>>() {
            @Override
            protected Comparator<ConsensusValue> build() {

                return (v1, v2) -> Double.compare(v2.getScore(scoreName), v1.getScore(scoreName));
            }
        };
    }

    @Override
    /*
    Precision for PeakList masses, float will do in most cases.
     */
    public PeakList.Precision precision() {

        return PeakList.Precision.FLOAT;
    }

    @Override
    /*
    SpectrumKeyFunction class is used to map the PSMs objects to MsnSpectrum objects.
     */
    public Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumPSMKeyFunction() {
        return PSM_CONVERTER_PARAMS.spectrumPSMKeyFunction();
    }

    @Override
    public Cached<PeakProcessorChain> processorChain() {

        return new Cached<PeakProcessorChain>() {
            @Override
            protected PeakProcessorChain build() {

                return specProcessorChain;
            }
        };
    }

    @Override
    public int maxConsensusMembers() {

        return maxConsensusMembers;
    }

    @Override
    /*
    Only spectra with Predicate value true will be included in the library creation.
     */
    public Cached<Predicate<MsnSpectrum>> inputSpectrumPredicate() {

        return new Cached<Predicate<MsnSpectrum>>() {
            @Override
            protected Predicate<MsnSpectrum> build() {

                return msnSpectrumPredicate;
            }
        };
    }

    @Override
    /*
    Fragment tolerance
     */
    public Cached<Tolerance> fragmentTolerance() {

        return new Cached<Tolerance>() {
            @Override
            protected Tolerance build() {
                return new AbsoluteTolerance(absFragmentTolerance);
            }
        };
    }

    @Override
    /*
    Consensus spectrum builder class
     */
    public Cached<PeptideConsensusSpectrum.Builder> consensusBuilder() {

        return new Cached<PeptideConsensusSpectrum.Builder>() {
            @Override
            protected PeptideConsensusSpectrum.Builder build() {

                return consensusBuilder;
            }
        };
    }

    @Override
    public Cached<ConsensusMemberSelector> consensusMemberSelector() {

        return new Cached<ConsensusMemberSelector>() {
            @Override
            protected ConsensusMemberSelector build() {

                //noinspection unchecked
                return consensusMemberSelector;
            }
        };
    }


    @Override
    public Cached<LibrarySpectrumShufflerFactory> spectrumShufflerFactory() {

        return new Cached<LibrarySpectrumShufflerFactory>() {
            @Override
            protected LibrarySpectrumShufflerFactory build() {
                return new LibrarySpectrumShufflerFactory() {


                    @Override
                    public ControlledLibrarySpectrumShuffler newSpectrumShuffler() {
                        return consensusShuffler;
                    }

                    @Override
                    public LibrarySpectrumShuffler.PeptideTerminiHandling getPeptideTerminiHandling() {
                        return peptideTerminiHandling;
                    }
                };
            }
        };
    }

    @Override
    public Cached<Tolerance> precursorTolerance() {

        return new CachedPpmTolerance(precursorTolerance);
    }

    @Override
    public boolean filterFDR() {return filterFDR;}

    @Override
    public float qValue(){return qValue;}


}