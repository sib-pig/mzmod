package org.expasy.mzjava.sparktool.liberator;

import org.expasy.mzjava.core.ms.cluster.SimEdge;
import org.expasy.mzjava.core.ms.cluster.SimilarityGraph;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.expasy.mzjava.utils.function.Procedure;

import java.util.Collection;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MaxSimScoreClusterSelector implements ClusterSelector {

    @Override
    public Set<ConsensusValue> getBestCluster(Collection<Set<ConsensusValue>> clusters, SimilarityGraph<ConsensusValue> simGraph) {

        // get largest cluster
        double maxClusterScore = 0.0;
        Set<ConsensusValue> bestCluster = null;
        for (Set<ConsensusValue> cluster : clusters) {

            SumProcedure<ConsensusValue> sumProcedure = new SumProcedure<>();
            for (ConsensusValue spectrum : cluster) {
                simGraph.forEachEdge(spectrum, sumProcedure);
            }
            double clusterScore = sumProcedure.getSum();
            if (clusterScore >= maxClusterScore) {
                maxClusterScore = clusterScore;
                bestCluster = cluster;
            }
        }

        return bestCluster;
    }

    private static class SumProcedure<S> implements Procedure<SimEdge<S>> {

        private double sum = 0;

        @Override
        public void execute(SimEdge<S> score) {

            sum += score.getScore();
        }

        public double getSum() {

            return sum;
        }
    }
}
