package org.expasy.mzjava.sparktool.liberator;

import org.expasy.mzjava.core.ms.cluster.*;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.spectrasim.SimFunc;
import org.expasy.mzjava.sparktool.data.ConsensusValue;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MSTConsensusMemberSelector implements ConsensusMemberSelector {

    private final SimFunc<PeakAnnotation, PeakAnnotation> simFunc;
    private final double scoreThreshold;
    private final ClusterSelector clusterSelector;
    private final ClusterBuilder<ConsensusValue> clusterBuilder;

    public MSTConsensusMemberSelector(ClusterSelector clusterSelector, SimFunc<PeakAnnotation, PeakAnnotation> simFunc, double scoreThreshold) {

        this.clusterSelector = clusterSelector;
        this.simFunc = simFunc;
        this.scoreThreshold = scoreThreshold;
        this.clusterBuilder = new MSTClusterBuilder<>(scoreThreshold);
    }

    @Override
    public Collection<ConsensusValue> selectSpectra(List<ConsensusValue> candidates) {

        boolean removeOutliers = false;
        final SimilarityGraphBuilder<ConsensusValue, DenseSimilarityGraph<ConsensusValue>> simGraphBuilder = new DenseSimilarityGraph.Builder<>();

        final int size = candidates.size();
        for (int i = 0; i < size; i++) {

            ConsensusValue spectrum1 = candidates.get(i);
            simGraphBuilder.add(spectrum1);
            for (int j = i + 1; j < size; j++) {

                ConsensusValue spectrum2 = candidates.get(j);
                simGraphBuilder.add(spectrum2);

                //noinspection unchecked
                double score = simFunc.calcSimilarity(spectrum1.getSpectrum(), spectrum2.getSpectrum());
                if (!Double.isNaN(score) && score >= scoreThreshold) {

                    simGraphBuilder.add(spectrum1, spectrum2, score);
                } else {

                    removeOutliers = true;
                }
            }
        }

        return removeOutliers ? evaluate(simGraphBuilder.build()) : candidates;
    }

    private Collection<ConsensusValue> evaluate(SimilarityGraph<ConsensusValue> simGraph) {

        Collection<Set<ConsensusValue>> clusters = clusterBuilder.cluster(simGraph);
        int clusterCount = clusters.size();
        if (clusterCount == 1) {

            return clusters.iterator().next();
        } else {

            return clusterSelector.getBestCluster(clusters, simGraph);
        }
    }
}
