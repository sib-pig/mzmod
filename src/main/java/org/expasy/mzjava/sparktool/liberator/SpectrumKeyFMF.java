package org.expasy.mzjava.sparktool.liberator;

import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import scala.Tuple2;

import java.util.Collections;
import java.util.Iterator;
import java.util.function.Predicate;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
public class SpectrumKeyFMF implements PairFlatMapFunction<Tuple2<SpectrumKey, MsnSpectrumValue>, String, MsnSpectrum> {

    private final Cached<SpectrumKeyFunction<MsnSpectrum>> keyFunction;
    private final Cached<Predicate<MsnSpectrum>> inputSpectrumPredicate;

    public SpectrumKeyFMF(Cached<SpectrumKeyFunction<MsnSpectrum>> keyFunction, Cached<Predicate<MsnSpectrum>> inputSpectrumPredicate) {

        this.inputSpectrumPredicate = inputSpectrumPredicate;
        this.keyFunction = keyFunction;
    }

    @Override
    public Iterator<Tuple2<String, MsnSpectrum>> call(Tuple2<SpectrumKey, MsnSpectrumValue> v1) throws Exception {

        //noinspection unchecked
        MsnSpectrum spectrum = v1._2().get();
        if (inputSpectrumPredicate.get().test(spectrum)) {
            return Collections.singletonList(new Tuple2<>(keyFunction.get().apply(spectrum), spectrum)).iterator();
        } else {
            return Collections.emptyIterator();
        }
    }
}
