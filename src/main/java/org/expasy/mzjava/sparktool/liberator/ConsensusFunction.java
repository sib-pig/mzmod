package org.expasy.mzjava.sparktool.liberator;

import com.google.common.collect.Lists;
import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeList;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.ConsensusKey;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import scala.Tuple2;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class ConsensusFunction implements Function<Tuple2<ConsensusKey, Iterable<ConsensusValue>>, PeptideConsensusSpectrum> {

    private static final Logger LOGGER = Logger.getLogger(ConsensusFunction.class.getName());

    private final Cached<ConsensusMemberSelector> consensusMemberSelector;
    private final int maxMemberCount;
    private final Cached<PeptideConsensusSpectrum.Builder> consensusBuilder;
    private final Cached<Comparator<ConsensusValue>> consensusValueComparator;

    public ConsensusFunction(LiberatorParameters parameters) {

        this.consensusMemberSelector = parameters.consensusMemberSelector();
        this.consensusBuilder = parameters.consensusBuilder();
        this.maxMemberCount = parameters.maxConsensusMembers();
        this.consensusValueComparator = parameters.consensusValueComparator();
    }

    @Override
    public PeptideConsensusSpectrum call(Tuple2<ConsensusKey, Iterable<ConsensusValue>> input) throws Exception {

        List<ConsensusValue> consensusValues = Lists.newArrayList(input._2());
        if(consensusValues.size() > maxMemberCount) {

            LOGGER.info("Trimmed consensus members from " + consensusValues.size() + " to " + maxMemberCount);
            Collections.sort(consensusValues, consensusValueComparator.get());
            consensusValues = consensusValues.subList(0, maxMemberCount);
        }

        Collection<ConsensusValue> coreConsensusValues = consensusMemberSelector.get().selectSpectra(consensusValues);
        List<MsnSpectrum> coreSpectra = new ArrayList<>(coreConsensusValues.size());
        Set<String> proteinAccessionNumbers = new HashSet<>();
        RetentionTimeList retentionTimes = new RetentionTimeList();
        for (ConsensusValue consensusValue : coreConsensusValues) {

            //noinspection unchecked
            coreSpectra.add(consensusValue.getSpectrum());
            proteinAccessionNumbers.addAll(consensusValue.getProteinAccSet());

            RetentionTimeList rtList = consensusValue.getSpectrum().getRetentionTimes();
            if (rtList != null) retentionTimes.addAll(rtList);
        }

        final ConsensusKey consensusKey = input._1();

        return consensusBuilder.get().buildConsensus(consensusKey.getCharge(), consensusKey.getPeptide(), coreSpectra, proteinAccessionNumbers, retentionTimes);
    }

}
