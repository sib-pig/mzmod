package org.expasy.mzjava.sparktool.util;

import gnu.trove.list.TDoubleList;
import org.expasy.mzjava.core.ms.peaklist.FloatPeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
* @author Oliver Horlacher
* @version sqrt -1
*/
public class Externalizables {

    private Externalizables() {
    }

    public static void writePeakList(FloatPeakList peakList, ObjectOutput out) throws IOException {

        out.writeInt(peakList.size());
        for(int i = 0; i < peakList.size(); i++){

            out.writeFloat((float)peakList.getMz(i));
            out.writeFloat((float)peakList.getIntensity(i));
        }
    }

    public static void writeDoubleList(TDoubleList list, ObjectOutput out) throws IOException {

        final int size = list.size();
        out.writeInt(size);
        for(int i = 0; i < size; i++){

            out.writeDouble(list.get(i));
        }
    }

    public static void writeArray(double[] doubles, ObjectOutput out) throws IOException {

        final int size = doubles.length;
        out.writeInt(size);
        for (double aDouble : doubles) {

            out.writeDouble(aDouble);
        }
    }

    public static FloatPeakList<PeakAnnotation> readFloatPeakList(ObjectInput in) throws IOException {

        int size = in.readInt();
        FloatPeakList<PeakAnnotation> peakList = new FloatPeakList<>(size);
        for(int i = 0; i < size; i++){

            peakList.add(in.readFloat(), in.readFloat());
        }

        return peakList;
    }

    public static double[] readArray(ObjectInput in) throws IOException {

        final double[] doubles = new double[in.readInt()];
        for (int i = 0; i < doubles.length; i++) {

            doubles[i] = in.readDouble();
        }
        return doubles;
    }
}
