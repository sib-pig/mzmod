package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.stats.Classification;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class RocValue {

    private final double score;
    private final Classification classification;

    public RocValue(double score, Classification classification) {

        this.score = score;
        this.classification = classification;
    }

    public double getScore() {

        return score;
    }

    public Classification getClassification() {

        return classification;
    }
}
