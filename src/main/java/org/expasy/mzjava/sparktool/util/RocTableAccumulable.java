package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.stats.Classification;

import java.text.NumberFormat;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class RocTableAccumulable {

    private final double[] thresholds;
    private final int[] truePositives;
    private final int[] falsePositives;
    private final int[] trueNegatives;
    private final int[] falseNegatives;

    private final double min;
    private final double max;
    private final double thresholdIncrements;


    public RocTableAccumulable(double min, double max, double thresholdIncrements) {

        this.min = min;
        this.max = max;
        this.thresholdIncrements = thresholdIncrements;

        int count = (int)((max - min)/thresholdIncrements) + 1;

        thresholds = new double[count];
        truePositives = new int[thresholds.length];
        falsePositives = new int[thresholds.length];
        trueNegatives = new int[thresholds.length];
        falseNegatives = new int[thresholds.length];

        double t = min;
        for (int i = 0; i < thresholds.length; i++) {

            thresholds[i] = t;
            t += thresholdIncrements;
        }
    }

    public double getMin() {

        return min;
    }

    public double getMax() {

        return max;
    }

    public double getThresholdIncrements() {

        return thresholdIncrements;
    }

    public void add(RocValue rocValue) {

        final Classification classification = rocValue.getClassification();
        final double score = rocValue.getScore();

        for (int i = 0; i < thresholds.length; i++) {

            double threshold = thresholds[i];

            if(classification == Classification.SAME) {

                if(score >= threshold) truePositives[i]++;
                else falseNegatives[i]++;
            } else {

                if(score < threshold) trueNegatives[i]++;
                else falsePositives[i]++;
            }
        }
    }

    @Override
    public String toString() {

        StringBuilder buff = new StringBuilder();

        NumberFormat format = NumberFormat.getInstance();

        buff.append("threshold\ttp\tfp\ttn\tfn\terror\tfp rate\ttp rate\n");

        for (int i = 0; i < thresholds.length; i++) {

            int tp = truePositives[i];
            int fp = falsePositives[i];
            int tn = trueNegatives[i];
            int fn = falseNegatives[i];

            buff.append(format.format(thresholds[i]));
            buff.append('\t');
            buff.append(tp);
            buff.append('\t');
            buff.append(fp);
            buff.append('\t');
            buff.append(tn);
            buff.append('\t');
            buff.append(fn);
            buff.append('\t');
            buff.append((fp + fn) / (double) (tp + fp + tn + fn));
            buff.append('\t');
            buff.append(fp / (double) (fp + tn));
            buff.append('\t');
            buff.append(tp / (double) (tp + fn));
            buff.append('\n');
        }

        return buff.toString();
    }

    public void add(RocTableAccumulable rocTable){

        checkArgument(rocTable.min == min);
        checkArgument(rocTable.max == max);
        checkArgument(rocTable.thresholdIncrements == thresholdIncrements);

        add(rocTable.truePositives, truePositives);
        add(rocTable.falsePositives, falsePositives);
        add(rocTable.trueNegatives, trueNegatives);
        add(rocTable.falseNegatives, falseNegatives);
    }

    private void add(int[] src, int[] dest){

        for (int i = 0; i < src.length; i++) {

            dest[i] += src[i];
        }
    }
}
