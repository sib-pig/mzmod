package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class XTandemMgfKeyFunction implements SpectrumKeyFunction<MsnSpectrum> {

    @Override
    public String apply(SpectrumIdentifier identifier, Peptide peptide) {

        final String spectrum = identifier.getSpectrum();
        final int scanNumber = identifier.getScanNumbers().getFirst().getValue();

        return String.format("%s.%d", spectrum.substring(0, spectrum.indexOf('.')), scanNumber);
    }

    @Override
    public String apply(MsnSpectrum spectrum) {

        final String comment = spectrum.getComment();

        int firstDot = comment.indexOf('.');

        return comment.substring(0, firstDot) + "." + (spectrum.getSpectrumIndex() + 1);
    }

    @Override
    public String getRunID(MsnSpectrum spectrum) {

        final String comment = spectrum.getComment();

        int firstDot = comment.indexOf('.');

        return comment.substring(0, firstDot);
    }


}
