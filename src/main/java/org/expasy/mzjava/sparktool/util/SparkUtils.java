package org.expasy.mzjava.sparktool.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.ReflectionUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.DefaultSpectrumKeyRawComparator;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.hadoop.io.PeptideConsensusSpectrumValue;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.hadoop.IndexedPeptideSpectrumValue;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;
import org.expasy.mzjava.sparktool.liberator.PeptideMatchFilter;
import scala.Tuple2;

import java.io.IOException;
import java.util.Collections;
import java.util.function.Predicate;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SparkUtils {

    private SparkUtils() {}

    public static void saveSortMsnSpectra(JavaSparkContext sc, String outputPath, JavaRDD<MsnSpectrum> msnSpectra) throws IOException {

        final String tmpPath = outputPath + "_part.hdmsn";
        msnSpectra
                .mapToPair(spectrum -> {

                    SpectrumKey key = new SpectrumKey();
                    key.set(spectrum.getPrecursor());
                    MsnSpectrumValue value = new MsnSpectrumValue();
                    value.set(spectrum);
                    return new Tuple2<>(key, value);
                })
                .saveAsNewAPIHadoopFile(tmpPath, SpectrumKey.class, MsnSpectrumValue.class, SequenceFileOutputFormat.class);

        mergeSortSequenceFiles(tmpPath, outputPath + ".hdmsn", SpectrumKey.class, MsnSpectrumValue.class, sc.hadoopConfiguration());
    }

    public static void saveMsnSpectra(JavaSparkContext sc, String outputPath, JavaRDD<MsnSpectrum> msnSpectra) throws IOException {

        final String tmpPath = outputPath + "_part.hdmsn";
        msnSpectra
                .mapToPair(spectrum -> {

                    SpectrumKey key = new SpectrumKey();
                    key.set(spectrum.getPrecursor());
                    MsnSpectrumValue value = new MsnSpectrumValue();
                    value.set(spectrum);
                    return new Tuple2<>(key, value);
                })
                .saveAsNewAPIHadoopFile(tmpPath, SpectrumKey.class, MsnSpectrumValue.class, SequenceFileOutputFormat.class);

        mergeSequenceFiles(tmpPath, outputPath + ".hdmsn", SpectrumKey.class, MsnSpectrumValue.class, sc.hadoopConfiguration());
    }

    public static void saveIndexedLibrary(JavaSparkContext sc, String outputPath, final JavaRDD<PeptideConsensusSpectrum> consensusSpectra, Cached<PeakProcessorChain> processorChain) throws IOException {

        final String tmpPath = outputPath + "_parts.hdlib";
        consensusSpectra
                .mapToPair(spectrum -> {

                    SpectrumKey key = new SpectrumKey();
                    key.set(spectrum.getPrecursor());
                    final IndexedPeptideSpectrum indexedSpectrum = new IndexedPeptideSpectrum(spectrum.getPeptide(), spectrum.getProteinAccessionNumbers(), spectrum.getMemberIds().size(), spectrum, processorChain.get());
                    IndexedPeptideSpectrumValue value = new IndexedPeptideSpectrumValue(indexedSpectrum);
                    return new Tuple2<>(key, value);
                })
                .saveAsNewAPIHadoopFile(tmpPath, SpectrumKey.class, IndexedPeptideSpectrumValue.class, SequenceFileOutputFormat.class);

        mergeSortSequenceFiles(tmpPath, outputPath + ".hdlib", SpectrumKey.class, IndexedPeptideSpectrumValue.class, sc.hadoopConfiguration());
    }

    public static void saveLibrary(JavaSparkContext sc, String outputPath, final JavaRDD<PeptideConsensusSpectrum> consensusSpectra) throws IOException {

        final String tmpPath = outputPath + "_parts.hdlib";
        consensusSpectra
                .mapToPair(spectrum -> {

                    final SpectrumKey key = new SpectrumKey();
                    key.set(spectrum.getPrecursor());

                    final PeptideConsensusSpectrumValue value = new PeptideConsensusSpectrumValue();

                    value.set(spectrum);
                    return new Tuple2<>(key, value);
                })
                .saveAsNewAPIHadoopFile(tmpPath, SpectrumKey.class, PeptideConsensusSpectrumValue.class, SequenceFileOutputFormat.class);

        mergeSortSequenceFiles(tmpPath, outputPath + ".hdlib", SpectrumKey.class, PeptideConsensusSpectrumValue.class, sc.hadoopConfiguration());
    }

    public static void mergeSortSequenceFiles(String tmpPath, String outFile, Class keyClass, Class valClass, Configuration conf) throws IOException {

        //Sort merge into one file
        final FileSystem fs = FileSystem.get(conf);
        SequenceFile.Sorter sorter = new SequenceFile.Sorter(fs, new DefaultSpectrumKeyRawComparator(), keyClass, valClass, conf);
        final Path srcPath = new Path(tmpPath);
        FileStatus[] contents = fs.listStatus(srcPath, path -> path.getName().charAt(0) != '_');
        Path[] inFiles = new Path[contents.length];
        for (int i = 0; i < contents.length; i++) {

            inFiles[i] = contents[i].getPath();
        }
        sorter.sort(inFiles, new Path(outFile), true);
        fs.delete(srcPath, true);
    }

    public static void mergeSequenceFiles(String srcDir, String outFile, Class keyClass, Class valueClass, Configuration conf) throws IOException {

        final FileSystem fs = FileSystem.get(conf);
        final Path scrPath = new Path(srcDir);

        if (!fs.isDirectory(scrPath)) {
            throw new IllegalArgumentException("'" + srcDir + "' is not a directory");
        }


        FileStatus[] hdFiles = fs.listStatus(scrPath);


        SequenceFile.Writer writer = null;

        for (FileStatus status : hdFiles) {
            if (status.isDirectory()) {
                System.out.println("Skip directory '" + status.getPath().getName() + "'");
                continue;
            }

            Path file = status.getPath();

            if (file.getName().startsWith("_")) {
                System.out.println("Skip \"_\"-file '" + file.getName() + "'"); //There are files such "_SUCCESS"-named in jobs' ouput folders
                continue;
            }

            System.out.println("Merging '" + file.getParent()+"/"+file.getName() + "'");

            SequenceFile.Reader reader = new SequenceFile.Reader(conf, SequenceFile.Reader.file(file));
            Writable key = (Writable) ReflectionUtils.newInstance(reader.getKeyClass(), conf);
            Writable value = (Writable) ReflectionUtils.newInstance(reader.getValueClass(), conf);

            if (writer==null)
                writer = SequenceFile.createWriter(
                    conf,
                    SequenceFile.Writer.file(new Path(outFile)),
                    SequenceFile.Writer.keyClass(keyClass),
                    SequenceFile.Writer.valueClass(valueClass));

            while (reader.next(key, value)) {
                writer.append(key, value);
            }

            reader.close();
        }

        if (writer!=null) writer.close();

        fs.delete(new Path(srcDir), true);
    }

    public static JavaRDD<MsnSpectrum> msnSpectra(final JavaSparkContext sc, final String queryPath, final Cached<Predicate<MsnSpectrum>> predicate) {

        return sc.sequenceFile(queryPath, SpectrumKey.class, MsnSpectrumValue.class)
                .flatMap(v1 -> {
                    //noinspection unchecked
                    final MsnSpectrum spectrum = v1._2().get();
                    if (predicate.get().test(spectrum)) {
                        return Collections.singletonList(spectrum).iterator();
                    } else {
                        return Collections.emptyIterator();
                    }
                });
    }

    public static JavaRDD<MsnSpectrum> msnSpectra(final JavaSparkContext sc, final String queryPath, final Cached<Predicate<MsnSpectrum>> predicate, final Cached<PeakProcessorChain<PeakAnnotation>> processorChain) {

        return sc.sequenceFile(queryPath, SpectrumKey.class, MsnSpectrumValue.class)
                .flatMap(v1 -> {
                    //noinspection unchecked
                    final MsnSpectrum spectrum = v1._2().get();
                    if (predicate.get().test(spectrum)) {
                        spectrum.apply(processorChain.get());
                        return Collections.singletonList(spectrum).iterator();
                    } else {
                        return Collections.emptyIterator();
                    }
                });
    }

    public static <K, V> void saveAsTextFile(JavaSparkContext sc, JavaPairRDD<K, V> results, Function<Tuple2<K, V>, String> toText, String outputPath) throws IOException {

        final String resultsDir = outputPath + "/" + "parts_result.txt";
        results.map(toText).saveAsTextFile(resultsDir);
        copyMergeText(sc, resultsDir, outputPath);
    }

    public static <T> void saveAsTextFile(JavaSparkContext sc, JavaRDD<T> results, Function<T, String> toText, String outputPath) throws IOException {

        final String resultsDir = outputPath + "/" + "parts_result.txt";
        results.map(toText).saveAsTextFile(resultsDir);
        copyMergeText(sc, resultsDir, outputPath);
    }

    public static void copyMergeText(JavaSparkContext sc, String srcPath, String destPath) throws IOException {

        FileSystem fs = FileSystem.get(sc.hadoopConfiguration());
        FileUtil.copyMerge(fs, new Path(srcPath), fs, new Path(destPath + "/result.txt"), true, sc.hadoopConfiguration(), null);
    }

    public static JavaPairRDD<String, PeptideMatch> peptideMatches(JavaSparkContext sc, String path, Cached<PeptideMatchPredicate> psmPredicate) {

        return sc.sequenceFile(path, Text.class, PeptideMatchValue.class)
                .flatMapToPair(new PeptideMatchFilter(psmPredicate));
    }
}
