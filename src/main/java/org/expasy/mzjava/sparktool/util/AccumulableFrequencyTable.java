package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.stats.FrequencyTable;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class AccumulableFrequencyTable implements Externalizable {

    private double binSize;
    private FrequencyTable table;

    public AccumulableFrequencyTable() {

    }

    public AccumulableFrequencyTable(double binSize) {

        this.binSize = binSize;
        this.table = new FrequencyTable(binSize);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

        int[] binIds = table.getBinIds();

        out.writeDouble(binSize);
        out.writeInt(binIds.length);
        for(int binId : binIds) {

            try {
                out.writeInt(binId);
                out.writeInt(table.getFrequency(binId));
            } catch (IOException e) {

                throw new IllegalStateException(e);
            }
        }
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

        binSize = in.readDouble();

        table = new FrequencyTable(binSize);

        int binCount = in.readInt();
        for(int i = 0; i < binCount; i++) {

            final double binCenter = table.getCenter(in.readInt());
            final int frequency = in.readInt();
            for (int f = 0; f < frequency; f++) {

                table.add(binCenter);
            }
        }
    }

    public void add(double value) {

        table.add(value);
    }

    public FrequencyTable getTable() {

        return table;
    }

    public void addInPlace(AccumulableFrequencyTable serializableTable){

        this.table.addAll(serializableTable.table);
    }
}
