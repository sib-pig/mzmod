package org.expasy.mzjava.sparktool.util;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.hadoop.io.PeptideConsensusSpectrumValue;
import org.expasy.mzjava.proteomics.io.ms.spectrum.SptxtWriter;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ExportLib2Sptxt extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(ExportLib2Sptxt.class.getName());

    private String libraryHdFile;
    private String reportDir;

    public ExportLib2Sptxt(String libraryHdFile, String reportDir, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,buffMemoryMB,runLocal);

        this.libraryHdFile = libraryHdFile;
        this.reportDir = reportDir;

        createOptions();
    }

    public ExportLib2Sptxt() {

        createOptions();
    }

    public static void main(String[] args) {

        ExportLib2Sptxt exportLib2TansTSV =  new ExportLib2Sptxt();
        try {
            exportLib2TansTSV.parseOptions(args).run();
        } catch (MissingOptionException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        } catch (IOException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int run() throws IOException {
        long start = System.currentTimeMillis();

        final SparkConf conf = initSpark();
        final JavaSparkContext sc = new JavaSparkContext(conf);

        final Lib2SptxtParams parameters = new Lib2SptxtParamsImpl();

        final List<PeptideConsensusSpectrum> libSpectra = sc.sequenceFile(libraryHdFile, SpectrumKey.class, PeptideConsensusSpectrumValue.class)
                .map(pair -> pair._2().get())
                .collect();

        write2Sptxt(libSpectra);

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdlib").withDescription("spectrum library sequence file (required)").hasArg().withArgName("path").isRequired().create("hl"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("reportDir").withDescription("directory for report files (required)").hasArg().withArgName("path").isRequired().create("or"));

    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.libraryHdFile = checkExistFileOption(line, "hl");
        this.reportDir = checkOutputDirOption(line,"or");
    }

    private void write2Sptxt(List<PeptideConsensusSpectrum> libSpectra) throws IOException{

        String outFileName = new File(libraryHdFile).getName().replace(".hdlib", ".sptxt");

        String sptxtFileName = reportDir +File.separator+outFileName;

        SptxtWriter writer = new SptxtWriter(new File(sptxtFileName),false, PeakList.Precision.DOUBLE);

        libSpectra.forEach(sp -> {
            try {
                writer.write(sp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        writer.close();

        LOGGER.info(libSpectra.size()+" spectra written to " + sptxtFileName );
    }

}
