package org.expasy.mzjava.sparktool.util;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.mstranslator.RTAffineTransform;
import org.expasy.mzjava.sparktool.mstranslator.RTTransform;
import scala.Tuple2;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class Lib2SptxtParamsImpl implements Lib2SptxtParams {

    @Override
    public Cached<? extends Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean>> librarySpectrumFilter() {
        return new Cached<LibSpectrumFilter>() {
            @Override
            protected LibSpectrumFilter build() {

                return new LibSpectrumFilter(true);
            }
        };

    }

    public Cached<? extends RTTransform> rtTransformer(){

        return new Cached<RTAffineTransform>() {
            @Override
            protected RTAffineTransform build() {

                return new RTAffineTransform();
            }
        };

    }

    public Cached<SpectrumKeyFunction<MsnSpectrum>> keyFunction() {
        return new Cached<SpectrumKeyFunction<MsnSpectrum>>() {
            @Override
            protected SpectrumKeyFunction<MsnSpectrum> build() {

                return new MgfTitleKeyFunction();
//                return XTandemMgfKeyFunction();
            }
        };
    }

}
