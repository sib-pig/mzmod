package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.proteomics.io.ms.ident.PsmReader;
import org.expasy.mzjava.proteomics.ms.ident.ModListModMatchResolver;
import java.io.File;

import java.io.IOException;
import java.io.Serializable;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface PsmReaderFactory extends Serializable {
    PsmReader newPsmReader(File file, ModListModMatchResolver modMatchResolver, String decoyTag) throws IOException;
}
