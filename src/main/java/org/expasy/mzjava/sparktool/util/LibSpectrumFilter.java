package org.expasy.mzjava.sparktool.util;

import com.google.common.base.Optional;
import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.mol.modification.ModificationList;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodManager;
import org.expasy.mzjava.proteomics.mol.modification.unimod.UnimodMod;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.mstranslator.NormalizeRTParamsImpl;
import scala.Tuple2;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class LibSpectrumFilter implements Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean> {

    private final Set<String> rtPeptides;
    private final boolean onlyRTPeptides;

    public  LibSpectrumFilter(boolean onlyRTPeptides) {

        rtPeptides = new HashSet<>();

        rtPeptides.add("TPVSDRVTK");
        rtPeptides.add("TC(Carbamidomethyl)VADESAENC(Carbamidomethyl)DK");
        rtPeptides.add("C(Carbamidomethyl)C(Carbamidomethyl)AAADPHEC(Carbamidomethyl)YAK");
        rtPeptides.add("ADDKETC(Carbamidomethyl)FAEEGK");
        rtPeptides.add("NEC(Carbamidomethyl)FLQHKDDNPNLPR");
        rtPeptides.add("KVPQVSTPTLVEVSR");
        rtPeptides.add("RHPDYSVVLLLR");
        rtPeptides.add("SLHTLFGDKLC(Carbamidomethyl)TVATLR");
        rtPeptides.add("VFDEFKPLVEEPQNLIK");
        rtPeptides.add("HPYFYAPELLFFAK");
        rtPeptides.add("YKAAFTEC(Carbamidomethyl)C(Carbamidomethyl)QAADK");
        rtPeptides.add("KQTALVELVK");
        rtPeptides.add("QNC(Carbamidomethyl)ELFEQLGEYK");
        rtPeptides.add("EFNAETFTFHADIC(Carbamidomethyl)TLSEK");
        rtPeptides.add("EFNAETFTFHADIC(Carbamidomethyl)TLSEKER");
        rtPeptides.add("NYAEAKDVFLGMFLYEYAR");
        rtPeptides.add("RMPC(Carbamidomethyl)AEDYLSVVLNQLC(Carbamidomethyl)VLHEK");
        rtPeptides.add("ALVLIAFAQYLQQC(Carbamidomethyl)PFEDHVK");

        this.onlyRTPeptides = onlyRTPeptides;
    }

    public  LibSpectrumFilter() {
        rtPeptides = new NormalizeRTParamsImpl().standardPeptidesRTMap().keySet();
        this.onlyRTPeptides = false;
    }

    @Override
    public Boolean call(Tuple2<SpectrumKey,PeptideConsensusSpectrum> peptideConsensusSpectrum) throws Exception {

        if (onlyRTPeptides && !rtPeptides.contains(peptideConsensusSpectrum._2().getPeptide().toString()))
            return false;


        return checkModif(peptideConsensusSpectrum._2());
    }

    private boolean checkModif(PeptideConsensusSpectrum consensusSpectrum) {
        Peptide peptide = consensusSpectrum.getPeptide();

        int[] indexes = peptide.getModificationIndexes(ModAttachment.all);

        for (int i=0;i<indexes.length;i++) {
            ModificationList modifs = peptide.getModificationsAt(indexes[i],ModAttachment.all);
            for (Modification modif : modifs) {
                Optional<UnimodMod> unimod = UnimodManager.getModification(modif.getLabel());
                if (!unimod.isPresent()) return false;
                if (!unimod.get().getSites().contains(peptide.getSymbol(indexes[i]).toString())) return false;
            }
        }

        return true;
    }
}
