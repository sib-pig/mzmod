package org.expasy.mzjava.sparktool.util;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ExportHdPsm2Txt extends SparkRunner {

    private static final Logger LOGGER = Logger.getLogger(ExportHdPsm2Txt.class.getName());

    private String psmHdFile;
    private String reportDir;

    public ExportHdPsm2Txt(String psmHdFile, String reportDir, int nrExec, int execMemoryMB, int buffMemoryMB, boolean runLocal) {

        super(nrExec,execMemoryMB,buffMemoryMB,runLocal);

        this.psmHdFile = psmHdFile;
        this.reportDir = reportDir;

        createOptions();
    }

    public ExportHdPsm2Txt() {

        createOptions();
    }

    public static void main(String[] args) {

        ExportHdPsm2Txt exportLib2TansTSV =  new ExportHdPsm2Txt();
        try {
            exportLib2TansTSV.parseOptions(args).run();
        } catch (MissingOptionException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        } catch (ParseException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        } catch (IOException e) {
            exportLib2TansTSV.printOptions(args, e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int run() throws IOException {
        long start = System.currentTimeMillis();

        final SparkConf conf = initSpark();
        final JavaSparkContext sc = new JavaSparkContext(conf);

        final Lib2SptxtParams parameters = new Lib2SptxtParamsImpl();

        final List<PeptideMatch> psms = sc.sequenceFile(psmHdFile, Text.class, PeptideMatchValue.class)
                .map(pair -> pair._2().get())
                .collect();

        write2Txt(psms);

        LOGGER.info("Ran in " + (System.currentTimeMillis() - start) / 1000d + "s");

        sc.stop();

        return 0;
    }

    protected void createOptions() {
        if (cmdLineOpts==null)
            super.createOptions();

        cmdLineOpts.addOption(OptionBuilder.withLongOpt("hdlib").withDescription("spectrum library sequence file (required)").hasArg().withArgName("path").isRequired().create("hl"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("reportDir").withDescription("directory for report files (required)").hasArg().withArgName("path").isRequired().create("or"));

    }

    @Override
    protected void check(CommandLine line) throws ParseException {

        super.check(line);

        this.psmHdFile = checkExistFileOption(line, "hl");
        this.reportDir = checkOutputDirOption(line,"or");
    }

    private void write2Txt(List<PeptideMatch> libSpectra) throws IOException{

        String outFileName = new File(psmHdFile).getName().replace(".hdpsm", ".txt");

        String txtFileName = reportDir +File.separator+outFileName;


        BufferedWriter writer = new BufferedWriter(new FileWriter(txtFileName));

        libSpectra.forEach(psm -> {
            try {
                writer.write(psm.getPeptide().toString()+"\t"+psm.getPeptide().toSymbolString()+"\t"+psm.getScore("hyperscore")+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        LOGGER.info(libSpectra.size() + " psms written to " + txtFileName);
    }

}
