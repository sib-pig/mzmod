package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.proteomics.io.ms.ident.BiomlPsmReader;
import org.expasy.mzjava.proteomics.io.ms.ident.PepXmlReader;
import org.expasy.mzjava.proteomics.io.ms.ident.PsmReader;
import org.expasy.mzjava.proteomics.ms.ident.ModListModMatchResolver;

import java.io.File;
import java.io.IOException;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class PsmReaderFactoryImpl implements PsmReaderFactory {
    public PsmReaderFactoryImpl(){}

    public PsmReader newPsmReader(File file, ModListModMatchResolver modMatchResolver, String decoyTag) throws IOException {

        String filename = file.getName().toLowerCase();
        if (filename.endsWith("pep.xml")||filename.endsWith("pepxml")) {

            return new PepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, true, modMatchResolver );

        } else if (filename.endsWith("t.xml")) {

            return new BiomlPsmReader(decoyTag);

        } else {

            throw new IllegalStateException("Do not have a reader that can read " + file );
        }
    }


}
