package org.expasy.mzjava.sparktool.util;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.PeptideMatch;

import java.util.function.Predicate;

/**
 * PeptideMatch Predicate that accepts only peptides which have no modifications or carbamidomethyl on C
 */
public class PeptideMatchPredicate implements Predicate<PeptideMatch> {

    public enum ScoreOrder {LARGER,SMALLER};

    private final int minCharge;
    private final int maxCharge;
    private final int minPeptideLength;
    private final int maxPeptideLength;
    private final String scoreName;
    private final double minPsmScore;
    private final int maxRank;
    private final ScoreOrder order;

    public PeptideMatchPredicate(int minCharge, int maxCharge,
                                 int minPeptideLength, int maxPeptideLength, String scoreName,
                                 double minPsmScore, ScoreOrder order, int maxRank) {
        Preconditions.checkArgument(minPeptideLength <= maxPeptideLength);
        Preconditions.checkArgument(minCharge <= maxCharge);
        Preconditions.checkArgument(maxRank >= 0);

        this.minCharge = minCharge;
        this.maxCharge = maxCharge;
        this.minPeptideLength = minPeptideLength;
        this.maxPeptideLength = maxPeptideLength;
        this.scoreName = scoreName;
        this.minPsmScore = minPsmScore;
        this.maxRank = maxRank;
        this.order = order;
    }

    public PeptideMatchPredicate(PeptideMatchPredicate peptideMatchPredicate,
                                 float minPsmScore) {

        this.minCharge = peptideMatchPredicate.minCharge;
        this.maxCharge = peptideMatchPredicate.maxCharge;
        this.minPeptideLength = peptideMatchPredicate.minPeptideLength;
        this.maxPeptideLength = peptideMatchPredicate.maxPeptideLength;
        this.scoreName = peptideMatchPredicate.scoreName;
        this.minPsmScore = minPsmScore;
        this.maxRank = peptideMatchPredicate.maxRank;
        this.order = peptideMatchPredicate.order;
    }

    public boolean test(PeptideMatch psm) {

        if (!psm.getScoreMap().containsKey(this.scoreName)) return false;

        if (order==ScoreOrder.LARGER) {
            if (psm.getScore(scoreName) < minPsmScore) return false;
        } else {
            if (psm.getScore(scoreName) > minPsmScore) return false;
        }

        if (psm.getScore("rank") > this.maxRank) return false;

        Peptide peptide = psm.getPeptide();
        if (peptide.size() < this.minPeptideLength) {
            return false;
        } else if (peptide.size() > this.maxPeptideLength) {
            return false;
        }

        if (psm.getCharge()<minCharge) return false;
        if (psm.getCharge()>maxCharge) return false;

        return true;
    }

    public ScoreOrder getOrder() {return order;}

    public String getScoreName() {return scoreName;}

    public double getMinPsmScore() {return minPsmScore;}

}
