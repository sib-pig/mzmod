package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class TitleKeyFunction implements SpectrumKeyFunction<MsnSpectrum> {

    @Override
    public String apply(SpectrumIdentifier identifier, Peptide peptide) {

        return identifier.getSpectrum();
    }

    @Override
    public String apply(MsnSpectrum spectrum) {

        return spectrum.getComment();
    }

    @Override
    public String getRunID(MsnSpectrum spectrum) {

        final String comment = spectrum.getComment();

        int firstDot = comment.indexOf('.');

        return comment.substring(0, firstDot);
    }


}
