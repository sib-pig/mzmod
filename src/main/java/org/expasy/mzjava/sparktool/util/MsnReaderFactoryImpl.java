package org.expasy.mzjava.sparktool.util;


import com.google.common.io.Files;
import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.io.ms.spectrum.MzxmlReader;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.external.io.ms.spectrum.mzml.EbiMzmlReader;
import org.expasy.mzjava.external.io.ms.spectrum.mzml.ScanNumberParser;
import org.expasy.mzjava.proteomics.io.ms.spectrum.DtaReader;
import org.expasy.mzjava.proteomics.io.ms.spectrum.MspReader;
import org.expasy.mzjava.proteomics.io.ms.spectrum.SptxtReader;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import uk.ac.ebi.jmzml.xml.io.MzMLUnmarshaller;

import java.io.File;
import java.io.IOException;
import java.net.URI;


/**
 * This class is a MS reader factory that provides new instance of MsReaders.
 *
 * @author nikitin
 * @version 1.0
 */
public class MsnReaderFactoryImpl implements MsnReaderFactory {

    public MsnReaderFactoryImpl() {}

    public IterativeReader<MsnSpectrum> newMsnReader(File file, PeakList.Precision precision) throws IOException {

        String extension = Files.getFileExtension(file.getName()).toLowerCase();

        if ("dta".equals(extension)) {

            return new DtaReader(file, precision);
        } else if ("mgf".equals(extension)) {

            return new MgfReader(file, precision);
        } else if ("mzxml".equals(extension)) {

            return new MzxmlReader(file, precision);
        }  else if ("mzml".equals(extension)) {

            URI uri = URI.create("file:///"+file.getPath().replace('\\','/'));
            EbiMzmlReader reader = EbiMzmlReader.newTolerantReader(new MzMLUnmarshaller(new File(uri)), precision);
            reader.setSpectrumIdParser(new ScanNumberParser());
            return reader;
        } else {

            throw new IllegalStateException("Do not have a reader that can read " + extension + " files");
        }
    }

    public static IterativeReader<PeptideConsensusSpectrum> newLibraryReader(File file, PeakList.Precision precision) throws IOException {

        String extension = Files.getFileExtension(file.getName()).toLowerCase();

        if ("msp".equals(extension)) {

            return MspReader.newBuilder(file, precision).useSkipAnnotationResolver().build();
        } else if ("sptxt".equals(extension)) {

            return SptxtReader.newBuilder(file, precision).useSkipAnnotationResolver().build();
        } else {

            throw new IllegalStateException("Do not have a reader that can read " + extension + " files");
        }
    }
}
