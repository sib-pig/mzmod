package org.expasy.mzjava.sparktool.util;

import org.apache.spark.api.java.function.Function;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.sparktool.data.Cached;
import scala.Tuple2;

import java.io.Serializable;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface Lib2SptxtParams extends Serializable {
    Cached<? extends Function<Tuple2<SpectrumKey,PeptideConsensusSpectrum>,Boolean>> librarySpectrumFilter();

    Cached<SpectrumKeyFunction<MsnSpectrum>> keyFunction();

}
