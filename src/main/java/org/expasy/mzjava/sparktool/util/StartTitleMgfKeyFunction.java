package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class StartTitleMgfKeyFunction implements SpectrumKeyFunction<MsnSpectrum> {

    @Override
    public String apply(SpectrumIdentifier identifier, Peptide peptide) {

        final String spectrum = identifier.getSpectrum();

        int idx = spectrum.indexOf(" File");

        return spectrum.substring(0, idx);
    }

    @Override
    public String apply(MsnSpectrum spectrum) {

        final String comment = spectrum.getComment();

        int idx = comment.indexOf(" File");

        return comment.substring(0, idx);
    }

    @Override
    public String getRunID(MsnSpectrum spectrum) {

        final String comment = spectrum.getComment();

        int firstDot = comment.indexOf('.');

        return comment.substring(0, firstDot);
    }


}
