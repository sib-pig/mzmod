package org.expasy.mzjava.sparktool.util;

import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.InvalidPathException;
import org.apache.spark.SparkConf;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author Markus Muller
 * @version 0.0
 */
public abstract class SparkRunner {
    private static final Logger LOGGER = Logger.getLogger(SparkRunner.class.getName());

    protected int nrExec;
    protected int execMemoryMB;
    protected int bufferMemoryMB;
    protected boolean runLocal;
    protected Options cmdLineOpts;

    protected SparkRunner(int nrExec, int execMemoryMB, int bufferMemoryMB, boolean runLocal) {

        this.nrExec = nrExec;
        this.execMemoryMB = execMemoryMB;
        this.bufferMemoryMB = bufferMemoryMB;
        this.runLocal = runLocal;

        createOptions();
    }


    protected SparkRunner() {

        createOptions();
    }

    public int getNrExec() {
        return nrExec;
    }

    public int getExecMemoryMB() {
        return execMemoryMB;
    }

    public int getBufferMemoryMB() {
        return bufferMemoryMB;
    }

    public boolean isRunLocal() {
        return runLocal;
    }

    public SparkConf initSpark() throws IOException {

        SparkConf conf = new SparkConf()
                .setAppName(this.getClass().getName())
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
                .set("spark.kryo.registrator", "org.expasy.mzjava.spark.MzJavaKryoRegistrator")
                .set("spark.kryoserializer.buffer.mb", Integer.toString(bufferMemoryMB))
                .set("spark.executor.memory", execMemoryMB + "m");

        if (runLocal) {
            String master = "local["+nrExec+"]";
            conf.setMaster(master);
        }

        return conf;
    }

    protected void createOptions() {
        this.cmdLineOpts = new Options();
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("execMem").withDescription("executor memory in MB").hasArg().withArgName("integer").create("em"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("bufferMem").withDescription("buffer memory in MB").hasArg().withArgName("integer").create("bm"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("nrExec").withDescription("number of spark executors").hasArg().withArgName("integer").create("n"));
        cmdLineOpts.addOption(OptionBuilder.withLongOpt("local").withDescription("execution on local machine").create("l"));
    }

    protected void check(CommandLine line) throws ParseException {

        this.nrExec = checkIntOption(line,"n",1,Integer.MAX_VALUE,1);
        float defaultExecMemMB = 0.8f*Runtime.getRuntime().maxMemory()/(1024*1024*nrExec);
        this.execMemoryMB = checkIntOption(line,"em",1,Integer.MAX_VALUE,(int) defaultExecMemMB);
        this.bufferMemoryMB = checkIntOption(line,"bm",1,Integer.MAX_VALUE,128);

        if( line.hasOption( "l" ) ) {
            this.runLocal = true;
        } else {
            this.runLocal = false;
        }
    }

    protected int checkIntOption(CommandLine line,String optionId, int min, int max, int defaultValue) {

        int value = defaultValue;

        if( line.hasOption( optionId ) ) {

            String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
            try {
                value = Integer.parseInt(line.getOptionValue(optionId));
            } catch(NumberFormatException e) {
                throw new NumberFormatException(optStr+": bad format for integer option value "+line.getOptionValue(optionId));
            }

            if (value<min || value>max) {
                throw new NumberFormatException(optStr+" option value must be >="+min+" && <="+max);
            }
        }

        return value;
    }

    protected float checkFloatOption(CommandLine line,String optionId, float min, float max, float defaultValue) {
        float value = defaultValue;

        if( line.hasOption( optionId ) ) {

            String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
            try {
                value = Float.parseFloat(line.getOptionValue(optionId));
            } catch(NumberFormatException e) {
                throw new NumberFormatException(optStr+": bad format for integer option value "+line.getOptionValue(optionId));
            }

            if (value<min || value>max) {
                throw new NumberFormatException(optStr+" option value must be >="+min+" && <="+max);
            }
        }

        return value;
    }

    protected String checkExistFileOption(CommandLine line, String optionId) {

        String fileName = "";
        if( line.hasOption( optionId ) ) {
            fileName = line.getOptionValue( optionId );
        }

        if (!(new File(fileName).exists())) {
            String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
            throw new InvalidPathException(line.getOptionValue(optionId)+" for option "+optStr+". File does not exist");
        }

        return fileName;
    }

    protected String checkExistDirOption(CommandLine line, String optionId) {

        String fileName = "";
        if( line.hasOption( optionId ) ) {
            fileName = line.getOptionValue( optionId );
        }

        File dir = new File(fileName);
        if (!dir.isDirectory()) {
            String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
            throw new InvalidPathException(line.getOptionValue(optionId)+" for option "+optStr+". Directory does not exist. Please create directory.");
        }

        return fileName;
    }

    protected String checkNotExistDirOption(CommandLine line, String optionId) {

        String fileName = "";
        if( line.hasOption( optionId ) ) {
            fileName = line.getOptionValue( optionId );
        }

        File dir = new File(fileName);
        if (dir.isDirectory()) {
            String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
            throw new InvalidPathException(line.getOptionValue(optionId)+" for option "+optStr+". Directory does already exist. Please choose another directory or delete old one.");
        }

        return fileName;
    }

    protected String checkSparkDirOption(CommandLine line, String optionId, boolean forceDeleteDir) {

        String dirName = "";
        if( line.hasOption( optionId ) ) {
            dirName = line.getOptionValue( optionId );
        }

        File dir = new File(dirName);
        try {
            if (dir.isDirectory()) {
                if (forceDeleteDir) {
                    FileUtils.deleteDirectory(dir);
                } else {
                    String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
                    throw new InvalidPathException(line.getOptionValue(optionId)+" for option "+optStr+" does already exist. Please choose another directory, delete old one or use -fd,--forceDelete option.");
                }
            }
        } catch (IOException e) {
            String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
            throw new InvalidPathException(line.getOptionValue(optionId)+" for option "+optStr+". Cannot write hadoop file to directory");
        }

        return dirName;
    }

    protected String checkOutputDirOption(CommandLine line, String optionId) {

        String dirName = "";
        if( line.hasOption( optionId ) ) {
            dirName = line.getOptionValue( optionId );
        }

        File dir = new File(dirName);
        try {
            if (!dir.exists())
                FileUtils.forceMkdir(dir);
        } catch (IOException e) {
            String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
            throw new InvalidPathException(line.getOptionValue(optionId)+" for option "+optStr+". Cannot delete directory");
        }

        return dirName;
    }

    protected Pattern checkRegExOption(CommandLine line, String optionId) {

        String regexStr = "";
        if( line.hasOption( optionId ) ) {
            regexStr = line.getOptionValue( optionId );
        }

        Pattern regExp = null;
        try {
            regExp = Pattern.compile(regexStr);
        } catch (PatternSyntaxException exception) {
            String optStr = "-"+optionId+",--"+cmdLineOpts.getOption(optionId).getLongOpt();
            throw new PatternSyntaxException(regexStr+" invalid regex for option "+optStr,regexStr,exception.getIndex());
        }

        return regExp;
    }

    public abstract int run() throws IOException;

    public void printOptions(String[] args, String msg) {
        String cmdl = "";
        for (int i=0;i<args.length;i++)  cmdl += args[i]+" ";
        System.out.println(cmdl);
        System.out.println(msg);
        System.out.println();
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( this.getClass().getName(), cmdLineOpts );
    }

    public SparkRunner parseOptions(String[] args) throws ParseException {


        CommandLineParser parser = new BasicParser();
        CommandLine line = parser.parse( cmdLineOpts, args );

        check(line);

        return this;
    }

}
