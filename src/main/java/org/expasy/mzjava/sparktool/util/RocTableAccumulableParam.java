package org.expasy.mzjava.sparktool.util;

import org.apache.spark.AccumulableParam;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class RocTableAccumulableParam implements AccumulableParam<RocTableAccumulable, RocValue> {

    @Override
    public RocTableAccumulable addAccumulator(RocTableAccumulable rocTableAccumulable, RocValue rocValue) {

        rocTableAccumulable.add(rocValue);
        return rocTableAccumulable;
    }

    @Override
    public RocTableAccumulable addInPlace(RocTableAccumulable r1, RocTableAccumulable r2) {

        r1.add(r2);
        return r1;
    }

    @Override
    public RocTableAccumulable zero(RocTableAccumulable initialValue) {

        return new RocTableAccumulable(initialValue.getMin(), initialValue.getMax(), initialValue.getThresholdIncrements());
    }
}
