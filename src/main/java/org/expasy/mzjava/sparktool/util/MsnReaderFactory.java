package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.core.io.IterativeReader;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author Markus Muller
 * @version 0.0
 */
public interface MsnReaderFactory extends Serializable {
    IterativeReader<MsnSpectrum> newMsnReader(File file, PeakList.Precision precision) throws IOException;
}
