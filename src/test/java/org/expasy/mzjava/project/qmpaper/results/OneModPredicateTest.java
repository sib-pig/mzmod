package org.expasy.mzjava.project.qmpaper.results;

import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

public class OneModPredicateTest {

    @Test
    public void test() throws Exception {

        OneModPredicate predicate = new OneModPredicate(7, 40, "hyperscore", 60);

        boolean accept = predicate.test(new PeptideMatch(Peptide.parse("PETM(O)IDE"), Collections.<String>emptySet().stream(), singletonMap("hyperscore", 70),2));
        Assert.assertEquals(true, accept);
    }

    @Test
    public void testNoMods() throws Exception {

        OneModPredicate predicate = new OneModPredicate(7, 40, "hyperscore", 60);

        boolean accept = predicate.test(new PeptideMatch(Peptide.parse("PETMIDE"), Collections.<String>emptySet().stream(), singletonMap("hyperscore", 70),2));
        Assert.assertEquals(false, accept);
    }

    @Test
    public void testLowScore() throws Exception {

        OneModPredicate predicate = new OneModPredicate(7, 40, "hyperscore", 60);

        boolean accept = predicate.test(new PeptideMatch(Peptide.parse("PETM(O)IDE"), Collections.<String>emptySet().stream(), singletonMap("hyperscore", 59),2));
        Assert.assertEquals(false, accept);
    }

    @Test
    public void testShortPeptide() throws Exception {

        OneModPredicate predicate = new OneModPredicate(7, 40, "hyperscore", 60);

        boolean accept = predicate.test(new PeptideMatch(Peptide.parse("TM(O)I"), Collections.<String>emptySet().stream(), singletonMap("hyperscore", 70),2));
        Assert.assertEquals(false, accept);
    }

    @Test
    public void testLongPeptide() throws Exception {

        OneModPredicate predicate = new OneModPredicate(3, 6, "hyperscore", 60);

        boolean accept = predicate.test(new PeptideMatch(Peptide.parse("PETM(O)IDE"), Collections.<String>emptySet().stream(), singletonMap("hyperscore", 70),2));
        Assert.assertEquals(false, accept);
    }

    @Test
    public void testNegativeMod() throws Exception {

        OneModPredicate predicate = new OneModPredicate(7, 40, "hyperscore", 60);

        boolean accept = predicate.test(new PeptideMatch(Peptide.parse("PETM(O-1)IDE"), Collections.<String>emptySet().stream(), singletonMap("hyperscore", 70),2));
        Assert.assertEquals(false, accept);
    }

    @Test
    public void testNegativeMod2() throws Exception {

        OneModPredicate predicate = new OneModPredicate(7, 40, "hyperscore", 60);

        boolean accept = predicate.test(new PeptideMatch(Peptide.parse("C(C2O)PETMIDE"), Collections.<String>emptySet().stream(), singletonMap("hyperscore", 70),2));
        Assert.assertEquals(false, accept);
    }

    @Test
    public void testNegativeMod3() throws Exception {

        OneModPredicate predicate = new OneModPredicate(7, 40, "hyperscore", 60);

        boolean accept = predicate.test(new PeptideMatch(Peptide.parse("M(H-2O-1)PEC(C2H3NO)MIDE"), Collections.<String>emptySet().stream(), singletonMap("hyperscore", 70),2));
        Assert.assertEquals(false, accept);
    }

    private TObjectDoubleMap<String> singletonMap(String name, double score) {

        TObjectDoubleMap<String> map = new TObjectDoubleHashMap<>();
        map.put(name, score);
        return map;
    }
}