package org.expasy.mzjava.project.qmpaper.fdr;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;

public class FdrScoresProcedureTest {

    @Test
    public void testExecute() throws Exception {

        List<List<Double>> allScores = new ArrayList<>();
        FdrScoresProcedure procedure = new FdrScoresProcedure(allScores);

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);
        List<OmsResult> results = new ArrayList<>();
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("PEPSIDEK"), UUID.randomUUID(), "", 1, 16, 2.0, 0.9, 0.3, 3, new double[]{0f, 0f, 0f, 1f, 1f}));
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("PEPTIDEK" ), UUID.randomUUID(), "", 1, 16, 1.9, 0.9, 0.3, 3, new double[]{0f, 0f, 0f, 1f, 1f}));
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("CERVILASR"), UUID.randomUUID(), "", 1, 40, 1.0, 0.9, 0.3, 3, new double[]{0f, 0f, 0f, 1f, 1f}));
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("CERVILASK"), UUID.randomUUID(), "", 1, 40, 0.9, 0.9, 0.3, 3, new double[]{0f, 0f, 0f, 1f, 1f}));
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("LLLSK"    ), UUID.randomUUID(), "", 1, 60, 0.2, 0.9, 0.3, 3, new double[]{0f, 0f, 0f, 1f, 1f}));
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("SSSEEEKK" ), UUID.randomUUID(), "", 1, 70, 0.1, 0.9, 0.3, 3, new double[]{0f, 0f, 0f, 1f, 1f}));

        OmsQuery query = new OmsQuery(spectrum, results);

        procedure.execute(query);

        List<Double> lastScores = allScores.get(0);

        Assert.assertEquals(5, lastScores.size());
        Assert.assertEquals(2.0 + 0.1, lastScores.get(0), 0.0001);
        Assert.assertEquals(1.9 + 0.9, lastScores.get(1), 0.0001);
        Assert.assertEquals(1.0 + 0.1, lastScores.get(2), 0.0001);
        Assert.assertEquals(0.9 + 0.7, lastScores.get(3), 0.0001);
        Assert.assertEquals(0.2 + 0.1, lastScores.get(4), 0.0001);
    }
}