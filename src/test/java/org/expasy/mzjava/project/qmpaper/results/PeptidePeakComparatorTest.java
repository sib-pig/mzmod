package org.expasy.mzjava.project.qmpaper.results;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.PpmTolerance;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.junit.Assert;
import org.junit.Test;

public class PeptidePeakComparatorTest {

    @Test
    public void testCompare1() {

        PeptidePeakComparator peakComparator = new PeptidePeakComparator(new PpmTolerance(10), new AbsoluteTolerance(0.05));

        Assert.assertEquals(false, peakComparator.compare(
                Peptide.parse("PLK"),
                Peptide.parse("PM(O)K")));
    }

    @Test
    public void testCompare2() {

        PeptidePeakComparator peakComparator = new PeptidePeakComparator(new PpmTolerance(10), new AbsoluteTolerance(0.05));

        Assert.assertEquals(true, peakComparator.compare(
                Peptide.parse("PMK"),
                Peptide.parse("PM(O)K")));
    }

    @Test
    public void testCompare3() {

        PeptidePeakComparator peakComparator = new PeptidePeakComparator(new PpmTolerance(10), new AbsoluteTolerance(0.05));

        Assert.assertEquals(false, peakComparator.compare(
                Peptide.parse("DJQJTQSPSSJSASVGDR"),
                Peptide.parse("DJQMTQSPSSJSASVGDR")));
    }

    @Test
    public void testCompare4() {

        PeptidePeakComparator peakComparator = new PeptidePeakComparator(new PpmTolerance(10), new AbsoluteTolerance(0.05));

        Assert.assertEquals(false, peakComparator.compare(
                Peptide.parse( "DDJAAJVVDNGSGMC(C2H3NO)K"),
                Peptide.parse("DDDJAALVVDNGSGMC(C2H3NO)K")));
    }

    @Test
    public void testCompare5() {

        PeptidePeakComparator peakComparator = new PeptidePeakComparator(new PpmTolerance(10), new AbsoluteTolerance(0.05));

        Assert.assertEquals(false, peakComparator.compare(
                Peptide.parse("JSEJEAAJQR"),
                Peptide.parse( "EEJEAAJQR")));
    }

    @Test
    public void testCompare6() {

        PeptidePeakComparator peakComparator = new PeptidePeakComparator(new PpmTolerance(10), new AbsoluteTolerance(0.05));

        Assert.assertEquals(false, peakComparator.compare(
                Peptide.parse( "EEJEAAJQR"),
                Peptide.parse("JSEJEAAJQR")));
    }

    @Test
    public void testCompare7() {

        PeptidePeakComparator peakComparator = new PeptidePeakComparator(new PpmTolerance(10), new AbsoluteTolerance(0.05));

        Assert.assertEquals(false, peakComparator.compare(
                Peptide.parse("EEEJAAJVJDNGSGMC(C2H3NO)K"),
                Peptide.parse("DDDJAAJVVDNGSGMC(C2H3NO)K")));
    }

    @Test
    public void testCompare8() {

        PeptidePeakComparator peakComparator = new PeptidePeakComparator(new PpmTolerance(10), new AbsoluteTolerance(0.05));

        Assert.assertEquals(true, peakComparator.compare(
                Peptide.parse("PLK"),
                Peptide.parse("PJ(O)K")));
    }
}