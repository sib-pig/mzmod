package org.expasy.mzjava.project.qmpaper.results;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.expasy.mzjava.sparktool.mzmod.UnmatchedPeakCounter;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RecordTest {

    @Test(expected = IllegalArgumentException.class)
    public void testWrongNumberOfColumns() throws Exception {

        new Record("");
    }

    @Test
    public void test() throws Exception {

        final String line = "0.6294158684669078\t2\tOPEN\t16.000501498538142\t11\t0.999794929764729\tJJVDHNJADYMTAK\t82.7\tLIVDHNIADYM(O)TAK\t11\tO\tFetal_Gut_bRP_Elite_17_f14.9161.9161.2\ttrue";
        Record record = new Record(line);

        Assert.assertEquals(0.6294158684669078, record.getScore(), 0);
        Assert.assertEquals(2, record.getCharge());
        Assert.assertEquals(16.000501498538142, record.getMassShift(), 0);
        Assert.assertEquals(line, record.getLine());
        Assert.assertEquals(82.7, record.getDbScore(), 0);
        Assert.assertEquals("JJVDHNJADYMTAK", record.getLibPeptide());
        Assert.assertEquals("LIVDHNIADYM(O)TAK", record.getCorrectPeptide());
        Assert.assertEquals("Fetal_Gut_bRP_Elite_17_f14.9161.9161.2", record.getSpectrum());
        Assert.assertEquals(true, record.isExact());

        final PeptideFragmenter fragmenter = mock(PeptideFragmenter.class);
        final UnmatchedPeakCounter unmatchedPeakCounter = mock(UnmatchedPeakCounter.class);
        Assert.assertEquals(0, record.getOrCalculateUnmatchedPeaks(fragmenter, unmatchedPeakCounter));
    }

    @Test
    public void testUnmatchedPeaks() throws Exception {

        final String line = "0.6294158684669078\t2\tOPEN\t16.000501498538142\t11\t0.999794929764729\tJJVDHNJADYMATAK\t82.7\tLIVDHNIADYM(O)TAK\t11\tO\tFetal_Gut_bRP_Elite_17_f14.9161.9161.2\tfalse";
        Record record = new Record(line);

        PeptideSpectrum spectrum1 = mock(PeptideSpectrum.class);
        PeptideSpectrum spectrum2 = mock(PeptideSpectrum.class);

        final PeptideFragmenter fragmenter = mock(PeptideFragmenter.class);
        when(fragmenter.fragment(Peptide.parse("JJVDHNJADYMATAK"), 1)).thenReturn(spectrum1);
        when(fragmenter.fragment(Peptide.parse("LIVDHNIADYMTAK"), 1)).thenReturn(spectrum2);

        final UnmatchedPeakCounter unmatchedPeakCounter = mock(UnmatchedPeakCounter.class);
        when(unmatchedPeakCounter.asymmetricUnmatched(spectrum1, spectrum2)).thenReturn(1);

        Assert.assertEquals(1, record.getOrCalculateUnmatchedPeaks(fragmenter, unmatchedPeakCounter));
    }
}