package org.expasy.mzjava.sparktool.spectraconverter;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ConvertMsn2HdMsnTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testInitServer() throws Exception {

        ConvertMsn2HdMsn runner = new ConvertMsn2HdMsn("/scratch/cluster/test/spectra", Pattern.compile("\\.mgf$"),"/scratch/cluster/test/results",true, true, 1, 1000, 128,true);

        SparkConf conf = runner.initSpark();
        Assert.assertEquals("org.apache.spark.serializer.KryoSerializer", conf.get("spark.serializer"));
    }

    @Test
    public void testCommandLineArgs1() throws IOException {

        File spectraDir = temporaryFolder.newFolder();
        File hdDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-sd",spectraDir.getAbsolutePath(),"-l","-hs",hdDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-rs","\\.mgf$","-ss","-fd" };

        ConvertMsn2HdMsn convertMsn2HdMsn = new ConvertMsn2HdMsn();
        try {
            convertMsn2HdMsn.parseOptions(args);

            Assert.assertEquals(hdDir.getAbsolutePath(), convertMsn2HdMsn.getSparkDir());
            Assert.assertEquals(spectraDir.getAbsolutePath(), convertMsn2HdMsn.getSpectraRootDirName());
            Assert.assertTrue(convertMsn2HdMsn.isRunLocal());
            Assert.assertEquals(4, convertMsn2HdMsn.getNrExec());
            Assert.assertEquals(1000, convertMsn2HdMsn.getExecMemoryMB());
            Assert.assertEquals(128, convertMsn2HdMsn.getBufferMemoryMB());
            Assert.assertEquals(Pattern.compile("\\.mgf$").toString(), convertMsn2HdMsn.getRegex().toString());
            Assert.assertTrue(convertMsn2HdMsn.isDoSort());

            FileUtils.deleteQuietly(spectraDir);
            FileUtils.deleteQuietly(hdDir);

        }
        catch( Exception exp ) {
            convertMsn2HdMsn.printOptions(args, exp.getMessage());
            Assert.fail();
        }

    }

    @Test
    public void testCommandLineArgs2() throws IOException {

        File spectraDir = temporaryFolder.newFolder();
        File hdDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-sd","X:\\dosnotexist","-l","-hs",hdDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-rs","\\.mgf$","-ss" };

        ConvertMsn2HdMsn convertMsn2HdMsn = new ConvertMsn2HdMsn();
        try {
            convertMsn2HdMsn.parseOptions(args);

            FileUtils.deleteQuietly(spectraDir);
            FileUtils.deleteQuietly(hdDir);

            Assert.fail();
        }
        catch( Exception exp ) {
            convertMsn2HdMsn.printOptions(args, exp.getMessage());

            FileUtils.deleteQuietly(spectraDir);
            FileUtils.deleteQuietly(hdDir);

        }

    }

    @Test
    public void testCommandLineArgs3() throws IOException {

        File spectraDir = temporaryFolder.newFolder();
        File hdDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-sd",spectraDir.getAbsolutePath(),"-l","-hs",hdDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-rs","\\.mgf$","-ss" };

        ConvertMsn2HdMsn convertMsn2HdMsn = new ConvertMsn2HdMsn();
        try {
            convertMsn2HdMsn.parseOptions(args);


            FileUtils.deleteQuietly(spectraDir);
            FileUtils.deleteQuietly(hdDir);

            Assert.fail();
        }
        catch( Exception exp ) {
            convertMsn2HdMsn.printOptions(args, exp.getMessage());

            FileUtils.deleteQuietly(spectraDir);
            FileUtils.deleteQuietly(hdDir);
        }

    }
}
