package org.expasy.mzjava.sparktool.psmconverter;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

public class PSMConverterAppTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();


    @Test
    public void testNoPepXml() throws Exception {

        final File resultsFolder = temporaryFolder.newFolder("pepXml");
        Assert.assertTrue(resultsFolder.exists());
        final String pepXmlPath = resultsFolder.getPath();

        PSMConverterApp converterApp = new PSMConverterApp(resultsFolder.getAbsolutePath(), Pattern.compile("pep\\.xml$"),"/scratch/cluster/test/output",true,true,true,6,1000,128,true);
        SparkConf conf = converterApp.initSpark();

        FileUtils.deleteDirectory(resultsFolder);
    }

    @Test
    public void testInitServer() throws Exception {

        PSMConverterApp converterApp = new PSMConverterApp("/scratch/cluster/test/pepxml",Pattern.compile("pep\\.xml$"),"/scratch/cluster/test/results",true,true,true,6,1000,128,true);
        SparkConf conf = converterApp.initSpark();
        Assert.assertEquals("org.apache.spark.serializer.KryoSerializer", conf.get("spark.serializer"));
    }


    @Test
    public void testCommandLineArgs1() throws IOException {

        File psmDir = temporaryFolder.newFolder();
        File hdDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-pd",psmDir.getAbsolutePath(),"-l","-hp",hdDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-rp",".*\\.xml$","-sp" };

        PSMConverterApp PSMConverterApp = new PSMConverterApp();
        try {
            FileUtils.deleteQuietly(hdDir);
            PSMConverterApp.parseOptions(args);

            Assert.assertEquals(hdDir.getAbsolutePath(), PSMConverterApp.getSparkDir());
            Assert.assertEquals(psmDir.getAbsolutePath(), PSMConverterApp.getPsmRootDirName());
            Assert.assertTrue(PSMConverterApp.isRunLocal());
            Assert.assertEquals(4, PSMConverterApp.getNrExec());
            Assert.assertEquals(1000, PSMConverterApp.getExecMemoryMB());
            Assert.assertEquals(128, PSMConverterApp.getBufferMemoryMB());
            Assert.assertEquals(Pattern.compile(".*\\.xml$").toString(), PSMConverterApp.getRegex().toString());
            Assert.assertTrue(PSMConverterApp.isDoSort());

            FileUtils.deleteQuietly(psmDir);

        }
        catch( Exception exp ) {
            PSMConverterApp.printOptions(args, exp.getMessage());
            Assert.fail();
        }

    }

    @Test
    public void testCommandLineArgs2() throws IOException {

        File psmDir = temporaryFolder.newFolder();
        File hdDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-pd",psmDir.getAbsolutePath(),"-l","-hp",hdDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-rp",".*\\.xml$","-sp" };

        PSMConverterApp PSMConverterApp = new PSMConverterApp();
        try {
            PSMConverterApp.parseOptions(args);

            FileUtils.deleteQuietly(psmDir);
            FileUtils.deleteQuietly(hdDir);

            Assert.fail();
        }
        catch( Exception exp ) {
            PSMConverterApp.printOptions(args, exp.getMessage());
            FileUtils.deleteQuietly(psmDir);
            FileUtils.deleteQuietly(hdDir);
        }

    }

    @Test
    public void testCommandLineArgs3() throws IOException {

        File psmDir = temporaryFolder.newFolder();
        File hdDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-pd",psmDir.getAbsolutePath(),"-l","-hp",hdDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-rp",".*\\.xml$","-fd" };

        PSMConverterApp PSMConverterApp = new PSMConverterApp();
        try {
            PSMConverterApp.parseOptions(args);

            FileUtils.deleteQuietly(psmDir);
            FileUtils.deleteQuietly(hdDir);
        }
        catch( Exception exp ) {
            PSMConverterApp.printOptions(args, exp.getMessage());

            Assert.fail();
        }

    }


    @Test
    public void testCommandLineArgs5() throws IOException {

        File psmDir = temporaryFolder.newFolder();
        File hdDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-pd",psmDir.getAbsolutePath(),"-l","-hp",hdDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-rp",".*\\.[xml$","-fd" };

        PSMConverterApp PSMConverterApp = new PSMConverterApp();
        try {
            PSMConverterApp.parseOptions(args);

            Assert.fail();
        }
        catch( Exception exp ) {
            PSMConverterApp.printOptions(args, exp.getMessage());

            FileUtils.deleteQuietly(psmDir);
            FileUtils.deleteQuietly(hdDir);
        }

    }

}