package org.expasy.mzjava.sparktool.psmconverter;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.apache.spark.Accumulator;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.util.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PSMPairFMFTest {

    public class PSMConverterParamsImplTest implements PSMConverterParams {

        private List<String> modifications = Lists.newArrayList("O", "C2H3NO", "C2H2O");
        private double massTol =  0.015;

        @Override
        public Cached<List<String>> modifications() {
            return new Cached<List<String>>() {
                @Override
                protected List<String> build() {
                    return modifications;

                }
            };
        }

        @Override
        public double modMassTol() {
            return 0.015;
        }

        @Override
        public Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumPSMKeyFunction() {
            return new Cached<SpectrumKeyFunction<MsnSpectrum>>() {
                @Override
                protected SpectrumKeyFunction<MsnSpectrum> build() {

//                    return new MgfTitleKeyFunction();
                    return new XTandemMgfKeyFunction();
                }
            };
        }

        @Override
        public Cached<PsmReaderFactory> psmReaderFactory() {
            return new Cached<PsmReaderFactory>() {
                @Override
                protected PsmReaderFactory build() {
                    return new PsmReaderFactoryImpl();
                }
            };
        }

        @Override
        public Cached<PeptideMatchPredicate> psmPredicate() {
            return new Cached<PeptideMatchPredicate>() {
                @Override
                protected PeptideMatchPredicate build() {
                    return new PeptideMatchPredicate(1,10,1,40,"hyperscore",30.0, PeptideMatchPredicate.ScoreOrder.LARGER,1) {
                        public boolean test(PeptideMatch psm) {
                            return true;
                        }
                    };
                }

            };
        }


        @Override
        public String getDecoyProtPattern(){ return "DECOY_";}

        @Override
        public boolean flagDecoyProteins() {
            return false;
        }

    }


    @Test
    public void testCall() throws Exception {

        File file = new File(getClass().getResource("pep_xml_xtandem.pep.xml").toURI());

        //noinspection unchecked
        Accumulator<Integer> skippedPsmCounter = mock(Accumulator.class);
        //noinspection unchecked
        Accumulator<Integer> psmCounter = mock(Accumulator.class);

        PSMPairFMF pepXmlFunc = new PSMPairFMF(new PSMConverterParamsImplTest(), psmCounter, skippedPsmCounter);

        Map<String, PeptideMatch> pepMatches = new HashMap<>();
        pepXmlFunc.call(file).forEachRemaining(tuple -> pepMatches.put(tuple._1(), tuple._2()));

        Assert.assertEquals(8, pepMatches.size());

        Assert.assertEquals(Peptide.parse("HFNAPSHIR"), pepMatches.get("Fetal_Liver_bRP_Elite_22_f26.194").getPeptide());
        Assert.assertEquals(Peptide.parse("SAPFIEC(C2H3NO)HGR"), pepMatches.get("Fetal_Liver_bRP_Elite_22_f26.987").getPeptide());
        Assert.assertEquals(Peptide.parse("SPDDPSR"), pepMatches.get("Fetal_Liver_bRP_Elite_22_f26.6").getPeptide());
        Assert.assertEquals(Peptide.parse("RTLPAIR"), pepMatches.get("Fetal_Liver_bRP_Elite_22_f26.659").getPeptide());
        Assert.assertEquals(Peptide.parse("(C2H2O)_M(O)LRSVWNFLK"), pepMatches.get("Fetal_Liver_bRP_Elite_22_f26.1011").getPeptide());
        Assert.assertEquals(Peptide.parse("AGADEERAETAR"), pepMatches.get("Fetal_Liver_bRP_Elite_22_f26.12").getPeptide());
        Assert.assertEquals(Peptide.parse("AVTPAPPIKR"), pepMatches.get("Fetal_Liver_bRP_Elite_22_f26.413").getPeptide());

        //Check the score map of the spectrum that has two results
        TObjectDoubleMap<String> expectedScores = new TObjectDoubleHashMap<>();
        expectedScores.put("hyperscore", 21.8);
        expectedScores.put("nextscore", 8.0);
        expectedScores.put("bscore", 11.9);
        expectedScores.put("yscore", 9.5);
        expectedScores.put("cscore", 0);
        expectedScores.put("zscore", 0);
        expectedScores.put("ascore", 0);
        expectedScores.put("xscore", 0);
        expectedScores.put("expect", 0.075);
        expectedScores.put("rank",1.0);

        final PeptideMatch peptideMatch = pepMatches.get("Fetal_Liver_bRP_Elite_22_f26.413");
        Assert.assertEquals(expectedScores, peptideMatch.getScoreMap());
        Assert.assertEquals(Sets.newHashSet("sp|O95182|NDUA7_HUMAN"), peptideMatch.getProteinAcc());
        
        verify(skippedPsmCounter).add(1);
        verify(psmCounter).add(8);
    }
}