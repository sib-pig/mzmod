package org.expasy.mzjava.sparktool.mstranslator;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class OpenSwathPeptideConsensusWriterTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void test_write() throws IOException{

        Map<SpectrumKey, Iterable<PeptideConsensusSpectrum>> transitions = new HashMap<>();
        Set<UUID> members = new HashSet<>();
        members.add(UUID.randomUUID());
        members.add(UUID.randomUUID());
        Set<String> prots = new HashSet<>();
        prots.add("prot1");
        prots.add("prot2");

        Peptide peptide = Peptide.parse("PEPTIDE");

        PeptideSpectrum theorSpec = new PeptideFragmenter(EnumSet.of(IonType.b,IonType.y), PeakList.Precision.DOUBLE).fragment(Peptide.parse("PEPTIDE"), 2);
        PeptideConsensusSpectrum consensusSpectrum1 =
                new PeptideConsensusSpectrum(peptide, PeakList.Precision.DOUBLE,members,prots,
                        new RetentionTimeDiscrete(600.0, TimeUnit.SECOND));
        for (int i=0;i<theorSpec.size();i++) {
            consensusSpectrum1.add(theorSpec.getMz(i),theorSpec.getIntensity(i),new PepLibPeakAnnotation(2,0.01,10.0,Optional.of(theorSpec.getAnnotations(i).get(0))));
        }

        transitions.put(new SpectrumKey(peptide.calculateMz(2), 2), Collections.singleton(consensusSpectrum1));

        OpenSwathPeptideConsensusWriter writer = new OpenSwathPeptideConsensusWriter();

        File outFile = temporaryFolder.newFile();

        writer.write(transitions, outFile.getAbsolutePath());

        BufferedReader reader = new BufferedReader(new FileReader(outFile.getAbsoluteFile()));

        String header = reader.readLine();
        Assert.assertEquals("PrecursorMz\tProductMz\tTr_recalibrated\ttransition_name\tCE\tLibraryIntensity\ttransition_group_id\tdecoy\tPeptideSequence\tProteinName\tAnnotation\tFullPeptideName\tMissedCleavages\tReplicates\tNrModifications\tCharge\tGroupLabel",header);
        String line = reader.readLine();
        Assert.assertEquals("400.68726\t49.53366\t600.00\tPEPTIDE\\2_b1^2\t0.0\t1.000\tPEPTIDE\\2\t0\tPEPTIDE\tprot2;prot1\tb1^2\tPEPTIDE\t0\t0\t0\t2\tlight",line);
        line = reader.readLine();
        Assert.assertEquals("400.68726\t74.53385\t600.00\tPEPTIDE\\2_y1^2\t0.0\t1.000\tPEPTIDE\\2\t0\tPEPTIDE\tprot2;prot1\ty1^2\tPEPTIDE\t0\t0\t0\t2\tlight",line);

    }
}
