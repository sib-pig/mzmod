package org.expasy.mzjava.sparktool.mstranslator;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class PepLibPeakComparatorTest {

    @Test
    public void test_compare(){

        PepLibPeakComparator comparator = new PepLibPeakComparator();

        Peptide peptide = Peptide.parse("PEPTIDE");
        PeptideSpectrum theorSpec = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE).fragment(Peptide.parse("PEPTIDE"), 2);

        PepLibPeakAnnotation b_Annot1 = new PepLibPeakAnnotation(5,0.01,10.0, Optional.of(theorSpec.getAnnotations(3).get(0)));
        PepLibPeakAnnotation b_Annot2 = new PepLibPeakAnnotation(5,0.01,5.0, Optional.of(theorSpec.getAnnotations(3).get(0)));
        PepLibPeakAnnotation b_Annot3 = new PepLibPeakAnnotation(5,0.01,5.0, Optional.of(theorSpec.getAnnotations(0).get(0)));
        PepLibPeakAnnotation b_Annot4 = new PepLibPeakAnnotation(10,0.01,20.0, Optional.of(theorSpec.getAnnotations(4).get(0)));
        PepLibPeakAnnotation b_Annot5 = new PepLibPeakAnnotation(12,0.01,11.0, Optional.absent());

        List<PepLibPeakAnnotation>  annots1 = new ArrayList<>();
        annots1.add(b_Annot1);
        List<PepLibPeakAnnotation>  annots2 = new ArrayList<>();
        annots2.add(b_Annot2);
        List<PepLibPeakAnnotation>  annots3 = new ArrayList<>();
        annots3.add(b_Annot3);
        List<PepLibPeakAnnotation>  annots4 = new ArrayList<>();
        annots4.add(b_Annot4);
        List<PepLibPeakAnnotation>  annots5 = new ArrayList<>();
        annots5.add(b_Annot5);

        PepLibPeak pepLibPeak1 = new PepLibPeak(theorSpec.getMz(0),1000.0,annots1);
        PepLibPeak pepLibPeak2 = new PepLibPeak(theorSpec.getMz(0),1100.0,annots1);

        Assert.assertTrue(comparator.compare(pepLibPeak1, pepLibPeak2) < 0);

        pepLibPeak1 = new PepLibPeak(theorSpec.getMz(0),1000.0,annots1);
        pepLibPeak2 = new PepLibPeak(theorSpec.getMz(0),1000.0,annots2);

        Assert.assertTrue(comparator.compare(pepLibPeak1,pepLibPeak2) < 0);

        pepLibPeak1 = new PepLibPeak(theorSpec.getMz(0),1000.0,annots1);
        pepLibPeak2 = new PepLibPeak(theorSpec.getMz(0),1000.0,annots3);

        Assert.assertTrue(comparator.compare(pepLibPeak1,pepLibPeak2) > 0);

        pepLibPeak1 = new PepLibPeak(theorSpec.getMz(0),1000.0,annots1);
        pepLibPeak2 = new PepLibPeak(theorSpec.getMz(0),1100.0,annots4);

        Assert.assertTrue(comparator.compare(pepLibPeak1,pepLibPeak2) < 0);

        pepLibPeak1 = new PepLibPeak(theorSpec.getMz(0),1000.0,annots1);
        pepLibPeak2 = new PepLibPeak(theorSpec.getMz(0),1100.0,annots5);

        Assert.assertTrue(comparator.compare(pepLibPeak1,pepLibPeak2) > 0);
    }
}
