package org.expasy.mzjava.sparktool.mstranslator;

import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.RetentionTimeDiscrete;
import org.expasy.mzjava.core.ms.spectrum.TimeUnit;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class TransitionSelectorTest {

    Comparator<PepLibPeak> comparator = new Comparator<PepLibPeak>() {
        @Override
        public int compare(PepLibPeak p1, PepLibPeak p2) {
            return Double.compare(p1.getIntens(),p2.getIntens());
        }
    };

    @Test
    public void test_TransitionSelector() {
        Map<SpectrumKey, PeptideConsensusSpectrum> transitions = new HashMap<>();
        Set<UUID> members = new HashSet<>();
        members.add(UUID.randomUUID());
        members.add(UUID.randomUUID());
        Set<String> prots = new HashSet<>();
        prots.add("prot1");
        prots.add("prot2");

        Peptide peptide = Peptide.parse("PEPTIDE");

        PeptideSpectrum theorSpec = new PeptideFragmenter(EnumSet.of(IonType.b,IonType.y), PeakList.Precision.DOUBLE).fragment(Peptide.parse("PEPTIDE"), 2);
        PeptideConsensusSpectrum consensusSpectrum =
                new PeptideConsensusSpectrum(peptide, PeakList.Precision.DOUBLE,members,prots,
                        new RetentionTimeDiscrete(600.0, TimeUnit.SECOND));
        for (int i=0;i<theorSpec.size();i++) {
            consensusSpectrum.add(theorSpec.getMz(i),i*100.0,new PepLibPeakAnnotation(2,0.01,10.0, com.google.common.base.Optional.of(theorSpec.getAnnotations(i).get(0))));
        }

        PeptideConsensusSpectrum copy = consensusSpectrum.copy(new TransitionSelector(comparator,6));

        Assert.assertEquals(6,copy.size());
        Assert.assertEquals(consensusSpectrum.getRetentionTime().get().getTime(), copy.getRetentionTime().get().getTime(),0.000001);

        int di = consensusSpectrum.size()-copy.size();
        for (int i=0;i<copy.size();i++) {
            Assert.assertEquals(consensusSpectrum.getMz(i + di), copy.getMz(i), 0.000001);
            Assert.assertEquals(consensusSpectrum.getIntensity(i + di), copy.getIntensity(i), 0.000001);
            Assert.assertTrue(consensusSpectrum.getAnnotations(i + di).get(0).equals(copy.getAnnotations(i).get(0)));
        }

    }

}
