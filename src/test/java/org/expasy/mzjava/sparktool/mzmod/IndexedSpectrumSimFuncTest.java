package org.expasy.mzjava.sparktool.mzmod;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakListFactory;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.expasy.mzjava.stats.NormalizedDotProduct;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IndexedSpectrumSimFuncTest {

    private static final double delta = 0.0001;

    @Test
    public void testCalcSim() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libMzs = new double[]{300, 400, 500};
        double[] libIntensities = new double[]{100, 5, 300};

        double[] queryMzs = new double[]{300, 400, 480, 500};
        double[] queryIntensities = new double[]{100, 5, 200, 300};

        int[] charges = new int[]{1, 1, 0};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 0, 5, 300}, new double[]{100, 5, 200, 300}), score, delta);
    }

    @Test
    public void testCalcSimWighted() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1.5);

        double[] libMzs = new double[]{300, 400, 500};
        double[] libIntensities = new double[]{100, 5, 300};

        double[] queryMzs = new double[]{300, 400, 480, 500};
        double[] queryIntensities = new double[]{100, 5, 200, 300};

        int[] charges = new int[]{1, 1, 0};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(0.630225039, score, delta);
    }

    @Test
    public void testCalcSim2() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libMzs = new double[]{300, 400, 500};
        double[] libIntensities = new double[]{100, 5, 300};

        double[] queryMzs = new double[]{300, 500};
        double[] queryIntensities = new double[]{100, 300};

        int[] charges = new int[]{1, 1, 1};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(libIntensities, new double[]{100, 0, 300}), score, delta);
    }

    @Test
    public void testCalcSim3() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 8, 300};
        double[] queryMzs = new double[]{300, 480, 500};

        int[] charges = new int[]{1, 1, 1};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(libIntensities, queryIntensities), score, delta);
    }

    @Test
    public void testCalcSim4() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 5, 8, 300};
        double[] queryMzs = new double[]{300, 480, 480.5, 500};

        int[] charges = new int[]{1, 1, 1};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 0, 5, 300}, new double[]{100, 5, 8, 300}), score, delta);
    }

    @Test
    public void testCalcSim5() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 8, 5, 300};
        double[] queryMzs = new double[]{300, 480, 480.5, 500};

        int[] charges = new int[]{1, 1, 1};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 5, 0, 300}, new double[]{100, 8, 5, 300}), score, delta);
    }

    @Test
    public void testCalcSim6() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libIntensities = new double[]{100, 80, 1, 300};
        double[] libMzs = new double[]{300, 400, 480, 500};

        double[] queryIntensities = new double[]{100, 8, 300};
        double[] queryMzs = new double[]{300, 480, 500};

        int[] charges = new int[]{1, 1, 1};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 80, 1, 300}, new double[]{100, 8, 0, 300}), score, delta);
    }

    @Test
    public void testCalcSim7() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libIntensities = new double[]{100, 80, 1, 300};
        double[] libMzs = new double[]{300, 400, 480, 500};

        double[] queryIntensities = new double[]{100, 8, 300};
        double[] queryMzs = new double[]{300, 480, 500};

        int[] charges = new int[]{1, 1, 1};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 80, 1, 300}, new double[]{100, 8, 0, 300}), score, delta);
    }

    @Test
    public void testCalcSim8() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libIntensities = new double[]{100, 1, 80, 300};
        double[] libMzs = new double[]{300, 400, 480, 500};

        double[] queryIntensities = new double[]{100, 8, 300};
        double[] queryMzs = new double[]{300, 480, 500};

        int[] charges = new int[]{1, 1, 1};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 1, 80, 300}, new double[]{100, 0, 8, 300}), score, delta);
    }

    @Test
    public void testCalcSim9() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.6), precursorCharge -> precursorCharge - 1, 1);

        double[] libIntensities = new double[]{100, 1, 80, 300};
        double[] libMzs = new double[]{300, 400, 480, 500};

        double[] queryIntensities = new double[]{100, 8, 300, 50, 50, 50};
        double[] queryMzs = new double[]{300, 480, 500, 600, 700, 800};

        int[] charges = new int[]{1, 1, 1};

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 1, 80, 300, 0, 0, 0}, new double[]{100, 0, 8, 300, 50, 50, 50}), score, delta);
    }

    @Test
    public void testCalcSim10() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.4), precursorCharge -> precursorCharge - 1, 1);

        double[] libIntensities = new double[]{4924.601563, 2722.90625, 2893.07959, 7118.64209, 3116.016846, 2689.081299, 5995.705566, 2956.065918, 10000, 3195.048584};
        double[] libMzs = new double[]{706.3181763, 736.3513184, 782.7261963, 785.3250732, 786.0776978, 910.4876099, 943.451416, 1000.140625, 1045.435547, 1046.379028};

        double[] queryIntensities = new double[]{162.9400024, 201.8630066, 118.5680008, 127.6940002, 202.4629974, 103.0339966, 147.8970032, 183.2559967, 107.1439972, 145.0800018};
        double[] queryMzs = new double[]{488.3349915, 601.3359985, 785.8359985, 899.9849854, 942.8140259, 943.5180054, 1000.362976, 1014.512024, 1113.483032, 1143.607056};

        int[] charges = new int[Math.max(libMzs.length, queryMzs.length)];

        List<IndexedSpectrum> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -0.0503, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(
                        new double[]{0, 0, 4924.601563, 2722.90625, 2893.07959, 7118.64209, 3116.016846, 0, 2689.081299, 0, 5995.705566, 2956.065918, 0, 10000, 3195.048584, 0, 0},
                        new double[]{162.9400024, 201.8630066, 0, 0, 0, 0, 118.5680008, 127.6940002, 0, 202.4629974, 103.0339966, 147.8970032, 183.2559967, 0, 0, 107.1439972, 145.0800018}),
                score, delta);
    }

    @Test
    public void testDist() throws Exception {

        IndexedSpectrumSimFunc simFunc = new IndexedSpectrumSimFunc(new AbsoluteTolerance(0.5), precursorCharge -> precursorCharge - 1, 1.5);

        PeakList src = new DoublePeakList<>();
        src.add(1, 4);
        src.add(2, 3);
        src.add(3, 2);
        src.add(4, 1);

        IndexedSpectrum peakList = new IndexedSpectrum(src);
        BitSet backbonePeaks = new BitSet(4);
        backbonePeaks.set(0);
        backbonePeaks.set(2);
        Assert.assertEquals(7.416198487, simFunc.dist(peakList, backbonePeaks), 0.0000001);
    }

    private List<IndexedSpectrum> makePeakLists(double[] libMzs, double[] libIntensities, double[] queryMzs, double[] queryIntensities,
                                                         double massDelta, int[] charges, int precursorCharge) {

        PeakList<PeakAnnotation> libPeakList = PeakListFactory.newPeakList(PeakList.Precision.DOUBLE, libMzs, libIntensities, libMzs.length);
        PeakList<PeakAnnotation> queryPeakList = PeakListFactory.newPeakList(PeakList.Precision.DOUBLE, queryMzs, queryIntensities, queryMzs.length);

        libPeakList.getPrecursor().setMzAndCharge(487.36/precursorCharge, precursorCharge);
        queryPeakList.getPrecursor().setMzAndCharge((487.36+ massDelta) / precursorCharge, precursorCharge);

        for (int i = 0; i < charges.length; i++) {

            int fragCharge = charges[i];

            if (fragCharge > 0) {

                final PeakAnnotation annotation = mock(PeakAnnotation.class);
                when(annotation.getCharge()).thenReturn(fragCharge);
                libPeakList.addAnnotation(i, annotation);
            }
        }

        List<IndexedSpectrum> peakLists = new ArrayList<>(2);
        peakLists.add(new IndexedSpectrum(libPeakList));
        peakLists.add(new IndexedSpectrum(queryPeakList));

        return peakLists;
    }
}