package org.expasy.mzjava.sparktool.mzmod;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.CachedAbsoluteTolerance;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.junit.Assert;
import org.junit.Test;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;

public class OmsResultsExportTest {

    @Test
    public void testMetaScoreExtractScoresPF() throws Exception {

        OmsResultsExport.MetaScoreExtractScoresPF metaScoreExport = new OmsResultsExport.MetaScoreExtractScoresPF(2, new CachedAbsoluteTolerance(0.05), true);

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);

        List<OmsResult> results = new ArrayList<>();
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("PLK"), UUID.randomUUID(), "PROT1 PROT2", 1, 33.9, 1.3, 0.8, 0.5, 1, new double[]{0.0, 1.0, 0.0}));
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("PMK"), UUID.randomUUID(), "PROT1 PROT2", 1, 15.9, 1.2, 0.7, 0.4, 1, new double[]{0.0, 1.0, 0.0}));
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("SRM"), UUID.randomUUID(), "PROT1 PROT2", 1,  1.9, 0.5, 0.4, 0.1, 1, new double[]{0.0, 1.0, 0.0}));
        results.add(new OmsResult(HitType.OPEN, Peptide.parse("ARK"), UUID.randomUUID(), "PROT1 PROT2", 1, 91.9, 0.4, 0.3, 0.1, 1, new double[]{0.0, 1.0, 0.0}));

        OmsQuery query = new OmsQuery(spectrum, results);
        Tuple2<Double, Double> result = metaScoreExport.call(query).next();

        Assert.assertEquals(1.3, result._1(), 0.000001);
        Assert.assertEquals(0.5, result._2(), 0.000001);
    }
}