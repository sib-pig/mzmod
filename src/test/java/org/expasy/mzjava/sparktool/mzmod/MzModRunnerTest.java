package org.expasy.mzjava.sparktool.mzmod;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.InvalidPathException;
import org.apache.spark.SparkConf;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import java.io.File;
import java.io.IOException;

public class MzModRunnerTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testWrongNumberOfArguments() throws Exception {

        MzModRunner.main(new String[0]);
    }

    @Test
    public void testInitServer() throws Exception {

        MzModRunner runner = new MzModRunner("/scratch/cluster/test/spectra", "/scratch/cluster/test/library", "/scratch/cluster/test/results", 1, true, 6, 1000, 128,true);

        SparkConf conf = runner.initSpark();
        Assert.assertEquals("org.apache.spark.serializer.KryoSerializer", conf.get("spark.serializer"));
    }

    @Test
    public void testCommandLineArgs1() throws IOException {

        File hdmsnFile = temporaryFolder.newFile();
        File hdpsmFile = temporaryFolder.newFile();
        File hdmzmodDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-hs",hdmsnFile.getAbsolutePath(),"-hl",hdpsmFile.getAbsolutePath(),"-l","-hm",hdmzmodDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-sf","0.15","-fd" };

        MzModRunner mzModRunner = new MzModRunner();
        try {
            mzModRunner.parseOptions(args);

            Assert.assertEquals(hdmzmodDir.getAbsolutePath(), mzModRunner.getSparkDir());
            Assert.assertEquals(hdmsnFile.getAbsolutePath(), mzModRunner.getSpectraHdFile());
            Assert.assertEquals(hdpsmFile.getAbsolutePath(), mzModRunner.getLibraryHdFile());
            Assert.assertTrue(mzModRunner.isRunLocal());
            Assert.assertEquals(4, mzModRunner.getNrExec());
            Assert.assertEquals(1000,mzModRunner.getExecMemoryMB());
            Assert.assertEquals(128,mzModRunner.getBufferMemoryMB());
            Assert.assertEquals(0.15,mzModRunner.getQuerySpectraSampelFraction(),0.001);

            FileUtils.deleteQuietly(hdmsnFile);
            FileUtils.deleteQuietly(hdpsmFile);
        }
        catch( Exception exp ) {
            mzModRunner.printOptions(args,exp.getMessage());
            Assert.fail();
        }

    }

    @Test
    public void testCommandLineArgs2() throws IOException {

        File hdmsnFile = temporaryFolder.newFile();
        File hdpsmFile = temporaryFolder.newFile();
        FileUtils.deleteQuietly(hdmsnFile);

        String[] args = new String[]{ "-hs",hdmsnFile.getAbsolutePath(),"-hl",hdpsmFile.getAbsolutePath(),"-l","-hm","/scratch/mzmod","-n","4","-em","1000","-bm","128" };

        MzModRunner mzModRunner = new MzModRunner();
        try {
            mzModRunner.parseOptions(args);
            FileUtils.deleteQuietly(hdpsmFile);

            Assert.fail();
        }
        catch( InvalidPathException exp ) {
            mzModRunner.printOptions(args,exp.getMessage());
            FileUtils.deleteQuietly(hdpsmFile);
            Assert.assertTrue(true);
            return;
        }
        catch( ParseException exp ) {
            mzModRunner.printOptions(args, exp.getMessage());
            FileUtils.deleteQuietly(hdpsmFile);
            Assert.fail();
        }
    }

    @Test
    public void testCommandLineArgs3() throws IOException {

        File hdmsnFile = temporaryFolder.newFile();
        File hdpsmFile = temporaryFolder.newFile();
        FileUtils.deleteQuietly(hdpsmFile);

        String[] args = new String[]{ "-hs",hdmsnFile.getAbsolutePath(),"-hl",hdpsmFile.getAbsolutePath(),"-l","-hm","/scratch/mzmod", };

        MzModRunner mzModRunner = new MzModRunner();
        try {
            mzModRunner.parseOptions(args);

            FileUtils.deleteQuietly(hdmsnFile);
            Assert.fail();
        }
        catch( InvalidPathException exp ) {
            mzModRunner.printOptions(args,exp.getMessage());

            FileUtils.deleteQuietly(hdmsnFile);
            Assert.assertTrue(true);
        }
        catch( ParseException exp ) {
            mzModRunner.printOptions(args, exp.getMessage());
            FileUtils.deleteQuietly(hdmsnFile);
            Assert.fail();
        }
    }

    @Test
    public void testCommandLineArgs4() throws IOException {
        File hdpsmFile = temporaryFolder.newFile();

        MzModRunner mzModRunner = new MzModRunner();
        String[] args = new String[]{"-hl", hdpsmFile.getAbsolutePath(), "-hm", "/scratch/mzmod"};

        try {
            mzModRunner.parseOptions(args);

            Assert.fail();
        } catch (ParseException exp) {
            mzModRunner.printOptions(args, exp.getMessage());
            Assert.assertTrue(exp.getMessage().contains(": hs"));
        }

        args = new String[]{"-hs", hdpsmFile.getAbsolutePath(), "-hm", "/scratch/mzmod"};

        try {
            mzModRunner.parseOptions(args);

            Assert.fail();
        } catch (ParseException exp) {
            mzModRunner.printOptions(args, exp.getMessage());
            Assert.assertTrue(exp.getMessage().contains(": hl"));
            FileUtils.deleteQuietly(hdpsmFile);
        }
    }

    @Test
    public void testCommandLineArgs5() throws IOException {

        File hdmsnFile = temporaryFolder.newFile();
        File hdpsmFile = temporaryFolder.newFile();
        File hdmzmodDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-hs",hdmsnFile.getAbsolutePath(),"-hl",hdpsmFile.getAbsolutePath(),"-l","-hm",hdmzmodDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-sf","0.15" };

        MzModRunner mzModRunner = new MzModRunner();

        try {
            mzModRunner.parseOptions(args);

            FileUtils.deleteQuietly(hdmsnFile);
            FileUtils.deleteQuietly(hdpsmFile);
            FileUtils.deleteQuietly(hdmzmodDir);

            Assert.fail();

        } catch (Exception e) {
            mzModRunner.printOptions(args, e.getMessage());
            Assert.assertTrue(e.getMessage().contains("-hm"));

            FileUtils.deleteQuietly(hdmsnFile);
            FileUtils.deleteQuietly(hdpsmFile);
            FileUtils.deleteQuietly(hdpsmFile);
        }
    }

    @Test
    public void testCommandLineArgs6() throws IOException {

        File hdmsnFile = temporaryFolder.newFile();
        File hdpsmFile = temporaryFolder.newFile();
        File hdmzmodDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-hs",hdmsnFile.getAbsolutePath(),"-hl",hdpsmFile.getAbsolutePath(),"-l","-hm",hdmzmodDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-sf","1.15","-fd" };

        MzModRunner mzModRunner = new MzModRunner();

        try {
            mzModRunner.parseOptions(args);

            FileUtils.deleteQuietly(hdmsnFile);
            FileUtils.deleteQuietly(hdpsmFile);
            FileUtils.deleteQuietly(hdpsmFile);
            Assert.fail();

        } catch (ParseException e) {
        } catch (NumberFormatException exp) {
            mzModRunner.printOptions(args, exp.getMessage());
            Assert.assertTrue(exp.getMessage().contains("-sf"));

            FileUtils.deleteQuietly(hdmsnFile);
            FileUtils.deleteQuietly(hdpsmFile);
            FileUtils.deleteQuietly(hdpsmFile);

        }

    }
}