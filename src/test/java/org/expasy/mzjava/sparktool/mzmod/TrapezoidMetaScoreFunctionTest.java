package org.expasy.mzjava.sparktool.mzmod;

import junit.framework.TestCase;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.NdpResult;
import org.expasy.mzjava.sparktool.position.data.PositionResult;
import org.junit.Assert;

import java.util.UUID;

public class TrapezoidMetaScoreFunctionTest extends TestCase {

    public void testCalcMetaScore() throws Exception {

        MetaScoreFunction function = new TrapezoidMetaScoreFunction();

        final NdpResult ndpResult = new NdpResult(Peptide.parse("PEK"), UUID.randomUUID(), "", 1, 0.9, 79.9, HitType.OPEN);
        final PositionResult positionResult = new PositionResult.Builder()
                .reset(3)
                .setScore(0, 0.1)
                .setScore(1, 0.8)
                .setScore(2, 0.1)
                .build(1, 0.6);

        Assert.assertEquals(0.9 + 1 * 0.6, function.calcMetaScore(ndpResult, positionResult), 0.00001);
    }

    public void testCalcMetaScore2() throws Exception {

        MetaScoreFunction function = new TrapezoidMetaScoreFunction();

        final NdpResult ndpResult = new NdpResult(Peptide.parse("PENNK"), UUID.randomUUID(), "", 1, 0.9, 79.9, HitType.OPEN);
        final PositionResult positionResult = new PositionResult.Builder()
                .reset(5)
                .setScore(0, 0.09)
                .setScore(1, 0.8)
                .setScore(2, 0.09)
                .setScore(3, 0.01)
                .setScore(4, 0.01)
                .build(1, 0.6);

        Assert.assertEquals(1.46210, function.calcMetaScore(ndpResult, positionResult), 0.00001);
    }

    //Changed the proportion in the residues adjacent to the position
    public void testCalcMetaScore3() throws Exception {

        MetaScoreFunction function = new TrapezoidMetaScoreFunction();

        final NdpResult ndpResult = new NdpResult(Peptide.parse("PENNK"), UUID.randomUUID(), "", 1, 0.9, 79.9, HitType.OPEN);
        final PositionResult positionResult = new PositionResult.Builder()
                .reset(5)
                .setScore(0, 0.06)
                .setScore(1, 0.8)
                .setScore(2, 0.12)
                .setScore(3, 0.01)
                .setScore(4, 0.01)
                .build(1, 0.6);

        Assert.assertEquals(1.45336, function.calcMetaScore(ndpResult, positionResult), 0.00001);
    }

    public void testCalcMetaScore4() throws Exception {

        MetaScoreFunction function = new TrapezoidMetaScoreFunction();

        final NdpResult ndpResult = new NdpResult(Peptide.parse("SSQVPR"), UUID.randomUUID(), "", 1, 0.331141833, 324.22731105281787, HitType.OPEN);
        final PositionResult positionResult = new PositionResult.Builder()
                .reset(6)
                .setScore(0, 0.49985626710642433)
                .setScore(1, 0.49985626710642433)
                .setScore(2, 2.3407695311217984E-4)
                .setScore(3, 2.3644136677997858E-6)
                .setScore(4, 2.388296634141205E-8)
                .setScore(5, 5.10005374050019E-5)
                .build(1, 0.6);

        Assert.assertEquals(0.9310258985337366, function.calcMetaScore(ndpResult, positionResult), 0.00001);
    }
}