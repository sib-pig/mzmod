package org.expasy.mzjava.sparktool.mzmod;

import com.google.common.collect.Lists;
import org.expasy.mzjava.core.io.IterativeReaders;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.*;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.spectrum.library.IntervalSpectraLibrary;
import org.expasy.mzjava.proteomics.ms.spectrum.library.SpectraLibrary;
import org.expasy.mzjava.sparktool.data.*;
import org.expasy.mzjava.sparktool.position.PositionFunction;
import org.expasy.mzjava.sparktool.position.data.PositionResult;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OmsFuncTest {

    @Test
    public void test() throws Exception {

        IndexedPeptideSpectrum lib1 = newPepSpectrum(Peptide.parse("PSPTIDEK"), 2);
        IndexedPeptideSpectrum lib2 = newPepSpectrum(Peptide.parse("PSPMIDEK"), 2);
        IndexedPeptideSpectrum lib3 = newPepSpectrum(Peptide.parse("PCPMIDEK"), 2);
        IndexedPeptideSpectrum lib4 = newPepSpectrum(Peptide.parse("PSPAIDEK"), 2);
        IndexedPeptideSpectrum lib5 = newPepSpectrum(Peptide.parse("PSPRIDEK"), 2);
        IndexedPeptideSpectrum lib6 = newPepSpectrum(Peptide.parse("PSPJIDEK"), 2);
        IndexedPeptideSpectrum lib7 = newPepSpectrum(Peptide.parse("PSPPIDEK"), 2);

        ArrayList<IndexedPeptideSpectrum> libSpectra = Lists.newArrayList(lib1, lib2, lib3, lib4, lib5, lib6, lib7);
        Collections.sort(libSpectra, new PeakListComparator());
        SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum> library = new IntervalSpectraLibrary<>(IterativeReaders.fromCollection(libSpectra), -10, 400);

        IndexedSpectrumSimFunc simFunc = mock(IndexedSpectrumSimFunc.class);
        when(simFunc.calcSimilarity(Mockito.eq(lib1), any(IndexedSpectrum.class))).thenReturn(0.9);
        when(simFunc.calcSimilarity(Mockito.eq(lib2), any(IndexedSpectrum.class))).thenReturn(0.8);
        when(simFunc.calcSimilarity(Mockito.eq(lib3), any(IndexedSpectrum.class))).thenReturn(0.7);
        when(simFunc.calcSimilarity(Mockito.eq(lib4), any(IndexedSpectrum.class))).thenReturn(0.6);
        when(simFunc.calcSimilarity(Mockito.eq(lib5), any(IndexedSpectrum.class))).thenReturn(0.5);
        when(simFunc.calcSimilarity(Mockito.eq(lib6), any(IndexedSpectrum.class))).thenReturn(0.4);
        when(simFunc.calcSimilarity(Mockito.eq(lib7), any(IndexedSpectrum.class))).thenReturn(0.3);

        MsnSpectrum querySpectrum = new MsnSpectrum();
        querySpectrum.getPrecursor().setValues(458.725 + 15.9 / 2, 45, 2); //PSPM(O)IDEK
        querySpectrum.add(479.731, 12);

        Tolerance precursorTolerance = new AbsoluteTolerance(0.1);
        PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<>(new IdentityPeakProcessor<>());

        PositionFunction positionFunction = mock(PositionFunction.class);
        when(positionFunction.position(querySpectrum, newOpenResult(0.9, querySpectrum, lib1))).thenReturn(newPositionResult(3, new double[]{0, 0, 0, 1, 0, 0, 0, 0}, 0.01));
        when(positionFunction.position(querySpectrum, newOpenResult(0.8, querySpectrum, lib2))).thenReturn(newPositionResult(3, new double[]{0, 0, 0, 1, 0, 0, 0, 0}, 0.3));
        when(positionFunction.position(querySpectrum, newExactResult(0.7, querySpectrum, lib3))).thenReturn(newPositionResult(3, new double[]{0, 0, 0, 1, 0, 0, 0, 0}, 0.01));
        when(positionFunction.position(querySpectrum, newOpenResult(0.6, querySpectrum, lib4))).thenReturn(newPositionResult(3, new double[]{0, 0, 0, 1, 0, 0, 0, 0}, 0.01));
        when(positionFunction.position(querySpectrum, newOpenResult(0.5, querySpectrum, lib5))).thenThrow(new IllegalStateException("Should not be position lib5"));
        when(positionFunction.position(querySpectrum, newOpenResult(0.4, querySpectrum, lib6))).thenThrow(new IllegalStateException("Should not be position lib6"));
        when(positionFunction.position(querySpectrum, newOpenResult(0.3, querySpectrum, lib7))).thenThrow(new IllegalStateException("Should not be position lib7"));

        MetaScoreFunction metaScoreFunction = new TrapezoidMetaScoreFunction();

        OmsFunc omsFunc = new OmsFunc(newModStarParameters(library, simFunc, precursorTolerance, processorChain, positionFunction, metaScoreFunction), "path");

        OmsQuery query = new OmsQuery(querySpectrum);

        OmsQuery result = omsFunc.call(query);

        Assert.assertEquals(3, result.getResultCount());
        Assert.assertEquals(Peptide.parse("PSPMIDEK"), result.getResult(0).getPeptide());
        Assert.assertEquals(Peptide.parse("PSPTIDEK"), result.getResult(1).getPeptide());
        Assert.assertEquals(Peptide.parse("PCPMIDEK"), result.getResult(2).getPeptide());
    }

    //Tests function when there are less library spectra than the result limit
    @Test
    public void test2() throws Exception {

        IndexedPeptideSpectrum lib1 = newPepSpectrum(Peptide.parse("PSPTIDEK"), 2);
        IndexedPeptideSpectrum lib2 = newPepSpectrum(Peptide.parse("PSPMIDEK"), 2);

        ArrayList<IndexedPeptideSpectrum> libSpectra = Lists.newArrayList(lib1, lib2);
        Collections.sort(libSpectra, new PeakListComparator());
        SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum> library = new IntervalSpectraLibrary<>(IterativeReaders.fromCollection(libSpectra), -10, 400);

        IndexedSpectrumSimFunc simFunc = mock(IndexedSpectrumSimFunc.class);
        when(simFunc.calcSimilarity(Mockito.eq(lib1), any(IndexedSpectrum.class))).thenReturn(0.9);
        when(simFunc.calcSimilarity(Mockito.eq(lib2), any(IndexedSpectrum.class))).thenReturn(0.8);

        MsnSpectrum querySpectrum = new MsnSpectrum();
        querySpectrum.getPrecursor().setValues(458.725 + 15.9 / 2, 45, 2); //PSPM(O)IDEK
        querySpectrum.add(479.731, 12);

        Tolerance precursorTolerance = new AbsoluteTolerance(0.1);
        PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<>(new IdentityPeakProcessor<>());

        PositionFunction positionFunction = mock(PositionFunction.class);
        when(positionFunction.position(querySpectrum, newOpenResult(0.9, querySpectrum, lib1))).thenReturn(newPositionResult(3, new double[]{0, 0, 0, 1, 0, 0, 0, 0}, 0.01));
        when(positionFunction.position(querySpectrum, newOpenResult(0.8, querySpectrum, lib2))).thenReturn(newPositionResult(3, new double[]{0, 0, 0, 1, 0, 0, 0, 0}, 0.3));

        MetaScoreFunction metaScoreFunction = new TrapezoidMetaScoreFunction();

        OmsFunc omsFunc = new OmsFunc(newModStarParameters(library, simFunc, precursorTolerance, processorChain, positionFunction, metaScoreFunction), "path");

        OmsQuery query = new OmsQuery(querySpectrum);

        OmsQuery result = omsFunc.call(query);

        Assert.assertEquals(2, result.getResultCount());
        Assert.assertEquals(Peptide.parse("PSPMIDEK"), result.getResult(0).getPeptide());
        Assert.assertEquals(Peptide.parse("PSPTIDEK"), result.getResult(1).getPeptide());
    }

    //Tests function when there are less library spectra than the result limit
    @Test
    public void testEmptyLib() throws Exception {

        SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum> library = new IntervalSpectraLibrary<>(IterativeReaders.empty(), -10, 400);

        IndexedSpectrumSimFunc simFunc = mock(IndexedSpectrumSimFunc.class);

        MsnSpectrum querySpectrum = new MsnSpectrum();
        querySpectrum.getPrecursor().setValues(458.725 + 15.9 / 2, 45, 2); //PSPM(O)IDEK
        querySpectrum.add(479.731, 12);

        Tolerance precursorTolerance = new AbsoluteTolerance(0.1);
        PeakProcessorChain<PeakAnnotation> processorChain = new PeakProcessorChain<>(new IdentityPeakProcessor<>());
        PositionFunction positionFunction = mock(PositionFunction.class);

        MetaScoreFunction metaScoreFunction = new TrapezoidMetaScoreFunction();

        OmsFunc omsFunc = new OmsFunc(newModStarParameters(library, simFunc, precursorTolerance, processorChain, positionFunction, metaScoreFunction), "path");

        OmsQuery query = new OmsQuery(querySpectrum);

        OmsQuery result = omsFunc.call(query);

        Assert.assertEquals(0, result.getResultCount());
    }

    private NdpResult newOpenResult(double wndp, MsnSpectrum queryPl, IndexedPeptideSpectrum libPl) {

        final double shift = queryPl.getPrecursor().getMass() - libPl.getPrecursor().getMass();
        return new NdpResult(libPl.getPeptide(), libPl.getId(), "PROT1", 1, wndp, shift, HitType.OPEN);
    }

    private NdpResult newExactResult(double wndp, MsnSpectrum queryPl, IndexedPeptideSpectrum libPl) {

        final double shift = queryPl.getPrecursor().getMass() - libPl.getPrecursor().getMass();
        return new NdpResult(libPl.getPeptide(), libPl.getId(), "PROT2", 1, wndp, shift, HitType.EXACT);
    }

    private PositionResult newPositionResult(int position, double[] scores, double coverage) {

        PositionResult.Builder builder = new PositionResult.Builder()
                .reset(scores.length);
        for (int i = 0; i < scores.length; i++) {
            builder.setScore(i, scores[i]);
        }
        return builder.build(position, coverage);
    }

    private IndexedPeptideSpectrum newPepSpectrum(Peptide peptide, int charge) {

        double precursorMz = peptide.calculateMz(charge);

        IndexedPeptideSpectrum.Builder builder = new IndexedSpectrum.Builder();
        builder.addPeak(precursorMz, 59, charge);
        Peak precursor = new Peak(precursorMz, 4, charge);

        return new IndexedPeptideSpectrum(peptide, Collections.emptySet(), 4, builder, precursor, UUID.randomUUID(), PeakList.Precision.FLOAT);
    }

    private MzModParameters newModStarParameters(SpectraLibrary<PeakAnnotation, IndexedPeptideSpectrum> library, IndexedSpectrumSimFunc simFunc, Tolerance precursorTolerance, PeakProcessorChain<PeakAnnotation> processorChain, PositionFunction positionFunction, MetaScoreFunction metaScoreFunction) {

        MzModParameters parameters = mock(MzModParameters.class);
        when(parameters.library(any())).thenReturn(new MockCached<>(library));
        when(parameters.libSpectrumPredicate()).thenReturn(new MockCached<>(libSpec -> true));
        when(parameters.omsSimFunc()).thenReturn(new MockCached<>(simFunc));
        when(parameters.processorChain()).thenReturn(new MockCached<>(processorChain));
        when(parameters.positionFunction()).thenReturn(new MockCached<>(positionFunction));
        when(parameters.maxOmsResults()).thenReturn(3);
        when(parameters.metaScoreFunction()).thenReturn(new MockCached<>(metaScoreFunction));
        when(parameters.precursorTolerance()).thenReturn(new MockCached<>(precursorTolerance));
        return parameters;
    }
}