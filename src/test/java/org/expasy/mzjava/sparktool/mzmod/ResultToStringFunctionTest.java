package org.expasy.mzjava.sparktool.mzmod;

import com.google.common.collect.Lists;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.expasy.mzjava.sparktool.data.OmsResult;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ResultToStringFunctionTest {

    @Test
    public void testCall() throws Exception {

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);
        when(spectrum.getPrecursor()).thenReturn(new Peak(1537.08, 12, 2));
        when(spectrum.getComment()).thenReturn("Spectrum 1");
        when(spectrum.getId()).thenReturn(UUID.fromString("98edeeb9-a5fd-4691-be07-205499ee938a"));

        List<OmsResult> results = Lists.newArrayList(

                new OmsResult(HitType.OPEN, Peptide.parse("PEK"), UUID.fromString("b84bbb8c-b23a-428b-af62-30a2be1d4e51"), "PROT1 PROT2", 1, 15.9, 1.8, 0.9, 0.2, 1, new double[]{0, 1, 0})
        );
        OmsQuery query = new OmsQuery(spectrum, results);

        Assert.assertEquals(
                "PE(15.9)K\tPEK\tPROT1,PROT2\t2\t1537.08\tSpectrum 1\t15.9\t1\t0.9\t1.8\t1.0\t0.2\t1\t1.0\t0.0,1.0,0.0\tOPEN",
                new ResultToStringFunction(1).call(query));
    }
}