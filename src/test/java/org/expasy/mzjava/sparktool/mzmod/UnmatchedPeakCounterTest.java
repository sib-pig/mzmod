package org.expasy.mzjava.sparktool.mzmod;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;

public class UnmatchedPeakCounterTest {

    private final PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b), PeakList.Precision.DOUBLE_CONSTANT);

    @Test
    public void testAsymmetric() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.asymmetricUnmatched(
                fragmenter.fragment(Peptide.parse("DJR"), 1),
                fragmenter.fragment(Peptide.parse("DMR"), 1)
        );
        Assert.assertEquals(0, peakDelta);
    }

    @Test
    public void testAsymmetric1() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.asymmetricUnmatched(
                fragmenter.fragment(Peptide.parse("DJQJTQSPSSJSASVGDR"), 1),
                fragmenter.fragment(Peptide.parse("DJQMTQSPSSJSASVGDR"), 1)
        );
        Assert.assertEquals(0, peakDelta);
    }

    @Test
    public void testAsymmetric2() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.asymmetricUnmatched(
                fragmenter.fragment(Peptide.parse("DDJAAJVVDNGSGMC(C2H3NO)K"), 1),
                fragmenter.fragment(Peptide.parse("DDDJAALVVDNGSGMC(C2H3NO)K"), 1)
        );
        Assert.assertEquals(0, peakDelta);
    }

    @Test
    public void testAsymmetric3() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.asymmetricUnmatched(
                fragmenter.fragment(Peptide.parse("JSEJEAAJQR"), 1),
                fragmenter.fragment(Peptide.parse("EEJEAAJQR"), 1)
        );
        Assert.assertEquals(1, peakDelta);
    }

    @Test
    public void testAsymmetric4() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.asymmetricUnmatched(
                fragmenter.fragment(Peptide.parse( "EEJEAAJQR"), 1),
                fragmenter.fragment(Peptide.parse("JSEJEAAJQR"), 1)
        );
        Assert.assertEquals(0, peakDelta);
    }

    @Test
    public void testAsymmetric5() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.asymmetricUnmatched(
                fragmenter.fragment(Peptide.parse("EEEJAAJVJDNGSGMC(C2H3NO)K"), 1),
                fragmenter.fragment(Peptide.parse("DDDJAAJVVDNGSGMC(C2H3NO)K"), 1)
        );
        Assert.assertEquals(8, peakDelta);
    }

    @Test
    public void testAsymmetric6() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.asymmetricUnmatched(
                fragmenter.fragment(Peptide.parse("AVFVDJEPTVVDEVR"), 1),
                fragmenter.fragment(Peptide.parse("AVMJDJEPTVVDEVR"), 1)
        );
        Assert.assertEquals(1, peakDelta);
    }

    @Test
    public void testAsymmetric7() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.asymmetricUnmatched(
                fragmenter.fragment(Peptide.parse("ASJEAAJADAEQR"), 1),
                fragmenter.fragment(Peptide.parse("RC(C2H3NO)EPJAMTVPR"), 1)
        );
        Assert.assertEquals(8, peakDelta);
    }

    @Test
    public void testSymmetric1() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.symmetricUnmatched(
                fragmenter.fragment(Peptide.parse("JSEJEAAJQR"), 1),
                fragmenter.fragment(Peptide.parse("EEJEAAJQR"), 1)
        );
        Assert.assertEquals(1, peakDelta);

        peakDelta = unmatchedPeakCounter.symmetricUnmatched(
                fragmenter.fragment(Peptide.parse("EEJEAAJQR"), 1),
                fragmenter.fragment(Peptide.parse("JSEJEAAJQR"), 1)
        );
        Assert.assertEquals(1, peakDelta);
    }

    @Test
    public void testSymmetric2() throws Exception {

        UnmatchedPeakCounter unmatchedPeakCounter = new UnmatchedPeakCounter(new AbsoluteTolerance(0.05));
        int peakDelta = unmatchedPeakCounter.symmetricUnmatched(
                fragmenter.fragment(Peptide.parse("EEEJAAJVJDNGSGMC(C2H3NO)K"), 1),
                fragmenter.fragment(Peptide.parse("DDDJAAJVVDNGSGMC(C2H3NO)K"), 1)
        );
        Assert.assertEquals(16, peakDelta);

        peakDelta = unmatchedPeakCounter.symmetricUnmatched(
                fragmenter.fragment(Peptide.parse("DDDJAAJVVDNGSGMC(C2H3NO)K"), 1),
                fragmenter.fragment(Peptide.parse("EEEJAAJVJDNGSGMC(C2H3NO)K"), 1)
        );
        Assert.assertEquals(16, peakDelta);
    }
}