package org.expasy.mzjava.sparktool.position.position;

import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.sparktool.position.data.PositionIntermediate;
import org.expasy.mzjava.sparktool.position.data.PositionResult;
import org.junit.Assert;
import org.junit.Test;

public class BayesPosScoreFunctionTest {

    @Test
    public void testCall() throws Exception {

        PositionIntermediate intermediate = new PositionIntermediate.Builder()
                .reset(7)
                .setModProbability(FragmentType.FORWARD, 0, 0.1)
                .setModProbability(FragmentType.FORWARD, 1, 0.1)
                .setModProbability(FragmentType.FORWARD, 2, 0.9)
                .setModProbability(FragmentType.FORWARD, 3, 0.9)
                .setModProbability(FragmentType.FORWARD, 4, 0.9)
                .setModProbability(FragmentType.FORWARD, 5, 0.9)
                .setModProbability(FragmentType.FORWARD, 6, 0.9)
                .setModProbability(FragmentType.REVERSE, 0, 0.9)
                .setModProbability(FragmentType.REVERSE, 1, 0.9)
                .setModProbability(FragmentType.REVERSE, 2, 0.9)
                .setModProbability(FragmentType.REVERSE, 3, 0.1)
                .setModProbability(FragmentType.REVERSE, 4, 0.1)
                .setModProbability(FragmentType.REVERSE, 5, 0.1)
                .setModProbability(FragmentType.REVERSE, 6, 0.1)
                .build(Composition.parseComposition("HPO3"), 0.54);

        BayesPosScoreFunction positionFunction = new BayesPosScoreFunction();

        PositionResult result = positionFunction.apply(intermediate);

        Assert.assertEquals(2, result.getPosition());

        Assert.assertEquals(0.000148699, result.getScore(0), 0.000000001);
        Assert.assertEquals(0.012044587, result.getScore(1), 0.000000001);
        Assert.assertEquals(0.97561157, result.getScore(2), 0.000000001);
        Assert.assertEquals(0.012044587, result.getScore(3), 0.000000001);
        Assert.assertEquals(0.000148699, result.getScore(4), 0.000000001);
        Assert.assertEquals(1.83579E-06, result.getScore(5), 0.000000001);
        Assert.assertEquals(2.2664E-08, result.getScore(6), 0.000000001);

        Assert.assertEquals(0.97561157, result.getScore(2), 0.000000001);
    }
}