package org.expasy.mzjava.sparktool.position;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.HitType;
import org.expasy.mzjava.sparktool.data.NdpResult;
import org.expasy.mzjava.sparktool.position.annotation.PosAnnotationFunction;
import org.expasy.mzjava.sparktool.position.data.PositionIntermediate;
import org.expasy.mzjava.sparktool.position.data.PositionResult;
import org.expasy.mzjava.sparktool.position.position.PosScoreFunction;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PositionFunctionTest {

    @Test
    public void testCall() throws Exception {

        PositionIntermediate intermediate = mock(PositionIntermediate.class);
        PositionResult result = mock(PositionResult.class);

        PosAnnotationFunction annotationFunction = mock(PosAnnotationFunction.class);
        when(annotationFunction.apply(any())).thenReturn(intermediate);

        PosScoreFunction scoreFunction = mock(PosScoreFunction.class);
        when(scoreFunction.apply(intermediate)).thenReturn(result);

        MsnSpectrum spectrum = new MsnSpectrum();

        NdpResult ndpResult = mock(NdpResult.class);
        when(ndpResult.getMassShift()).thenReturn(15.9);
        when(ndpResult.getLibPeptide()).thenReturn(Peptide.parse("SEK"));
        when(ndpResult.getHitType()).thenReturn(HitType.OPEN);

        PositionFunction positionFunction = new PositionFunction(annotationFunction, scoreFunction);
        Assert.assertEquals(result, positionFunction.position(spectrum, ndpResult));
    }

    @Test
    public void testCallExact() throws Exception {

        PosAnnotationFunction annotationFunction = mock(PosAnnotationFunction.class);
        when(annotationFunction.getIonCurrentCoverage(any(), any())).thenReturn(0.375);
        PosScoreFunction scoreFunction = mock(PosScoreFunction.class);

        PositionFunction positionFunction = new PositionFunction(annotationFunction, scoreFunction);

        MsnSpectrum spectrum = new MsnSpectrum();

        NdpResult ndpResult = mock(NdpResult.class);
        when(ndpResult.getMassShift()).thenReturn(0.0001);
        when(ndpResult.getLibPeptide()).thenReturn(Peptide.parse("SEK"));
        when(ndpResult.getHitType()).thenReturn(HitType.EXACT);

        PositionResult result = positionFunction.position(spectrum, ndpResult);
        Assert.assertEquals(0.375, result.getIonCurrentCoverage(), 0.001);
        Assert.assertEquals(0, result.getPosition());
        Assert.assertEquals(1, result.getScore(0), 0.000001);
        Assert.assertEquals(1, result.getScore(1), 0.000001);
        Assert.assertEquals(1, result.getScore(2), 0.000001);
    }
}