package org.expasy.mzjava.sparktool.position.annotation;

import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.position.data.PositionCandidate;
import org.expasy.mzjava.sparktool.position.data.PositionIntermediate;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class DeterministicPosAnnotationFunctionTest {

    @Test
    public void testCall() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.setPrecursor(new Peak(237.5745434672, 100, 2));
        peakList.add(74.0600399587, 1.0); //y1 2+ (K)
        peakList.add(81.02260021269998, 2.0); //b1 2+ (C(C2H3NO))
        peakList.add(147.1128038097, 3.0); //y1 1+ (K)
        peakList.add(157.5592193622, 4.0); //y2 2+ (S(HPO3)K)
        peakList.add(161.03792431769998, 5.0); //b1 1+ (C(C2H3NO))
        peakList.add(164.52177961620004, 6.0); //b2 2+ (C(C2H3NO)S(HPO3))
        peakList.add(314.11116261669997, 7.0); //y2 1+ (S(HPO3)K)
        peakList.add(328.03628312470005, 8.0); //b2 1+ (C(C2H3NO)S(HPO3))

        PositionCandidate candidate = new PositionCandidate(Composition.parseComposition("HPO3"), Peptide.parse("C(C2H3NO)SK"), peakList);
        DeterministicPosAnnotationFunction function = new DeterministicPosAnnotationFunction(EnumSet.of(IonType.a, IonType.b), EnumSet.of(IonType.y),
                new AbsoluteTolerance(0.05),
                precursor -> {

                    int precursorCharge = precursor.getCharge();
                    Set<Integer> charges = new HashSet<>(precursorCharge);
                    for (int i = 1; i <= precursorCharge; i++) {

                        charges.add(i);
                    }
                    return charges;
                },
                0.1);
        PositionIntermediate intermediate = function.apply(candidate);

        Assert.assertEquals(0.1, intermediate.getModProbability(FragmentType.FORWARD, 0), 0.00000001);
        Assert.assertEquals(0.9, intermediate.getModProbability(FragmentType.FORWARD, 1), 0.00000001);
        Assert.assertEquals(0.9, intermediate.getModProbability(FragmentType.FORWARD, 2), 0.00000001);

        Assert.assertEquals(0.9, intermediate.getModProbability(FragmentType.REVERSE, 0), 0.00000001);
        Assert.assertEquals(0.9, intermediate.getModProbability(FragmentType.REVERSE, 1), 0.00000001);
        Assert.assertEquals(0.1, intermediate.getModProbability(FragmentType.REVERSE, 2), 0.00000001);
    }

    @Test
    public void testCallWithMissing() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.setPrecursor(new Peak(237.5745434672, 100, 2));
        peakList.add(74.0600399587, 1.0); //y1 2+ (K)
        peakList.add(81.02260021269998, 2.0); //b1 2+ (C(C2H3NO))
        peakList.add(147.1128038097, 3.0); //y1 1+ (K)
        //y2 2+ (S(HPO3)K)
        peakList.add(161.03792431769998, 5.0); //b1 1+ (C(C2H3NO))
        peakList.add(164.52177961620004, 6.0); //b2 2+ (C(C2H3NO)S(HPO3))
        //y2 1+ (S(HPO3)K)
        peakList.add(328.03628312470005, 8.0); //b2 1+ (C(C2H3NO)S(HPO3))

        PositionCandidate candidate = new PositionCandidate(Composition.parseComposition("HPO3"), Peptide.parse("C(C2H3NO)SK"), peakList);
        DeterministicPosAnnotationFunction function = new DeterministicPosAnnotationFunction(EnumSet.of(IonType.b), EnumSet.of(IonType.y),
                new AbsoluteTolerance(0.05),
                precursor -> {

                    int precursorCharge = precursor.getCharge();
                    Set<Integer> charges = new HashSet<>(precursorCharge);
                    for (int i = 1; i <= precursorCharge; i++) {

                        charges.add(i);
                    }
                    return charges;
                },
                0.1);
        PositionIntermediate intermediate = function.apply(candidate);

        Assert.assertEquals(0.1, intermediate.getModProbability(FragmentType.FORWARD, 0), 0.00000001);
        Assert.assertEquals(0.9, intermediate.getModProbability(FragmentType.FORWARD, 1), 0.00000001);
        Assert.assertEquals(0.9, intermediate.getModProbability(FragmentType.FORWARD, 2), 0.00000001);

        Assert.assertEquals(0.9, intermediate.getModProbability(FragmentType.REVERSE, 0), 0.00000001);
        Assert.assertEquals(0.5, intermediate.getModProbability(FragmentType.REVERSE, 1), 0.00000001);
        Assert.assertEquals(0.1, intermediate.getModProbability(FragmentType.REVERSE, 2), 0.00000001);
    }
}