package org.expasy.mzjava.sparktool.position.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;

public class PositionCandidateTest {

    @Test
    public void testGetIntensity() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(49.53425, 1);      //b1(P)++
        peakList.add(74.06063, 2);      //y1(K)++
        peakList.add(98.06063, 3);      //b1(P)+
        peakList.add(123.05195, 4);     //b2(PM)++
        peakList.add(138.58193, 5);     //y2(EK)++
        peakList.add(147.1134, 6);      //y1(K)+
        peakList.add(171.57833, 7);     //b3(PMP)++
        peakList.add(196.0954, 8);      //y3(DEK)++
        peakList.add(215.09435, 9);     //b4(PMPS)++
        peakList.add(245.09603, 10);    //b2(PM)+
        peakList.add(252.63743, 11);    //y4(IDEK)++
        peakList.add(271.63638, 12);    //b5(PMPSI)++
        peakList.add(276.15599, 13);    //y2(EK)+
        peakList.add(296.15345, 14);    //y5(SIDEK)++
        peakList.add(329.14985, 15);    //b6(PMPSID)++
        peakList.add(342.1488, 16);     //b3(PMP)+
        peakList.add(344.67983, 17);    //y6(PSIDEK)++
        peakList.add(391.18293, 18);    //y3(DEK)+
        peakList.add(393.67115, 19);    //b7(PMPSIDE)++
        peakList.add(418.19753, 20);    //y7(MPSIDEK)++
        peakList.add(429.18083, 21);    //b4(PMPS)+
        peakList.add(466.72391, 100);   //p(PMPSIDEK)++
        peakList.add(504.267, 22);      //y4(IDEK)+
        peakList.add(542.26489, 23);    //b5(PMPSI)+
        peakList.add(591.29902, 24);    //y5(SIDEK)+
        peakList.add(657.29183, 25);    //b6(PMPSID)+
        peakList.add(688.35179, 26);    //y6(PSIDEK)+
        peakList.add(786.33442, 27);    //b7(PMPSIDE)+
        peakList.add(835.38719, 28);    //y7(MPSIDEK)+

        PositionCandidate positionCandidate = new PositionCandidate(Composition.parseComposition("O"), Peptide.parse("PMPSIDEK"), peakList);

        AbsoluteTolerance tolerance = new AbsoluteTolerance(0.2);
        Mass oxidisation = Composition.parseComposition("O");

        Assert.assertEquals(3, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(10, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(16, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(21, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(23, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(25, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(27, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.b, oxidisation, 1, tolerance), 0.0000001);

        Assert.assertEquals(1, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(4, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(7, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(9, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(12, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(15, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(19, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.b, oxidisation, 2, tolerance), 0.0000001);

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);

        Assert.assertEquals(28, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.y, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(26, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(24, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(22, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(18, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(13, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(6, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);

        Assert.assertEquals(20, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(17, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(14, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(11, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(8, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(5, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(2, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.y, oxidisation, 2, tolerance), 0.0000001);

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 12, IonType.a, Mass.ZERO, 2, tolerance), 0.0000001);
    }

    @Test
    public void testFixedModification() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(49.534, 1);    //b1(P)++
        peakList.add(74.06, 2);        //y1(K)++
        peakList.add(98.06, 3);        //b1(P)+
        peakList.add(123.051, 4);    //b2(PM(O))++
        peakList.add(138.581, 5);    //y2(EK)++
        peakList.add(147.113, 6);    //y1(K)+
        peakList.add(171.578, 7);    //b3(PM(O)P)++
        peakList.add(196.095, 8);    //y3(DEK)++
        peakList.add(245.095, 9);    //b2(PM(O))+
        peakList.add(251.593, 10);    //b4(PM(O)PC(C2H3NO))++
        peakList.add(252.637, 11);    //y4(IDEK)++
        peakList.add(276.155, 12);    //y2(EK)+
        peakList.add(308.135, 13);    //b5(PM(O)PC(C2H3NO)I)++
        peakList.add(332.652, 14);    //y5(C(C2H3NO)IDEK)++
        peakList.add(342.148, 15);    //b3(PM(O)P)+
        peakList.add(365.649, 16);    //b6(PM(O)PC(C2H3NO)ID)++
        peakList.add(381.179, 17);    //y6(PC(C2H3NO)IDEK)++
        peakList.add(391.182, 18);    //y3(DEK)+
        peakList.add(430.17, 19);    //b7(PM(O)PC(C2H3NO)IDE)++
        peakList.add(454.696, 20);    //y7(M(O)PC(C2H3NO)IDEK)++
        peakList.add(502.179, 21);    //b4(PM(O)PC(C2H3NO))+
        peakList.add(504.266, 22);    //y4(IDEK)+
        peakList.add(615.263, 23);    //b5(PM(O)PC(C2H3NO)I)+
        peakList.add(664.297, 24);    //y5(C(C2H3NO)IDEK)+
        peakList.add(730.29, 25);    //b6(PM(O)PC(C2H3NO)ID)+
        peakList.add(761.35, 26);    //y6(PC(C2H3NO)IDEK)+
        peakList.add(859.332, 27);    //b7(PM(O)PC(C2H3NO)IDE)+
        peakList.add(908.385, 28);    //y7(M(O)PC(C2H3NO)IDEK)+

        PositionCandidate positionCandidate = new PositionCandidate(Composition.parseComposition("O"), Peptide.parse("PMPC(C2H3NO)IDEK"), peakList);
        AbsoluteTolerance tolerance = new AbsoluteTolerance(0.2);
        Mass oxidisation = Composition.parseComposition("O");

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(9, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(15, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(21, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(23, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(25, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(27, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.b, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.b, oxidisation, 1, tolerance), 0.0000001);

        Assert.assertEquals(3, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.b, Mass.ZERO, 1, tolerance), 0.0000001);

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(4, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(7, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(10, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(13, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(16, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(19, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.b, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.b, oxidisation, 2, tolerance), 0.0000001);

        Assert.assertEquals(1, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.b, Mass.ZERO, 2, tolerance), 0.0000001);

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.y, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(28, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.y, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.y, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.y, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.y, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.y, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.y, oxidisation, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.y, oxidisation, 1, tolerance), 0.0000001);

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(26, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(24, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(22, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(18, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(12, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);
        Assert.assertEquals(6, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.y, Mass.ZERO, 1, tolerance), 0.0000001);

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(20, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.y, oxidisation, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.y, oxidisation, 2, tolerance), 0.0000001);

        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 0, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(0, positionCandidate.getIntensity(new TIntHashSet(), 1, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(17, positionCandidate.getIntensity(new TIntHashSet(), 2, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(14, positionCandidate.getIntensity(new TIntHashSet(), 3, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(11, positionCandidate.getIntensity(new TIntHashSet(), 4, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(8, positionCandidate.getIntensity(new TIntHashSet(), 5, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(5, positionCandidate.getIntensity(new TIntHashSet(), 6, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
        Assert.assertEquals(2, positionCandidate.getIntensity(new TIntHashSet(), 7, IonType.y, Mass.ZERO, 2, tolerance), 0.0000001);
    }

    @Test
    public void testExtractIntensity() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(49.53425, 1);      //b1(P)++
        peakList.add(74.06063, 2);      //y1(K)++
        peakList.add(98.06063, 3);      //b1(P)+
        peakList.add(123.05195, 4);     //b2(PM)++
        peakList.add(138.58193, 5);     //y2(EK)++
        peakList.add(147.1134, 6);      //y1(K)+
        peakList.add(171.57833, 7);     //b3(PMP)++
        peakList.add(196.0954, 8);      //y3(DEK)++
        peakList.add(215.09435, 9);     //b4(PMPS)++
        peakList.add(245.09603, 10);    //b2(PM)+
        peakList.add(252.63743, 11);    //y4(IDEK)++
        peakList.add(271.63638, 12);    //b5(PMPSI)++
        peakList.add(276.15599, 13);    //y2(EK)+
        peakList.add(296.15345, 14);    //y5(SIDEK)++
        peakList.add(329.14985, 15);    //b6(PMPSID)++
        peakList.add(342.1488, 16);     //b3(PMP)+
        peakList.add(344.67983, 17);    //y6(PSIDEK)++
        peakList.add(391.18293, 18);    //y3(DEK)+
        peakList.add(393.67115, 19);    //b7(PMPSIDE)++
        peakList.add(418.19753, 20);    //y7(MPSIDEK)++
        peakList.add(429.18083, 21);    //b4(PMPS)+
        peakList.add(466.72391, 100);   //p(PMPSIDEK)++
        peakList.add(504.267, 22);      //y4(IDEK)+
        peakList.add(542.26489, 23);    //b5(PMPSI)+
        peakList.add(591.29902, 24);    //y5(SIDEK)+
        peakList.add(657.29183, 25);    //b6(PMPSID)+
        peakList.add(688.35179, 26);    //y6(PSIDEK)+
        peakList.add(786.33442, 27);    //b7(PMPSIDE)+
        peakList.add(835.38719, 28);    //y7(MPSIDEK)+

        PositionCandidate positionCandidate = new PositionCandidate(Composition.parseComposition("O"), Peptide.parse("PMPSIDEK"), peakList);

        AbsoluteTolerance tolerance = new AbsoluteTolerance(0.2);
        Mass oxidisation = Composition.parseComposition("O");

        Assert.assertArrayEquals(new double[]{10 + 4, 28 + 20},
                positionCandidate.extractIntensity(new TIntHashSet(), Lists.newArrayList(
                                new PeakSet(Sets.newHashSet(1, 2), EnumSet.of(IonType.b), oxidisation),
                                new PeakSet(Sets.newHashSet(1, 2), EnumSet.of(IonType.y), oxidisation)
                        ),
                        1, tolerance), 0.00001);

        //noinspection PointlessArithmeticExpression
        Assert.assertArrayEquals(new double[]{16 + 7, 0 + 0},
                positionCandidate.extractIntensity(new TIntHashSet(), Lists.newArrayList(
                                new PeakSet(Sets.newHashSet(1, 2), EnumSet.of(IonType.b), oxidisation),
                                new PeakSet(Sets.newHashSet(1, 2), EnumSet.of(IonType.y), oxidisation)
                        ),
                        2, tolerance), 0.00001);

        //noinspection PointlessArithmeticExpression
        Assert.assertArrayEquals(new double[]{0 + 0, 26 + 17},
                positionCandidate.extractIntensity(new TIntHashSet(), Lists.newArrayList(
                                new PeakSet(Sets.newHashSet(1, 2), EnumSet.of(IonType.b), Mass.ZERO),
                                new PeakSet(Sets.newHashSet(1, 2), EnumSet.of(IonType.y), Mass.ZERO)
                        ),
                        2, tolerance), 0.00001);
    }

    //Tests all residues
    @Test
    public void testGetMatchedIonCoverage() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(49.53425, 2);      //b1(P)++
        peakList.add(52.18425, 20);
        peakList.add(74.06063, 2);      //y1(K)++
        peakList.add(98.06063, 2);      //b1(P)+
        peakList.add(123.05195, 2);     //b2(PM)++
        peakList.add(138.58193, 2);     //y2(EK)++
        peakList.add(147.1134, 2);      //y1(K)+
        peakList.add(171.57833, 2);     //b3(PMP)++
        peakList.add(196.0954, 2);      //y3(DEK)++
        peakList.add(215.09435, 2);     //b4(PMPS)++
        peakList.add(245.09603, 2);    //b2(PM)+
        peakList.add(252.63743, 2);    //y4(IDEK)++
        peakList.add(257.87603, 20);
        peakList.add(271.63638, 2);    //b5(PMPSI)++
        peakList.add(276.15599, 2);    //y2(EK)+
        peakList.add(296.15345, 2);    //y5(SIDEK)++
        peakList.add(329.14985, 2);    //b6(PMPSID)++
        peakList.add(342.1488, 2);     //b3(PMP)+
        peakList.add(344.67983, 2);    //y6(PSIDEK)++
        peakList.add(391.18293, 2);    //y3(DEK)+
        peakList.add(393.67115, 2);    //b7(PMPSIDE)++
        peakList.add(418.19753, 2);    //y7(MPSIDEK)++
        peakList.add(429.18083, 2);    //b4(PMPS)+
        peakList.add(450.06753, 20);
        peakList.add(466.72391, 2);   //p(PMPSIDEK)++
        peakList.add(504.267, 2);      //y4(IDEK)+
        peakList.add(542.26489, 2);    //b5(PMPSI)+
        peakList.add(591.29902, 2);    //y5(SIDEK)+
        peakList.add(657.29183, 2);    //b6(PMPSID)+
        peakList.add(688.35179, 2);    //y6(PSIDEK)+
        peakList.add(786.33442, 2);    //b7(PMPSIDE)+
        peakList.add(835.38719, 2);    //y7(MPSIDEK)+

        PositionCandidate positionCandidate = new PositionCandidate(Composition.parseComposition("O"), Peptide.parse("PMPSIDEK"), peakList);

        TIntSet peaks = new TIntHashSet();
        positionCandidate.addPeaksMatching(peaks,
                EnumSet.of(IonType.b, IonType.y),
                Sets.newHashSet(Mass.ZERO, Composition.parseComposition("O")),
                1,
                new AbsoluteTolerance(0.02)
        );

        Assert.assertEquals(new TIntHashSet(new int[]{3, 6, 10, 14, 17, 19, 22, 25, 26, 27, 28, 29, 30, 31}), peaks);

        peaks.clear();
        positionCandidate.addPeaksMatching(peaks,
                EnumSet.of(IonType.b, IonType.y),
                Sets.newHashSet(Mass.ZERO, Composition.parseComposition("O")),
                2,
                new AbsoluteTolerance(0.02)
        );
        Assert.assertEquals(new TIntHashSet(new int[]{0, 2, 4, 5, 7, 8, 9, 11, 13, 15, 16, 18, 20, 21, 24}), peaks);
    }

    //Tests a subset of the residues
    @Test
    public void testGetMatchedIonCoverage2() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.add(52.18425, 20);     //                  0
        peakList.add(74.06063, 2);      //y1(K)++           1
        peakList.add(245.09603, 2);     //b2(PM)+           2
        peakList.add(252.63743, 2);     //y4(IDEK)++        3
        peakList.add(257.87603, 20);    //                  4
        peakList.add(271.63638, 2);     //b5(PMPSI)++       5
        peakList.add(329.14985, 2);     //b6(PMPSID)++      6
        peakList.add(344.67983, 2);     //y6(PSIDEK)++      7
        peakList.add(429.18083, 2);     //b4(PMPS)+         8
        peakList.add(450.06753, 20);    //                  9
        peakList.add(466.72391, 2);     //p(PMPSIDEK)++    10
        peakList.add(657.29183, 2);     //b6(PMPSID)+      11
        peakList.add(786.33442, 2);     //b7(PMPSIDE)+     12
        peakList.add(835.38719, 2);     //y7(MPSIDEK)+     13

        PositionCandidate positionCandidate = new PositionCandidate(Composition.parseComposition("O"), Peptide.parse("PMPSIDEK"), peakList);

        TIntSet peaks = new TIntHashSet();
        positionCandidate.addPeaksMatching(peaks,
                EnumSet.of(IonType.b, IonType.y),
                Sets.newHashSet(Mass.ZERO, Composition.parseComposition("O")),
                1,
                new AbsoluteTolerance(0.02)
        );
        Assert.assertEquals(new TIntHashSet(new int[]{2, 8, 11, 12, 13}), peaks);

        peaks.clear();
        positionCandidate.addPeaksMatching(peaks,
                EnumSet.of(IonType.b, IonType.y),
                Sets.newHashSet(Mass.ZERO, Composition.parseComposition("O")),
                2,
                new AbsoluteTolerance(0.02)
        );
        Assert.assertEquals(new TIntHashSet(new int[]{1, 3, 5, 6, 7, 10}), peaks);
    }
}