package org.expasy.mzjava.sparktool.position.data;

import com.google.common.collect.Sets;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.junit.Test;

import java.util.EnumSet;

public class PeakSetTest {

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalConstructorArguments1() throws Exception {

        new PeakSet(Sets.newHashSet(), EnumSet.of(IonType.y), Mass.ZERO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalConstructorArguments() throws Exception {

        new PeakSet(Sets.newHashSet(1, 2), EnumSet.noneOf(IonType.class), Mass.ZERO);
    }
}