package org.expasy.mzjava.sparktool.position.data;

import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.junit.Assert;
import org.junit.Test;

public class PositionIntermediateTest {

    @Test
    public void testGetProbability() throws Exception {

        PositionIntermediate intermediate = new PositionIntermediate.Builder()
                .reset(7)
                .setModProbability(FragmentType.FORWARD, 0, 0.1)
                .setModProbability(FragmentType.FORWARD, 1, 0.1)
                .setModProbability(FragmentType.FORWARD, 2, 0.1)
                .setModProbability(FragmentType.FORWARD, 3, 0.9)
                .setModProbability(FragmentType.FORWARD, 4, 0.9)
                .setModProbability(FragmentType.FORWARD, 5, 0.9)
                .setModProbability(FragmentType.FORWARD, 6, 0.9)
                .setModProbability(FragmentType.REVERSE, 0, 0.9)
                .setModProbability(FragmentType.REVERSE, 1, 0.9)
                .setModProbability(FragmentType.REVERSE, 2, 0.9)
                .setModProbability(FragmentType.REVERSE, 3, 0.9)
                .setModProbability(FragmentType.REVERSE, 4, 0.1)
                .setModProbability(FragmentType.REVERSE, 5, 0.1)
                .setModProbability(FragmentType.REVERSE, 6, 0.1)
                .build(Composition.parseComposition("HPO3"), 0.9245);

        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 0, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 1, 2), 0.0000001);
        Assert.assertEquals(0.1, intermediate.getProbability(FragmentType.FORWARD, 2, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 3, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 4, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 5, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 6, 2), 0.0000001);

        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 0, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 1, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 2, 2), 0.0000001);
        Assert.assertEquals(0.1, intermediate.getProbability(FragmentType.REVERSE, 3, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 4, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 5, 2), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 6, 2), 0.0000001);

        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 0, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 1, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 2, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 3, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 4, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 5, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.FORWARD, 6, 3), 0.0000001);

        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 0, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 1, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 2, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 3, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 4, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 5, 3), 0.0000001);
        Assert.assertEquals(0.9, intermediate.getProbability(FragmentType.REVERSE, 6, 3), 0.0000001);
    }
}