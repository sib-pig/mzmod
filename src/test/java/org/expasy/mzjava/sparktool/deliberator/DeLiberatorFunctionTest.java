package org.expasy.mzjava.sparktool.deliberator;

import com.google.common.collect.Lists;
import org.apache.spark.api.java.Optional;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.consensus.shuffling.ControlledLibrarySpectrumShuffler;
import org.expasy.mzjava.proteomics.ms.consensus.shuffling.LibrarySpectrumShuffler;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DeLiberatorFunctionTest {

    @Test
    public void testCall() throws Exception {

        PeptideConsensusSpectrum shuffledSpectrum = mock(PeptideConsensusSpectrum.class);

        ControlledLibrarySpectrumShuffler spectrumShuffler = mock(ControlledLibrarySpectrumShuffler.class);
        //noinspection unchecked
        when(spectrumShuffler.shuffle(any(), any(LibrarySpectrumShuffler.PeptideTerminiHandling.class))).thenReturn(Optional.of(shuffledSpectrum), Optional.<PeptideConsensusSpectrum>absent());

        LibrarySpectrumShufflerFactory spectrumShufflerFactory = mock(LibrarySpectrumShufflerFactory.class);
        when(spectrumShufflerFactory.newSpectrumShuffler()).thenReturn(spectrumShuffler);

        DeLiberatorFunction function = new DeLiberatorFunction(spectrumShufflerFactory);
        Assert.assertEquals(shuffledSpectrum, Lists.newArrayList(function.call(mock(PeptideConsensusSpectrum.class))).get(0));
        Assert.assertEquals(Collections.<PeptideConsensusSpectrum>emptyIterator(), function.call(mock(PeptideConsensusSpectrum.class)));
    }
}