package org.expasy.mzjava.sparktool.hadoop;

import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.hadoop.io.MockDataInput;
import org.expasy.mzjava.hadoop.io.MockDataOutput;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class IndexedSpectrumValueTest {

    private static final String base64 = "AAIEAEjhehSuU3xAAAAAAAAkyUCUkrrSzP+p/ucBwZyJy8C8u+3YAQAACEjhehSuD3tAAAAAAACA\n" +
            "QUACSOF6FK7vf0AAAAAAAIBBQACkcD0K13+IQAAAAAAAgEFAAlK4HoXrO5BAAAAAAACAQUAEAA==";

    @Test
    public void testWrite() throws Exception {

        IndexedSpectrum spectrum = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                .build(new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE);

        IndexedSpectrumValue value = new IndexedSpectrumValue(spectrum);

        MockDataOutput out = new MockDataOutput(256);

        value.write(out);
        Assert.assertEquals(base64, out.getBase64());
    }

    @Test
    public void testRead() throws Exception {

        MockDataInput in = new MockDataInput(base64);

        IndexedSpectrumValue value = new IndexedSpectrumValue();

        value.readFields(in);

        IndexedSpectrum expected = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                .build(new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE);

        Assert.assertEquals(expected.getPrecision(), value.get().getPrecision());
        Assert.assertEquals(expected.getPrecursor(), value.get().getPrecursor());
        Assert.assertEquals(expected.getId(), value.get().getId());
        Assert.assertArrayEquals(expected.getMzs(new double[4]), value.get().getMzs(new double[4]), 0.00000001);
        Assert.assertArrayEquals(expected.getIntensities(new double[4]), value.get().getIntensities(new double[4]), 0.00000001);
    }
}