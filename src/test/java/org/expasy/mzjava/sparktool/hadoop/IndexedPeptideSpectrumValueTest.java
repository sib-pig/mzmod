package org.expasy.mzjava.sparktool.hadoop;

import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.hadoop.io.MockDataInput;
import org.expasy.mzjava.hadoop.io.MockDataOutput;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class IndexedPeptideSpectrumValueTest {

    private static final String base64 = "BhgKHAAAAAQECEFDQzEIQUNDMgAAAgQASOF6FK5TfEAAAAAAACTJQJSSutLM/6n+5wHBnInLwLy7\n" +
            "7dgBAAAISOF6FK4Pe0AAAAAAAIBBQAJI4XoUru9/QAAAAAAAgEFAAKRwPQrXf4hAAAAAAACAQUAC\n" +
            "Urgehes7kEAAAAAAAIBBQAQA";

    @Test
    public void testWrite() throws Exception {

        IndexedPeptideSpectrum spectrum = new IndexedPeptideSpectrum(Peptide.parse("PET"), Sets.newHashSet("ACC1", "ACC2"), 2, new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                , new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE);

        IndexedPeptideSpectrumValue value = new IndexedPeptideSpectrumValue(spectrum);

        MockDataOutput out = new MockDataOutput(256);

        value.write(out);
        Assert.assertEquals(base64, out.getBase64());
    }

    @Test
    public void testRead() throws Exception {

        MockDataInput in = new MockDataInput(base64);

        IndexedPeptideSpectrumValue value = new IndexedPeptideSpectrumValue();

        value.readFields(in);

        IndexedPeptideSpectrum expected = new IndexedPeptideSpectrum(Peptide.parse("PET"), Sets.newHashSet("ACC1", "ACC2"), 2, new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                , new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE);

        Assert.assertEquals(expected.getPeptide(), value.get().getPeptide());
        Assert.assertEquals(expected.getMemberCount(), value.get().getMemberCount());
        Assert.assertEquals(expected.getProteinAccessionNumbers(), value.get().getProteinAccessionNumbers());
        Assert.assertEquals(expected.getPrecision(), value.get().getPrecision());
        Assert.assertEquals(expected.getPrecursor(), value.get().getPrecursor());
        Assert.assertEquals(expected.getId(), value.get().getId());
        Assert.assertArrayEquals(expected.getMzs(new double[4]), value.get().getMzs(new double[4]), 0.00000001);
        Assert.assertArrayEquals(expected.getIntensities(new double[4]), value.get().getIntensities(new double[4]), 0.00000001);
    }
}