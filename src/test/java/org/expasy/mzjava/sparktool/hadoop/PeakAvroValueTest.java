package org.expasy.mzjava.sparktool.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PeakAvroValueTest {

    private String path;
    private static final String name = "test.hdio";

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void setup() throws Exception {

        path = folder.getRoot().getPath();
    }

    @Test    
    public void test() throws IOException {

        IndexedSpectrumValue value = new IndexedSpectrumValue();

        IndexedSpectrum spectrum1 = newSpectrum(235.975, 12, 154, 2);
        IndexedSpectrum spectrum2 = newSpectrum(1537.648, 1087, 15, 2);
        IndexedSpectrum spectrum3 = newSpectrum(171.542, 100, 1043, 3);

        FileSystem fs = FileSystem.getLocal(new Configuration());
        
        PeakAvroValueWriter<IndexedSpectrum> writer = new PeakAvroValueWriter<>(fs, new Path(path, name), value, SequenceFile.CompressionType.NONE);
        writer.write(spectrum1.getPrecursor(), spectrum1);
        writer.write(spectrum3.getPrecursor(), spectrum3);
        writer.write(spectrum2.getPrecursor(), spectrum2);
        writer.close();

        //Test reading
        List<IndexedSpectrum> readSpectra = new ArrayList<>();

        PeakAvroValueReader<IndexedSpectrum> reader = new PeakAvroValueReader<>(fs, new Path(path, name), value);
        while (reader.hasNext()) {

            readSpectra.add(reader.next());
        }
        reader.close();

        Assert.assertEquals(3, readSpectra.size());
        checkSpectrum(spectrum1, readSpectra.get(0));
        checkSpectrum(spectrum2, readSpectra.get(1));
        checkSpectrum(spectrum3, readSpectra.get(2));
    }

    @Test
    public void testReaderNoHasNext() throws IOException {

        IndexedSpectrumValue value = new IndexedSpectrumValue();

        IndexedSpectrum spectrum1 = newSpectrum(235.975, 12, 154, 2);
        IndexedSpectrum spectrum2 = newSpectrum(1537.648, 1087, 15, 2);
        IndexedSpectrum spectrum3 = newSpectrum(171.542, 100, 1043, 3);

        FileSystem fs = FileSystem.getLocal(new Configuration());

        PeakAvroValueWriter<IndexedSpectrum> writer = new PeakAvroValueWriter<>(fs, new Path(path, name), value, SequenceFile.CompressionType.NONE);
        writer.write(spectrum1.getPrecursor(), spectrum1);
        writer.write(spectrum3.getPrecursor(), spectrum3);
        writer.write(spectrum2.getPrecursor(), spectrum2);
        writer.close();

        //Test reading
        List<IndexedSpectrum> readSpectra = new ArrayList<>();

        PeakAvroValueReader<IndexedSpectrum> reader = new PeakAvroValueReader<>(fs, new Path(path, name), value);
        readSpectra.add(reader.next());
        readSpectra.add(reader.next());
        readSpectra.add(reader.next());
        reader.close();

        Assert.assertEquals(3, readSpectra.size());
        checkSpectrum(spectrum1, readSpectra.get(0));
        checkSpectrum(spectrum2, readSpectra.get(1));
        checkSpectrum(spectrum3, readSpectra.get(2));
    }

    @Test
    public void testChargeList() throws Exception {

        IndexedSpectrumValue value = new IndexedSpectrumValue();

        IndexedSpectrum spectrum1 = newSpectrum(235.975, 12, 154, 2, 3);
        IndexedSpectrum spectrum2 = newSpectrum(1537.648, 1087, 15, 2, 3);
        IndexedSpectrum spectrum3 = newSpectrum(171.542, 100, 1043, 3, 4);

        FileSystem fs = FileSystem.getLocal(new Configuration());
        
        PeakAvroValueWriter<IndexedSpectrum> writer = new PeakAvroValueWriter<>(fs, new Path(path, name), value, SequenceFile.CompressionType.NONE);
        writer.write(spectrum1.getPrecursor(), spectrum1);
        writer.write(spectrum3.getPrecursor(), spectrum3);
        writer.write(spectrum2.getPrecursor(), spectrum2);
        writer.close();

        //Test reading
        List<IndexedSpectrum> readSpectra = new ArrayList<>();

        PeakAvroValueReader<IndexedSpectrum> reader = new PeakAvroValueReader<>(fs, new Path(path, name), value);
        while (reader.hasNext()) {

            readSpectra.add(reader.next());
        }
        reader.close();

        Assert.assertEquals(3, readSpectra.size());
        checkSpectrum(spectrum1, readSpectra.get(0));
        checkSpectrum(spectrum2, readSpectra.get(1));
        checkSpectrum(spectrum3, readSpectra.get(2));
    }

    @Test
    public void testCloseWithoutWrite() throws Exception {

        IndexedSpectrumValue value = new IndexedSpectrumValue();

        FileSystem fs = FileSystem.getLocal(new Configuration());

        PeakAvroValueWriter<IndexedSpectrum> writer = new PeakAvroValueWriter<>(fs, new Path(path, name), value, SequenceFile.CompressionType.NONE);
        writer.close();

        Assert.assertEquals(false, new File(path, name).exists());
    }

    private void checkSpectrum(IndexedSpectrum expected, IndexedSpectrum actual) {

        Assert.assertEquals(expected.getPrecision(), actual.getPrecision());
        Assert.assertEquals(expected.getMsLevel(), actual.getMsLevel());

        Assert.assertEquals(expected.getPrecursor(), actual.getPrecursor());

        Assert.assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); i++) {

            Assert.assertEquals(expected.getMz(i), actual.getMz(i), 0.0000001);
            Assert.assertEquals(expected.getIntensity(i), expected.getIntensity(i), 0.0000001);
        }

        for (int i = 0; i < expected.size(); i++) {

            Assert.assertEquals(expected.hasAnnotationsAt(i), actual.hasAnnotationsAt(i));
            if (expected.hasAnnotationsAt(i)) {

                Assert.assertEquals(expected.getAnnotations(i), actual.getAnnotations(i));
            }
        }
    }

    private double[] randomArray(int size, double min, double max) {

        Random random = new Random();
        double range = max - min;

        double[] arr = new double[size];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = min + random.nextDouble() * range;
        }

        return arr;
    }

    private IndexedSpectrum newSpectrum(double precursorMz, double precursorIntensity, int size, int... precursorCharges) {


        double[] mzs = randomArray(size, 200, 3000);
        Arrays.sort(mzs);
        double[] intensities = randomArray(size, 1, 1000);

        final DoublePeakList<PeakAnnotation> src = new DoublePeakList<>(mzs.length);
        Peak precursor = src.getPrecursor();
        precursor.setValues(precursorMz, precursorIntensity, precursorCharges);

        for (int i = 0; i < mzs.length; i++) {

            src.add(mzs[i], intensities[i]);
        }


        return new IndexedSpectrum(src);
    }
}