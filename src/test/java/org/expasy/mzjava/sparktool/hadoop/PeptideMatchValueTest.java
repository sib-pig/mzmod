package org.expasy.mzjava.sparktool.hadoop;

import com.google.common.collect.Sets;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.hadoop.io.MockDataInput;
import org.expasy.mzjava.hadoop.io.MockDataOutput;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.junit.Assert;
import org.junit.Test;

public class PeptideMatchValueTest {

    private static final String base64 = "EBgKGBwaJgYKAAAABApQUk9UMgpQUk9UMQAEDHNjb3JlMi1DHOviNho/DHNjb3JlMQAAAAAA2I5A\n" +
            "AAoA";

    @Test
    public void testWrite() throws Exception {

        TObjectDoubleMap<String> scoreMap = new TObjectDoubleHashMap<>();
        scoreMap.put("score1", 987);
        scoreMap.put("score2", 0.0001);

        PeptideMatch peptideMatch = new PeptideMatch(Peptide.parse("PEPTSIDE"), Sets.newHashSet("PROT1", "PROT2").stream(), scoreMap, 5);
        PeptideMatchValue value = new PeptideMatchValue(peptideMatch);

        MockDataOutput out = new MockDataOutput(256);

        value.write(out);
        Assert.assertEquals(base64, out.getBase64());
    }

    @Test
    public void testRead() throws Exception {

        MockDataInput in = new MockDataInput(base64);

        PeptideMatchValue value = new PeptideMatchValue();

        value.readFields(in);

        PeptideMatch match = value.get();
        Assert.assertEquals(Peptide.parse("PEPTSIDE"), match.getPeptide());
        Assert.assertEquals(Sets.newHashSet("PROT1", "PROT2"), match.getProteinAcc());

        Assert.assertEquals(987.0, match.getScore("score1"), 0.000000000001);
        Assert.assertEquals(0.0001, match.getScore("score2"), 0.000000000001);
    }
}