package org.expasy.mzjava.sparktool.liberator;

import com.google.common.collect.Lists;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.sparktool.data.MockCached;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import org.junit.Assert;
import org.junit.Test;
import scala.Tuple2;

import java.util.List;
import java.util.function.Predicate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SpectrumKeyFMFTest {

    @Test
    public void testCall() throws Exception {

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);

        //noinspection unchecked
        SpectrumKeyFunction<MsnSpectrum> keyFunction = mock(SpectrumKeyFunction.class);
        when(keyFunction.apply(spectrum)).thenReturn("key1");

        MsnSpectrumValue value = mock(MsnSpectrumValue.class);
        when(value.get()).thenReturn(spectrum);

        //noinspection unchecked
        Predicate<MsnSpectrum> predicate = mock(Predicate.class);
        when(predicate.test(spectrum)).thenReturn(true);

        SpectrumKeyFMF fmf = new SpectrumKeyFMF( new MockCached<>(keyFunction), new MockCached<>(predicate));

        List<Tuple2<String, MsnSpectrum>> results = Lists.newArrayList(fmf.call(new Tuple2<>(mock(SpectrumKey.class), value)));

        Assert.assertEquals(1, results.size());

        Tuple2<String, MsnSpectrum> result = results.get(0);

        Assert.assertEquals("key1", result._1());
        Assert.assertEquals(spectrum, result._2());
    }

    @Test
    public void testCallFailPredicate() throws Exception {

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);

        //noinspection unchecked
        SpectrumKeyFunction<MsnSpectrum> keyFunction = mock(SpectrumKeyFunction.class);
        when(keyFunction.apply(spectrum)).thenReturn("key1");

        MsnSpectrumValue value = mock(MsnSpectrumValue.class);
        when(value.get()).thenReturn(spectrum);

        //noinspection unchecked
        Predicate<MsnSpectrum> predicate = mock(Predicate.class);
        when(predicate.test(spectrum)).thenReturn(false);

        SpectrumKeyFMF fmf = new SpectrumKeyFMF( new MockCached<>(keyFunction), new MockCached<>(predicate));

        List<Tuple2<String, MsnSpectrum>> results = Lists.newArrayList(fmf.call(new Tuple2<>(mock(SpectrumKey.class), value)));

        Assert.assertEquals(0, results.size());
    }
}