package org.expasy.mzjava.sparktool.liberator;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.spectrasim.SimFunc;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MSTConsensusMemberSelectorTest {

    @SuppressWarnings("unchecked")
    @Test
    public void testSelectSpectraTwoEqualClusters() throws Exception {

        final MsnSpectrum sA1 = mock(MsnSpectrum.class);
        final MsnSpectrum sA2 = mock(MsnSpectrum.class);
        final MsnSpectrum sA3 = mock(MsnSpectrum.class);

        final MsnSpectrum sB1 = mock(MsnSpectrum.class);
        final MsnSpectrum sB2 = mock(MsnSpectrum.class);
        final MsnSpectrum sB3 = mock(MsnSpectrum.class);

        final SimFunc<PeakAnnotation, PeakAnnotation> simFunc = mock(SimFunc.class);
        when(simFunc.calcSimilarity(sA1, sA2)).thenReturn(0.9);
        when(simFunc.calcSimilarity(sA1, sA3)).thenReturn(0.9);
        when(simFunc.calcSimilarity(sA2, sA3)).thenReturn(0.9);

        when(simFunc.calcSimilarity(sB1, sB2)).thenReturn(0.8);
        when(simFunc.calcSimilarity(sB1, sB3)).thenReturn(0.8);
        when(simFunc.calcSimilarity(sB2, sB3)).thenReturn(0.8);

        when(simFunc.calcSimilarity(sA1, sB1)).thenReturn(0.2);
        when(simFunc.calcSimilarity(sA1, sB2)).thenReturn(0.2);
        when(simFunc.calcSimilarity(sA1, sB3)).thenReturn(0.2);

        when(simFunc.calcSimilarity(sA2, sB1)).thenReturn(0.2);
        when(simFunc.calcSimilarity(sA2, sB2)).thenReturn(0.2);
        when(simFunc.calcSimilarity(sA2, sB3)).thenReturn(0.2);

        when(simFunc.calcSimilarity(sA3, sB1)).thenReturn(0.2);
        when(simFunc.calcSimilarity(sA3, sB2)).thenReturn(0.2);
        when(simFunc.calcSimilarity(sA3, sB3)).thenReturn(0.2);

        final ConsensusValue a1 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA1);
        final ConsensusValue a2 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA2);
        final ConsensusValue a3 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA3);

        final ConsensusValue b1 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sB1);
        final ConsensusValue b2 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sB2);
        final ConsensusValue b3 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sB3);

        MSTConsensusMemberSelector clusterSelector = new MSTConsensusMemberSelector(new MaxSimScoreClusterSelector(), simFunc, 0.6);

        Collection<ConsensusValue> coreSpectra = clusterSelector.selectSpectra(Lists.newArrayList(a1, a2, a3, b1, b2, b3));
        Assert.assertEquals(Sets.newHashSet(a1, a2, a3), new HashSet<>(coreSpectra));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSelectSpectraOneCluster() throws Exception {

        final MsnSpectrum sA1 = mock(MsnSpectrum.class);
        final MsnSpectrum sA2 = mock(MsnSpectrum.class);
        final MsnSpectrum sA3 = mock(MsnSpectrum.class);

        final SimFunc<PeakAnnotation, PeakAnnotation> simFunc = mock(SimFunc.class);
        when(simFunc.calcSimilarity(any(), any())).thenReturn(0.9);

        final ConsensusValue a1 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA1);
        final ConsensusValue a2 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA2);
        final ConsensusValue a3 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA3);

        MSTConsensusMemberSelector clusterSelector = new MSTConsensusMemberSelector(new MaxSimScoreClusterSelector(), simFunc, 0.6);

        Collection<ConsensusValue> coreSpectra = clusterSelector.selectSpectra(Lists.newArrayList(a1, a2, a3));
        Assert.assertEquals(Sets.newHashSet(a1, a2, a3), new HashSet<>(coreSpectra));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSelectSpectraOneCluster2() throws Exception {

        final MsnSpectrum sA1 = mock(MsnSpectrum.class);
        final MsnSpectrum sA2 = mock(MsnSpectrum.class);
        final MsnSpectrum sA3 = mock(MsnSpectrum.class);

        final SimFunc<PeakAnnotation, PeakAnnotation> simFunc = mock(SimFunc.class);
        when(simFunc.calcSimilarity(sA1, sA2)).thenReturn(0.9);
        when(simFunc.calcSimilarity(sA1, sA3)).thenReturn(0.9);
        when(simFunc.calcSimilarity(sA2, sA3)).thenReturn(0.5);

        final ConsensusValue a1 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA1);
        final ConsensusValue a2 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA2);
        final ConsensusValue a3 = new ConsensusValue(new HashSet<>(), new TObjectDoubleHashMap<>(), sA3);

        MSTConsensusMemberSelector clusterSelector = new MSTConsensusMemberSelector(new MaxSimScoreClusterSelector(), simFunc, 0.6);

        Collection<ConsensusValue> coreSpectra = clusterSelector.selectSpectra(Lists.newArrayList(a1, a2, a3));
        Assert.assertEquals(Sets.newHashSet(a1, a2, a3), new HashSet<>(coreSpectra));
    }
}