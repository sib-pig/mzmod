package org.expasy.mzjava.sparktool.liberator;

import com.google.common.collect.Lists;
import org.apache.hadoop.io.Text;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;
import org.expasy.mzjava.sparktool.util.PeptideMatchPredicate;
import org.junit.Assert;
import org.junit.Test;
import scala.Tuple2;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PeptideMatchFilterTest {

    @SuppressWarnings("unchecked")
    @Test
    public void testCall() throws Exception {

        PeptideMatch peptideMatchAccept = mock(PeptideMatch.class);
        PeptideMatch peptideMatchReject = mock(PeptideMatch.class);

        PeptideMatchPredicate predicate = mock(PeptideMatchPredicate.class);
        when(predicate.test(peptideMatchAccept)).thenReturn(true);
        when(predicate.test(peptideMatchReject)).thenReturn(false);

        Cached<PeptideMatchPredicate> cachedPredicate = mock(Cached.class);
        when(cachedPredicate.get()).thenReturn(predicate);

        PeptideMatchFilter peptideMatchFilter = new PeptideMatchFilter(cachedPredicate);

        List<Tuple2<String, PeptideMatch>> results = Lists.newArrayList(
                peptideMatchFilter.call(new Tuple2<>(new Text("KEY1"), mockValue(peptideMatchAccept)))
        );
        Assert.assertEquals(1, results.size());
        Assert.assertEquals("KEY1", results.get(0)._1());
        Assert.assertEquals(peptideMatchAccept, results.get(0)._2());

        results = Lists.newArrayList(
                peptideMatchFilter.call(new Tuple2<>(new Text("KEY2"), mockValue(peptideMatchReject)))
        );
        Assert.assertEquals(true, results.isEmpty());
    }

    private PeptideMatchValue mockValue(PeptideMatch peptideMatch) {

        PeptideMatchValue value = mock(PeptideMatchValue.class);
        when(value.get()).thenReturn(peptideMatch);
        return value;
    }
}