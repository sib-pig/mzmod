package org.expasy.mzjava.sparktool.liberator;

import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.apache.spark.api.java.Optional;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.CachedAbsoluteTolerance;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.junit.Assert;
import org.junit.Test;
import scala.Tuple2;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ToleranceCheckFunctionTest {

    @Test
    public void testCallPsmAbsent() throws Exception {

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);
        when(spectrum.getPrecursor()).thenReturn(new Peak(305.48, 123, 3));
        Tuple2<Optional<PeptideMatch>, MsnSpectrum> tuple = new Tuple2<>(Optional.<PeptideMatch>absent(), spectrum);

        ToleranceCheckFunction function = new ToleranceCheckFunction(new CachedAbsoluteTolerance(0.3));
        Tuple2<Optional<PeptideMatch>, MsnSpectrum> result = function.call(tuple);

        Assert.assertSame(result, tuple);
    }

    @Test
    public void testCallPsmWithinTolerance() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(Peptide.parse("PEPSIDEK"), Collections.<String>emptySet().stream(), new TObjectDoubleHashMap<>(),3);

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);
        when(spectrum.getPrecursor()).thenReturn(new Peak(305.43, 123, 3));
        Tuple2<Optional<PeptideMatch>, MsnSpectrum> tuple = new Tuple2<>(Optional.of(peptideMatch), spectrum);

        ToleranceCheckFunction function = new ToleranceCheckFunction(new CachedAbsoluteTolerance(0.3));
        Tuple2<Optional<PeptideMatch>, MsnSpectrum> result = function.call(tuple);

        Assert.assertSame(result, tuple);
    }

    @Test
    public void testCallPsmNotInTolerance() throws Exception {

        PeptideMatch peptideMatch = new PeptideMatch(Peptide.parse("PEPSIDEK"), Collections.<String>emptySet().stream(), new TObjectDoubleHashMap<>(),3);

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);
        when(spectrum.getPrecursor()).thenReturn(new Peak(305.81, 123, 3));     // Is out by 1Da (0.3333m/z)
        Tuple2<Optional<PeptideMatch>, MsnSpectrum> tuple = new Tuple2<>(Optional.of(peptideMatch), spectrum);

        ToleranceCheckFunction function = new ToleranceCheckFunction(new CachedAbsoluteTolerance(0.3));
        Tuple2<Optional<PeptideMatch>, MsnSpectrum> result = function.call(tuple);

        Assert.assertEquals(false, result._1().isPresent());
        Assert.assertSame(result._2(), tuple._2());
    }
}