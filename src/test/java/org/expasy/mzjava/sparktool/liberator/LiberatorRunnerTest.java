package org.expasy.mzjava.sparktool.liberator;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;

public class LiberatorRunnerTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testInitServer() throws Exception {

        LiberatorRunner runner = new LiberatorRunner("/scratch", "/scratch", "/scratch/cluster",true,true,6,1000,128,true);

        SparkConf conf = runner.initSpark();
        Assert.assertEquals("org.apache.spark.serializer.KryoSerializer", conf.get("spark.serializer"));
    }

    @Test
    public void testProgArgs() throws Exception {
        File hdpsm = temporaryFolder.newFile();
        File hdmsn = temporaryFolder.newFile();
        File outDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-hs",hdmsn.getAbsolutePath(),"-hp",hdpsm.getAbsolutePath(),"-l","-hl",outDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-ic" };

        LiberatorRunner liberatorRunner = new LiberatorRunner();
        try {
            FileUtils.deleteQuietly(outDir);
            liberatorRunner.parseOptions(args);

            Assert.assertEquals(outDir.getAbsolutePath(), liberatorRunner.getSparkDir());
            Assert.assertTrue(liberatorRunner.isRunLocal());
            Assert.assertEquals(4, liberatorRunner.getNrExec());
            Assert.assertEquals(1000, liberatorRunner.getExecMemoryMB());
            Assert.assertEquals(128, liberatorRunner.getBufferMemoryMB());
            Assert.assertTrue(liberatorRunner.isIndexConsensus());

            FileUtils.deleteQuietly(hdpsm);
            FileUtils.deleteQuietly(hdmsn);
        }
        catch( Exception exp ) {
            FileUtils.deleteQuietly(hdpsm);
            FileUtils.deleteQuietly(hdmsn);
            FileUtils.deleteQuietly(outDir);
            liberatorRunner.printOptions(args, exp.getMessage());
            Assert.fail();
        }

    }

    @Test
    public void testProgArgs2() throws Exception {
        File hdpsm = temporaryFolder.newFile();
        File hdmsn = temporaryFolder.newFile();
        File outDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-hs",hdmsn.getAbsolutePath(),"-hp",hdpsm.getAbsolutePath(),"-l","-hl",outDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128" };

        LiberatorRunner liberatorRunner = new LiberatorRunner();
        try {
            FileUtils.deleteQuietly(hdpsm);
            liberatorRunner.parseOptions(args);

            Assert.fail();
        }
        catch( Exception exp ) {
            FileUtils.deleteQuietly(hdmsn);
            FileUtils.deleteQuietly(outDir);
            liberatorRunner.printOptions(args, exp.getMessage());
        }

    }

    @Test
    public void testProgArgs3() throws Exception {
        File hdpsm = temporaryFolder.newFile();
        File hdmsn = temporaryFolder.newFile();
        File outDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-hs",hdmsn.getAbsolutePath(),"-hp",hdpsm.getAbsolutePath(),"-l","-hl",outDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128" };

        LiberatorRunner liberatorRunner = new LiberatorRunner();
        try {

            FileUtils.forceMkdir(outDir);
            liberatorRunner.parseOptions(args);

            Assert.fail();
        }
        catch( Exception exp ) {
            FileUtils.deleteQuietly(hdpsm);
            FileUtils.deleteQuietly(hdmsn);
            FileUtils.deleteQuietly(outDir);
            liberatorRunner.printOptions(args, exp.getMessage());
        }

    }

    @Test
    public void testProgArgs4() throws Exception {
        File hdpsm = temporaryFolder.newFile();
        File hdmsn = temporaryFolder.newFile();
        File outDir = temporaryFolder.newFolder();

        String[] args = new String[]{ "-hs",hdmsn.getAbsolutePath(),"-hp",hdpsm.getAbsolutePath(),"-l","-hl",outDir.getAbsolutePath(),"-n","4","-em","1000","-bm","128","-ic","-fd" };

        LiberatorRunner liberatorRunner = new LiberatorRunner();
        try {
            FileUtils.forceMkdir(outDir);
            liberatorRunner.parseOptions(args);

            Assert.assertEquals(outDir.getAbsolutePath(), liberatorRunner.getSparkDir());
            Assert.assertTrue(liberatorRunner.isRunLocal());
            Assert.assertEquals(4, liberatorRunner.getNrExec());
            Assert.assertEquals(1000, liberatorRunner.getExecMemoryMB());
            Assert.assertEquals(128, liberatorRunner.getBufferMemoryMB());
            Assert.assertTrue(liberatorRunner.isIndexConsensus());

            FileUtils.deleteQuietly(hdpsm);
            FileUtils.deleteQuietly(hdmsn);
            FileUtils.deleteQuietly(outDir);
        }
        catch( Exception exp ) {
            FileUtils.deleteQuietly(hdpsm);
            FileUtils.deleteQuietly(hdmsn);
            FileUtils.deleteQuietly(outDir);
            liberatorRunner.printOptions(args, exp.getMessage());
            Assert.fail();
        }

    }

}