package org.expasy.mzjava.sparktool.liberator;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.fragment.BackbonePeakGenerator;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideNeutralLossPeakGenerator;
import org.expasy.mzjava.proteomics.ms.fragment.PeptidePeakGenerator;
import org.expasy.mzjava.proteomics.ms.spectrum.PepFragAnnotation;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.sparktool.data.ConsensusKey;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.expasy.mzjava.sparktool.data.MockCached;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Assert;
import org.junit.Test;
import scala.Tuple2;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class ConsensusFunctionTest {

    @Test
    public void testTrimSpectra() throws Exception {

        Peptide peptide = Peptide.parse("CERV");

        PeptideConsensusSpectrum consensus = mock(PeptideConsensusSpectrum.class);

        PeptideConsensusSpectrum.Builder consensusBuilder = mock(PeptideConsensusSpectrum.Builder.class);
        //noinspection unchecked
        when(consensusBuilder.buildConsensus(anyInt(), any(Peptide.class), any(Collection.class), any(Set.class))).thenReturn(consensus);

        final ConsensusValue cv1 = new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.87), mock(MsnSpectrum.class));
        final ConsensusValue cv2 = new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.86), mock(MsnSpectrum.class));
        final ConsensusValue cv3 = new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.85), mock(MsnSpectrum.class));

        List<ConsensusValue> values = Lists.newArrayList(cv3, cv2, cv1);

        final ConsensusMemberSelector memberSelector = mock(ConsensusMemberSelector.class);
        when(memberSelector.selectSpectra(any())).thenReturn(Lists.newArrayList(cv1, cv2));

        final LiberatorParameters parameters = mock(LiberatorParameters.class);
        when(parameters.maxConsensusMembers()).thenReturn(2);
        when(parameters.consensusValueComparator()).thenReturn(new MockCached<>((v1, v2) -> Double.compare(v2.getScore("score1"), v1.getScore("score1"))));
        when(parameters.consensusMemberSelector()).thenReturn(new MockCached<>(memberSelector));
        when(parameters.consensusBuilder()).thenReturn(new MockCached<>(consensusBuilder));

        ConsensusFunction function = new ConsensusFunction(parameters);

        function.call(new Tuple2<>(new ConsensusKey(2, peptide), values));

        verify(memberSelector).selectSpectra(Lists.newArrayList(cv1, cv2));
    }

    @Test
    public void testCall() throws Exception {

        Peptide peptide = Peptide.parse("CERV");

        List<MsnSpectrum> spectra = makeSpectra(peptide);

        List<ConsensusValue> values = new ArrayList<>();
        values.add(new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.87), spectra.get(0)));
        values.add(new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.86), spectra.get(1)));
        values.add(new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.85), spectra.get(2)));

        PeptideConsensusSpectrum.Builder consensusBuilder = newConsensusBuilder();

        final ConsensusMemberSelector memberSelector = mock(ConsensusMemberSelector.class);
        when(memberSelector.selectSpectra(any())).thenReturn(values);

        final LiberatorParameters parameters = mock(LiberatorParameters.class);
        when(parameters.consensusBuilder()).thenReturn(new MockCached<>(consensusBuilder));
        when(parameters.maxConsensusMembers()).thenReturn(Integer.MAX_VALUE);
        when(parameters.consensusMemberSelector()).thenReturn(new MockCached<>(memberSelector));

        ConsensusFunction function = new ConsensusFunction(parameters);

        PeptideConsensusSpectrum consensusSpectrum = function.call(new Tuple2<>(new ConsensusKey(2, peptide), values));

        checkConsensus(spectra, consensusSpectrum);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCallWithOutliers() throws Exception {

        Peptide peptide = Peptide.parse("CERV");

        List<MsnSpectrum> spectra = makeSpectra(peptide);

        MsnSpectrum spectrum4 = new MsnSpectrum();
        spectrum4.add(103.50, 12.0);    //unknown
        spectrum4.add(104.60, 23.0);    //unknown
        spectrum4.add(371.66, 23.0);    //unknown
        spectrum4.add(402.63, 23.0);    //unknown
        spectrum4.setPrecursor(new Peak(peptide.calculateMz(2) + 0.001, 12.0, 2));
        spectrum4.setId(UUID.randomUUID());

        List<ConsensusValue> values = new ArrayList<>();
        values.add(new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.87), spectra.get(0)));
        values.add(new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.86), spectra.get(1)));
        values.add(new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.85), spectra.get(2)));
        values.add(new ConsensusValue(Sets.newHashSet("Prot1"), newScoreMap("score1", 0.5), spectrum4));

        PeptideConsensusSpectrum.Builder consensusBuilder = newConsensusBuilder();

        final ConsensusMemberSelector memberSelector = mock(ConsensusMemberSelector.class);
        when(memberSelector.selectSpectra(any())).thenReturn(values.subList(0, 3));

        final LiberatorParameters parameters = mock(LiberatorParameters.class);
        when(parameters.consensusBuilder()).thenReturn(new MockCached<>(consensusBuilder));
        when(parameters.maxConsensusMembers()).thenReturn(Integer.MAX_VALUE);
        when(parameters.consensusMemberSelector()).thenReturn(new MockCached<>(memberSelector));

        ConsensusFunction function = new ConsensusFunction(parameters);

        PeptideConsensusSpectrum consensusSpectrum = function.call(new Tuple2<>(new ConsensusKey(2, peptide), values));

        checkConsensus(spectra, consensusSpectrum);
    }

    private void checkConsensus(List<MsnSpectrum> spectra, PeptideConsensusSpectrum consensusSpectrum) {

        Assert.assertEquals(spectra.stream().map(Spectrum::getId).collect(Collectors.toSet()), consensusSpectrum.getMemberIds());

        Assert.assertEquals(8, consensusSpectrum.size());

        Assert.assertEquals(8, consensusSpectrum.getAnnotationIndexes().length);

        Assert.assertEquals(1, consensusSpectrum.getAnnotations(0).size());
        Assert.assertEquals(new PepLibPeakAnnotation(3, 0.0, Math.sqrt(2.0 / 3.0)), consensusSpectrum.getAnnotations(0).get(0));
        Assert.assertEquals(1, consensusSpectrum.getAnnotations(1).size());

        final double delta = 0.00001;

        PepFragAnnotation fragAnnotation = new PepFragAnnotation(IonType.b,1, Peptide.parse("C"));
        Assert.assertEquals(1, consensusSpectrum.getAnnotations(1).size());
        Assert.assertEquals(3, consensusSpectrum.getAnnotations(1).get(0).getMergedPeakCount());
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(1).get(0).getMzStd(), delta);
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(1).get(0).getIntensityStd(), delta);
        Assert.assertEquals(fragAnnotation, consensusSpectrum.getAnnotations(1).get(0).getOptFragmentAnnotation().get());

        PepFragAnnotation.Builder builder = new PepFragAnnotation.Builder(IonType.b,1, Peptide.parse("CER"));
        fragAnnotation = builder.setNeutralLoss(Composition.parseComposition("H-2O-1")).build();
        Assert.assertEquals(1, consensusSpectrum.getAnnotations(2).size());
        Assert.assertEquals(1, consensusSpectrum.getAnnotations(2).get(0).getMergedPeakCount());
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(2).get(0).getMzStd(), delta);
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(2).get(0).getIntensityStd(), delta);
        Assert.assertEquals(fragAnnotation, consensusSpectrum.getAnnotations(2).get(0).getOptFragmentAnnotation().get());

        Assert.assertEquals(1, consensusSpectrum.getAnnotations(3).size());
        Assert.assertEquals(3, consensusSpectrum.getAnnotations(3).get(0).getMergedPeakCount());
        double sd = Math.sqrt(2.0*0.0001/3);
        Assert.assertEquals(sd, consensusSpectrum.getAnnotations(3).get(0).getMzStd(), delta);
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(3).get(0).getIntensityStd(), delta);
        Assert.assertEquals(false, consensusSpectrum.getAnnotations(3).get(0).getOptFragmentAnnotation().isPresent());

        fragAnnotation = new PepFragAnnotation(IonType.b,1, Peptide.parse("CER"));
        Assert.assertEquals(1, consensusSpectrum.getAnnotations(4).size());
        Assert.assertEquals(3, consensusSpectrum.getAnnotations(4).get(0).getMergedPeakCount());
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(4).get(0).getMzStd(), delta);
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(4).get(0).getIntensityStd(), delta);
        Assert.assertEquals(fragAnnotation, consensusSpectrum.getAnnotations(4).get(0).getOptFragmentAnnotation().get());

        Assert.assertEquals(1, consensusSpectrum.getAnnotations(5).size());
        Assert.assertEquals(2, consensusSpectrum.getAnnotations(5).get(0).getMergedPeakCount());
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(5).get(0).getMzStd(), delta);
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(5).get(0).getIntensityStd(), delta);
        Assert.assertEquals(false, consensusSpectrum.getAnnotations(5).get(0).getOptFragmentAnnotation().isPresent());

        fragAnnotation = new PepFragAnnotation(IonType.y,1, Peptide.parse("ERV"));
        Assert.assertEquals(1, consensusSpectrum.getAnnotations(6).size());
        Assert.assertEquals(3, consensusSpectrum.getAnnotations(6).get(0).getMergedPeakCount());
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(6).get(0).getMzStd(), delta);
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(6).get(0).getIntensityStd(), delta);
        Assert.assertEquals(fragAnnotation, consensusSpectrum.getAnnotations(6).get(0).getOptFragmentAnnotation().get());

        Assert.assertEquals(1, consensusSpectrum.getAnnotations(7).size());
        Assert.assertEquals(2, consensusSpectrum.getAnnotations(7).get(0).getMergedPeakCount());
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(7).get(0).getMzStd(), delta);
        Assert.assertEquals(0.0, consensusSpectrum.getAnnotations(7).get(0).getIntensityStd(), delta);
        Assert.assertEquals(false, consensusSpectrum.getAnnotations(7).get(0).getOptFragmentAnnotation().isPresent());
    }

    private List<MsnSpectrum> makeSpectra(Peptide peptide) {

        MsnSpectrum spectrum1 = new MsnSpectrum();
        spectrum1.add(103.50, 10.0);    //unknown
        spectrum1.add(104.02, 12.0);    //b1
        spectrum1.add(371.15, 23.0);    //b3-18.011
        spectrum1.add(371.64, 23.0);    //unknown
        spectrum1.add(389.16, 23.0);    //b3
        spectrum1.add(402.63, 23.0);    //unknown
        spectrum1.add(403.23, 23.0);    //y3
        spectrum1.add(457.23, 23.0);    //unknown
        spectrum1.setPrecursor(new Peak(peptide.calculateMz(2) + 0.001, 10.0, 2));
        spectrum1.setId(UUID.randomUUID());

        MsnSpectrum spectrum2 = new MsnSpectrum();
        spectrum2.add(103.50, 11.0);    //unknown
        spectrum2.add(104.02, 12.0);    //b1
        spectrum2.add(371.65, 23.0);    //unknown
        spectrum2.add(389.16, 23.0);    //b3
        spectrum2.add(403.23, 23.0);    //y3
        spectrum2.add(457.23, 23.0);    //unknown
        spectrum2.setPrecursor(new Peak(peptide.calculateMz(2) + 0.001, 11.0, 2));
        spectrum2.setId(UUID.randomUUID());

        MsnSpectrum spectrum3 = new MsnSpectrum();
        spectrum3.add(103.50, 12.0);    //unknown
        spectrum3.add(104.02, 12.0);    //b1
        spectrum3.add(104.60, 23.0);    //unknown
        spectrum3.add(371.66, 23.0);    //unknown
        spectrum3.add(389.16, 23.0);    //b3
        spectrum3.add(402.63, 23.0);    //unknown
        spectrum3.add(403.23, 23.0);    //y3
        spectrum3.setPrecursor(new Peak(peptide.calculateMz(2) + 0.001, 12.0, 2));
        spectrum3.setId(UUID.randomUUID());

        //noinspection unchecked
        return Lists.newArrayList(spectrum1, spectrum2, spectrum3);
    }

    private PeptideConsensusSpectrum.Builder newConsensusBuilder() {

        List<PeptidePeakGenerator<PepFragAnnotation>> peakGeneratorList = new ArrayList<>();
        peakGeneratorList.add(new BackbonePeakGenerator(EnumSet.of(IonType.a, IonType.b, IonType.y), 10));
        Mass waterLoss = Composition.parseComposition("H-2O-1");
        Set<AminoAcid> aa = EnumSet.of(AminoAcid.S, AminoAcid.T, AminoAcid.D, AminoAcid.E);
        Set<IonType> ions = EnumSet.of(IonType.b, IonType.y);
        peakGeneratorList.add(new PeptideNeutralLossPeakGenerator(waterLoss, aa, ions, 5));

        return PeptideConsensusSpectrum.builder(PeakList.Precision.DOUBLE, new URIBuilder("org.expasy.mzjava", "test").build())
                .setConsensusParameters(0.2, 0.2, AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY)
                .setAnnotationParameters(new AbsoluteTolerance(0.2), new PeptideFragmenter(peakGeneratorList, PeakList.Precision.DOUBLE))
                .setFilterParameters(0.2, 2);
    }

    private TObjectDoubleMap<String> newScoreMap(String name, double value) {

        TObjectDoubleMap<String> scores = new TObjectDoubleHashMap<>(1);
        scores.put(name, value);

        return scores;
    }
}