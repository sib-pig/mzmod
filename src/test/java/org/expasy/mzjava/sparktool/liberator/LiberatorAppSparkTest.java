package org.expasy.mzjava.sparktool.liberator;

import org.apache.spark.api.java.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.hadoop.io.MsnSpectrumValue;
import org.expasy.mzjava.hadoop.io.SpectrumKey;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.consensus.shuffling.ControlledLibrarySpectrumShuffler;
import org.expasy.mzjava.proteomics.ms.consensus.shuffling.LibrarySpectrumShuffler;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.expasy.mzjava.sparktool.data.Cached;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.expasy.mzjava.sparktool.deliberator.LibrarySpectrumShufflerFactory;
import org.expasy.mzjava.sparktool.hadoop.PeptideMatchValue;
import org.expasy.mzjava.sparktool.util.PeptideMatchPredicate;
import org.expasy.mzjava.sparktool.util.SpectrumKeyFunction;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import scala.Tuple2;

import java.util.*;
import java.util.function.Predicate;

import static org.mockito.Mockito.mock;

public class LiberatorAppSparkTest {

    private static JavaSparkContext sc;

    @BeforeClass
    public static void initSpark() {

        System.clearProperty("spark.driver.port");
        System.clearProperty("spark.hostPort");

        SparkConf conf = new SparkConf()
                .setAppName(LiberatorAppSparkTest.class.getName())
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
                .set("spark.kryo.registrator", "org.expasy.mzjava.spark.MzJavaKryoRegistrator")
                .setMaster("local");
        sc = new JavaSparkContext(conf);
    }

    @AfterClass
    public static void stopSpark() {

        sc.stop();

        System.clearProperty("spark.driver.port");
        System.clearProperty("spark.hostPort");
    }

    @Test
    public void testCreateDecoy() throws Exception {

        PeptideConsensusSpectrum consensus1 = newPeptideConsensusSpectrum("PEPTSIDE", "213a0384-8a3f-42c9-991a-b91604a16f77");
        PeptideConsensusSpectrum consensus2 = newPeptideConsensusSpectrum("PEPTSID", "e2e3f250-e637-4ef0-9b50-daa15e17180f");
        PeptideConsensusSpectrum consensus3 = newPeptideConsensusSpectrum("PEPTSI", "35651be8-6873-4ab8-8cb4-ccd32fb77e80");

        JavaRDD<PeptideConsensusSpectrum> consensusSpectra = sc.parallelize(Lists.newArrayList(consensus1, consensus2, consensus3));

        LibrarySpectrumShufflerFactory spectrumShufflerFactory = MockControlledLibrarySpectrumShuffler::new;

        List<PeptideConsensusSpectrum> decoySpectra = new LiberatorApp(mock(LiberatorParameters.class)).createDecoy(consensusSpectra, spectrumShufflerFactory)
                .collect();

        Assert.assertEquals(Lists.newArrayList(newPeptideConsensusSpectrum("EDISTPEP", "19eb228d-3a89-44d2-a1bb-9f4a34c2954b"), newPeptideConsensusSpectrum("ISTPEP", "469fb3d5-0dbe-449b-89bd-b54242c2fb47")), decoySpectra);
    }

    @Test
    public void testGetUnidentifiedSpectra() throws Exception {

        List<Tuple2<Optional<PeptideMatch>, MsnSpectrum>> psmSpectrumJoin = new ArrayList<>();
        psmSpectrumJoin.add(new Tuple2<>(Optional.<PeptideMatch>absent(), newMsnSpectrum(799.23, 2, "ac5b4b04-c7f2-4139-af52-e137a2b73ae9")));
        psmSpectrumJoin.add(new Tuple2<>(Optional.of(new PeptideMatch(Peptide.parse("SSKK"), Collections.singleton("PROT1").stream(), new TObjectDoubleHashMap<>(),2)), newMsnSpectrum(799.23, 2, "b87bac5a-ea64-4f04-9402-c536f192707b")));
        psmSpectrumJoin.add(new Tuple2<>(Optional.<PeptideMatch>absent(), newMsnSpectrum(799.23, 2, "1d2b2b20-56d4-4ac7-b3b9-b61b24ac5dff")));

        List<MsnSpectrum> unidentifiedSpectra = new LiberatorApp(mock(LiberatorParameters.class)).getUnidentifiedSpectra(sc.parallelizePairs(psmSpectrumJoin)).collect();
        //noinspection unchecked
        Assert.assertEquals(Lists.newArrayList(newMsnSpectrum(799.23, 2, "ac5b4b04-c7f2-4139-af52-e137a2b73ae9"), newMsnSpectrum(799.23, 2, "1d2b2b20-56d4-4ac7-b3b9-b61b24ac5dff")),
                unidentifiedSpectra);
    }

    @Test
    public void testCreateLibrary() throws Exception {

        List<Tuple2<Optional<PeptideMatch>, MsnSpectrum>> psmSpectrumJoin = new ArrayList<>();
        psmSpectrumJoin.add(new Tuple2<>(Optional.<PeptideMatch>absent(), newMsnSpectrum(799.23, 2, "ac5b4b04-c7f2-4139-af52-e137a2b73ae9")));
        psmSpectrumJoin.add(new Tuple2<>(Optional.of(new PeptideMatch(Peptide.parse("SSKK"), Collections.singleton("PROT1").stream(), new TObjectDoubleHashMap<>(),2)), newMsnSpectrum(799.23, 2, "b87bac5a-ea64-4f04-9402-c536f192707b")));
        psmSpectrumJoin.add(new Tuple2<>(Optional.of(new PeptideMatch(Peptide.parse("SSKK"), Collections.singleton("PROT1").stream(), new TObjectDoubleHashMap<>(),2)), newMsnSpectrum(799.23, 2, "6b75b3b7-b0cf-4e69-93bb-ba4798402ad8")));
        psmSpectrumJoin.add(new Tuple2<>(Optional.<PeptideMatch>absent(), newMsnSpectrum(799.23, 2, "1d2b2b20-56d4-4ac7-b3b9-b61b24ac5dff")));

        List<PeptideConsensusSpectrum> consensusSpectra = new LiberatorApp(new MockLiberatorParameters()).createLibrary(sc.parallelizePairs(psmSpectrumJoin)).collect();

        Assert.assertEquals(1, consensusSpectra.size());
        Assert.assertEquals(Sets.newHashSet(UUID.fromString("b87bac5a-ea64-4f04-9402-c536f192707b"), UUID.fromString("6b75b3b7-b0cf-4e69-93bb-ba4798402ad8")), consensusSpectra.get(0).getMemberIds());
    }

    @Test
    public void testExecutePsmSpectraJoin() throws Exception {

        List<Tuple2<Text, PeptideMatchValue>> psmRdd = new ArrayList<>();
        psmRdd.add(new Tuple2<>(new Text("key_1"), new PeptideMatchValue(new PeptideMatch(Peptide.parse("CERVPEPK"), Collections.<String>emptySet().stream(), new TObjectDoubleHashMap<>(),2))));
        psmRdd.add(new Tuple2<>(new Text("key_2"), new PeptideMatchValue(new PeptideMatch(Peptide.parse("PEAILLR"), Collections.<String>emptySet().stream(), new TObjectDoubleHashMap<>(),2))));
        psmRdd.add(new Tuple2<>(new Text("key_3"), new PeptideMatchValue(new PeptideMatch(Peptide.parse("RR"), Collections.<String>emptySet().stream(), new TObjectDoubleHashMap<>(),2))));

        List<Tuple2<SpectrumKey, MsnSpectrumValue>> spectrumRdd = new ArrayList<>();
        spectrumRdd.add(new Tuple2<>(new SpectrumKey(), newMsnSpectrumValue("key_1", 733.32, 2, "c012e1c1-40b8-4145-af24-3e25aab65367")));
        spectrumRdd.add(new Tuple2<>(new SpectrumKey(), newMsnSpectrumValue("key_2", 833.32, 2, "949d57de-b566-4909-92fb-7005cd97ce4f")));
        spectrumRdd.add(new Tuple2<>(new SpectrumKey(), newMsnSpectrumValue("key_4", 933.32, 2, "47a9c48a-9092-4f2b-a8be-2f337487e01b")));

        List<Tuple2<Optional<PeptideMatch>, MsnSpectrum>> joins = new LiberatorApp(new MockLiberatorParameters()).executePsmSpectraJoin(sc.parallelizePairs(psmRdd), sc.parallelizePairs(spectrumRdd)).collect();

        Assert.assertEquals(3, joins.size());

        Tuple2<Optional<PeptideMatch>, MsnSpectrum> join1 = joins.get(0);
        Assert.assertEquals("PEAILLR", join1._1().get().getPeptide().toString());
        Assert.assertEquals(UUID.fromString("949d57de-b566-4909-92fb-7005cd97ce4f"), join1._2().getId());

        Tuple2<Optional<PeptideMatch>, MsnSpectrum> join2 = joins.get(1);
        Assert.assertEquals("CERVPEPK", join2._1().get().getPeptide().toString());
        Assert.assertEquals(UUID.fromString("c012e1c1-40b8-4145-af24-3e25aab65367"), join2._2().getId());

        Tuple2<Optional<PeptideMatch>, MsnSpectrum> join3 = joins.get(2);
        Assert.assertEquals(false, join3._1().isPresent());
        Assert.assertEquals(UUID.fromString("47a9c48a-9092-4f2b-a8be-2f337487e01b"), join3._2().getId());
    }

    private MsnSpectrumValue newMsnSpectrumValue(String key, double mz, int charge, String id) {

        final MsnSpectrumValue spectrumValue = new MsnSpectrumValue();
        final MsnSpectrum spectrum = newMsnSpectrum(mz, charge, id);
        spectrum.setComment(key);
        spectrumValue.set(spectrum);
        return spectrumValue;
    }

    private MsnSpectrum newMsnSpectrum(double mz, int charge, String id) {

        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.getPrecursor().setValues(mz, 100, charge);
        spectrum.setId(UUID.fromString(id));
        return spectrum;
    }

    private PeptideConsensusSpectrum newPeptideConsensusSpectrum(String peptide, String uuid) {

        PeptideConsensusSpectrum consensus1 = new PeptideConsensusSpectrum(Peptide.parse(peptide));
        consensus1.setId(UUID.fromString(uuid));
        return consensus1;
    }

    private static class MockControlledLibrarySpectrumShuffler extends ControlledLibrarySpectrumShuffler {

        public MockControlledLibrarySpectrumShuffler() {

            super(0.7, 10);
        }

        @Override
        public Optional<PeptideConsensusSpectrum> shuffle(PeptideConsensusSpectrum spectrum, LibrarySpectrumShuffler.PeptideTerminiHandling peptideTerminiHandling) {

            final PeptideConsensusSpectrum peptide;
            if("PEPTSIDE".equals(spectrum.getPeptide().toString())) {

                peptide = new PeptideConsensusSpectrum(Peptide.parse("EDISTPEP"));
                peptide.setId(UUID.fromString("19eb228d-3a89-44d2-a1bb-9f4a34c2954b"));
            } else if("PEPTSI".equals(spectrum.getPeptide().toString())) {

                peptide = new PeptideConsensusSpectrum(Peptide.parse("ISTPEP"));
                peptide.setId(UUID.fromString("469fb3d5-0dbe-449b-89bd-b54242c2fb47"));
            } else {

                peptide = null;
            }

            return Optional.fromNullable(peptide);
        }
    }

    private static class MockLiberatorParameters implements LiberatorParameters {

        @Override
        public Cached<PeptideConsensusSpectrum.Builder> consensusBuilder() {

            return new Cached<PeptideConsensusSpectrum.Builder>() {
                @Override
                protected PeptideConsensusSpectrum.Builder build() {

                    return PeptideConsensusSpectrum.builder(PeakList.Precision.FLOAT, new URIBuilder("org.expasy", "liberator").build())
                            .setConsensusParameters(0.2, 0.2, AbstractMergePeakFilter.IntensityMode.SUM_INTENSITY)
                            .setAnnotationParameters(new AbsoluteTolerance(0.2), new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.FLOAT))
                            .setFilterParameters(0.2, 2);
                }
            };
        }

        @Override
        public Cached<PeptideMatchPredicate> peptideMatchPredicate() {

            return new Cached<PeptideMatchPredicate>() {
                @Override
                protected PeptideMatchPredicate build() {

                    return new PeptideMatchPredicate(1,10,1,40,"hyperscore",30.0, PeptideMatchPredicate.ScoreOrder.LARGER,1) {
                        public boolean test(PeptideMatch psm) {
                            return true;
                        }
                    };
                };
            };
        }

        @Override
        public Cached<Comparator<ConsensusValue>> consensusValueComparator() {

            return new Cached<Comparator<ConsensusValue>>() {
                @Override
                protected Comparator<ConsensusValue> build() {

                    return (v1, v2) -> Double.compare(v2.getScore("score"), v1.getScore("score"));
                }
            };
        }

        @Override
        public PeakList.Precision precision() {

            return null;
        }

        @Override
        public Cached<SpectrumKeyFunction<MsnSpectrum>> spectrumPSMKeyFunction() {

            return new Cached<SpectrumKeyFunction<MsnSpectrum>>() {
                @Override
                protected SpectrumKeyFunction<MsnSpectrum> build() {

                    return new SpectrumKeyFunction<MsnSpectrum>() {
                        @Override
                        public String apply (SpectrumIdentifier identifier, Peptide peptide){

                            return null;
                        }

                        @Override
                        public String apply (MsnSpectrum spectrum){

                            return spectrum.getComment();
                        }

                        @Override
                        public String getRunID(MsnSpectrum spectrum) {
                            return null;
                        }
                    };
                }
            };
        }

        @Override
        public Cached<PeakProcessorChain> processorChain() {

            return null;
        }

        @Override
        public int maxConsensusMembers() {

            return Integer.MAX_VALUE;
        }

        @Override
        public Cached<Predicate<MsnSpectrum>> inputSpectrumPredicate() {

            return new Cached<Predicate<MsnSpectrum>>() {
                @Override
                protected Predicate<MsnSpectrum> build() {

                    return spectrum -> true;
                }
            };
        }

        @Override
        public Cached<ConsensusMemberSelector> consensusMemberSelector() {

            return new Cached<ConsensusMemberSelector>() {
                @Override
                protected ConsensusMemberSelector build() {

                    return candidates -> candidates;
                }
            };
        }

        @Override
        public Cached<LibrarySpectrumShufflerFactory> spectrumShufflerFactory() {

            return null;
        }

        @Override
        public Cached<Tolerance> precursorTolerance() {

            return null;
        }

        @Override
        public Cached<Tolerance> fragmentTolerance() {
            return null;
        }

        @Override
        public boolean filterFDR() {
            return false;
        }

        @Override
        public float qValue() {
            return 0.01f;
        }


    }
}