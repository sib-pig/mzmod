package org.expasy.mzjava.sparktool.avro;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.ConsensusKey;
import org.junit.Test;

public class ConsensusKeyAvroTest {

    @Test
    public void testRoundTrip() throws Exception {

        ConsensusKey value = new ConsensusKey(3, Peptide.parse("PEPTS(HPO3)IDE"));

        String expectedJson = "{\n" +
                "  \"charge\" : 3,\n" +
                "  \"spectrum\" : {\n" +
                "    \"seq\" : [ \"P\", \"E\", \"P\", \"T\", \"S\", \"I\", \"D\", \"E\" ],\n" +
                "    \"sideChainModMap\" : [ {\n" +
                "      \"index\" : 4,\n" +
                "      \"modification\" : {\n" +
                "        \"label\" : \"HPO3\",\n" +
                "        \"mass\" : {\n" +
                "          \"org.expasy.mzjava_avro.core.mol.Composition\" : {\n" +
                "            \"composition\" : {\n" +
                "              \"H\" : 1,\n" +
                "              \"O\" : 3,\n" +
                "              \"P\" : 1\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    } ],\n" +
                "    \"termModMap\" : [ ]\n" +
                "  }\n" +
                "}";

        AvroAssert.assertRoundTrip(expectedJson, value);
    }

    @Test
    public void testCreateSchema() throws Exception {

        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"ConsensusKey\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"charge\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"spectrum\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peptide\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"seq\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"AminoAcid\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "            \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"sideChainModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"SideChainModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"index\",\n" +
                "              \"type\" : \"int\"\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"Modification\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"label\",\n" +
                "                  \"type\" : \"string\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"mass\",\n" +
                "                  \"type\" : [ {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"NumericMass\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"mass\",\n" +
                "                      \"type\" : \"double\"\n" +
                "                    } ]\n" +
                "                  }, {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"Composition\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"composition\",\n" +
                "                      \"type\" : {\n" +
                "                        \"type\" : \"map\",\n" +
                "                        \"values\" : \"int\"\n" +
                "                      }\n" +
                "                    } ]\n" +
                "                  } ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"termModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"TermModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"attachment\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"enum\",\n" +
                "                \"name\" : \"ModAttachment\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "                \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "              }\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : \"Modification\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  } ]\n" +
                "}";

        AvroAssert.assertSchema(expected, new ConsensusKeySchemaBuilder());
    }
}