package org.expasy.mzjava.sparktool.avro;

import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.OmsQuery;
import org.junit.Test;

import java.util.Collections;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class OmsQueryAvroTest {

    @Test
    public void testRoundTrip() throws Exception {

        MsnSpectrum spectrum = newSpectrum(723.3, 2, "2b181369-8fd6-4234-8bf4-764a59bdc279", new double[]{723.31, 724.31}, new double[]{100, 2});

        OmsQuery omsQuery = new OmsQuery(spectrum, Collections.emptyList());

        final String expectedJson = "{\n" +
                "  \"spectrum\" : {\n" +
                "    \"precursor\" : {\n" +
                "      \"polarity\" : \"POSITIVE\",\n" +
                "      \"charge\" : [ 2 ],\n" +
                "      \"mz\" : 723.3,\n" +
                "      \"intensity\" : 100.0\n" +
                "    },\n" +
                "    \"msLevel\" : 0,\n" +
                "    \"spectrumIndex\" : -1,\n" +
                "    \"parentScanNumber\" : {\n" +
                "      \"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\" : {\n" +
                "        \"value\" : -1\n" +
                "      }\n" +
                "    },\n" +
                "    \"fragMethod\" : \"\",\n" +
                "    \"retentionTimes\" : [ ],\n" +
                "    \"scanNumbers\" : [ ],\n" +
                "    \"source\" : \"/dev/null\",\n" +
                "    \"comment\" : \"\",\n" +
                "    \"id\" : {\n" +
                "      \"mostSignificantBits\" : 3105253287177634356,\n" +
                "      \"leastSignificantBits\" : -8361928546414902663\n" +
                "    },\n" +
                "    \"precision\" : \"FLOAT\",\n" +
                "    \"peaks\" : {\n" +
                "      \"org.expasy.mzjava_avro.core.ms.peaklist.FloatPeakList\" : {\n" +
                "        \"peaks\" : [ {\n" +
                "          \"mz\" : 723.31,\n" +
                "          \"i\" : 100.0\n" +
                "        }, {\n" +
                "          \"mz\" : 724.31,\n" +
                "          \"i\" : 2.0\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"omsResults\" : [ ]\n" +
                "}";
        AvroAssert.assertRoundTrip(expectedJson, omsQuery);
    }

    private MsnSpectrum newSpectrum(double mz, int z, String id, double[] mzs, double[] intensities) {

        MsnSpectrum spectrum = new MsnSpectrum(mzs.length, PeakList.Precision.FLOAT);
        spectrum.getPrecursor().setValues(mz, 100, z);
        spectrum.setId(UUID.fromString(id));
        spectrum.addSorted(mzs, intensities);

        return spectrum;
    }

    @Test
    public void testSchema() throws Exception {

        final String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"OmsQuery\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"spectrum\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"MsnSpectrum\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"precursor\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"Peak\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"polarity\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"enum\",\n" +
                "              \"name\" : \"Polarity\",\n" +
                "              \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "              \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "            }\n" +
                "          }, {\n" +
                "            \"name\" : \"charge\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : \"int\"\n" +
                "            }\n" +
                "          }, {\n" +
                "            \"name\" : \"mz\",\n" +
                "            \"type\" : \"double\"\n" +
                "          }, {\n" +
                "            \"name\" : \"intensity\",\n" +
                "            \"type\" : \"double\"\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"msLevel\",\n" +
                "        \"type\" : \"int\"\n" +
                "      }, {\n" +
                "        \"name\" : \"spectrumIndex\",\n" +
                "        \"type\" : \"int\"\n" +
                "      }, {\n" +
                "        \"name\" : \"parentScanNumber\",\n" +
                "        \"type\" : [ {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"ScanNumberDiscrete\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"value\",\n" +
                "            \"type\" : \"int\"\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"ScanNumberInterval\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"minScanNumber\",\n" +
                "            \"type\" : \"int\"\n" +
                "          }, {\n" +
                "            \"name\" : \"maxScanNumber\",\n" +
                "            \"type\" : \"int\"\n" +
                "          } ]\n" +
                "        } ]\n" +
                "      }, {\n" +
                "        \"name\" : \"fragMethod\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"retentionTimes\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : [ {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"RetentionTimeDiscrete\",\n" +
                "            \"doc\" : \"Retention time in seconds\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"time\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }, {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"RetentionTimeInterval\",\n" +
                "            \"doc\" : \"Retention time in seconds\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"minRetentionTime\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"maxRetentionTime\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"scanNumbers\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : [ \"ScanNumberDiscrete\", \"ScanNumberInterval\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"source\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"comment\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"id\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"UUID\",\n" +
                "          \"namespace\" : \"java.util\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"mostSignificantBits\",\n" +
                "            \"type\" : \"long\"\n" +
                "          }, {\n" +
                "            \"name\" : \"leastSignificantBits\",\n" +
                "            \"type\" : \"long\"\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"precision\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Precision\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : [ {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"DoublePeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"DoublePeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"i\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"FloatPeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"FloatPeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"float\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"i\",\n" +
                "                  \"type\" : \"float\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"DoubleFloatPeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"DoubleFloatPeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"i\",\n" +
                "                  \"type\" : \"float\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"DoubleConstantPeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"intensity\",\n" +
                "            \"type\" : \"double\"\n" +
                "          }, {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"DoubleConstantPeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"FloatConstantPeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"intensity\",\n" +
                "            \"type\" : \"double\"\n" +
                "          }, {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"FloatConstantPeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"float\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        } ]\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"omsResults\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : {\n" +
                "        \"type\" : \"record\",\n" +
                "        \"name\" : \"OmsResult\",\n" +
                "        \"fields\" : [ {\n" +
                "          \"name\" : \"hitType\",\n" +
                "          \"type\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"HitType\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.sparktool.data\",\n" +
                "            \"symbols\" : [ \"OPEN\", \"EXACT\" ]\n" +
                "          }\n" +
                "        }, {\n" +
                "          \"name\" : \"peptide\",\n" +
                "          \"type\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"Peptide\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"seq\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : {\n" +
                "                  \"type\" : \"enum\",\n" +
                "                  \"name\" : \"AminoAcid\",\n" +
                "                  \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "                  \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "                }\n" +
                "              }\n" +
                "            }, {\n" +
                "              \"name\" : \"sideChainModMap\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : {\n" +
                "                  \"type\" : \"record\",\n" +
                "                  \"name\" : \"SideChainModRecord\",\n" +
                "                  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "                  \"fields\" : [ {\n" +
                "                    \"name\" : \"index\",\n" +
                "                    \"type\" : \"int\"\n" +
                "                  }, {\n" +
                "                    \"name\" : \"modification\",\n" +
                "                    \"type\" : {\n" +
                "                      \"type\" : \"record\",\n" +
                "                      \"name\" : \"Modification\",\n" +
                "                      \"fields\" : [ {\n" +
                "                        \"name\" : \"label\",\n" +
                "                        \"type\" : \"string\"\n" +
                "                      }, {\n" +
                "                        \"name\" : \"mass\",\n" +
                "                        \"type\" : [ {\n" +
                "                          \"type\" : \"record\",\n" +
                "                          \"name\" : \"NumericMass\",\n" +
                "                          \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                          \"fields\" : [ {\n" +
                "                            \"name\" : \"mass\",\n" +
                "                            \"type\" : \"double\"\n" +
                "                          } ]\n" +
                "                        }, {\n" +
                "                          \"type\" : \"record\",\n" +
                "                          \"name\" : \"Composition\",\n" +
                "                          \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                          \"fields\" : [ {\n" +
                "                            \"name\" : \"composition\",\n" +
                "                            \"type\" : {\n" +
                "                              \"type\" : \"map\",\n" +
                "                              \"values\" : \"int\"\n" +
                "                            }\n" +
                "                          } ]\n" +
                "                        } ]\n" +
                "                      } ]\n" +
                "                    }\n" +
                "                  } ]\n" +
                "                }\n" +
                "              }\n" +
                "            }, {\n" +
                "              \"name\" : \"termModMap\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"array\",\n" +
                "                \"items\" : {\n" +
                "                  \"type\" : \"record\",\n" +
                "                  \"name\" : \"TermModRecord\",\n" +
                "                  \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "                  \"fields\" : [ {\n" +
                "                    \"name\" : \"attachment\",\n" +
                "                    \"type\" : {\n" +
                "                      \"type\" : \"enum\",\n" +
                "                      \"name\" : \"ModAttachment\",\n" +
                "                      \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "                      \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "                    }\n" +
                "                  }, {\n" +
                "                    \"name\" : \"modification\",\n" +
                "                    \"type\" : \"Modification\"\n" +
                "                  } ]\n" +
                "                }\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }, {\n" +
                "          \"name\" : \"libSpectrumId\",\n" +
                "          \"type\" : \"java.util.UUID\"\n" +
                "        }, {\n" +
                "          \"name\" : \"proteinAccessionNumbers\",\n" +
                "          \"type\" : \"string\"\n" +
                "        }, {\n" +
                "          \"name\" : \"libMemberCount\",\n" +
                "          \"type\" : \"int\"\n" +
                "        }, {\n" +
                "          \"name\" : \"massShift\",\n" +
                "          \"type\" : \"double\"\n" +
                "        }, {\n" +
                "          \"name\" : \"metaScore\",\n" +
                "          \"type\" : \"double\"\n" +
                "        }, {\n" +
                "          \"name\" : \"wndp\",\n" +
                "          \"type\" : \"double\"\n" +
                "        }, {\n" +
                "          \"name\" : \"ionCurrentCoverage\",\n" +
                "          \"type\" : \"double\"\n" +
                "        }, {\n" +
                "          \"name\" : \"position\",\n" +
                "          \"type\" : \"int\"\n" +
                "        }, {\n" +
                "          \"name\" : \"residuePosScores\",\n" +
                "          \"type\" : {\n" +
                "            \"type\" : \"array\",\n" +
                "            \"items\" : \"double\"\n" +
                "          }\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  } ]\n" +
                "}";
        AvroAssert.assertSchema(expected, new OmsQuerySchemaBuilder());
    }
}
