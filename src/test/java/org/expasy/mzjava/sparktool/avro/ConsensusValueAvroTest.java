package org.expasy.mzjava.sparktool.avro;

import com.google.common.collect.Sets;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.sparktool.data.ConsensusValue;
import org.junit.Test;

import java.util.UUID;

public class ConsensusValueAvroTest {

    @Test
    public void testRoundTrip() throws Exception {

        TObjectDoubleMap<String> scoreMap = new TObjectDoubleHashMap<>();
        scoreMap.put("aScore", 0.87);
        scoreMap.put("fdr", 0.002);

        MsnSpectrum spectrum = new MsnSpectrum();
        spectrum.setId(UUID.fromString("ff70efca-035f-4e1d-bd81-bae8bbf63c32"));
        spectrum.getPrecursor().setValues(784.25, 12, 2);
        spectrum.add(234.567, 12);
        spectrum.add(334.567, 13);
        spectrum.add(434.567, 14);

        ConsensusValue value = new ConsensusValue(Sets.newHashSet("ACC1", "ACC2"), scoreMap, spectrum);

        String expectedJson = "{\n" +
                "  \"proteinAcc\" : [ \"ACC1\", \"ACC2\" ],\n" +
                "  \"scores\" : {\n" +
                "    \"fdr\" : 0.002,\n" +
                "    \"aScore\" : 0.87\n" +
                "  },\n" +
                "  \"spectrum\" : {\n" +
                "    \"precursor\" : {\n" +
                "      \"polarity\" : \"POSITIVE\",\n" +
                "      \"charge\" : [ 2 ],\n" +
                "      \"mz\" : 784.25,\n" +
                "      \"intensity\" : 12.0\n" +
                "    },\n" +
                "    \"msLevel\" : 0,\n" +
                "    \"spectrumIndex\" : -1,\n" +
                "    \"parentScanNumber\" : {\n" +
                "      \"org.expasy.mzjava_avro.core.ms.spectrum.ScanNumberDiscrete\" : {\n" +
                "        \"value\" : -1\n" +
                "      }\n" +
                "    },\n" +
                "    \"fragMethod\" : \"\",\n" +
                "    \"retentionTimes\" : [ ],\n" +
                "    \"scanNumbers\" : [ ],\n" +
                "    \"source\" : \"/dev/null\",\n" +
                "    \"comment\" : \"\",\n" +
                "    \"id\" : {\n" +
                "      \"mostSignificantBits\" : -40268745727324643,\n" +
                "      \"leastSignificantBits\" : -4791343019796841422\n" +
                "    },\n" +
                "    \"precision\" : \"DOUBLE\",\n" +
                "    \"peaks\" : {\n" +
                "      \"org.expasy.mzjava_avro.core.ms.peaklist.DoublePeakList\" : {\n" +
                "        \"peaks\" : [ {\n" +
                "          \"mz\" : 234.567,\n" +
                "          \"i\" : 12.0\n" +
                "        }, {\n" +
                "          \"mz\" : 334.567,\n" +
                "          \"i\" : 13.0\n" +
                "        }, {\n" +
                "          \"mz\" : 434.567,\n" +
                "          \"i\" : 14.0\n" +
                "        } ]\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";

        AvroAssert.assertRoundTrip(expectedJson, value);
    }

    @Test
    public void testCreateSchema() throws Exception {

        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"ConsensusValue\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"proteinAcc\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : \"string\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"scores\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"map\",\n" +
                "      \"values\" : \"double\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"spectrum\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"MsnSpectrum\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.spectrum\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"precursor\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"Peak\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"polarity\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"enum\",\n" +
                "              \"name\" : \"Polarity\",\n" +
                "              \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "              \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "            }\n" +
                "          }, {\n" +
                "            \"name\" : \"charge\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : \"int\"\n" +
                "            }\n" +
                "          }, {\n" +
                "            \"name\" : \"mz\",\n" +
                "            \"type\" : \"double\"\n" +
                "          }, {\n" +
                "            \"name\" : \"intensity\",\n" +
                "            \"type\" : \"double\"\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"msLevel\",\n" +
                "        \"type\" : \"int\"\n" +
                "      }, {\n" +
                "        \"name\" : \"spectrumIndex\",\n" +
                "        \"type\" : \"int\"\n" +
                "      }, {\n" +
                "        \"name\" : \"parentScanNumber\",\n" +
                "        \"type\" : [ {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"ScanNumberDiscrete\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"value\",\n" +
                "            \"type\" : \"int\"\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"ScanNumberInterval\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"minScanNumber\",\n" +
                "            \"type\" : \"int\"\n" +
                "          }, {\n" +
                "            \"name\" : \"maxScanNumber\",\n" +
                "            \"type\" : \"int\"\n" +
                "          } ]\n" +
                "        } ]\n" +
                "      }, {\n" +
                "        \"name\" : \"fragMethod\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"retentionTimes\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : [ {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"RetentionTimeDiscrete\",\n" +
                "            \"doc\" : \"Retention time in seconds\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"time\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          }, {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"RetentionTimeInterval\",\n" +
                "            \"doc\" : \"Retention time in seconds\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"minRetentionTime\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"maxRetentionTime\",\n" +
                "              \"type\" : \"double\"\n" +
                "            } ]\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"scanNumbers\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : [ \"ScanNumberDiscrete\", \"ScanNumberInterval\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"source\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"comment\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"id\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"UUID\",\n" +
                "          \"namespace\" : \"java.util\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"mostSignificantBits\",\n" +
                "            \"type\" : \"long\"\n" +
                "          }, {\n" +
                "            \"name\" : \"leastSignificantBits\",\n" +
                "            \"type\" : \"long\"\n" +
                "          } ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"precision\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Precision\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"peaks\",\n" +
                "        \"type\" : [ {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"DoublePeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"DoublePeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"i\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"FloatPeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"FloatPeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"float\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"i\",\n" +
                "                  \"type\" : \"float\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"DoubleFloatPeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"DoubleFloatPeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"i\",\n" +
                "                  \"type\" : \"float\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"DoubleConstantPeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"intensity\",\n" +
                "            \"type\" : \"double\"\n" +
                "          }, {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"DoubleConstantPeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"double\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        }, {\n" +
                "          \"type\" : \"record\",\n" +
                "          \"name\" : \"FloatConstantPeakList\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "          \"fields\" : [ {\n" +
                "            \"name\" : \"intensity\",\n" +
                "            \"type\" : \"double\"\n" +
                "          }, {\n" +
                "            \"name\" : \"peaks\",\n" +
                "            \"type\" : {\n" +
                "              \"type\" : \"array\",\n" +
                "              \"items\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"FloatConstantPeak\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"mz\",\n" +
                "                  \"type\" : \"float\"\n" +
                "                } ]\n" +
                "              }\n" +
                "            }\n" +
                "          } ]\n" +
                "        } ]\n" +
                "      } ]\n" +
                "    }\n" +
                "  } ]\n" +
                "}";

        AvroAssert.assertSchema(expected, new ConsensusValueSchemaBuilder());
    }
}