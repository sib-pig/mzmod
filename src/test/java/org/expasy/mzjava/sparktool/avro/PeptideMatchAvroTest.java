package org.expasy.mzjava.sparktool.avro;

import com.google.common.collect.Sets;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.PeptideMatch;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class PeptideMatchAvroTest {

    @Test
    public void testRoundTrip() throws Exception {

        TObjectDoubleMap<String> scoreMap = new TObjectDoubleHashMap<>();
        scoreMap.put("aScore", 0.87);
        scoreMap.put("fdr", 0.002);
        PeptideMatch value = new PeptideMatch(Peptide.parse("PEPTS(H3PO4)IDE"), Sets.newHashSet("ACC1", "ACC2").stream(), scoreMap, 5, false);

        String expected = "{\n" +
                "  \"peptide\" : {\n" +
                "    \"seq\" : [ \"P\", \"E\", \"P\", \"T\", \"S\", \"I\", \"D\", \"E\" ],\n" +
                "    \"sideChainModMap\" : [ {\n" +
                "      \"index\" : 4,\n" +
                "      \"modification\" : {\n" +
                "        \"label\" : \"H3PO4\",\n" +
                "        \"mass\" : {\n" +
                "          \"org.expasy.mzjava_avro.core.mol.Composition\" : {\n" +
                "            \"composition\" : {\n" +
                "              \"H\" : 3,\n" +
                "              \"O\" : 4,\n" +
                "              \"P\" : 1\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      }\n" +
                "    } ],\n" +
                "    \"termModMap\" : [ ]\n" +
                "  },\n" +
                "  \"proteinAcc\" : [ \"ACC1\", \"ACC2\" ],\n" +
                "  \"scores\" : {\n" +
                "    \"fdr\" : 0.002,\n" +
                "    \"aScore\" : 0.87\n" +
                "  },\n" +
                "  \"charge\" : 5,\n" +
                "  \"isDecoy\" : false\n" +
                "}";

        AvroAssert.assertRoundTrip(expected, value);
    }

    @Test
    public void testCreateSchema() throws Exception {

        final String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"PeptideMatch\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"peptide\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peptide\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"seq\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"AminoAcid\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "            \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"sideChainModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"SideChainModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"index\",\n" +
                "              \"type\" : \"int\"\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"Modification\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"label\",\n" +
                "                  \"type\" : \"string\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"mass\",\n" +
                "                  \"type\" : [ {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"NumericMass\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"mass\",\n" +
                "                      \"type\" : \"double\"\n" +
                "                    } ]\n" +
                "                  }, {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"Composition\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"composition\",\n" +
                "                      \"type\" : {\n" +
                "                        \"type\" : \"map\",\n" +
                "                        \"values\" : \"int\"\n" +
                "                      }\n" +
                "                    } ]\n" +
                "                  } ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"termModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"TermModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"attachment\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"enum\",\n" +
                "                \"name\" : \"ModAttachment\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "                \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "              }\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : \"Modification\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"proteinAcc\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : \"string\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"scores\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"map\",\n" +
                "      \"values\" : \"double\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"charge\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"isDecoy\",\n" +
                "    \"type\" : \"boolean\"\n" +
                "  } ]\n" +
                "}\n";

        AvroAssert.assertSchema(expected, new PeptideMatchSchemaBuilder());
    }
}
