package org.expasy.mzjava.sparktool.avro;

import org.junit.Test;

public class StringKeyAvroTest {

    @Test
    public void testRoundTrip() throws Exception {

        String value = "Hello!!!";

        String expectedJson = "{\n" +
                "  \"key\" : \"Hello!!!\"\n" +
                "}";

        AvroAssert.assertRoundTrip(expectedJson, value);
    }
}