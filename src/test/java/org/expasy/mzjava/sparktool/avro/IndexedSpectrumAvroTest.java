package org.expasy.mzjava.sparktool.avro;

import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.junit.Test;

import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedSpectrumAvroTest {

    @Test
    public void testDoubleRoundTrip() throws Exception {

        IndexedSpectrum spectrum = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                .build(new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE);

        final String expectedJson = "{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 453.23,\n" +
                "    \"intensity\" : 12872.0\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : 8358210310530090122,\n" +
                "    \"leastSignificantBits\" : -7813031710390560545\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE\",\n" +
                "  \"peakList\" : {\n" +
                "    \"org.expasy.mzjava_avro.sparktool.data.peaklist.DoubleIndexedPeakList\" : {\n" +
                "      \"peakList\" : [ {\n" +
                "        \"mz\" : 432.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 510.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 0\n" +
                "      }, {\n" +
                "        \"mz\" : 783.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 1038.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 2\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}";
        AvroAssert.assertRoundTrip(expectedJson, spectrum);
    }

    @Test
    public void testDoubleFloatRoundTrip() throws Exception {

        IndexedSpectrum spectrum = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                .build(new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE_FLOAT);

        final String expectedJson = "{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 453.23,\n" +
                "    \"intensity\" : 12872.0\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : 8358210310530090122,\n" +
                "    \"leastSignificantBits\" : -7813031710390560545\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE_FLOAT\",\n" +
                "  \"peakList\" : {\n" +
                "    \"org.expasy.mzjava_avro.sparktool.data.peaklist.DoubleFloatIndexedPeakList\" : {\n" +
                "      \"peakList\" : [ {\n" +
                "        \"mz\" : 432.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 510.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 0\n" +
                "      }, {\n" +
                "        \"mz\" : 783.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 1038.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 2\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}";
        AvroAssert.assertRoundTrip(expectedJson, spectrum);
    }

    @Test
    public void testFloatRoundTrip() throws Exception {

        IndexedSpectrum spectrum = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                .build(new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.FLOAT);

        final String expectedJson = "{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 453.23,\n" +
                "    \"intensity\" : 12872.0\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : 8358210310530090122,\n" +
                "    \"leastSignificantBits\" : -7813031710390560545\n" +
                "  },\n" +
                "  \"precision\" : \"FLOAT\",\n" +
                "  \"peakList\" : {\n" +
                "    \"org.expasy.mzjava_avro.sparktool.data.peaklist.FloatIndexedPeakList\" : {\n" +
                "      \"peakList\" : [ {\n" +
                "        \"mz\" : 432.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 510.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 0\n" +
                "      }, {\n" +
                "        \"mz\" : 783.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 1038.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 2\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}";
        AvroAssert.assertRoundTrip(expectedJson, spectrum);
    }
    @Test
    public void testDoubleConstantRoundTrip() throws Exception {

        IndexedSpectrum spectrum = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                .build(new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE_CONSTANT);

        final String expectedJson = "{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 453.23,\n" +
                "    \"intensity\" : 12872.0\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : 8358210310530090122,\n" +
                "    \"leastSignificantBits\" : -7813031710390560545\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE_CONSTANT\",\n" +
                "  \"peakList\" : {\n" +
                "    \"org.expasy.mzjava_avro.sparktool.data.peaklist.DoubleIndexedPeakList\" : {\n" +
                "      \"peakList\" : [ {\n" +
                "        \"mz\" : 432.98,\n" +
                "        \"intensity\" : 1.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 510.98,\n" +
                "        \"intensity\" : 1.0,\n" +
                "        \"charge\" : 0\n" +
                "      }, {\n" +
                "        \"mz\" : 783.98,\n" +
                "        \"intensity\" : 1.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 1038.98,\n" +
                "        \"intensity\" : 1.0,\n" +
                "        \"charge\" : 2\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}";
        AvroAssert.assertRoundTrip(expectedJson, spectrum);
    }

    @Test
    public void testFloatConstantRoundTrip() throws Exception {

        IndexedSpectrum spectrum = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2)
                .build(new Peak(453.23, 12872, 2), UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.FLOAT_CONSTANT);

        final String expectedJson = "{\n" +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 453.23,\n" +
                "    \"intensity\" : 12872.0\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : 8358210310530090122,\n" +
                "    \"leastSignificantBits\" : -7813031710390560545\n" +
                "  },\n" +
                "  \"precision\" : \"FLOAT_CONSTANT\",\n" +
                "  \"peakList\" : {\n" +
                "    \"org.expasy.mzjava_avro.sparktool.data.peaklist.FloatIndexedPeakList\" : {\n" +
                "      \"peakList\" : [ {\n" +
                "        \"mz\" : 432.98,\n" +
                "        \"intensity\" : 1.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 510.98,\n" +
                "        \"intensity\" : 1.0,\n" +
                "        \"charge\" : 0\n" +
                "      }, {\n" +
                "        \"mz\" : 783.98,\n" +
                "        \"intensity\" : 1.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 1038.98,\n" +
                "        \"intensity\" : 1.0,\n" +
                "        \"charge\" : 2\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}";
        AvroAssert.assertRoundTrip(expectedJson, spectrum);
    }

    @Test
    public void testCreateRecordFields() throws Exception {

        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"IndexedSpectrum\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"precursor\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peak\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"polarity\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Polarity\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"mz\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"UUID\",\n" +
                "      \"namespace\" : \"java.util\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"mostSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      }, {\n" +
                "        \"name\" : \"leastSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precision\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Precision\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "      \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peakList\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleIndexedPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data.peaklist\",\n" +
                "      \"doc\" : \"\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peakList\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoublePeak\",\n" +
                "            \"doc\" : \"\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"intensity\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"charge\",\n" +
                "              \"type\" : \"int\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleFloatIndexedPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data.peaklist\",\n" +
                "      \"doc\" : \"\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peakList\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleFloatPeak\",\n" +
                "            \"doc\" : \"\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"intensity\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"charge\",\n" +
                "              \"type\" : \"int\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatIndexedPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data.peaklist\",\n" +
                "      \"doc\" : \"\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peakList\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatPeak\",\n" +
                "            \"doc\" : \"\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"intensity\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"charge\",\n" +
                "              \"type\" : \"int\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}";

        AvroAssert.assertSchema(expected, new IndexedSpectrumSchemaBuilder());
    }
}
