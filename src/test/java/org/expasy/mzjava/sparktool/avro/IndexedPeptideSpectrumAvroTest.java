package org.expasy.mzjava.sparktool.avro;

import com.google.common.collect.Sets;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.sparktool.data.IndexedPeptideSpectrum;
import org.expasy.mzjava.sparktool.data.IndexedSpectrum;
import org.junit.Test;

import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexedPeptideSpectrumAvroTest {

    @Test
    public void testDoubleRoundTrip() throws Exception {

        IndexedSpectrum.Builder builder = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2);

        IndexedPeptideSpectrum spectrum = new IndexedPeptideSpectrum(
                Peptide.parse("PET"), Sets.newHashSet("ACC1", "ACC2"), 4, builder, new Peak(453.23, 12872, 2),
                UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE);

        final String expectedJson = "{\n" +
                "  \"peptide\" : {\n" +
                "    \"seq\" : [ \"P\", \"E\", \"T\" ],\n" +
                "    \"sideChainModMap\" : [ ],\n" +
                "    \"termModMap\" : [ ]\n" +
                "  },\n" +
                "  \"memberCount\" : 4,\n" +
                "  \"proteinAccessionNumbers\" : [ \"ACC1\", \"ACC2\" ]," +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 453.23,\n" +
                "    \"intensity\" : 12872.0\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : 8358210310530090122,\n" +
                "    \"leastSignificantBits\" : -7813031710390560545\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE\",\n" +
                "  \"peakList\" : {\n" +
                "    \"org.expasy.mzjava_avro.sparktool.data.peaklist.DoubleIndexedPeakList\" : {\n" +
                "      \"peakList\" : [ {\n" +
                "        \"mz\" : 432.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 510.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 0\n" +
                "      }, {\n" +
                "        \"mz\" : 783.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 1038.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 2\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}";
        AvroAssert.assertRoundTrip(expectedJson, spectrum);
    }

    @Test
    public void testDoubleFloatRoundTrip() throws Exception {

        IndexedSpectrum.Builder builder = new IndexedSpectrum.Builder()
                .addPeak(432.98, 35, 1)
                .addPeak(510.98, 35, 0)
                .addPeak(783.98, 35, 1)
                .addPeak(1038.98, 35, 2);

        IndexedPeptideSpectrum spectrum = new IndexedPeptideSpectrum(
                Peptide.parse("PET"), Sets.newHashSet("ACC1", "ACC2"), 4, builder, new Peak(453.23, 12872, 2),
                UUID.fromString("73fe53fe-6527-448a-9392-890dfb4ed8df"), PeakList.Precision.DOUBLE_FLOAT);

        final String expectedJson = "{\n" +
                "  \"peptide\" : {\n" +
                "    \"seq\" : [ \"P\", \"E\", \"T\" ],\n" +
                "    \"sideChainModMap\" : [ ],\n" +
                "    \"termModMap\" : [ ]\n" +
                "  },\n" +
                "  \"memberCount\" : 4,\n" +
                "  \"proteinAccessionNumbers\" : [ \"ACC1\", \"ACC2\" ]," +
                "  \"precursor\" : {\n" +
                "    \"polarity\" : \"POSITIVE\",\n" +
                "    \"charge\" : [ 2 ],\n" +
                "    \"mz\" : 453.23,\n" +
                "    \"intensity\" : 12872.0\n" +
                "  },\n" +
                "  \"id\" : {\n" +
                "    \"mostSignificantBits\" : 8358210310530090122,\n" +
                "    \"leastSignificantBits\" : -7813031710390560545\n" +
                "  },\n" +
                "  \"precision\" : \"DOUBLE_FLOAT\",\n" +
                "  \"peakList\" : {\n" +
                "    \"org.expasy.mzjava_avro.sparktool.data.peaklist.DoubleFloatIndexedPeakList\" : {\n" +
                "      \"peakList\" : [ {\n" +
                "        \"mz\" : 432.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 510.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 0\n" +
                "      }, {\n" +
                "        \"mz\" : 783.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 1\n" +
                "      }, {\n" +
                "        \"mz\" : 1038.98,\n" +
                "        \"intensity\" : 35.0,\n" +
                "        \"charge\" : 2\n" +
                "      } ]\n" +
                "    }\n" +
                "  }\n" +
                "}";
        AvroAssert.assertRoundTrip(expectedJson, spectrum);
    }

    @Test
    public void testCreateRecordFields() throws Exception {

        String expected = "{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"IndexedPeptideSpectrum\",\n" +
                "  \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"peptide\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peptide\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"seq\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"enum\",\n" +
                "            \"name\" : \"AminoAcid\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava.proteomics.mol\",\n" +
                "            \"symbols\" : [ \"A\", \"R\", \"N\", \"D\", \"C\", \"E\", \"Q\", \"G\", \"H\", \"K\", \"M\", \"F\", \"P\", \"S\", \"T\", \"W\", \"Y\", \"V\", \"J\", \"I\", \"L\", \"O\", \"U\", \"X\", \"B\", \"Z\" ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"sideChainModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"SideChainModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"index\",\n" +
                "              \"type\" : \"int\"\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"record\",\n" +
                "                \"name\" : \"Modification\",\n" +
                "                \"fields\" : [ {\n" +
                "                  \"name\" : \"label\",\n" +
                "                  \"type\" : \"string\"\n" +
                "                }, {\n" +
                "                  \"name\" : \"mass\",\n" +
                "                  \"type\" : [ {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"NumericMass\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"mass\",\n" +
                "                      \"type\" : \"double\"\n" +
                "                    } ]\n" +
                "                  }, {\n" +
                "                    \"type\" : \"record\",\n" +
                "                    \"name\" : \"Composition\",\n" +
                "                    \"namespace\" : \"org.expasy.mzjava_avro.core.mol\",\n" +
                "                    \"fields\" : [ {\n" +
                "                      \"name\" : \"composition\",\n" +
                "                      \"type\" : {\n" +
                "                        \"type\" : \"map\",\n" +
                "                        \"values\" : \"int\"\n" +
                "                      }\n" +
                "                    } ]\n" +
                "                  } ]\n" +
                "                } ]\n" +
                "              }\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"termModMap\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"TermModRecord\",\n" +
                "            \"namespace\" : \"org.expasy.mzjava_avro.proteomics.mol.modification\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"attachment\",\n" +
                "              \"type\" : {\n" +
                "                \"type\" : \"enum\",\n" +
                "                \"name\" : \"ModAttachment\",\n" +
                "                \"namespace\" : \"org.expasy.mzjava.proteomics.mol.modification\",\n" +
                "                \"symbols\" : [ \"SIDE_CHAIN\", \"N_TERM\", \"C_TERM\" ]\n" +
                "              }\n" +
                "            }, {\n" +
                "              \"name\" : \"modification\",\n" +
                "              \"type\" : \"Modification\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"memberCount\",\n" +
                "    \"type\" : \"int\"\n" +
                "  }, {\n" +
                "    \"name\" : \"proteinAccessionNumbers\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"array\",\n" +
                "      \"items\" : \"string\"\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precursor\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"Peak\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.core.ms.peaklist\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"polarity\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"enum\",\n" +
                "          \"name\" : \"Polarity\",\n" +
                "          \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "          \"symbols\" : [ \"POSITIVE\", \"NEGATIVE\", \"UNKNOWN\" ]\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"charge\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : \"int\"\n" +
                "        }\n" +
                "      }, {\n" +
                "        \"name\" : \"mz\",\n" +
                "        \"type\" : \"double\"\n" +
                "      }, {\n" +
                "        \"name\" : \"intensity\",\n" +
                "        \"type\" : \"double\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"id\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"UUID\",\n" +
                "      \"namespace\" : \"java.util\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"mostSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      }, {\n" +
                "        \"name\" : \"leastSignificantBits\",\n" +
                "        \"type\" : \"long\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"precision\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"enum\",\n" +
                "      \"name\" : \"Precision\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava.core.ms.peaklist\",\n" +
                "      \"symbols\" : [ \"DOUBLE\", \"FLOAT\", \"DOUBLE_FLOAT\", \"DOUBLE_CONSTANT\", \"FLOAT_CONSTANT\" ]\n" +
                "    }\n" +
                "  }, {\n" +
                "    \"name\" : \"peakList\",\n" +
                "    \"type\" : [ {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleIndexedPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data.peaklist\",\n" +
                "      \"doc\" : \"\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peakList\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoublePeak\",\n" +
                "            \"doc\" : \"\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"intensity\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"charge\",\n" +
                "              \"type\" : \"int\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"DoubleFloatIndexedPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data.peaklist\",\n" +
                "      \"doc\" : \"\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peakList\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"DoubleFloatPeak\",\n" +
                "            \"doc\" : \"\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"double\"\n" +
                "            }, {\n" +
                "              \"name\" : \"intensity\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"charge\",\n" +
                "              \"type\" : \"int\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    }, {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"FloatIndexedPeakList\",\n" +
                "      \"namespace\" : \"org.expasy.mzjava_avro.sparktool.data.peaklist\",\n" +
                "      \"doc\" : \"\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"peakList\",\n" +
                "        \"type\" : {\n" +
                "          \"type\" : \"array\",\n" +
                "          \"items\" : {\n" +
                "            \"type\" : \"record\",\n" +
                "            \"name\" : \"FloatPeak\",\n" +
                "            \"doc\" : \"\",\n" +
                "            \"fields\" : [ {\n" +
                "              \"name\" : \"mz\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"intensity\",\n" +
                "              \"type\" : \"float\"\n" +
                "            }, {\n" +
                "              \"name\" : \"charge\",\n" +
                "              \"type\" : \"int\"\n" +
                "            } ]\n" +
                "          }\n" +
                "        }\n" +
                "      } ]\n" +
                "    } ]\n" +
                "  } ]\n" +
                "}";

        AvroAssert.assertSchema(expected, new IndexedPeptideSpectrumSchemaBuilder());
    }
}