package org.expasy.mzjava.sparktool.data;

import org.expasy.mzjava.core.ms.peaklist.DoublePeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.junit.Assert;
import org.junit.Test;

public class IndexedSpectrumTest {

    @Test
    public void testConstructor() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.addSorted( //              0                   1                  2                 3                   4                   5                  6                  7                   8                 9                  10                  11                  12                   13                  14                  15                  16                  17                  18                 19
                new double[]{532.2181757006471, 592.0813696890917, 610.4389304593608, 819.528204667349, 1008.9731541854521, 1114.2026733872658, 1228.8277418928208, 1250.291877455562, 1458.4947369611507, 1617.97592028064, 1673.0009209859188, 1711.5858053298323, 1900.6064002325252, 1978.9769588099969, 2004.1818947649767, 2121.1340129945756, 2242.9549664843066, 2268.9075118303103, 2297.4721551530297, 2348.226927064944},
                new double[]{              5.0,               8.0,              11.0,              7.0,                6.0,               21.0,               11.0,               2.0,               15.0,             14.0,               23.0,                4.0,                7.0,               18.0,                1.0,               26.0,                3.0,               24.0,                6.0,              12.0});

        IndexedSpectrum indexedSpectrum = new IndexedSpectrum(peakList);

        Assert.assertEquals(20, indexedSpectrum.size());
        Assert.assertEquals(peakList.calcVectorLength(), indexedSpectrum.getVectorLength(), 0.000001);
        Assert.assertEquals(peakList.getId(), indexedSpectrum.getId());
        Assert.assertEquals(peakList.getPrecursor(), indexedSpectrum.getPrecursor());
        Assert.assertEquals(peakList.getPrecision(), indexedSpectrum.getPrecision());

        Assert.assertEquals(15, indexedSpectrum.toIntensityIndex(0));
        Assert.assertEquals(10, indexedSpectrum.toIntensityIndex(1));
        Assert.assertEquals(8, indexedSpectrum.toIntensityIndex(2));
        Assert.assertEquals(11, indexedSpectrum.toIntensityIndex(3));
        Assert.assertEquals(13, indexedSpectrum.toIntensityIndex(4));
        Assert.assertEquals(3, indexedSpectrum.toIntensityIndex(5));
        Assert.assertEquals(9, indexedSpectrum.toIntensityIndex(6));
        Assert.assertEquals(18, indexedSpectrum.toIntensityIndex(7));
        Assert.assertEquals(5, indexedSpectrum.toIntensityIndex(8));
        Assert.assertEquals(6, indexedSpectrum.toIntensityIndex(9));
        Assert.assertEquals(2, indexedSpectrum.toIntensityIndex(10));
        Assert.assertEquals(16, indexedSpectrum.toIntensityIndex(11));
        Assert.assertEquals(12, indexedSpectrum.toIntensityIndex(12));
        Assert.assertEquals(4, indexedSpectrum.toIntensityIndex(13));
        Assert.assertEquals(19, indexedSpectrum.toIntensityIndex(14));
        Assert.assertEquals(0, indexedSpectrum.toIntensityIndex(15));
        Assert.assertEquals(17, indexedSpectrum.toIntensityIndex(16));
        Assert.assertEquals(1, indexedSpectrum.toIntensityIndex(17));
        Assert.assertEquals(14, indexedSpectrum.toIntensityIndex(18));
        Assert.assertEquals(7, indexedSpectrum.toIntensityIndex(19));

        final double delta = 0.000001;
        Assert.assertEquals(26, indexedSpectrum.getNthIntensity(0), delta);
        Assert.assertEquals(24, indexedSpectrum.getNthIntensity(1), delta);
        Assert.assertEquals(23, indexedSpectrum.getNthIntensity(2), delta);
        Assert.assertEquals(21, indexedSpectrum.getNthIntensity(3), delta);
        Assert.assertEquals(18, indexedSpectrum.getNthIntensity(4), delta);
        Assert.assertEquals(15, indexedSpectrum.getNthIntensity(5), delta);
        Assert.assertEquals(14, indexedSpectrum.getNthIntensity(6), delta);
        Assert.assertEquals(12, indexedSpectrum.getNthIntensity(7), delta);
        Assert.assertEquals(11, indexedSpectrum.getNthIntensity(8), delta);
        Assert.assertEquals(11, indexedSpectrum.getNthIntensity(9), delta);
        Assert.assertEquals(8, indexedSpectrum.getNthIntensity(10), delta);
        Assert.assertEquals(7, indexedSpectrum.getNthIntensity(11), delta);
        Assert.assertEquals(7, indexedSpectrum.getNthIntensity(12), delta);
        Assert.assertEquals(6, indexedSpectrum.getNthIntensity(13), delta);
        Assert.assertEquals(6, indexedSpectrum.getNthIntensity(14), delta);
        Assert.assertEquals(5, indexedSpectrum.getNthIntensity(15), delta);
        Assert.assertEquals(4, indexedSpectrum.getNthIntensity(16), delta);
        Assert.assertEquals(3, indexedSpectrum.getNthIntensity(17), delta);
        Assert.assertEquals(2, indexedSpectrum.getNthIntensity(18), delta);
        Assert.assertEquals(1, indexedSpectrum.getNthIntensity(19), delta);


        Assert.assertEquals(2121.134013, indexedSpectrum.getNthMz(0), delta);
        Assert.assertEquals(2268.907512, indexedSpectrum.getNthMz(1), delta);
        Assert.assertEquals(1673.000921, indexedSpectrum.getNthMz(2), delta);
        Assert.assertEquals(1114.202673, indexedSpectrum.getNthMz(3), delta);
        Assert.assertEquals(1978.976959, indexedSpectrum.getNthMz(4), delta);
        Assert.assertEquals(1458.494737, indexedSpectrum.getNthMz(5), delta);
        Assert.assertEquals(1617.97592, indexedSpectrum.getNthMz(6), delta);
        Assert.assertEquals(2348.226927, indexedSpectrum.getNthMz(7), delta);
        Assert.assertEquals(610.4389305, indexedSpectrum.getNthMz(8), delta);
        Assert.assertEquals(1228.827742, indexedSpectrum.getNthMz(9), delta);
        Assert.assertEquals(592.0813697, indexedSpectrum.getNthMz(10), delta);
        Assert.assertEquals(819.5282047, indexedSpectrum.getNthMz(11), delta);
        Assert.assertEquals(1900.6064, indexedSpectrum.getNthMz(12), delta);
        Assert.assertEquals(1008.973154, indexedSpectrum.getNthMz(13), delta);
        Assert.assertEquals(2297.472155, indexedSpectrum.getNthMz(14), delta);
        Assert.assertEquals(532.2181757, indexedSpectrum.getNthMz(15), delta);
        Assert.assertEquals(1711.585805, indexedSpectrum.getNthMz(16), delta);
        Assert.assertEquals(2242.954966, indexedSpectrum.getNthMz(17), delta);
        Assert.assertEquals(1250.291877, indexedSpectrum.getNthMz(18), delta);
        Assert.assertEquals(2004.181895, indexedSpectrum.getNthMz(19), delta);
    }

    @Test
    public void testFastIndex1() throws Exception {

        PeakList<PeakAnnotation> peakList = new DoublePeakList<>();
        peakList.addSorted( //              0                   1                  2                 3                   4                   5                  6                  7                   8                 9                  10                  11                  12                   13                  14                  15                  16                  17                  18                 19
                new double[]{532.2181757006471, 592.0813696890917, 610.4389304593608, 819.528204667349, 1008.9731541854521, 1114.2026733872658, 1228.8277418928208, 1250.291877455562, 1458.4947369611507, 1617.97592028064, 1673.0009209859188, 1711.5858053298323, 1900.6064002325252, 1978.9769588099969, 2004.1818947649767, 2121.1340129945756, 2242.9549664843066, 2268.9075118303103, 2297.4721551530297, 2348.226927064944},
                new double[]{              5.0,               8.0,              11.0,              7.0,                6.0,               21.0,               11.0,               2.0,               15.0,             14.0,               23.0,                4.0,                7.0,               18.0,                1.0,               26.0,                3.0,               24.0,                6.0,              12.0});

        IndexedSpectrum indexedSpectrum = new IndexedSpectrum(peakList);

        for(int i = 0; i < indexedSpectrum.size(); i++) {

            double mz = indexedSpectrum.getMz(i);
            Assert.assertEquals(indexedSpectrum.indexOf(mz), indexedSpectrum.fastIndexOf(mz));
        }

        Assert.assertEquals(0, indexedSpectrum.fastIndexOf(533.8));
    }
}