package org.expasy.mzjava.sparktool.data;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class MockCached<T> extends Cached<T> {

    private final T cached;

    public MockCached(T cached) {

        this.cached = cached;
    }

    @Override
    protected T build() {

        return cached;
    }
}
