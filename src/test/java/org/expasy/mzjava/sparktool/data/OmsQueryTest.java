package org.expasy.mzjava.sparktool.data;

import com.google.common.collect.Lists;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;

public class OmsQueryTest {

    @Test
    public void testGetMetaPlusDeltaScore() throws Exception {

        //noinspection unchecked
        MsnSpectrum spectrum = mock(MsnSpectrum.class);

        List<OmsResult> results = Lists.newArrayList(

                new OmsResult(HitType.OPEN, Peptide.parse("PEK"), UUID.randomUUID(), "", 1, 15.9, 1.8, 0.9, 0.2, 1, new double[]{0, 1, 0}),
                new OmsResult(HitType.OPEN, Peptide.parse("PSK"), UUID.randomUUID(), "", 1, 45.8, 1, 0.9, 0.2, 1, new double[]{0, 1, 0}),
                new OmsResult(HitType.OPEN, Peptide.parse("PTK"), UUID.randomUUID(), "", 1, 12.8, 0.5, 0.9, 0.2, 1, new double[]{0, 1, 0})
        );
        OmsQuery query = new OmsQuery(spectrum, results);

        Assert.assertEquals(1.8 + (1.8 - 1), query.getMetaPlusDeltaScore(0, 1.5), 0.000001);
        Assert.assertEquals(1.0 + (1 - 0.5), query.getMetaPlusDeltaScore(1, 1.5), 0.000001);
        Assert.assertEquals(0.5 + (1.5), query.getMetaPlusDeltaScore(2, 1.5), 0.000001);
    }
}