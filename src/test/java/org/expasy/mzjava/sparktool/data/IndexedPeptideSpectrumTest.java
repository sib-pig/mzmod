package org.expasy.mzjava.sparktool.data;

import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.fragment.PeptideFragmenter;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.EnumSet;
import java.util.UUID;

public class IndexedPeptideSpectrumTest {

    @Test
    public void testEqualsAndHash() throws Exception {

        UUID id1 = UUID.randomUUID();

        IndexedPeptideSpectrum spec1p1Id1 = newSpectrum("PEPTIDE", 12, id1);
        IndexedPeptideSpectrum spec2p1Id1 = newSpectrum("PEPTIDE", 12, id1);

        IndexedPeptideSpectrum spec1p1id2 = newSpectrum("PEPTIDE", 12, UUID.randomUUID());
        IndexedPeptideSpectrum spec1p2id1 = newSpectrum("SSSSSSS", 12, UUID.randomUUID());

        //noinspection EqualsWithItself
        Assert.assertEquals(true, spec1p1Id1.equals(spec1p1Id1));
        Assert.assertEquals(true, spec1p1Id1.equals(spec2p1Id1));
        Assert.assertEquals(true, spec2p1Id1.equals(spec1p1Id1));

        Assert.assertEquals(false, spec1p1Id1.equals(spec1p1id2));
        Assert.assertEquals(false, spec1p1id2.equals(spec1p2id1));

        Assert.assertEquals(spec1p1Id1.hashCode(), spec2p1Id1.hashCode());
    }

    private IndexedPeptideSpectrum newSpectrum(String seq, int memberCount, UUID id) {


        Peptide peptide = Peptide.parse(seq);

        PeptideFragmenter fragmenter = new PeptideFragmenter(EnumSet.of(IonType.b, IonType.y), PeakList.Precision.FLOAT);
        PeptideSpectrum src = fragmenter.fragment(peptide, 2);
        src.setId(id);

        return new IndexedPeptideSpectrum(peptide, Collections.<String>emptySet(), memberCount, src, new PeakProcessorChain<>(new IdentityPeakProcessor<>()));
    }
}