package org.expasy.mzjava.sparktool.data;

import org.expasy.mzjava.proteomics.mol.Peptide;
import org.junit.Assert;
import org.junit.Test;

public class ConsensusKeyTest {

    @Test
    public void testEqualsAndHashDifferentCharge() throws Exception {

        ConsensusKey key1 = new ConsensusKey(2, Peptide.parse("PEPTSIDE"));
        ConsensusKey key2 = new ConsensusKey(2, Peptide.parse("PEPTSIDE"));
        ConsensusKey key3 = new ConsensusKey(1, Peptide.parse("PEPTSIDE"));

        Assert.assertEquals(true, key1.equals(key2));
        Assert.assertEquals(true, key2.equals(key1));
        Assert.assertEquals(key1.hashCode(), key2.hashCode());
        Assert.assertEquals(false, key1.equals(key3));
    }

    @Test
    public void testEqualsAndHashDifferentPeptide() throws Exception {

        ConsensusKey key1 = new ConsensusKey(2, Peptide.parse("PEPTSIDE"));
        ConsensusKey key2 = new ConsensusKey(2, Peptide.parse("PEPTSIDE"));
        ConsensusKey key3 = new ConsensusKey(2, Peptide.parse("CERVILAS"));

        Assert.assertEquals(true, key1.equals(key2));
        Assert.assertEquals(true, key2.equals(key1));
        Assert.assertEquals(key1.hashCode(), key2.hashCode());
        Assert.assertEquals(false, key1.equals(key3));
    }
}