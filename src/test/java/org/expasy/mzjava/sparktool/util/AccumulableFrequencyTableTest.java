package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.stats.FrequencyTable;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccumulableFrequencyTableTest {

    @Test
    public void testSerialize() throws Exception {

        AccumulableFrequencyTable table = new AccumulableFrequencyTable(1);
        table.add(1);
        table.add(2);
        table.add(2);
        table.add(3);
        table.add(3);
        table.add(3);

        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(10000);
        ObjectOutputStream out = new ObjectOutputStream(byteArrayOutputStream);
        out.writeObject(table);
        out.flush();
        out.close();

        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        FrequencyTable readTable = ((AccumulableFrequencyTable) in.readObject()).getTable();
        in.close();

        Assert.assertEquals(1, readTable.getFrequency(readTable.getBinId(1)));
        Assert.assertEquals(2, readTable.getFrequency(readTable.getBinId(2)));
        Assert.assertEquals(3, readTable.getFrequency(readTable.getBinId(3)));

        Assert.assertEquals(6, readTable.getTotal());
        Assert.assertEquals(1.0, readTable.getBinSize(), 0.00001);
    }

    @Test
    public void testRead() throws Exception {

        AccumulableFrequencyTable frequencyTable = new AccumulableFrequencyTable();

        ObjectInputStream in = mock(ObjectInputStream.class);
        when(in.readDouble()).thenReturn(1.0);
        when(in.readInt()).thenReturn(
                20, //bin count
                -24, 1,
                -8, 1,
                -7, 1,
                -6, 8,
                -5, 11,
                -4, 8,
                -3, 42,
                -2, 85,
                -1, 161,
                0, 1992,
                1, 93,
                2, 38,
                3, 22,
                4, 20,
                5, 6,
                6, 3,
                7, 2,
                8, 1,
                9, 1,
                10, 1
        );

        frequencyTable.readExternal(in);

        Assert.assertEquals("-24.0\t1\n" +
                "-8.0\t1\n" +
                "-7.0\t1\n" +
                "-6.0\t8\n" +
                "-5.0\t11\n" +
                "-4.0\t8\n" +
                "-3.0\t42\n" +
                "-2.0\t85\n" +
                "-1.0\t161\n" +
                "0.0\t1992\n" +
                "1.0\t93\n" +
                "2.0\t38\n" +
                "3.0\t22\n" +
                "4.0\t20\n" +
                "5.0\t6\n" +
                "6.0\t3\n" +
                "7.0\t2\n" +
                "8.0\t1\n" +
                "9.0\t1\n" +
                "10.0\t1\n", frequencyTable.getTable().toString());
    }

    @Test
    public void test() throws Exception {

        AccumulableFrequencyTable table1 = new AccumulableFrequencyTable(1);
        table1.add(1);
        table1.add(2);
        table1.add(2);
        table1.add(3);
        table1.add(3);
        table1.add(3);
        table1.add(-3);

        AccumulableFrequencyTable table2 = new AccumulableFrequencyTable(1);
        table2.add(1);
        table2.add(2);
        table2.add(3);
        table2.add(3);
        table2.add(12);
        table2.add(13);
        table2.add(13);
        table2.add(-3);

        table1.addInPlace(table2);

        FrequencyTable table = table1.getTable();

        Assert.assertEquals(2, table.getFrequency(table.getBinId(-3)));
        Assert.assertEquals(2, table.getFrequency(table.getBinId(1)));
        Assert.assertEquals(3, table.getFrequency(table.getBinId(2)));
        Assert.assertEquals(5, table.getFrequency(table.getBinId(3)));
        Assert.assertEquals(1, table.getFrequency(table.getBinId(12)));
        Assert.assertEquals(2, table.getFrequency(table.getBinId(13)));

        Assert.assertEquals(15, table.getTotal());
    }
}