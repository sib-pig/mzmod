package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class StartTitleMgfKeyFunctionTest {

    @Test
    public void testApplyIdentifier() throws Exception {

        StartTitleMgfKeyFunction keyFunction = new StartTitleMgfKeyFunction();

        SpectrumIdentifier identifier = new SpectrumIdentifier("20161103_06_Y1.3.3.2 File:20161103_06_Y1.raw, NativeID:controllerType=0 controllerNumber=1 scan=3 RTINSECONDS=1.364584672");

        String key = keyFunction.apply(identifier, Peptide.parse("LNDLEEALQQAK"));

        Assert.assertEquals("20161103_06_Y1.3.3.2", key);
    }

    @Test
    public void testApplySpectrum() throws Exception {

        MsnSpectrum spectrum = Mockito.mock(MsnSpectrum.class);
        when(spectrum.getComment()).thenReturn("20161103_06_Y1.3.3.2 File:20161103_06_Y1.raw, NativeID:controllerType=0 controllerNumber=1 scan=3");

        StartTitleMgfKeyFunction keyFunction = new StartTitleMgfKeyFunction();
        Assert.assertEquals("20161103_06_Y1.3.3.2", keyFunction.apply(spectrum));
    }
}