package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class XTandemMgfKeyFunctionTest {

    @Test
    public void testApplyIdentifier() throws Exception {

        XTandemMgfKeyFunction keyFunction = new XTandemMgfKeyFunction();

        SpectrumIdentifier identifier = new SpectrumIdentifier("Adult_Spinalcord_Gel_Elite_67_f10.mgf.00031.00031.3");
        identifier.addScanNumber(31);
        identifier.setAssumedCharge(3);

        String key = keyFunction.apply(identifier, Peptide.parse("LNDLEEALQQAK"));

        Assert.assertEquals("Adult_Spinalcord_Gel_Elite_67_f10.31", key);
    }

    @Test
    public void testApplySpectrum() throws Exception {

        MsnSpectrum spectrum = Mockito.mock(MsnSpectrum.class);
        when(spectrum.getComment()).thenReturn("Adult_Spinalcord_Gel_Elite_67_f10.843.843.3");
        when(spectrum.getSpectrumIndex()).thenReturn(30);

        XTandemMgfKeyFunction keyFunction = new XTandemMgfKeyFunction();
        Assert.assertEquals("Adult_Spinalcord_Gel_Elite_67_f10.31", keyFunction.apply(spectrum));
    }
}