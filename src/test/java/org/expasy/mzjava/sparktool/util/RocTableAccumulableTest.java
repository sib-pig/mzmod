package org.expasy.mzjava.sparktool.util;

import cern.jet.random.Normal;
import org.expasy.mzjava.stats.Classification;
import org.expasy.mzjava.stats.SimROCTable;
import org.junit.Assert;
import org.junit.Test;

public class RocTableAccumulableTest {

    @Test
    public void test() throws Exception {

        final double mean1 = 12;
        final double stdev1 = 10;

        final double mean2 = 14;
        final double stdev2 =10;

        final double thresholdIncrements = 0.01;
        final int min = 0;
        final int max = 1;
        SimROCTable simROCTable = new SimROCTable(min, max, thresholdIncrements);
        RocTableAccumulable rocTableAc1 = new RocTableAccumulable(min, max, thresholdIncrements);
        RocTableAccumulable rocTableAc2 = new RocTableAccumulable(min, max, thresholdIncrements);

        addScores(mean1, stdev1, Classification.DIFFERENT, simROCTable, rocTableAc1);
        addScores(mean1, stdev1, Classification.DIFFERENT, simROCTable, rocTableAc2);
        addScores(mean2, stdev2, Classification.SAME, simROCTable, rocTableAc1);
        addScores(mean2, stdev2, Classification.SAME, simROCTable, rocTableAc2);

        rocTableAc1.add(rocTableAc2);
        Assert.assertEquals(simROCTable.toString(), rocTableAc1.toString());
    }

    private void addScores(double mean, double stdev, Classification classification, SimROCTable simROCTable, RocTableAccumulable rocTableAc) {

        for(int i = 0; i < 500; i++){

            final double score = Normal.staticNextDouble(mean, stdev);
            simROCTable.add(score, classification);
            rocTableAc.add(new RocValue(score, classification));
        }
    }
}