package org.expasy.mzjava.sparktool.util;

import gnu.trove.list.array.TDoubleArrayList;
import org.expasy.mzjava.core.ms.peaklist.FloatPeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.io.ObjectInput;
import java.io.ObjectOutput;

import static org.mockito.Mockito.*;

public class ExternalizablesTest {

    @Test
    public void testWritePeakList() throws Exception {

        FloatPeakList<PeakAnnotation> peakList = new FloatPeakList<>(8);
        peakList.add(235.98f, 457.5f);
        peakList.add(243.97f, 12.87f);
        peakList.add(371.86f, 1298.87f);
        peakList.add(371.92f, 1.87f);
        peakList.add(464.89f, 0.012f);
        peakList.add(593.06f, 4576.98f);
        peakList.add(673.87f, 643.87f);
        peakList.add(975.98f, 87.2f);

        ObjectOutput out = mock(ObjectOutput.class);
        Externalizables.writePeakList(peakList, out);

        verify(out).writeInt(8);
        verify(out).writeFloat(235.98f); verify(out).writeFloat(457.5f);
        verify(out).writeFloat(243.97f); verify(out).writeFloat(12.87f);
        verify(out).writeFloat(371.86f); verify(out).writeFloat(1298.87f);
        verify(out).writeFloat(371.92f); verify(out).writeFloat(1.87f);
        verify(out).writeFloat(464.89f); verify(out).writeFloat(0.012f);
        verify(out).writeFloat(593.06f); verify(out).writeFloat(4576.98f);
        verify(out).writeFloat(673.87f); verify(out).writeFloat(643.87f);
        verify(out).writeFloat(975.98f); verify(out).writeFloat(87.2f);
        verifyNoMoreInteractions(out);
    }

    @Test
    public void testWriteDoubleList() throws Exception {

        ObjectOutput out = mock(ObjectOutput.class);
        Externalizables.writeDoubleList(new TDoubleArrayList(new double[]{12, 56, 7, 23, 98.7}), out);

        verify(out).writeInt(5);
        verify(out).writeDouble(12);
        verify(out).writeDouble(56);
        verify(out).writeDouble(7);
        verify(out).writeDouble(23);
        verify(out).writeDouble(98.7);
        verifyNoMoreInteractions(out);
    }

    @Test
    public void testWriteArray() throws Exception {

        ObjectOutput out = mock(ObjectOutput.class);
        Externalizables.writeArray(new double[]{12, 56, 7, 23, 98.7}, out);

        verify(out).writeInt(5);
        verify(out).writeDouble(12);
        verify(out).writeDouble(56);
        verify(out).writeDouble(7);
        verify(out).writeDouble(23);
        verify(out).writeDouble(98.7);
        verifyNoMoreInteractions(out);
    }

    @Test
    public void testReadFloatPeakList() throws Exception {

        ObjectInput in = mock(ObjectInput.class);
        when(in.readInt()).thenReturn(8);
        when(in.readFloat()).thenReturn(235.98f, 457.5f,
                                        243.97f, 12.87f,
                                        371.86f, 1298.87f,
                                        371.92f, 1.87f,
                                        464.89f, 0.012f,
                                        593.06f, 4576.98f,
                                        673.87f, 643.87f,
                                        975.98f, 87.2f);

        FloatPeakList<PeakAnnotation> peakList = Externalizables.readFloatPeakList(in);

        Assert.assertEquals(8, peakList.size());

        double delta = 0.0;
        Assert.assertEquals(235.98f, peakList.getMz(0), delta); Assert.assertEquals(457.5f, peakList.getIntensity(0), delta);
        Assert.assertEquals(243.97f, peakList.getMz(1), delta); Assert.assertEquals(12.87f, peakList.getIntensity(1), delta);
        Assert.assertEquals(371.86f, peakList.getMz(2), delta); Assert.assertEquals(1298.87f, peakList.getIntensity(2), delta);
        Assert.assertEquals(371.92f, peakList.getMz(3), delta); Assert.assertEquals(1.87f, peakList.getIntensity(3), delta);
        Assert.assertEquals(464.89f, peakList.getMz(4), delta); Assert.assertEquals(0.012f, peakList.getIntensity(4), delta);
        Assert.assertEquals(593.06f, peakList.getMz(5), delta); Assert.assertEquals(4576.98f, peakList.getIntensity(5), delta);
        Assert.assertEquals(673.87f, peakList.getMz(6), delta); Assert.assertEquals(643.87f, peakList.getIntensity(6), delta);
        Assert.assertEquals(975.98f, peakList.getMz(7), delta); Assert.assertEquals(87.2f, peakList.getIntensity(7), delta);
    }

    @Test
    public void testReadArray() throws Exception {

        ObjectInput in = mock(ObjectInput.class);
        when(in.readInt()).thenReturn(5);
        when(in.readDouble()).thenReturn(12.0, 56.0, 7.0, 23.0, 98.7);

        double[] array = Externalizables.readArray(in);
        Assert.assertArrayEquals(new double[]{12, 56, 7, 23, 98.7}, array, 0.0);
    }
}