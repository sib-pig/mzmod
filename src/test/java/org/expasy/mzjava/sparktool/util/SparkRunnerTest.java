package org.expasy.mzjava.sparktool.util;

import org.apache.commons.cli.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class SparkRunnerTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    class TestSparkRunner extends SparkRunner {

        @Override
        public int run() throws IOException {
            return 0;
        }

        @Override
        public SparkRunner parseOptions(String[] args) throws ParseException {
            CommandLineParser parser = new BasicParser();
            CommandLine line = parser.parse( cmdLineOpts, args );

            check(line);

            return this;
        }
    }

    @Test
    public void testCommandLineArgs1() throws IOException {

        String[] args = new String[]{ "-l","-n","4","-em","1000","-bm","128" };

        TestSparkRunner testSparkRunner = new TestSparkRunner();
        try {
            testSparkRunner.parseOptions(args);

            Assert.assertTrue(testSparkRunner.isRunLocal());
            Assert.assertEquals(4, testSparkRunner.getNrExec());
            Assert.assertEquals(1000,testSparkRunner.getExecMemoryMB());
            Assert.assertEquals(128, testSparkRunner.getBufferMemoryMB());

        }
        catch( Exception exp ) {
            Assert.fail();
        }
    }

    @Test
    public void testCommandLineArgs2()  throws IOException {

        TestSparkRunner testSparkRunner = new TestSparkRunner();

        String[] args = new String[]{ "-l","-o","C:\\MzMod","-n","4" };

        try {
            testSparkRunner.parseOptions(args);
        }
        catch( ParseException e ) {
            Assert.assertTrue(e.getMessage().contains("-o"));
        }

    }

    @Test
    public void testCommandLineArgs3()  throws IOException {

        TestSparkRunner testSparkRunner = new TestSparkRunner();

        String[] args = new String[]{ "-l","-n","abc" };

        try {
            testSparkRunner.parseOptions(args);
        }
        catch( NumberFormatException e ) {
            Assert.assertTrue(e.getMessage().contains("abc"));
        }
        catch( ParseException e ) {
            Assert.fail();
        }

    }

    @Test
    public void testCommandLineArgs4()  throws IOException {

        TestSparkRunner testSparkRunner = new TestSparkRunner();

        String[] args = new String[]{ "-l","-n","0" };

        try {
            testSparkRunner.parseOptions(args);
        }
        catch( NumberFormatException e ) {
            Assert.assertTrue(e.getMessage().contains("-n"));
        }
        catch( ParseException e ) {
            Assert.fail();
        }

        args = new String[]{ "-l","-n","1","-em","0" };

        try {
            testSparkRunner.parseOptions(args);
        }
        catch( NumberFormatException e ) {
            Assert.assertTrue(e.getMessage().contains("-em"));
        }
        catch( ParseException e ) {
            Assert.fail();
        }

        args = new String[]{ "-l","-n","1","-bm","-10" };

        try {
            testSparkRunner.parseOptions(args);
        }
        catch( NumberFormatException e ) {
            Assert.assertTrue(e.getMessage().contains("-bm"));
        }
        catch( ParseException e ) {
            Assert.fail();
        }
    }
}
