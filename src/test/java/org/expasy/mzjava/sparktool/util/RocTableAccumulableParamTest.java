package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.stats.Classification;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RocTableAccumulableParamTest {

    @Test
    public void testAddAccumulator() throws Exception {

        RocTableAccumulable rocTable = mock(RocTableAccumulable.class);
        RocValue value = new RocValue(0.8, Classification.SAME);

        RocTableAccumulableParam param = new RocTableAccumulableParam();
        param.addAccumulator(rocTable, value);

        verify(rocTable).add(value);
    }

    @Test
    public void testAddInPlace() throws Exception {

        RocTableAccumulable rocTable1 = mock(RocTableAccumulable.class);
        RocTableAccumulable rocTable2 = mock(RocTableAccumulable.class);

        RocTableAccumulableParam param = new RocTableAccumulableParam();
        param.addInPlace(rocTable1, rocTable2);

        verify(rocTable1).add(rocTable2);
    }

    @Test
    public void testZero() throws Exception {

        RocTableAccumulable rocTable = new RocTableAccumulable(0, 1, 0.01);
        RocTableAccumulableParam param = new RocTableAccumulableParam();

        RocTableAccumulable zero = param.zero(rocTable);

        Assert.assertEquals(0.0, zero.getMin(), 0.000000001);
        Assert.assertEquals(1.0, zero.getMax(), 0.000000001);
        Assert.assertEquals(0.01, zero.getThresholdIncrements(), 0.000000001);
    }
}