package org.expasy.mzjava.sparktool.util;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class TitleKeyFunctionTest {

    @Test
    public void testApplyIdentifier() throws Exception {

        TitleKeyFunction keyFunction = new TitleKeyFunction();

        SpectrumIdentifier identifier = new SpectrumIdentifier("20161103_06_Y1.3.3.2");

        String key = keyFunction.apply(identifier, Peptide.parse("LNDLEEALQQAK"));

        Assert.assertEquals("20161103_06_Y1.3.3.2", key);
    }

    @Test
    public void testApplySpectrum() throws Exception {

        MsnSpectrum spectrum = Mockito.mock(MsnSpectrum.class);
        when(spectrum.getComment()).thenReturn("20161103_06_Y1.3.3.2");

        TitleKeyFunction keyFunction = new TitleKeyFunction();
        Assert.assertEquals("20161103_06_Y1.3.3.2", keyFunction.apply(spectrum));
    }
}