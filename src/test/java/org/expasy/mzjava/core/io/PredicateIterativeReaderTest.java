package org.expasy.mzjava.core.io;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PredicateIterativeReaderTest {

    @Test
    public void test() throws Exception {

        IterativeReader<Integer> reader = new PredicateIterativeReader<>(
                IterativeReaders.fromCollection(Lists.newArrayList(1, 2, 3, 4, 5, 6)),
                integer -> integer % 2 == 0
        );

        Assert.assertEquals(true, reader.hasNext());
        Assert.assertEquals(2, reader.next().intValue());

        Assert.assertEquals(true, reader.hasNext());
        Assert.assertEquals(true, reader.hasNext());
        Assert.assertEquals(4, reader.next().intValue());

        Assert.assertEquals(true, reader.hasNext());
        Assert.assertEquals(6, reader.next().intValue());

        Assert.assertEquals(false, reader.hasNext());
    }

    @Test
    public void test2() throws Exception {

        IterativeReader<Integer> reader = new PredicateIterativeReader<>(
                IterativeReaders.fromCollection(Lists.newArrayList(1, 2, 3, 4, 5, 6, 7)),
                integer -> integer % 2 == 0
        );

        Assert.assertEquals(true, reader.hasNext());
        Assert.assertEquals(2, reader.next().intValue());

        Assert.assertEquals(true, reader.hasNext());
        Assert.assertEquals(true, reader.hasNext());
        Assert.assertEquals(4, reader.next().intValue());

        Assert.assertEquals(true, reader.hasNext());
        Assert.assertEquals(6, reader.next().intValue());

        Assert.assertEquals(false, reader.hasNext());
    }

    @Test
    public void test3() throws Exception {

        IterativeReader<Integer> reader = new PredicateIterativeReader<>(
                IterativeReaders.fromCollection(Lists.newArrayList(1, 3, 5, 7)),
                integer -> integer % 2 == 0
        );

        Assert.assertEquals(false, reader.hasNext());
    }

    @Test
    public void testClose() throws Exception {

        //noinspection unchecked
        IterativeReader<String> reader = mock(IterativeReader.class);

        new PredicateIterativeReader<>(reader, t -> true).close();

        verify(reader).close();
    }
}