/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakListFactory;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MockPeakAnnotation;
import org.expasy.mzjava.stats.NormalizedDotProduct;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AlignNdpSimFuncTest {

    private final double delta = 0.0001;

    @Test
    public void testCalcSim() throws Exception {

        AlignNdpSimFunc<PeakAnnotation, PeakAnnotation> simFunc = new AlignNdpSimFunc<>(new AbsoluteTolerance(0.6));

        double[] libMzs = new double[]{300, 400, 500};
        double[] libIntensities = new double[]{100, 5, 300};

        double[] queryMzs = new double[]{300, 400, 480, 500};
        double[] queryIntensities = new double[]{100, 5, 200, 300};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists =  makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 5, 0, 300}, queryIntensities), score, delta);
        Assert.assertEquals(0, simFunc.getLibPeaksShifted());
        Assert.assertEquals(3, simFunc.getMatches());
    }

    @Test
    public void testCalcSim2() throws Exception {

        AlignNdpSimFunc<PeakAnnotation, PeakAnnotation> simFunc = new AlignNdpSimFunc<>(new AbsoluteTolerance(0.6));

        double[] libMzs = new double[]{300, 400, 500};
        double[] libIntensities = new double[]{100, 5, 300};

        double[] queryMzs = new double[]{300, 500};
        double[] queryIntensities = new double[]{100, 300};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists =  makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(libIntensities, new double[]{100, 0, 300}), score, delta);

        Assert.assertEquals(0, simFunc.getLibPeaksShifted());
        Assert.assertEquals(2, simFunc.getMatches());
    }

    @Test
    public void testCalcSim3() throws Exception {

        AlignNdpSimFunc<PeakAnnotation, PeakAnnotation> simFunc = new AlignNdpSimFunc<>(new AbsoluteTolerance(0.6));

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 8, 300};
        double[] queryMzs = new double[]{300, 480, 500};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists =  makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(libIntensities, queryIntensities), score, delta);
    }

    @Test
    public void testCalcSim4() throws Exception {

        AlignNdpSimFunc<PeakAnnotation, PeakAnnotation> simFunc = new AlignNdpSimFunc<>(new AbsoluteTolerance(0.6));

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 5, 8, 300};
        double[] queryMzs = new double[]{300, 480, 480.5, 500};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists =  makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 5, 0, 300}, new double[]{100, 5, 8, 300}), score, delta);
    }

    @Test
    public void testCalcSim5() throws Exception {

        AlignNdpSimFunc<PeakAnnotation, PeakAnnotation> simFunc = new AlignNdpSimFunc<>(new AbsoluteTolerance(0.6));

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 8, 5, 300};
        double[] queryMzs = new double[]{300, 480, 480.5, 500};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists =  makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        double score = simFunc.calcSimilarity(lists.get(0), lists.get(1));

        Assert.assertEquals(NormalizedDotProduct.cosim(new double[]{100, 0, 5, 300}, new double[]{100, 8, 5, 300}), score, delta);
    }

    private List<PeakList<PeakAnnotation>> makePeakLists(double[] libMzs, double[] libIntensities, double[] queryMzs, double[] queryIntensities,
                                                             int massDelta, int[] charges, int precursorCharge) {

        PeakList<PeakAnnotation> libPeakList = PeakListFactory.newPeakList(PeakList.Precision.DOUBLE, libMzs, libIntensities, libMzs.length);
        PeakList<PeakAnnotation> queryPeakList = PeakListFactory.newPeakList(PeakList.Precision.DOUBLE, queryMzs, queryIntensities, queryMzs.length);

        libPeakList.getPrecursor().setMzAndCharge(487.36, precursorCharge);
        queryPeakList.getPrecursor().setMzAndCharge(487.36 + massDelta/precursorCharge, precursorCharge);

        for(int i = 0; i < charges.length; i++) {

            int fragCharge = charges[i];

            if(fragCharge > 0) libPeakList.addAnnotation(i, new MockPeakAnnotation(IonType.y, fragCharge, 1));
        }

        List<PeakList<PeakAnnotation>> peakLists = new ArrayList<>(2);
        peakLists.add(libPeakList);
        peakLists.add(queryPeakList);

        return peakLists;
    }
}
