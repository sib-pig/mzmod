/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakListFactory;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.MockPeakPairSink;
import org.expasy.mzjava.core.ms.spectrasim.peakpairprocessor.PeakListAligner;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class ModPeakListAlignerTest {

    @Test
    public void test() throws Exception {

        PeakList<PeakAnnotation> libPeaks = PeakListFactory.newPeakList(PeakList.Precision.FLOAT,
                new double[]{12, 45},
                new double[]{1, 2},
                2);
        libPeaks.getPrecursor().setValues(100, 10, 1);

        PeakList<PeakAnnotation> queryPeaks = PeakListFactory.newPeakList(PeakList.Precision.FLOAT,
                new double[]{15, 75},
                new double[]{3, 4},
                2);
        queryPeaks.getPrecursor().setValues(100, 10, 1);

        MockPeakPairSink<PeakAnnotation> callback = new MockPeakPairSink<PeakAnnotation>();

        PeakListAligner<PeakAnnotation, PeakAnnotation> aligner = new ModPeakListAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.6));
        aligner.setSink(callback);

        aligner.align(libPeaks, queryPeaks);

        Assert.assertArrayEquals(new double[]{12, 15, 45, 75}, callback.getCentroids(), 0.00001);
    }
}
