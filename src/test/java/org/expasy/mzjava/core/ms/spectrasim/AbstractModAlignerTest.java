/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.Tolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakListFactory;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.core.ms.spectrum.MockPeakAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version 1.0
 */
public class AbstractModAlignerTest {

    @Test
    public void testMakeAlignedPeakList() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs = new double[]{300, 400, 500};
        double[] libIntensities = new double[]{100, 5, 300};

        double[] queryMzs = new double[]{300, 400, 480, 500};
        double[] queryIntensities = new double[]{100, 5, 200, 300};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        PeakList<PeakAnnotation> alignedPeakList = modAligner.makeAlignedPeakList(lists.get(0), lists.get(1));

        Assert.assertEquals(3, alignedPeakList.size());
        Assert.assertArrayEquals(libMzs, alignedPeakList.getMzs(null), 0.000001);
        Assert.assertArrayEquals(libIntensities, alignedPeakList.getIntensities(null), 0.000001);
        Assert.assertArrayEquals(new int[]{0, 1, 2}, alignedPeakList.getAnnotationIndexes());
    }

    @Test
    public void testMakeAlignedPeakList2() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 8, 300};
        double[] queryMzs = new double[]{300, 480, 500};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        PeakList<PeakAnnotation> alignedPeakList = modAligner.makeAlignedPeakList(lists.get(0), lists.get(1));

        Assert.assertEquals(3, alignedPeakList.size());
        Assert.assertArrayEquals(new double[]{300, 480, 500}, alignedPeakList.getMzs(null), 0.000001);
        Assert.assertArrayEquals(libIntensities, alignedPeakList.getIntensities(null), 0.000001);
        Assert.assertArrayEquals(new int[]{0, 1, 2}, alignedPeakList.getAnnotationIndexes());
    }

    @Test
    public void testCalcSim() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs = new double[]{300, 400, 500};
        double[] libIntensities = new double[]{100, 5, 300};

        double[] queryMzs = new double[]{300, 400, 480, 500};
        double[] queryIntensities = new double[]{100, 5, 200, 300};

        int[] charges = new int[]{1, 1, 1};


        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), 80, charges, 1);

        Assert.assertEquals(4, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 400, false, alignmentPairs, 1);
        checkScorePair(1, 2, 480, true, alignmentPairs, 2);
        checkScorePair(2, 3, 500, false, alignmentPairs, 3);
    }

    @Test
    public void testCalcSimReverse() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs = new double[]{300, 400, 480, 500};
        double[] libIntensities = new double[]{100, 5, 200, 300};

        double[] queryMzs = new double[]{300, 400, 500};
        double[] queryIntensities = new double[]{100, 5, 300};

        int[] charges = new int[]{1, 1, 1, 1};


        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), -80, charges, 1);

        Assert.assertEquals(4, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 400, false, alignmentPairs, 1);
        checkScorePair(2, 1, 400, true, alignmentPairs, 2);
        checkScorePair(3, 2, 500, false, alignmentPairs, 3);
    }

    @Test
    public void testCalcSim2() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs = new double[]{300, 400, 500};
        double[] libIntensities = new double[]{100, 5, 300};

        double[] queryMzs = new double[]{300, 500};
        double[] queryIntensities = new double[]{100, 300};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), 80, charges, 1);

        Assert.assertEquals(3, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, -1, 400, false, alignmentPairs, 1);
        checkScorePair(2, 1, 500, false, alignmentPairs, 2);
    }

    @Test
    public void testCalcSimReverse2() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs = new double[]{300, 500};
        double[] libIntensities = new double[]{100, 300};

        double[] queryMzs = new double[]{300, 400, 500};
        double[] queryIntensities = new double[]{100, 5, 300};

        int[] charges = new int[]{1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), -80, charges, 1);

        Assert.assertEquals(2, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 2, 500, false, alignmentPairs, 1);
    }

    @Test
    public void testCalcSim3() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 8, 300};
        double[] queryMzs = new double[]{300, 480, 500};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), 80, charges, 1);

        Assert.assertEquals(3, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 480, true, alignmentPairs, 1);
        checkScorePair(2, 2, 500, false, alignmentPairs, 2);
    }

    @Test
    public void testCalcSimReverse3() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 480, 500};

        double[] queryIntensities = new double[]{100, 8, 300};
        double[] queryMzs = new double[]{300, 400, 500};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), -80, charges, 1);

        Assert.assertEquals(3, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 400, true, alignmentPairs, 1);
        checkScorePair(2, 2, 500, false, alignmentPairs, 2);
    }

    @Test
    public void testCalcSim4() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libIntensities = new double[]{100, 5, 300};
        double[] libMzs = new double[]{300, 400, 500};

        double[] queryIntensities = new double[]{100, 5, 8, 300};
        double[] queryMzs = new double[]{300, 480, 480.5, 500};

        int[] charges = new int[]{1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), 80, charges, 1);

        Assert.assertEquals(4, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 480, true, alignmentPairs, 1);
        checkScorePair(1, 2, 480.5, true, alignmentPairs, 2);
        checkScorePair(2, 3, 500, false, alignmentPairs, 3);
    }

    @Test
    public void testCalcSimReverse4() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libIntensities = new double[]{100, 5, 8, 300};
        double[] libMzs = new double[]{300, 400, 400.5, 500};

        double[] queryIntensities = new double[]{100, 5, 300};
        double[] queryMzs = new double[]{300, 320, 500};

        int[] charges = new int[]{1, 1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), -80, charges, 1);

        Assert.assertEquals(4, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 320, true, alignmentPairs, 1);
        checkScorePair(2, 1, 320, true, alignmentPairs, 2);
        checkScorePair(3, 2, 500, false, alignmentPairs, 3);
    }

    @Test
    public void testCalcSim5() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs =           new double[]{300, 300.2, 300.3, 400.5, 500};
        double[] libIntensities =   new double[]{100,   5,    12,     8,   300};

        double[] queryMzs =         new double[]{300, 300.3, 380, 400, 500};
        double[] queryIntensities = new double[]{100,   5,    12,      5, 300};

        int[] charges = new int[]{1, 1, 1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), 80, charges, 1);

        Assert.assertEquals(11, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(0, 1, 300, false, alignmentPairs, 1);
        checkScorePair(0, 2, 380, true, alignmentPairs, 2);
        checkScorePair(1, 0, 300.2, false, alignmentPairs, 3);
        checkScorePair(1, 1, 300.2, false, alignmentPairs, 4);
        checkScorePair(1, 2, 380, true, alignmentPairs, 5);
        checkScorePair(2, 0, 300.3, false, alignmentPairs, 6);
        checkScorePair(2, 1, 300.3, false, alignmentPairs, 7);
        checkScorePair(2, 2, 380, true, alignmentPairs, 8);
        checkScorePair(3, 3, 400.5, false, alignmentPairs, 9);
        checkScorePair(4, 4, 500, false, alignmentPairs, 10);
    }

    @Test
    public void testTwoMods1() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs =           new double[]{300, 400, 500, 600};
        double[] libIntensities =   new double[]{100, 150,  23, 100};

        double[] queryMzs =         new double[]{300, 416, 580, 696};
        double[] queryIntensities = new double[]{100, 140,  30, 100};

        int[] charges = new int[]{1, 1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80 + 16, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), new double[]{16, 80}, charges, 1);

        Assert.assertEquals(4, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 416, true, alignmentPairs, 1);
        checkScorePair(2, 2, 580, true, alignmentPairs, 2);
        checkScorePair(3, 3, 696, true, alignmentPairs, 3);
    }

    @Test
    public void testTwoModsReverse1() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs =           new double[]{300, 416, 580, 696};
        double[] libIntensities =   new double[]{100, 140,  30, 100};

        double[] queryMzs =         new double[]{300, 400, 500, 600};
        double[] queryIntensities = new double[]{100, 150,  23, 100};

        int[] charges = new int[]{1, 1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -80 - 16, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), new double[]{-16, -80}, charges, 1);

        Assert.assertEquals(4, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 400, true, alignmentPairs, 1);
        checkScorePair(2, 2, 500, true, alignmentPairs, 2);
        checkScorePair(3, 3, 600, true, alignmentPairs, 3);
    }

    @Test
    public void testTwoMods2() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs =           new double[]{300, 400, 500, 600};
        double[] libIntensities =   new double[]{100, 150,  23, 100};

        double[] queryMzs =         new double[]{300, 400, 416, 580, 696};
        double[] queryIntensities = new double[]{100, 100, 140,  30, 100};

        int[] charges = new int[]{1, 1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, 80 + 16, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), new double[]{16, 80}, charges, 1);

        Assert.assertEquals(5, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 400, false, alignmentPairs, 1);
        checkScorePair(1, 2, 416, true, alignmentPairs, 2);
        checkScorePair(2, 3, 580, true, alignmentPairs, 3);
        checkScorePair(3, 4, 696, true, alignmentPairs, 4);
    }

    @Test
    public void testTwoModsReverse2() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs =           new double[]{300, 400, 416, 580, 696};
        double[] libIntensities =   new double[]{100, 100, 140,  30, 100};

        double[] queryMzs =         new double[]{300, 400, 500, 600};
        double[] queryIntensities = new double[]{100, 150,  23, 100};

        int[] charges = new int[]{1, 1, 1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -80 - 16, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), new double[]{-16, -80}, charges, 1);

        Assert.assertEquals(5, alignmentPairs.size());
        checkScorePair(0, 0, 300, false, alignmentPairs, 0);
        checkScorePair(1, 1, 400, false, alignmentPairs, 1);
        checkScorePair(2, 1, 400, true, alignmentPairs, 2);
        checkScorePair(3, 2, 500, true, alignmentPairs, 3);
        checkScorePair(4, 3, 600, true, alignmentPairs, 4);
    }

    @Test
    public void testEmptyLib() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs =           new double[]{};
        double[] libIntensities =   new double[]{};

        double[] queryMzs =         new double[]{300, 400, 500, 600};
        double[] queryIntensities = new double[]{100, 150,  23, 100};

        int[] charges = new int[]{};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -80 - 16, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), new double[]{-16, -80}, charges, 1);

        Assert.assertEquals(0, alignmentPairs.size());
    }

    @Test
    public void testEmptyQuery() throws Exception {

        MockModAligner modAligner = new MockModAligner(new AbsoluteTolerance(0.6));

        double[] libMzs =           new double[]{300, 400, 500, 600};
        double[] libIntensities =   new double[]{100, 150,  23, 100};

        double[] queryMzs =         new double[]{};
        double[] queryIntensities = new double[]{};

        int[] charges = new int[]{1, 1, 1, 1};

        List<PeakList<PeakAnnotation>> lists = makePeakLists(libMzs, libIntensities, queryMzs, queryIntensities, -80 - 16, charges, 2);
        AlignmentPairList alignmentPairs = modAligner.collectScorePairs(lists.get(0), lists.get(1), new double[]{-16, -80}, charges, 1);

        Assert.assertEquals(0, alignmentPairs.size());
    }

    private void checkScorePair(int expectedLibIndex, int expectedQueryIndex, double expectedMz, boolean shouldBeMod, AlignmentPairList alignmentPairList, int index) {

        Assert.assertEquals("lib index", expectedLibIndex, alignmentPairList.getLibIndex(index));
        Assert.assertEquals("query index", expectedQueryIndex, alignmentPairList.getQueryIndex(index));
        Assert.assertEquals("mz", expectedMz, alignmentPairList.getMz(index), 0.00001);
        Assert.assertEquals("mod", shouldBeMod, alignmentPairList.isModified(index));
    }

    private List<PeakList<PeakAnnotation>> makePeakLists(double[] libMzs, double[] libIntensities, double[] queryMzs, double[] queryIntensities,
                                                             int massDelta, int[] charges, int precursorCharge) {

        PeakList<PeakAnnotation> libPeakList = PeakListFactory.newPeakList(PeakList.Precision.DOUBLE, libMzs, libIntensities, libMzs.length);
        PeakList<PeakAnnotation> queryPeakList = PeakListFactory.newPeakList(PeakList.Precision.DOUBLE, queryMzs, queryIntensities, queryMzs.length);

        libPeakList.getPrecursor().setMzAndCharge(487.36, precursorCharge);
        queryPeakList.getPrecursor().setMzAndCharge(487.36 + massDelta/precursorCharge, precursorCharge);

        for (int i = 0; i < charges.length; i++) {

            int fragCharge = charges[i];

            if (fragCharge > 0)
                libPeakList.addAnnotation(i, new MockPeakAnnotation(IonType.y, fragCharge, 1));
        }

        List<PeakList<PeakAnnotation>> peakLists = new ArrayList<PeakList<PeakAnnotation>>(2);
        peakLists.add(libPeakList);
        peakLists.add(queryPeakList);

        return peakLists;
    }

    private static class MockModAligner extends AbstractModAligner<PeakAnnotation, PeakAnnotation> {

        private MockModAligner(Tolerance tolerance) {

            super(tolerance);
        }

        public AlignmentPairList collectScorePairs(PeakList<PeakAnnotation> plX, PeakList<PeakAnnotation> plY, double deltaMass, int[] peakCharges, int maxCharge) {

            AlignmentPairList alignmentPairs = new AlignmentPairList();
            collectScorePairs(alignmentPairs, plX, plY, new double[]{deltaMass}, peakCharges, maxCharge);

            return alignmentPairs;
        }

        public AlignmentPairList collectScorePairs(PeakList<PeakAnnotation> plX, PeakList<PeakAnnotation> plY, double[] deltaMasses, int[] peakCharges, int maxCharge) {

            AlignmentPairList alignmentPairs = new AlignmentPairList();
            collectScorePairs(alignmentPairs, plX, plY, deltaMasses, peakCharges, maxCharge);

            return alignmentPairs;
        }
    }
}
