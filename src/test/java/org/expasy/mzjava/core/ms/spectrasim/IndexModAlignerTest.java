/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.core.ms.spectrasim;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IndexModAlignerTest {

    @Test
    public void testCalculateAlignment() throws Exception {

        MsnSpectrum unmodSpectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);
        unmodSpectrum.setPrecursor(new Peak(311.65249846719996, 1.0, 2));
        unmodSpectrum.add(52.51186752319336, 1.0);            //b1++
        unmodSpectrum.add(74.06004333496094, 1.0);            //y1++
        unmodSpectrum.add(104.01646423339844, 1.0);            //b1
        unmodSpectrum.add(117.03316497802734, 1.0);            //b2++
        unmodSpectrum.add(147.11280822753906, 1.0);            //y1
        unmodSpectrum.add(152.110595703125, 1.0);            //y2++
        unmodSpectrum.add(160.54917907714844, 1.0);            //b3++
        unmodSpectrum.add(195.62660217285156, 1.0);            //y3++
        unmodSpectrum.add(233.05905151367188, 1.0);            //b2
        unmodSpectrum.add(238.5997314453125, 1.0);            //b4++
        unmodSpectrum.add(260.1479187011719, 1.0);            //y4++
        unmodSpectrum.add(303.21392822265625, 1.0);            //y2
        unmodSpectrum.add(311.6524963378906, 1.0);            //p5++
        unmodSpectrum.add(320.0910949707031, 1.0);            //b3
        unmodSpectrum.add(390.2459411621094, 1.0);            //y3
        unmodSpectrum.add(476.19219970703125, 1.0);            //b4
        unmodSpectrum.add(519.2885131835938, 1.0);            //y4
        unmodSpectrum.add(622.2977294921875, 1.0);            //p5

        MsnSpectrum modSpectrum = new MsnSpectrum(PeakList.Precision.DOUBLE);
        modSpectrum.setPrecursor(new Peak(359.63312097719995, 1.0, 2));
        modSpectrum.add(52.51186752319336, 1.0);            //b1++
        modSpectrum.add(74.06004333496094, 1.0);            //y1++
        modSpectrum.add(104.01646423339844, 1.0);            //b1
        modSpectrum.add(117.03316497802734, 1.0);            //b2++
        modSpectrum.add(147.11280822753906, 1.0);            //y1
        modSpectrum.add(152.110595703125, 1.0);            //y2++
        modSpectrum.add(208.52980041503906, 1.0);            //b3++
        modSpectrum.add(233.05905151367188, 1.0);            //b2
        modSpectrum.add(243.60723876953125, 1.0);            //y3++
        modSpectrum.add(286.5803527832031, 1.0);            //b4++
        modSpectrum.add(303.21392822265625, 1.0);            //y2
        modSpectrum.add(308.1285400390625, 1.0);            //y4++
        modSpectrum.add(359.63311767578125, 1.0);            //p5++
        modSpectrum.add(416.0523376464844, 1.0);            //b3
        modSpectrum.add(486.2071838378906, 1.0);            //y3
        modSpectrum.add(572.1534423828125, 1.0);            //b4
        modSpectrum.add(615.249755859375, 1.0);            //y4
        modSpectrum.add(718.2589721679688, 1.0);            //p5

        List<int[]> expectedAlignment = new ArrayList<int[]>();
        expectedAlignment.add(new int[]{0, 0});
        expectedAlignment.add(new int[]{1, 1});
        expectedAlignment.add(new int[]{2, 2});
        expectedAlignment.add(new int[]{3, 3});
        expectedAlignment.add(new int[]{4, 4});
        expectedAlignment.add(new int[]{5, 5});
        expectedAlignment.add(new int[]{6, 6});
        expectedAlignment.add(new int[]{8, 7});
        expectedAlignment.add(new int[]{7, 8});
        expectedAlignment.add(new int[]{9, 9});
        expectedAlignment.add(new int[]{11, 10});
        expectedAlignment.add(new int[]{10, 11});
        expectedAlignment.add(new int[]{12, 12});
        expectedAlignment.add(new int[]{13, 13});
        expectedAlignment.add(new int[]{14, 14});
        expectedAlignment.add(new int[]{15, 15});
        expectedAlignment.add(new int[]{16, 16});
        expectedAlignment.add(new int[]{17, 17});

        IndexModAligner<PeakAnnotation, PeakAnnotation> modAligner = new IndexModAligner<PeakAnnotation, PeakAnnotation>(new AbsoluteTolerance(0.5));
        List<int[]> alignment = modAligner.calculateAlignment(unmodSpectrum, modSpectrum);
        Assert.assertEquals(expectedAlignment.size(), alignment.size());
        for (int i = 0, alignmentSize = alignment.size(); i < alignmentSize; i++) {

            Assert.assertArrayEquals("pair at index " + i, expectedAlignment.get(i), alignment.get(i));
        }
    }
}
