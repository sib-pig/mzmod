package org.expasy.mzjava.proteomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;

public class CompositionCalcTest {

    @Test
    public void testPeptideCompositionDifference() throws Exception {

        Peptide peptide1 = Peptide.parse("M");
        Peptide peptide2 = Peptide.parse("M(O)");

        Composition difference = CompositionCalc.subtract(peptide2, peptide1);

        Assert.assertEquals(Composition.parseComposition("O"), difference);
    }

    @Test
    public void testGet() throws Exception {

        Peptide peptide = Peptide.parse("M(O)");

        Assert.assertEquals(Composition.parseComposition("C5H11NO3S"), CompositionCalc.get(peptide));
    }
}