package org.expasy.mzjava.proteomics.mol.shuffling;

import com.google.common.base.Optional;
import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.expasy.mzjava.proteomics.ms.spectrum.PeptideSpectrum;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class ControlledPeptideShufflerTest {
    @Test
    public void testShuffle() {

        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MEDIFICATINMSSTWPK")
                .addModification(ModAttachment.N_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(11,oxid)
                .build();

        Assert.assertEquals(peptide.toString(), "(C2H2O)_M(O)EDIFICATINM(O)SSTWPK");

        ControlledPeptideShuffler shuffler = new ControlledPeptideShuffler(0.5,10);
        Optional<Peptide> shuffled = shuffler.shuffle(peptide);

        PeptideSpectrum peptideSpec = shuffler.getSpectrum(peptide);
        if (shuffled.isPresent()) {
            PeptideSpectrum shuffledSpec = shuffler.getSpectrum(shuffled.get());
            Assert.assertTrue(shuffler.calcSimilarity(peptideSpec,shuffledSpec)<=0.5);
        } else {
            shuffled = shuffler.getBestShuffledPeptide();
            PeptideSpectrum shuffledSpec = shuffler.getSpectrum(shuffled.get());
            Assert.assertTrue(shuffler.calcSimilarity(peptideSpec,shuffledSpec)>0.5);
        }

        Assert.assertEquals(shuffled.get().getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(peptide.getSymbolIndexes(AminoAcid.M)[0], ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.nTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());

    }

    @Test
    public void testShuffleKeepNTerm() {

        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MEDIFICATINMSSTWPK")
                .addModification(ModAttachment.N_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(11,oxid)
                .build();

        Assert.assertEquals(peptide.toString(), "(C2H2O)_M(O)EDIFICATINM(O)SSTWPK");

        ControlledPeptideShuffler shuffler = new ControlledPeptideShuffler(0.5,10);
        Optional<Peptide> shuffled = shuffler.shuffleKeepNTerm(peptide);

        PeptideSpectrum peptideSpec = shuffler.getSpectrum(peptide);
        if (shuffled.isPresent()) {
            PeptideSpectrum shuffledSpec = shuffler.getSpectrum(shuffled.get());
            Assert.assertTrue(shuffler.calcSimilarity(peptideSpec,shuffledSpec)<=0.5);
        } else {
            shuffled = shuffler.getBestShuffledPeptide();
            PeptideSpectrum shuffledSpec = shuffler.getSpectrum(shuffled.get());
            Assert.assertTrue(shuffler.calcSimilarity(peptideSpec,shuffledSpec)>0.5);
        }

        Assert.assertEquals(shuffled.get().getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(0, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.nTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());

    }

    @Test
    public void testShuffleKeepAA() {

        Modification methyl = Modification.parseModification("CH2");
        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MEDIK")
                .addModification(ModAttachment.N_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(4,methyl)
                .build();

        Assert.assertEquals(peptide.toString(), "(C2H2O)_M(O)EDIK(CH2)");

        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        keepAA.add(1);
        keepAA.add(2);
        keepAA.add(4);

        ControlledPeptideShuffler shuffler = new ControlledPeptideShuffler(0.5,10);
        Optional<Peptide> shuffled = shuffler.shuffle(peptide,keepAA);

        Assert.assertFalse(shuffled.isPresent());

        shuffled = shuffler.getBestShuffledPeptide();
        PeptideSpectrum shuffledSpec = shuffler.getSpectrum(shuffled.get());
        PeptideSpectrum peptideSpec = shuffler.getSpectrum(peptide);
        Assert.assertTrue(shuffler.calcSimilarity(peptideSpec,shuffledSpec)>0.5);

        Assert.assertEquals(shuffled.get().getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(0, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.nTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());

        shuffledModif = peptide.getModificationsAt(4, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,methyl.toString());

        shuffledModif = peptide.getModificationsAt(0, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());
    }

}
