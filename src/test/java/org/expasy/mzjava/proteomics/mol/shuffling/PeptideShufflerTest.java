package org.expasy.mzjava.proteomics.mol.shuffling;

import org.expasy.mzjava.proteomics.mol.AminoAcid;
import org.expasy.mzjava.proteomics.mol.Peptide;
import org.expasy.mzjava.proteomics.mol.PeptideBuilder;
import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.mol.modification.Modification;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class PeptideShufflerTest {
    @Test
    public void testShuffle() {

        Modification methyl = Modification.parseModification("CH2");
        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MODIK")
                .addModification(ModAttachment.N_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(4,methyl)
                .build();

        Assert.assertEquals(peptide.toString(), "(C2H2O)_M(O)ODIK(CH2)");

        PeptideShuffler shuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffled = shuffler.shuffle(peptide);

//        System.out.println(peptide.toString());
//        System.out.println(shuffled.toString());

        Assert.assertEquals(shuffled.getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(peptide.getSymbolIndexes(AminoAcid.M)[0], ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.nTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());
    }

    @Test
    public void testReverse() {

        Modification methyl = Modification.parseModification("CH2");
        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MEDIK")
                .addModification(ModAttachment.N_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(4,methyl)
                .build();

        Assert.assertEquals(peptide.toString(), "(C2H2O)_M(O)EDIK(CH2)");

        PeptideShuffler shuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.REVERSE);
        Peptide shuffled = shuffler.shuffle(peptide);

        Assert.assertEquals(shuffled.toString(), "(C2H2O)_K(CH2)IDEM(O)");

        Assert.assertEquals(shuffled.getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(peptide.getSymbolIndexes(AminoAcid.M)[0], ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.nTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());
    }

    @Test
    public void testShuffleKeepNTerm() {

        Modification methyl = Modification.parseModification("CH2");
        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MEDIK")
                .addModification(ModAttachment.N_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(4,methyl)
                .build();

        Assert.assertEquals(peptide.toString(), "(C2H2O)_M(O)EDIK(CH2)");

        PeptideShuffler shuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffled = shuffler.shuffleKeepNTerm(peptide);

        Assert.assertEquals(shuffled.getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(peptide.getSymbolIndexes(AminoAcid.M)[0], ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.nTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());
    }

    @Test
    public void testShuffleKeepCTerm() {

        Modification methyl = Modification.parseModification("CH2");
        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MEDIK")
                .addModification(ModAttachment.C_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(4,methyl)
                .build();

        Assert.assertEquals(peptide.toString(), "M(O)EDIK(CH2)_(C2H2O)");

        PeptideShuffler shuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffled = shuffler.shuffleKeepCTerm(peptide);

        Assert.assertEquals(shuffled.getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(0, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.cTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());

        shuffledModif = peptide.getModificationsAt(4, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,methyl.toString());
    }

    @Test
    public void testShuffleKeepAA() {

        Modification methyl = Modification.parseModification("CH2");
        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MEDIK")
                .addModification(ModAttachment.N_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(4,methyl)
                .build();

        Assert.assertEquals(peptide.toString(), "(C2H2O)_M(O)EDIK(CH2)");

        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        keepAA.add(4);
        PeptideShuffler shuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.SHUFFLE);
        Peptide shuffled = shuffler.shuffle(peptide, keepAA);

        System.out.println(peptide.toString());
        System.out.println(shuffled.toString());

        Assert.assertEquals(shuffled.getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(0, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.nTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());

        shuffledModif = peptide.getModificationsAt(4, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,methyl.toString());

        shuffledModif = peptide.getModificationsAt(0, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());
    }

    @Test
    public void testReverseKeepAA() {

        Modification methyl = Modification.parseModification("CH2");
        Modification acetyl = Modification.parseModification("C2H2O");
        Modification oxid = Modification.parseModification("O");

        Peptide peptide = new PeptideBuilder("MEDIK")
                .addModification(ModAttachment.N_TERM,acetyl)
                .addModification(0,oxid)
                .addModification(4,methyl)
                .build();

        Assert.assertEquals(peptide.toString(), "(C2H2O)_M(O)EDIK(CH2)");

        Set<Integer> keepAA = new HashSet<Integer>();
        keepAA.add(0);
        keepAA.add(4);
        PeptideShuffler shuffler = new PeptideShuffler(PeptideShuffler.ShufflingMethod.REVERSE);
        Peptide shuffled = shuffler.shuffle(peptide, keepAA);

        Assert.assertEquals(shuffled.toString(), "(C2H2O)_M(O)IDEK(CH2)");

        Assert.assertEquals(shuffled.getModificationCount(),3);
        String shuffledModif = peptide.getModificationsAt(0, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());

        shuffledModif = peptide.getModifications(ModAttachment.nTermSet).get(0).toString();
        Assert.assertEquals(shuffledModif,acetyl.toString());

        shuffledModif = peptide.getModificationsAt(4, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,methyl.toString());

        shuffledModif = peptide.getModificationsAt(0, ModAttachment.sideChainSet).get(0).toString();
        Assert.assertEquals(shuffledModif,oxid.toString());
    }
}
