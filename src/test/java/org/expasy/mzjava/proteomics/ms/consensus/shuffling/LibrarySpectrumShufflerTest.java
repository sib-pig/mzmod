package org.expasy.mzjava.proteomics.ms.consensus.shuffling;

import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrasim.NdpSimFunc;
import org.expasy.mzjava.proteomics.io.ms.spectrum.SptxtReader;
import org.expasy.mzjava.proteomics.ms.consensus.PeptideConsensusSpectrum;
import org.expasy.mzjava.proteomics.ms.spectrum.PepLibPeakAnnotation;
import org.expasy.mzjava.utils.URIBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class LibrarySpectrumShufflerTest {

    StringReader sptxtEntry = new StringReader("Name: VMMVAK/2\n" +
            "LibID: 6991\n" +
            "MW: 999.5689448544\n" +
            "PrecursorMZ: 499.7844724272\n" +
            "FullName: (iTRAQ4plex)_VM(Oxidation)M(Oxidation)VAK(iTRAQ4plex)/2\n" +
            "Comment: Mz_exact=499.7844724272 Mz_diff=0.0000000000 Parent=499.7844724272 PrecursorIntensity=1.0000000000 TotalIonCurrent=76461.1060376836 Protein=1/P27773 Mods=4/-1,V,iTRAQ4plex/1,M,Oxidation/2,M,Oxidation/5,K,iTRAQ4plex\n" +
            "NumPeaks: 82\n" +
            "91.2331115052\t78.6733817975\t?\n" +
            "92.7335406814\t99.4296892803\t?\n" +
            "101.0879572441\t116.8018645966\t?\n" +
            "109.0706304810\t88.9323119551\t?\n" +
            "110.0719267703\t492.8619766523\t?\n" +
            "112.0878000115\t94.9250408277\t?\n" +
            "114.1114241787\t5415.7447884096\t?\n" +
            "115.1085150975\t4885.5992428899\t?\n" +
            "116.1109512104\t3843.3980193456\t?\n" +
            "117.1150840714\t5177.3245891417\t?\n" +
            "140.1116127176\t95.0691451876\t?\n" +
            "145.1096283836\t1338.7003542514\t?\n" +
            "163.1189952962\t458.6738522316\t?\n" +
            "175.1198147233\t304.1825055180\t?\n" +
            "216.1833058160\t2127.0740751080\ta1/-0\n" +
            "228.1834276817\t240.9497057053\t?\n" +
            "231.0775000000\t128.6976657344\ty3^2/0.09\n" +
            "244.1783813049\t1275.1271128149\tb1/-0\n" +
            "266.1508229763\t162.3507129796\t?\n" +
            "291.2149051611\t1439.1716698862\ty1/-0\n" +
            "314.2026600789\t167.2636446342\t?\n" +
            "327.2145084048\t242.3637173071\t?\n" +
            "330.1630706963\t203.2551131001\t?\n" +
            "346.1631109090\t107.8269919352\t?\n" +
            "356.2432089146\t467.8594948153\t?\n" +
            "362.2530007172\t2227.0559742234\ty2/-0\n" +
            "363.2604960000\t112.7588208443\ta2/-0.04\n" +
            "391.2138051856\t493.1970903840\tb2/-0\n" +
            "410.2541962567\t391.8588223486\t?\n" +
            "414.7630839011\t346.5735414591\t?\n" +
            "415.7739118235\t242.6751041767\t?\n" +
            "422.2774291773\t241.8694532020\t?\n" +
            "422.7801399576\t884.1202771410\t?\n" +
            "426.7834719804\t152.9644506287\t?\n" +
            "434.7829477509\t206.3645158093\t?\n" +
            "435.2869624197\t777.9843230214\t?\n" +
            "435.7877563037\t10000.0000000000\t?\n" +
            "436.2907180411\t403.4046969399\t?\n" +
            "446.7618683671\t433.8294172410\t?\n" +
            "454.2716789661\t183.8266684726\t?\n" +
            "455.3119271080\t402.5061248159\t?\n" +
            "458.7815070507\t279.3079066119\t?\n" +
            "460.2748674237\t639.2642321645\t?\n" +
            "461.3215051933\t1121.1871379349\ty3/-0\n" +
            "467.2871258143\t1191.7497024269\t?\n" +
            "467.7866142112\t9384.6900886518\t?\n" +
            "468.2884343919\t367.9367049462\t?\n" +
            "471.3080301214\t365.4574102008\t?\n" +
            "474.2511777574\t502.4034665573\t?\n" +
            "492.2744649055\t518.6428097796\t?\n" +
            "499.5836519953\t1549.3345535179\t?\n" +
            "500.2921905108\t270.3261717087\t?\n" +
            "509.3207243278\t269.7949484003\t?\n" +
            "510.3251910000\t120.2114841359\ta3/-0.07\n" +
            "519.3094363722\t1148.6315137427\t?\n" +
            "538.2505518346\t264.6369150859\tb3/-0\n" +
            "544.3594975055\t1496.0432040756\t?\n" +
            "573.3206109262\t439.0804021121\t?\n" +
            "580.3609234689\t259.6844871146\t?\n" +
            "601.3810580173\t359.8004775569\t?\n" +
            "608.3602958498\t284.8396092088\ty4/-0\n" +
            "627.3967220625\t3691.6244589693\t?\n" +
            "628.4002721655\t250.1643485687\t?\n" +
            "637.3222720000\t178.4363621184\tb4/-0.01\n" +
            "644.3574399748\t267.4372796038\t?\n" +
            "662.3687230513\t163.6690883767\t?\n" +
            "665.3840692310\t215.3347637406\t?\n" +
            "673.3879193265\t127.9104086994\t?\n" +
            "679.4109206592\t97.7113400378\t?\n" +
            "691.3946732691\t1559.7729844709\t?\n" +
            "700.4479383855\t248.7420689235\t?\n" +
            "708.4385080000\t137.9193094415\tb5/-0.08\n" +
            "725.4614408397\t224.1563605739\t?\n" +
            "726.4642757174\t3590.1171322784\t?\n" +
            "727.4720619411\t279.5308266672\t?\n" +
            "755.3926638258\t201.6114633009\ty5/-0\n" +
            "772.4571075828\t165.8430379561\t?\n" +
            "775.4460230231\t168.1737272044\t?\n" +
            "789.4627882437\t240.4491680549\t?\n" +
            "790.4624335656\t2350.2719759485\t?\n" +
            "853.5298862510\t819.4680590502\t?\n" +
            "854.4906960164\t635.4404975760\t?\n" +
            "\n");
    @Test
    public void testMoveShuffle() throws IOException {
        SptxtReader reader = SptxtReader.newBuilder(sptxtEntry, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        //Spectrum 0
        PeptideConsensusSpectrum spectrum = reader.next();
        LibrarySpectrumShuffler shuffler = new LibrarySpectrumShuffler();
        PeptideConsensusSpectrum shuffled = shuffler.shuffle(spectrum);

        NdpSimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation> simFunc = new NdpSimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation>(0, new AbsoluteTolerance(0.05));
        double maxSim = simFunc.calcSimilarity(spectrum, spectrum);
        double sim = simFunc.calcSimilarity(spectrum, shuffled);

        Assert.assertTrue(sim < maxSim);

        shuffler = new LibrarySpectrumShuffler(LibrarySpectrumShuffler.PrecursorHandling.REPLACE,0.9,4.0);
        shuffled = shuffler.shuffle(spectrum);

        double sim2 = simFunc.calcSimilarity(spectrum, shuffled);

        Assert.assertTrue(sim2 < maxSim);

        shuffled = shuffler.shuffleKeepCTerm(spectrum);
        Assert.assertEquals(shuffled.getComment(),spectrum.getComment());
        Assert.assertEquals(shuffled.getStatus(), PeptideConsensusSpectrum.Status.DECOY);
        Assert.assertEquals(shuffled.getMemberIds(),spectrum.getMemberIds());
        Assert.assertEquals(shuffled.getSpectrumSource(), new URIBuilder("www.expasy.ch", "deliberator").build());
        Assert.assertEquals(shuffled.getPrecursor(),spectrum.getPrecursor());
    }

    @Test
    public void testSwapShuffle() throws IOException {
        SptxtReader reader = SptxtReader.newBuilder(sptxtEntry, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        //Spectrum 0
        PeptideConsensusSpectrum spectrum = reader.next();
        LibrarySpectrumShuffler shuffler = new LibrarySpectrumShuffler(spectrum);
        PeptideConsensusSpectrum shuffled = shuffler.shuffle(spectrum);

        shuffler = new LibrarySpectrumShuffler();
        PeptideConsensusSpectrum shuffled2 = shuffler.shuffle(spectrum);

        shuffler = new LibrarySpectrumShuffler(LibrarySpectrumShuffler.PrecursorHandling.REPLACE,0.9,0.5);
        PeptideConsensusSpectrum shuffled3 = shuffler.shuffle(spectrum);

        NdpSimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation> simFunc = new NdpSimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation>(0, new AbsoluteTolerance(0.05));
        double sim = simFunc.calcSimilarity(spectrum, shuffled);
        double sim1 = simFunc.calcSimilarity(spectrum, shuffled2);
        double sim2 = simFunc.calcSimilarity(spectrum, shuffled3);

        Assert.assertTrue(sim > sim2 && sim1 > sim2);
    }

    @Test
    public void testSampleShuffle() throws IOException {
        SptxtReader reader = SptxtReader.newBuilder(sptxtEntry, new URIBuilder("www.expasy.ch", "test").build(), PeakList.Precision.DOUBLE)
                .useSpectraStAnnotationResolver()
                .build();

        //Spectrum 0
        PeptideConsensusSpectrum spectrum = reader.next();
        LibrarySpectrumShuffler shuffler = new LibrarySpectrumShuffler(spectrum);
        PeptideConsensusSpectrum shuffled = shuffler.shuffle(spectrum);

        List<PeptideConsensusSpectrum> bgSpectra = new ArrayList<PeptideConsensusSpectrum>();
        bgSpectra.add(spectrum);
        shuffler = new LibrarySpectrumShuffler(LibrarySpectrumShuffler.PrecursorHandling.KEEP,0.9,bgSpectra);
        PeptideConsensusSpectrum shuffled3 = shuffler.shuffle(spectrum);

        NdpSimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation> simFunc = new NdpSimFunc<PepLibPeakAnnotation,PepLibPeakAnnotation>(0, new AbsoluteTolerance(0.05));
        double sim = simFunc.calcSimilarity(spectrum, shuffled);
        double sim2 = simFunc.calcSimilarity(spectrum, shuffled3);

        Assert.assertTrue(sim > sim2);
    }
}
