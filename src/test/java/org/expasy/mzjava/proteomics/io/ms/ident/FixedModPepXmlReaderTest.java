package org.expasy.mzjava.proteomics.io.ms.ident;

import org.expasy.mzjava.proteomics.mol.modification.ModAttachment;
import org.expasy.mzjava.proteomics.ms.ident.ModificationMatch;
import org.expasy.mzjava.proteomics.ms.ident.PeptideMatch;
import org.expasy.mzjava.proteomics.ms.ident.SpectrumIdentifier;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Markus Muller
 * @version 0.0
 */
public class FixedModPepXmlReaderTest {

    List<PeptideMatch>  peptideMatches = new ArrayList<>();

    PSMReaderCallback callback = new PSMReaderCallback() {
        @Override
        public void resultRead(SpectrumIdentifier spectrumIdentifier, PeptideMatch peptideMatch) {
            peptideMatches.add(peptideMatch);
        }
    };

    @Test
    public void testWithResolver() throws Exception {

        InputStream fr = new FileInputStream(new File(getClass().getResource("pep_xml_xtandem.pep.xml").getFile()));

        FixedModPepXmlReader reader = new FixedModPepXmlReader(PepXmlReader.ModMassStorage.AA_MASS_PLUS_MOD_MASS, false);
        reader.parse(fr, callback);

        for (PeptideMatch psm : peptideMatches) {
            List<ModificationMatch> mods = psm.getModifications(ModAttachment.nTermSet);
            Assert.assertEquals("C3H4O", psm.getModifications(ModAttachment.nTermSet).get(mods.size()-1).getModificationCandidate(0).getLabel());
        }

    }

}
